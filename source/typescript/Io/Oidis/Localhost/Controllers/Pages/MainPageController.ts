/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { AsyncBasePageController } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/Resolvers/AsyncBasePageController.js";
import { MainPage } from "../../Gui/Pages/MainPage.js";

export class MainPageController extends AsyncBasePageController {

    constructor() {
        super();
        this.setInstanceOwner(new MainPage());
        this.setPageTitle("Oidis Localhost");
    }
}

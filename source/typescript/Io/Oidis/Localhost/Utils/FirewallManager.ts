/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Loader } from "../Loader.js";
import { EnvironmentHelper } from "./EnvironmentHelper.js";

export class FirewallManager extends BaseObject {

    public static AddServiceRule($groupName : string, $serviceName : string, $appName : string, $description : string,
                                 $servicePath : string, $callback : () => void) : void {
        const release : number = parseFloat(require("os").release);
        if (EnvironmentHelper.IsWindows() && release >= 6.0) {
            EnvironmentHelper.IsElevated(($status : boolean) : void => {
                if ($status) {
                    if (!StringUtils.EndsWith($servicePath, ".exe")) {
                        $servicePath += ".exe";
                    }
                    $servicePath = Loader.getInstance().getFileSystemHandler().NormalizePath($servicePath, true);
                    Loader.getInstance().getTerminal().Execute("netsh.exe", [
                            "advfirewall", "firewall", "show", "rule",
                            "name=\"" + $appName + "\"",
                            "verbose"
                        ], {verbose: false},
                        ($exitCode : number, $std? : string[]) : void => {
                            if ($exitCode === 0 && !StringUtils.IsEmpty($std[0]) ||
                                $exitCode === 1 && StringUtils.Contains($std[0], "No rules match the specified criteria")) {
                                if (!StringUtils.Contains($std[0], $servicePath)) {
                                    Loader.getInstance().getTerminal().Spawn("netsh.exe", [
                                            "advfirewall", "firewall", "add", "rule",
                                            "name=\"" + $appName + "\"",
                                            "dir=in",
                                            "action=allow",
                                            "program=\"" + $servicePath + "\"",
                                            "service=\"" + $serviceName + "\"",
                                            "description=\"" + $description + "\"",
                                            "enable=yes",
                                            "profile=any"
                                        ], null,
                                        ($exitCode : number, $std? : string[]) : void => {
                                            if ($exitCode !== 0) {
                                                LogIt.Warning("Firewall Rule Add failed.");
                                            }
                                            $callback();
                                        });
                                } else {
                                    LogIt.Warning("Adding of service rule skipped: rule already exists");
                                    $callback();
                                }
                            } else {
                                LogIt.Warning("Unable to get list of Firewall rules.");
                                $callback();
                            }
                        });
                } else {
                    LogIt.Info("Add firewall rule skipped: running without admin rights", LogSeverity.LOW);
                    $callback();
                }
            });
        } else {
            LogIt.Debug("Add firewall rule skipped: supported only on WINDOWS 7+");
            $callback();
        }
    }
}

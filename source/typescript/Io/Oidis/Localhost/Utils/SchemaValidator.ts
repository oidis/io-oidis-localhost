/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";

export class SchemaValidator extends BaseObject {
    protected errorHandler : any;
    private validator : any;
    private schema : any;

    constructor() {
        super();

        const ZSchema : any = require("z-schema");
        this.validator = new ZSchema({
            assumeAdditional: true,
            noExtraKeywords : true
        });
        this.schema = {};
        this.errorHandler = ($error : Error) : void => {
            throw $error;
        };
    }

    public ErrorHandler($handler : ($error : Error | ErrorEvent) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull($handler)) {
            this.errorHandler = $handler;
        }
    }

    public Schema($value? : any, $formats? : any) : any {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if (ObjectValidator.IsString($value)) {
                const fs : any = require("fs");
                $value = require("path").normalize($value);
                if (fs.existsSync($value)) {
                    try {
                        this.schema = JSON.parse(fs.readFileSync($value).toString());
                    } catch (ex) {
                        this.errorHandler(new Error("> failed parsing of schema\n" + ex.stack));
                    }
                } else if (ObjectValidator.IsObject($value)) {
                    this.errorHandler(new Error("> schema not found at: " + $value));
                } else {
                    this.errorHandler(new Error("> unsupported schema type: " + Convert.ToType($value)));
                }
            } else {
                this.schema = $value;
            }
        }
        if (!ObjectValidator.IsEmptyOrNull($formats)) {
            const ZSchema : any = require("z-schema");
            let format : string;
            for (format in $formats) {
                if ($formats.hasOwnProperty(format)) {
                    ZSchema.registerFormat(format, $formats[format]);
                }
            }
        }
        return this.schema;
    }

    public Validate($value : any, $parentKey : string = "#") : ISchemaError[] {
        try {
            const errors : ISchemaError[] = [];
            this.validator.validate($value, this.schema);
            const schemaErrors : any[] = this.validator.getLastErrors();
            if (!ObjectValidator.IsEmptyOrNull(schemaErrors)) {
                schemaErrors.forEach(($schemaError : any) : void => {
                    const fields : string[] = StringUtils.Split(StringUtils.Remove($schemaError.path, "#/"), "/");
                    let value : any = $value;
                    const iterate : any = ($index : number = 0) : void => {
                        if ($index < fields.length) {
                            if (value.hasOwnProperty(fields[$index])) {
                                value = value[fields[$index]];
                                iterate($index + 1);
                            }
                        }
                    };
                    iterate();
                    if (ObjectValidator.IsObject(value) || ObjectValidator.IsArray(value)) {
                        value = JSON.stringify(value);
                    }
                    let fieldsToString : string = "";
                    fields.forEach(($field : string) : void => {
                        if (ObjectValidator.IsInteger($field)) {
                            fieldsToString += "[" + $field + "]";
                        } else {
                            fieldsToString += "." + $field;
                        }
                    });
                    errors.push({
                        message: $schemaError.message,
                        field  : $parentKey + fieldsToString,
                        value
                    });
                });
            }
            return errors;
        } catch (ex) {
            this.errorHandler(new Error("> failed processing of validation\n" + ex.stack));
        }
    }

    public ErrorsToString($errors : ISchemaError[], $includeValue : boolean = false) : string {
        let messages : string = "";
        const singleError : boolean = $errors.length === 1;
        let index : number = 1;
        $errors.forEach(($error : ISchemaError) : void => {
            messages +=
                (singleError ? "> " : "[" + index + "] ") + "field " + $error.field + ", error: " + $error.message + "\n" +
                ($includeValue ? "    " + $error.value + "\n" : "");
            index++;
        });
        return messages;
    }
}

export interface ISchemaError {
    message : string;
    field : string;
    value : string;
}

// generated-code-start
export const ISchemaError = globalThis.RegisterInterface(["message", "field", "value"]);
// generated-code-end

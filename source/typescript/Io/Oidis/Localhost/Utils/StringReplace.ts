/*! ******************************************************************************************************** *
 *
 * Copyright 2021 PASAJA Authors
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FileSystemHandler } from "../Connectors/FileSystemHandler.js";
import { StringReplaceType } from "../Enums/StringReplaceType.js";
import { EnvironmentArgs } from "../EnvironmentArgs.js";
import { Loader } from "../Loader.js";

export class StringReplace extends BaseObject {
    private static singleton : StringReplace;
    protected varFormatter : ($value : string) => string = null;
    protected readonly fileSystem : FileSystemHandler;
    protected readonly appDataPath : string;
    protected readonly projectBase : string;
    private env : any;

    public static getInstance() : StringReplace {
        if (ObjectValidator.IsEmptyOrNull(StringReplace.singleton)) {
            StringReplace.singleton = new StringReplace();
        }

        return StringReplace.singleton;
    }

    public static Content($input : string, $type : StringReplaceType, $varFormatter? : ($value : string) => string) : string {
        return StringReplace.getInstance().Content($input, $type, $varFormatter);
    }

    public static ContentFrom($path : string, $type : StringReplaceType, $varFormatter? : ($value : string) => string) : string {
        return StringReplace.getInstance().ContentFrom($path, $type, $varFormatter);
    }

    constructor($environment? : any) {
        super();
        this.fileSystem = Loader.getInstance().getFileSystemHandler();
        this.projectBase = Loader.getInstance().getProgramArgs().ProjectBase();
        this.appDataPath = Loader.getInstance().getProgramArgs().AppDataPath();
        if (ObjectValidator.IsEmptyOrNull($environment)) {
            const env : EnvironmentArgs = Loader.getInstance().getEnvironmentArgs();
            this.env = {
                build       : env.getProjectConfig().build,
                project     : env.getProjectConfig().target,
                serverConfig: env.getProjectConfig(),
                templates   : this.getTemplates()
            };
        } else {
            this.env = $environment;
        }
    }

    public setEnvironment($value : any) : void {
        this.env = $value;
    }

    public Content($input : string, $type : StringReplaceType, $varFormatter? : ($value : string) => string) : string {
        this.varFormatter = $varFormatter;
        if (ObjectValidator.IsEmptyOrNull($input)) {
            $input = "";
        }
        this.getReplacements($type).forEach(($replacement : any) : void => {
            $input = $input.replace($replacement.pattern, ($match : string, $what : string, $where : string) : string => {
                if (ObjectValidator.IsFunction($replacement.replacement)) {
                    return $replacement.replacement.apply(this, [$match, $what, $where]);
                }
                return $replacement.replacement;
            });
        }, this);
        this.varFormatter = null;
        return $input;
    }

    public ContentFrom($path : string, $type : StringReplaceType, $varFormatter? : ($value : string) => string) : string {
        return this.Content(this.fileSystem.Read(this.resolveFilePath($path)).toString(), $type, $varFormatter);
    }

    public File($path : string, $type : StringReplaceType, $dest? : string) : boolean {
        if (ObjectValidator.IsEmptyOrNull($dest)) {
            $dest = $path;
        }
        return this.fileSystem.Write(this.resolveFilePath($dest), this.ContentFrom($path, $type));
    }

    protected getReplacements($type : StringReplaceType) : any[] {
        let replacements : any[] = [];
        switch ($type) {
        case StringReplaceType.IMPORTS_PREPARE:
            replacements = [
                {
                    pattern    : /<\? @import (.*?) \?>/ig,
                    replacement: this.importsPrepareReplacement
                },
                {
                    pattern    : /<!-- @import (.*?) -->/ig,
                    replacement: this.importsPrepareReplacement
                },
                {
                    pattern    : /#?\/\* @import (.*?) \*\//ig,
                    replacement: this.importsPrepareReplacement
                },
                {
                    pattern    : /\/\*\*\s*<!-- @import (.*?) -->\s*\*\//ig,
                    replacement: this.importsPrepareReplacement
                }
            ];
            break;
        case StringReplaceType.IMPORTS_FINISH:
            replacements = [
                {
                    pattern    : /<\? @import (.*?) \?>/ig,
                    replacement: this.importsFinishReplacement
                },
                {
                    pattern    : /<!-- @import (.*?) -->/ig,
                    replacement: this.importsFinishReplacement
                },
                {
                    pattern    : /\/\* @import (.*?) \*\//ig,
                    replacement: this.importsFinishReplacement
                },
                {
                    pattern    : /\/\*\*\s*<!-- @import (.*?) -->\s*\*\//ig,
                    replacement: this.importsPrepareReplacement
                }
            ];
            break;
        case StringReplaceType.FOR_EACH:
            replacements = [
                {
                    pattern    : /<\? @forEach (.*?) in (.*?) \?>/ig,
                    replacement: this.forEachReplacement
                },
                {
                    pattern    : /<!-- @forEach (.*?) in (.*?) -->/ig,
                    replacement: this.forEachReplacement
                }
            ];
            break;
        case StringReplaceType.VARIABLES:
            replacements = [
                {
                    pattern    : /<\? @var (.*?) \?>/ig,
                    replacement: this.varReplacement
                },
                {
                    pattern    : /<!-- @var (.*?) -->/ig,
                    replacement: this.varReplacement
                }
            ];
            break;

        default:
            LogIt.Warning("Unsupported StringReplace type: " + $type);
            break;
        }
        return replacements;
    }

    protected getTemplates() : any {
        return {};
    }

    protected getProperty($name : string, $from? : any) : any {
        if (ObjectValidator.IsEmptyOrNull($from)) {
            $from = this.env;
        }
        const split : string[] = $name.split(".");
        let fromCopy : any = JsonUtils.Clone($from);
        let index : number;
        let found : boolean = false;
        for (index = 0; index < split.length; index++) {
            let name : string = split[index];
            let nameIndex : number = -1;
            if (StringUtils.Contains(name, "[")) {
                nameIndex = StringUtils.ToInteger(name.split("[")[1].replace("]", ""));
                name = name.split("[")[0];
            }
            if (fromCopy.hasOwnProperty(name)) {
                fromCopy = fromCopy[name];
                if (nameIndex >= 0) {
                    fromCopy = fromCopy[nameIndex];
                }
                found = true;
            } else {
                found = false;
            }
        }
        if (found) {
            return fromCopy;
        }
        return undefined;
    }

    protected forEachReplacement($match : string, $what : string, $where : string) : string {
        const os : any = require("os");
        try {
            const whatValue : any = this.getProperty($what);
            const whereValue : any = this.getProperty($where);
            let output : string = "";
            const replaceSingle : any = ($target : any) : void => {
                if (output !== "") {
                    output += os.EOL;
                }
                if (ObjectValidator.IsString($target)) {
                    output += whatValue.replace(/<\? @var this\.value \?>/gi, $target);
                } else {
                    output += whatValue.replace(/<\? @var this\.(.*?) \?>/gi, ($match : string, $value : string) : string => {
                        try {
                            const replacement : string = this.getProperty($value, $target);
                            if (ObjectValidator.IsSet(replacement)) {
                                return replacement;
                            }
                        } catch (ex) {
                            LogIt.Debug(ex);
                        }
                        LogIt.Warning(
                            "string-replace: property \"" + $value + "\" was not found, so replace was skipped.");
                        return "<? @var this." + $value + " ?>";
                    });
                }
            };
            if (ObjectValidator.IsArray(whereValue)) {
                for (const item of whereValue) {
                    replaceSingle(item);
                }
            } else {
                for (const item in whereValue) {
                    if (whereValue.hasOwnProperty(item)) {
                        replaceSingle(whereValue[item]);
                    }
                }
            }

            return output;
        } catch (ex) {
            LogIt.Warning(ex);
        }
        LogIt.Warning("string-replace: forEach replacement for \"" + $where + "\" has failed, so replace was skipped.");
        return $match;
    }

    protected importsFinishReplacement($match : string, $path : string) : string {
        let content : string = Loader.getInstance().getFileSystemHandler().Read($path).toString();
        content = StringReplace.Content(content, StringReplaceType.FOR_EACH);
        content = StringReplace.Content(content, StringReplaceType.VARIABLES);
        return content;
    }

    protected importPathResolver($path : string) : any {
        let path : string = $path;
        if ($path.toString().indexOf(".html") !== -1) {
            path = "source/html/" + $path;
        } else if ($path.toString().indexOf(".js") !== -1) {
            path = "resource/javascript/" + $path;
        }
        const index : number = $path.toString().indexOf("dependencies");
        const isDependency : boolean = index !== -1;
        if (isDependency) {
            path = $path.substr(index, $path.length);
        }
        return {
            isDependency,
            path
        };
    }

    protected importsPrepareReplacement($match : string, $value : string) : string {
        const resolver : any = this.importPathResolver($value);
        if (this.fileSystem.Exists(resolver.path)) {
            const dest : string = this.appDataPath + "/compiled/" + resolver.path;
            if (resolver.isDependency) {
                this.fileSystem.Write(dest, this.fileSystem.Read(resolver.path));
            }
            return "<!-- @import " + dest + " -->";
        } else {
            return "<!-- @import " + $value + " -->";
        }
    }

    protected varReplacement($match : string, $value : string) : string {
        try {
            let replacement : string = this.getProperty($value);
            if (ObjectValidator.IsSet(replacement)) {
                if (!ObjectValidator.IsEmptyOrNull(this.varFormatter)) {
                    replacement = this.varFormatter(replacement);
                }
                if (ObjectValidator.IsObject(replacement)) {
                    replacement = JSON.stringify(JSON.stringify(replacement));
                    replacement = replacement.substring(1, replacement.length - 1);
                }
                return replacement;
            }
        } catch (ex) {
            // ignore unresolved replacement
        }
        LogIt.Warning("string-replace: property \"" + $value + "\" was not found, so replace was skipped.");
        return $match;
    }

    private resolveFilePath($path : string) : string {
        $path = this.fileSystem.NormalizePath($path);
        if (StringUtils.StartsWith($path, "./")) {
            $path = StringUtils.Substring($path, 2);
        }
        if (!StringUtils.StartsWith($path, "/") && !StringUtils.Contains($path, ":/")) {
            let absolutePath : string = this.projectBase + "/" + $path;
            if (!this.fileSystem.Exists(absolutePath)) {
                absolutePath = this.appDataPath + "/" + $path;
            }
            $path = absolutePath;
        }
        return $path;
    }
}

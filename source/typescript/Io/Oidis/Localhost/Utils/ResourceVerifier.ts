/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { FileSystemHandler } from "../Connectors/FileSystemHandler.js";
import { Loader } from "../Loader.js";

export class ResourceVerifier extends BaseObject {

    public static DecryptResource($data : Buffer, $key : Buffer) : Buffer {
        const iv : Buffer = $data.slice(0, 16);
        const tag : Buffer = $data.slice(16, 32);
        const dataEnc : Buffer = $data.slice(32);

        const decipher : any = require("crypto").createDecipheriv("aes-256-gcm", $key, iv);
        decipher.setAuthTag(tag);

        let dec : Buffer = null;

        try {
            const tmp : Buffer = decipher.update(dataEnc);
            const final : Buffer = decipher.final();
            dec = Buffer.concat([tmp, final]);
        } catch (ex) {
            LogIt.Error(this.ClassName(), ex);
        }

        return dec;
    }

    public static VerifyMap($publicKey : string, $map : any) : boolean {
        let success : boolean = true;
        const filesystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        for (const resourceName in $map) {
            if (Object.prototype.hasOwnProperty.call($map, resourceName)) {
                if (!filesystem.Exists(resourceName)) {
                    success = false;
                    break;
                }
                if (!ResourceVerifier.VerifyByName(<Buffer>filesystem.Read(resourceName), $publicKey, $map, resourceName)) {
                    success = false;
                    break;
                }
            }
        }
        return success;
    }

    public static VerifyByName($dataToVerify : Buffer, $publicKey : string, $map : any, $name : string) : boolean {
        if (ObjectValidator.IsEmptyOrNull($map) || !$map.hasOwnProperty($name)) {
            return false;
        }

        return ResourceVerifier.VerifySignature($dataToVerify, $publicKey, $map[$name]);
    }

    public static VerifySignature($dataToVerify : Buffer, $publicKey : string, $signature : string) : boolean {
        const verify : any = require("crypto").createVerify("sha512");

        verify.write($dataToVerify);
        verify.end();

        return verify.verify($publicKey, $signature, "hex");
    }

    public static VerifyHash($dataToVerify : Buffer, $protectionMap : any, $name : string) : boolean {
        if (ObjectValidator.IsEmptyOrNull($protectionMap) || !$protectionMap.hasOwnProperty($name)) {
            return false;
        }

        const hashEngine : any = require("crypto").createHash("sha512");
        hashEngine.update($dataToVerify);

        return $protectionMap[$name] === hashEngine.digest().toString("hex");
    }
}

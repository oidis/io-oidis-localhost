/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { isBrowser } from "@io-oidis-commons/Io/Oidis/Commons/Utils/EnvironmentHelper.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IProxyConfig } from "@io-oidis-services/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";

const nodejsRoot : string = globalThis.nodejsRoot;

const isMainThread : boolean = true; // TODO: validate how it should be connected with globalThis
if (isBrowser) {
    require = <any>(() : any => {
        return {};
    });
    module = <any>{
        exports: {
            Load() : void {
                // loader declare
            }
        }
    };
    (<any>Buffer) = {};
} else {
    WebSocket = <any>{}; // eslint-disable-line no-global-assign
    if (process.env.IS_OIDIS_DETACHED_PROCESS !== undefined && process.env.IS_OIDIS_DETACHED_PROCESS === "1") {
        if (!process.stdout.write) {
            (<any>process).stdout = <any>{
                write: console.log // eslint-disable-line no-console
            };
            (<any>process).stderr = <any>{
                write: console.log // eslint-disable-line no-console
            };
        } else {
            const Writable : any = require("stream").Writable;
            const stream : any = new Writable({
                write($chunk : Buffer, $encoding : string, $callback : () => void) : void {
                    console.log($chunk.toString()); // eslint-disable-line no-console
                    $callback();
                }
            });
            process.stdout.write = stream.write.bind(stream);
            process.stderr.write = stream.write.bind(stream);
        }
    }
}

export class EnvironmentHelper extends BaseObject {

    public static IsTTY() : boolean {
        let status : boolean = false;
        if (!this.IsDetached() && process.stdout.isTTY) {
            try {
                process.stdin.setRawMode(true);
                status = true;
            } catch ($ex) {
                // runtime has not been detected as TTY
            }
        }
        EnvironmentHelper.IsTTY = () : boolean => {
            return status;
        };
        return status;
    }

    public static IsWindows() : boolean {
        const os : any = require("os");
        const status : boolean = os.platform().toString() === "win32" || os.platform().toString() === "win64";
        EnvironmentHelper.IsWindows = () : boolean => {
            return status;
        };
        return status;
    }

    public static IsLinux() : boolean {
        const status : boolean = !EnvironmentHelper.IsWindows() && !EnvironmentHelper.IsMac();
        EnvironmentHelper.IsLinux = () : boolean => {
            return status;
        };
        return status;
    }

    public static IsMac() : boolean {
        const status : boolean = require("os").platform() === "darwin";
        EnvironmentHelper.IsMac = () : boolean => {
            return status;
        };
        return status;
    }

    public static Is64bit() : boolean {
        const status : boolean =
            StringUtils.Contains(process.arch, "64") || process.env.hasOwnProperty("PROCESSOR_ARCHITEW6432") ||
            StringUtils.Contains(require("os").arch(), "64") ||
            EnvironmentHelper.IsMac();
        EnvironmentHelper.Is64bit = () : boolean => {
            return status;
        };
        return status;
    }

    public static IsArm() : boolean {
        const status : boolean = StringUtils.ContainsIgnoreCase(require("os").arch(), "arm");
        EnvironmentHelper.IsArm = () : boolean => {
            return status;
        };
        return status;
    }

    public static getCores() : number {
        const cores : number = require("os").cpus().length;
        EnvironmentHelper.getCores = () : number => {
            return cores;
        };
        return cores;
    }

    public static IsElevated($callback : ($status : boolean) => void) : void {
        let status : boolean = false;
        (async () : Promise<boolean> => {
            return (await import("@nodejs/is-elevated/index.js")).default();
        })().then(($elevated : boolean) : void => {
            status = $elevated;
            EnvironmentHelper.IsElevated = ($callback : ($status : boolean) => void) : void => {
                $callback(status);
            };
            $callback(status);
        });
    }

    public static IsEmbedded() : boolean {
        const status : boolean = StringUtils.StartsWith(__dirname, "/snapshot/");
        this.IsEmbedded = () : boolean => {
            return status;
        };
        return status;
    }

    public static IsDetached() : boolean {
        const status : boolean = !ObjectValidator.IsEmptyOrNull(process.env.IS_OIDIS_DETACHED_PROCESS) &&
            process.env.IS_OIDIS_DETACHED_PROCESS === "1";
        this.IsDetached = () : boolean => {
            return status;
        };
        return status;
    }

    public static getNodejsRoot() : string {
        return nodejsRoot;
    }

    public static NormalizeProxySetting($config : IProxyConfig) : IProxyConfig {
        const URL : any = require("url").URL;
        // ALL_PROXY env is accepted by curl, and it means that HTTP_PROXY=HTTPS_PROXY=ALL_PROXY if http/https are not specified
        //  and some other application uses all_proxy for ftp/ws/etc and http/https proxy then only for these protocols
        //  but this is super-corner-case supported only by some tools (rhel, partially curl+ftp,..)
        // 1: project config has higher priority
        // 2: process.env needs to have synced to its lower and upper case equivalent because some tools supports both, some only
        //      one case, and some like curl/wget are mixed (differs also over different version of the same tool [gitlab guidelines])
        // 3: no_proxy = comma separated list of machine/domain names with optional port
        //   (if no port part is present then applied for all ports for domains). Domain names should be in lowercase (except Go)
        //   and some other tools so DO NOT APPLY any toLower/toUpper to enable user to specify whatever case (but handle matching
        //   as ignorecase in JS).
        // 4: *_proxy envs will be just synced without any manipulation
        //   throw error if http_proxy and HTTP_PROXY exists in env and has different data -> user will need to fix it or
        //   if httpProxy is in config then simply override both (with warning)
        // 5: no_proxy filter: applied separately based on destination protocol (with handling of :<port> <=> protocol).
        //     pattern matching needs to be supported in order to support "*" for any count of chars, ignore leading "."
        // 6: if proxy settings are in env and not in config then sync them in (env should automatically turn on proxy)
        const checkEnvVar : any = ($envVar : string) : string => {
            if (ObjectValidator.IsEmptyOrNull($envVar)) {
                throw new Error("Internal error occurred, env variable is not specified for check.");
            }
            let envValue : string = "";
            const envVarLC : string = $envVar.toLowerCase();
            const envVarUC : string = $envVar.toUpperCase();
            if (!ObjectValidator.IsEmptyOrNull(process.env[envVarLC]) && !ObjectValidator.IsEmptyOrNull(process.env[envVarUC])) {
                if (process.env[envVarLC] !== process.env[envVarUC]) {
                    throw new Error("process.env contains both of " + envVarLC + " and " + envVarUC + " with different values");
                }
                envValue = process.env[envVarLC];
            } else if (!ObjectValidator.IsEmptyOrNull(process.env[envVarLC])) {
                envValue = process.env[envVarLC];
            } else if (!ObjectValidator.IsEmptyOrNull(process.env[envVarUC])) {
                envValue = process.env[envVarUC];
            }
            return envValue;
        };

        let httpProxy : string = "";
        let httpsProxy : string = "";
        let noProxy : string = "";

        if (!ObjectValidator.IsEmptyOrNull($config.httpProxy)) {
            httpProxy = $config.httpProxy;
        } else {
            httpProxy = checkEnvVar("http_proxy");
        }
        if (!ObjectValidator.IsEmptyOrNull($config.httpsProxy)) {
            httpsProxy = $config.httpsProxy;
        } else {
            httpsProxy = checkEnvVar("https_proxy");
        }
        if (!ObjectValidator.IsEmptyOrNull($config.noProxy)) {
            noProxy = $config.noProxy;
        } else {
            noProxy = checkEnvVar("no_proxy");
        }

        if (!ObjectValidator.IsEmptyOrNull(httpProxy)) {
            const url : URL = new URL(httpProxy);
            if (StringUtils.StartsWith(url.protocol, "https")) {
                throw new Error("Secured proxy is not supported, please use http://<proxy>:<port> for http_proxy instead.");
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(httpsProxy)) {
            const url : URL = new URL(httpsProxy);
            if (StringUtils.StartsWith(url.protocol, "https")) {
                throw new Error("Secured proxy is not supported, please use http://<proxy>:<port> for https_proxy instead.");
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(noProxy)) {
            // eslint-disable-next-line no-useless-escape
            if (!ObjectValidator.IsEmptyOrNull(noProxy.match(/[ ;\\\/!@#$%^&()_+{}\[\]]/gi))) {
                throw new Error("no_proxy option contains unsupported symbols: " + noProxy);
            }
            StringUtils.Split(noProxy, ",")
                .forEach(($part : string) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($part) && StringUtils.Contains($part, ":") &&
                        !($part.match(/:\d{1,5}/gi))) {
                        throw new Error("no_proxy option contains host with wrong port definition: " + $part);
                    }
                });
        }

        // sync resolved env back
        process.env.http_proxy = process.env.HTTP_PROXY = $config.httpProxy = httpProxy;
        process.env.https_proxy = process.env.HTTPS_PROXY = $config.httpsProxy = httpsProxy;
        process.env.no_proxy = process.env.NO_PROXY = $config.noProxy = noProxy;
        return $config;
    }
}

export { nodejsRoot, isMainThread };

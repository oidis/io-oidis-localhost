/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IResponseFormatter } from "../HttpProcessor/ResponseApi/Formatters/BaseResponseFormatter.js";
import { IResponse } from "./IResponse.js";

export enum ExternResponseType {
    DEFAULT,
    IMPLICIT_RETURN,
    WITH_PROGRESS,
    FULL_CONTROL
}

export interface IExternOptions {
    responseType? : ExternResponseType;
    responseHandler? : IResponse;
    outputFormatter? : IResponseFormatter;
}

// generated-code-start
export const IExternOptions = globalThis.RegisterInterface(["responseType", "responseHandler", "outputFormatter"]);
// generated-code-end

/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";

export interface IResponse {
    Send(...$args : any[]) : IResponsePromise;

    OnStart(...$args : any[]) : IResponsePromise;

    OnChange(...$args : any[]) : IResponsePromise;

    OnComplete(...$args : any[]) : IResponsePromise;

    OnError($message : string | Error, ...$args : any[]) : void;

    OnMessage($data : any) : void;

    FireEvent($name : string, $args? : any) : Promise<void>;

    getId() : number;

    getOwnerId() : string;

    AddAbortHandler($handler : () => void) : void;

    Abort() : void;

    KeepAlive($value? : boolean) : boolean;

    Dispatch() : void;
}

export interface IMessage {
    OnChange : ($message : string) => void;
}

export interface IResponsePromise {
    Then($callback : (...$args : any[]) => void) : void;
}

export interface IResponseProtocol {
    status : HttpStatusType;
    headers : any;
    body : any;
    normalize? : boolean;
}

export interface IAuthPromise {
    resolve : ($token : string, $version? : string) => void;
    auth : string;
}

// generated-code-start
/* eslint-disable */
export const IResponse = globalThis.RegisterInterface(["Send", "OnStart", "OnChange", "OnComplete", "OnError", "OnMessage", "FireEvent", "getId", "getOwnerId", "AddAbortHandler", "Abort", "KeepAlive", "Dispatch"]);
export const IMessage = globalThis.RegisterInterface(["OnChange"]);
export const IResponsePromise = globalThis.RegisterInterface(["Then"]);
export const IResponseProtocol = globalThis.RegisterInterface(["status", "headers", "body", "normalize"]);
export const IAuthPromise = globalThis.RegisterInterface(["resolve", "auth"]);
/* eslint-enable */
// generated-code-end

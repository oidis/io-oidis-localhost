/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import {
    ILoggerConfiguration as CommonsILoggerConfiguration,
    IProject as Parent,
    IProjectTarget as CommonsIProjectTarget
} from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IProject.js";

export interface IProject extends Parent {
    domain : IServerConfigurationDomain;
    httpProxy : string;
    forceHttpProxy : boolean;
    httpsProxy : string;
    noProxy : string;
    reverseProxy : IReverseProxyConfiguration;
    connectorProxy : boolean;
    certs : ICertsConfiguration;
    target : IProjectTarget;
    disableStrictSSL : boolean;
    elastic : IElasticConfiguration;
    logger : ILoggerConfiguration;
}

export interface IProjectTarget extends CommonsIProjectTarget {
    projectBase : string;
    appDataPath : string;
    tmpPath : string;
    requestPatterns : string[];
    metrics : IMetricsConfiguration;
}

export interface IServerConfigurationDomain {
    location : string;
    ports : IServerConfigurationPorts;
    ssl : IServerConfigurationSSL | string;
    origins : string[];
    localization : IServerLocalization;
    csp : ICSPConfiguration;
}

export interface IServerConfigurationPorts {
    http : number;
    https : number;
}

export interface IServerConfigurationSSL {
    enabled : boolean;
    certificatePath : string;
    privateKeyPath : string;
}

export interface IServerLocalization {
    default : string;
    requestDriven : boolean;
}

export interface IMetricsConfiguration {
    enabled : boolean;
    location : string;
    port : number;
}

export interface ICertsConfiguration {
    external : boolean;
    mail : string;
    port : number;
    location : string;
}

export interface IReverseProxyConfiguration {
    enabled : boolean;
    withServer : boolean;
    tunnels : IProxyTunnelsConfiguration[];
}

export interface IProxyTunnelsConfiguration {
    port : number;
    ssl : IServerConfigurationSSL | string;
    location : string;
}

export interface IElasticConfiguration {
    location : string;
    index : string;
    user : string;
    pass : string;
    cert : string;
    mapping : string;
}

export interface ILoggerConfiguration extends CommonsILoggerConfiguration {
    httpGetFilterPatterns : string[];
}

export interface ICSPConfiguration {
    enabled : boolean;
    reportUri : string;
    policies : IContentSecurityPolicy[];
}

export interface IContentSecurityPolicy {
    enabled? : boolean;
    isDefault? : boolean;
    pattern? : string;
    policy : string[];
}

// generated-code-start
/* eslint-disable */
export const IProject = globalThis.RegisterInterface(["domain", "httpProxy", "forceHttpProxy", "httpsProxy", "noProxy", "reverseProxy", "connectorProxy", "certs", "target", "disableStrictSSL", "elastic", "logger"], <any>Parent);
export const IProjectTarget = globalThis.RegisterInterface(["projectBase", "appDataPath", "tmpPath", "requestPatterns", "metrics"], <any>CommonsIProjectTarget);
export const IServerConfigurationDomain = globalThis.RegisterInterface(["location", "ports", "ssl", "origins", "localization", "csp"]);
export const IServerConfigurationPorts = globalThis.RegisterInterface(["http", "https"]);
export const IServerConfigurationSSL = globalThis.RegisterInterface(["enabled", "certificatePath", "privateKeyPath"]);
export const IServerLocalization = globalThis.RegisterInterface(["default", "requestDriven"]);
export const IMetricsConfiguration = globalThis.RegisterInterface(["enabled", "location", "port"]);
export const ICertsConfiguration = globalThis.RegisterInterface(["external", "mail", "port", "location"]);
export const IReverseProxyConfiguration = globalThis.RegisterInterface(["enabled", "withServer", "tunnels"]);
export const IProxyTunnelsConfiguration = globalThis.RegisterInterface(["port", "ssl", "location"]);
export const IElasticConfiguration = globalThis.RegisterInterface(["location", "index", "user", "pass", "cert", "mapping"]);
export const ILoggerConfiguration = globalThis.RegisterInterface(["httpGetFilterPatterns"], <any>CommonsILoggerConfiguration);
export const ICSPConfiguration = globalThis.RegisterInterface(["enabled", "reportUri", "policies"]);
export const IContentSecurityPolicy = globalThis.RegisterInterface(["enabled", "isDefault", "pattern", "policy"]);
/* eslint-enable */
// generated-code-end

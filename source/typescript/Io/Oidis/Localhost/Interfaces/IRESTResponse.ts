/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";

export interface IRESTResponse {
    code : HttpStatusType;
    status : string;
    message? : string;
    data? : any;
    errors? : IRESTError[];
}

export interface IRESTError {
    message : string;
    field : string;
    value : string;
}

export interface IRESTResponseOptions {
    force? : boolean;
    etags? : ArrayList<string>;
    acceptType? : string;
}

// generated-code-start
export const IRESTResponse = globalThis.RegisterInterface(["code", "status", "message", "data", "errors"]);
export const IRESTError = globalThis.RegisterInterface(["message", "field", "value"]);
export const IRESTResponseOptions = globalThis.RegisterInterface(["force", "etags", "acceptType"]);
// generated-code-end

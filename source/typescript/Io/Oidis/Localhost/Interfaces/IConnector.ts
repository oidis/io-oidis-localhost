/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";

export interface IConnector extends IBaseObject {
    ExternInvokeEnabled($value? : boolean) : boolean;
}

// generated-code-start
export const IConnector = globalThis.RegisterInterface(["ExternInvokeEnabled"], <any>IBaseObject);
// generated-code-end

/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IResponseFormatter } from "../HttpProcessor/ResponseApi/Formatters/BaseResponseFormatter.js";
import { IExternOptions } from "./IExternOptions.js";
import { IAuthPromise } from "./IResponse.js";
import { IRESTResponse } from "./IRESTResponse.js";

export interface IRoute {
    method : (...$args : any[]) => IAuthPromise;
    methodName : () => string;
    route : string;
    options : IRouteOptions;
    schema : string;
    ClassName : () => string;
}

export interface IRouteOptions extends IExternOptions {
    httpMethod? : HttpMethodType;
    version? : string;
    /** @deprecated */
    autoResponse? : boolean;
    /**
     * ToJSON formatter is used for PUT and POST HTTP methods automatically and base (identity) formatter for other methods.
     */
    inputFormatter? : IResponseFormatter;
    /**
     * Base formatter is used by default.
     */
    args? : Array<string | BaseRouteArgument>;
    priority? : number;
}

export interface IHttpResponse {
    status? : HttpStatusType;
    headers? : any;
    body? : IRESTResponse | string | Buffer;
}

export class BaseRouteArgument extends BaseObject {
    private readonly key : string;
    private value : any;
    private readonly converter : ($value : any) => any;

    constructor($key : string, $value) {
        super();
        this.converter = ($val : any) : any => {
            return $val;
        };
        this.key = $key;
        if (ObjectValidator.IsString($value)) {
            const group = /(\w+)\((.*)\)/.exec($value);
            if (ObjectValidator.IsArray(group) && group.length === 3) {  // exactly [fullmatch, type-keyword, value] nothing more
                switch (group[1].toLowerCase()) {
                case "int":
                case "integer":
                case "number":
                    this.converter = Convert.StringToInteger;
                    break;
                case "float":
                case "double":
                    this.converter = Convert.StringToDouble;
                    break;
                case "bool":
                case "boolean":
                    this.converter = Convert.StringToBoolean;
                    break;
                case "string":
                    this.converter = ($value : string) : string => {
                        return $value;
                    };
                    break;
                default:
                }
                $value = this.converter(group[2]);
            }
        }
        this.value = $value;
    }

    get Key() {
        return this.key;
    }

    get Value() {
        return this.value;
    }

    public UpdateValue($value : any) : void {
        if (ObjectValidator.IsFunction(this.converter)) {
            this.value = this.converter($value);
        }
    }

    public ResolveValue($value : any) : any {
        if (ObjectValidator.IsFunction(this.converter)) {
            return this.converter($value);
        }
        return $value;
    }
}

// generated-code-start
/* eslint-disable */
export const IRoute = globalThis.RegisterInterface(["method", "methodName", "route", "options", "schema", "ClassName"]);
export const IRouteOptions = globalThis.RegisterInterface(["httpMethod", "version", "autoResponse", "inputFormatter", "args", "priority"], <any>IExternOptions);
export const IHttpResponse = globalThis.RegisterInterface(["status", "headers", "body"]);
/* eslint-enable */
// generated-code-end

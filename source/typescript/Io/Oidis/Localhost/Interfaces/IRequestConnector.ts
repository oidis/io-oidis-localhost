/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ILiveContentProtocol, IWebServiceProtocol } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceProtocol.js";
import { HttpServer } from "../HttpProcessor/HttpServer.js";
import { IResponseProtocol } from "./IResponse.js";

export interface IRequestConnector {
    Send : ($data : string | IWebServiceProtocol | ILiveContentProtocol | IResponseProtocol) => void;
    getOwnerId : () => string;
    getParentProcess : () => HttpServer;
}

// generated-code-start
export const IRequestConnector = globalThis.RegisterInterface(["Send", "getOwnerId", "getParentProcess"]);
// generated-code-end

/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpServerEventArgs } from "../../Events/Args/HttpServerEventArgs.js";

export interface IHttpServerEvents {
    OnStart($eventHandler : ($eventArgs? : HttpServerEventArgs) => void) : void;

    OnConnect($eventHandler : ($eventArgs? : HttpServerEventArgs) => void) : void;

    OnDisconnect($eventHandler : ($eventArgs? : HttpServerEventArgs) => void) : void;
}

// generated-code-start
export const IHttpServerEvents = globalThis.RegisterInterface(["OnStart", "OnConnect", "OnDisconnect"]);
// generated-code-end

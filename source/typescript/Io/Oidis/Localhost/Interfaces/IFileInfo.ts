/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IFileInfo {
    path : string;
    metadata? : any;
    maxChunkSizeKb? : any;
    chunkTimeout? : number;
}

export interface IRemoteFileInfo extends IFileInfo {
    cwd : string;
}

// generated-code-start
export const IFileInfo = globalThis.RegisterInterface(["path", "metadata", "maxChunkSizeKb", "chunkTimeout"]);
export const IRemoteFileInfo = globalThis.RegisterInterface(["cwd"], <any>IFileInfo);
// generated-code-end

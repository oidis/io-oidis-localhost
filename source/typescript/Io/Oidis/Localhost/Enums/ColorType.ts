/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { isBrowser } from "@io-oidis-commons/Io/Oidis/Commons/Utils/EnvironmentHelper.js";
import { isMainThread } from "../Utils/EnvironmentHelper.js";

export class ColorType extends BaseEnum {
    public static readonly YELLOW : string = "yellow";
    public static readonly RED : string = "red";
    public static readonly GREEN : string = "green";

    public static Underline($input : string) : string {
        if (!isBrowser && isMainThread) {
            return require("colors").underline($input);
        }
        return $input;
    }

    public static setColor($input : string, $type : ColorType) : string {
        if (!isBrowser && isMainThread) {
            return require("colors")[<any>$type]($input);
        }
        return $input;
    }
}

interface String { // eslint-disable-line @typescript-eslint/no-unused-vars
    setColor($type : ColorType) : string;

    Underline() : string;
}

if (!isBrowser) {
    Object.defineProperty(String.prototype, "underline", {
        get() : string {
            return ColorType.Underline(this.toString());
        }
    });
    [
        ColorType.YELLOW,
        ColorType.RED,
        ColorType.GREEN
    ].forEach(($value : string) : void => {
        Object.defineProperty(String.prototype, $value, {
            get() : string {
                return ColorType.setColor(this.toString(), $value);
            }
        });
    });

    (<any>String).prototype.setColor = function ($type : ColorType) : string {
        return ColorType.setColor(this.toString(), $type);
    };

    (<any>String).prototype.Underline = function () : string {
        return ColorType.Underline(this.toString());
    };
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2021 PASAJA Authors
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export class StringReplaceType extends BaseEnum {
    public static readonly IMPORTS_PREPARE : string = "importsPrepare";
    public static readonly IMPORTS_FINISH : string = "importsFinish";
    public static readonly FOR_EACH : string = "forEach";
    public static readonly VARIABLES : string = "variables";
}

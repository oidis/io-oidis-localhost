/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export class ProtectionType extends BaseEnum {
    public static readonly ENCRYPTION : string = "ENCRYPTION";
    public static readonly HASH : string = "HASH";
    public static readonly SIGNATURE : string = "SIGNATURE";
    public static readonly NONE : string = "NONE";
}

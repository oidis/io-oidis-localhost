/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { LiveContentArgumentType } from "@io-oidis-services/Io/Oidis/Services/Enums/LiveContentArgumentType.js";
import { BaseResponseFormatter } from "../HttpProcessor/ResponseApi/Formatters/BaseResponseFormatter.js";
import { ToJSONRouteFormatter } from "../HttpProcessor/ResponseApi/Formatters/ToJSONRouteFormatter.js";
import { ResponseFactory } from "../HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType, IExternOptions } from "../Interfaces/IExternOptions.js";
import { IAuthPromise, IResponse } from "../Interfaces/IResponse.js";
import {
    BaseRouteArgument,
    IRoute,
    IRouteOptions
} from "../Interfaces/IRoute.js";

/**
 * This decorator wraps decorated method to method with authorization promise as return value.
 * @param $autoResponse Set false to disable automatic check for IResponse argument at last position in given arguments and force to
 * call method with each arguments include IResponse from LiveContentResolver. Fallback for options.responseType = FULL_CONTROL.
 * @example Method($arg: number) will be automatically called in response.Send(...) and Method($arg: number, $callback: IResponse)
 * will be automatically called as is.
 * @returns {Function} Returns decorator which enable to invoke decorated method externally over LCP.
 */
export function Extern($autoResponse? : boolean) : any;
/**
 * This decorator wraps decorated method to method with authorization promise as return value.
 * @param $responseType Set behavior of automatic IResponse argument handling.
 * @returns {Function} Returns decorator which enable to invoke decorated method externally over LCP.
 */
export function Extern($responseType? : ExternResponseType) : any;
/**
 * This decorator wraps decorated method to method with authorization promise as return value.
 * @param $options Define deeper handling of extern behavior.
 * @returns {Function} Returns decorator which enable to invoke decorated method externally over LCP.
 */
export function Extern($options? : IExternOptions) : any;
export function Extern($options? : IExternOptions | ExternResponseType | boolean) : any {
    const options : IExternOptions = <IExternOptions>{
        outputFormatter: new BaseResponseFormatter(),
        responseHandler: null,
        responseType   : ExternResponseType.DEFAULT
    };
    if (ObjectValidator.IsBoolean($options) && !<boolean>$options) {
        options.responseType = ExternResponseType.FULL_CONTROL;
    } else if (ObjectValidator.IsInteger($options)) {
        options.responseType = <ExternResponseType>$options;
    } else {
        JsonUtils.Extend(options, $options);
    }
    return function ($target : any, $propertyKey : string, $descriptor : PropertyDescriptor) : any {
        $target[$propertyKey + "_Extern"] = function (...$args : any[]) : IAuthPromise {
            return {
                auth: Convert.ToType(this) + "." + $propertyKey,
                resolve($token : string, $version : string) : void {
                    resolveResponse(this, {
                        args: $args, descriptor: $descriptor, noVoidSymbol: false, propertyKey: $propertyKey, target: this
                    }, {
                        options, token: $token, version: $version
                    });
                }
            };
        };
    };
}

/**
 * Mark STATIC method by this decorator to register it for integrity checks execution as another step.
 * Registered integrity checks are processed by default order produced by transpiled JS file (order is not guaranteed) so
 * all steps should be fully isolated and independent (that is also reason for static only method limitation and instances
 * should be created internally if needed).
 * If you need some kind of partial generalisation then implement this chain manually.
 * It is strongly recommended to mark integrity check methods as "PRIVATE" because it should not be exposed in API.
 * @param {IIntegrityCheckOption} [$options] Specify integrity check test options
 * @returns {Function} Returns decorator which enable to generate interface expected by integrity test processor.
 */
export function IntegrityCheck($options? : IIntegrityCheckOption) : any {
    return function ($target : any, $propertyKey : string, $descriptor : PropertyDescriptor) : any {
        $target[$propertyKey + "_IntegrityCheck"] = () : any => {
            let name : string = $target.ClassName() + "." + $propertyKey;
            if (!ObjectValidator.IsEmptyOrNull($options) && !ObjectValidator.IsEmptyOrNull($options.name)) {
                name = $options.name;
            }

            return {
                method: async () : Promise<void> => {
                    return $descriptor.value.apply(this, []);
                },
                name
            };
        };
        if (!window.hasOwnProperty("oidisIntegrityCheckSteps")) {
            (<any>window).oidisIntegrityCheckSteps = [];
        }
        (<any>window).oidisIntegrityCheckSteps.push($target[$propertyKey + "_IntegrityCheck"]);
    };
}

export function Route($path : string, $options? : IRouteOptions) : any {
    return function ($target : any, $propertyKey : string, $descriptor : PropertyDescriptor) : any {
        let pathRoute : string = StringUtils.Split($path, "#", "?")[0];
        let pathRequest : string = $path;
        let method : HttpMethodType = HttpMethodType.GET;
        if (pathRoute.indexOf(" ") > -1) {
            const subs : string[] = pathRoute.split(" ");
            if (subs.length !== 2) {
                throw new Error("Invalid syntax for @Route definition string: " + pathRoute + ". " +
                    "Only <method> /<path>?<key>=<type>(value) with optional parts method|key|type|value is allowed.");
            }
            method = HttpMethodType[subs[0].toUpperCase()];
            const types : string[] = Object.keys(HttpMethodType);
            if (ObjectValidator.IsEmptyOrNull(method) || !types.includes(<any>method)) {
                throw new Error("Unsupported HTTP method defined in route: \"" + pathRoute + "\". Only " +
                    types.sort().join("|") + " are supported by HTTP standard.");
            }
            pathRoute = subs[1];
            pathRequest = $path.substring(subs[0].length + 1);
        }
        const key : string = $propertyKey + "_" + pathRoute + "_Route";
        $target[key] = () : IRoute => {
            if (ObjectValidator.IsEmptyOrNull($options)) {
                $options = {};
            }

            if (ObjectValidator.IsEmptyOrNull($options.httpMethod)) {
                $options.httpMethod = method;
            }
            if (ObjectValidator.IsEmptyOrNull($options.args)) {
                $options.args = [];
                const parser : URL = new (require("url").URL)("https://oidis.io" + pathRequest);
                parser.searchParams.forEach(($value : string, $key : string) : void => {
                    $options.args.push(<any>new BaseRouteArgument($key, $value));
                });
            }
            $options.args = $options.args.map(($value : string | BaseRouteArgument) : any => {
                if (ObjectValidator.IsString($value)) {
                    const items : string[] = StringUtils.Split(<string>$value, "=");
                    return new BaseRouteArgument(items[0], items[1]);
                } else if (Reflection.getInstance().IsInstanceOf(<IBaseObject>$value, BaseRouteArgument)) {
                    return $value;
                }
                return null;
            });
            if (ObjectValidator.IsEmptyOrNull($options.inputFormatter)) {
                if ($options.httpMethod === HttpMethodType.PUT || $options.httpMethod === HttpMethodType.POST) {
                    $options.inputFormatter = new ToJSONRouteFormatter();
                } else {
                    $options.inputFormatter = new BaseResponseFormatter();
                }
            }
            if (ObjectValidator.IsEmptyOrNull($options.outputFormatter)) {
                $options.outputFormatter = new BaseResponseFormatter();
            }

            const resolveOptions : IExternOptions = <IExternOptions>{
                outputFormatter: $options.outputFormatter,
                responseHandler: $options.responseHandler,
                responseType   : ExternResponseType.DEFAULT
            };
            if (!ObjectValidator.IsEmptyOrNull($options.responseType)) {
                resolveOptions.responseType = $options.responseType;
            }
            return {
                ClassName: () : string => {
                    if ($target.isPartial && ObjectValidator.IsSet($target.__partialTarget)) {
                        return $target.__partialTarget.getClassName();
                    }
                    if (ObjectValidator.IsSet($target.getClassName)) {
                        return $target.getClassName();
                    }
                    return $target.ClassName();
                },
                method(...$args : any[]) : IAuthPromise {
                    return {
                        auth: Convert.ToType(this) + "." + $propertyKey,
                        resolve($token : string) : void {
                            if ($options.args.length > $descriptor.value.length) {
                                throw new Error("Passed more arguments then declared in route schema: " + $path);
                            }
                            if (!ObjectValidator.IsEmptyOrNull($options.autoResponse) && !$options.autoResponse) {
                                resolveOptions.responseType = ExternResponseType.FULL_CONTROL;
                                LogIt.Warning("Usage of deprecated option autoResponse for " +
                                    Convert.ToType(this) + "." + $propertyKey +
                                    ", please migrate to responseType. Automatically switching to FULL_CONTROL responseType option now.");
                            }
                            resolveResponse(this, {
                                args: $args, descriptor: $descriptor, noVoidSymbol: true, propertyKey: $propertyKey, target: this
                            }, {
                                options: resolveOptions, token: $token
                            });
                        }
                    };
                },
                methodName() : string {
                    return this.ClassName() + "." + $propertyKey;
                },
                options: $options,
                route  : pathRoute,
                schema : $path
            };
        };
        if (!globalThis.hasOwnProperty("oidisAPIRoutes")) {
            globalThis.oidisAPIRoutes = [];
        }
        globalThis.oidisAPIRoutes.push($target[key]);
    };
}

export interface IIntegrityCheckOption {
    name? : string;
}

interface IResponseDecorator {
    args : any[];
    descriptor : PropertyDescriptor;
    target : any;
    propertyKey : string;
    noVoidSymbol : boolean;
}

interface IResponseResolveAttributes {
    token : any;
    version? : string;
    options : IExternOptions;
}

function resolveResponse($self : any, $decoratorParams : IResponseDecorator, $invokeParams : IResponseResolveAttributes) : void {
    if (ObjectValidator.IsSet($self.currentToken)) {
        // replace static method
        $self.currentToken = () => {
            return $invokeParams.token;
        };
    } else {
        // replace getter inside instance
        $self.getCurrentToken = () => {
            return $invokeParams.token;
        };
    }

    if (!ObjectValidator.IsEmptyOrNull($invokeParams.version)) {
        $self.getProtocolVersion = () => {
            return $invokeParams.version;
        };
    }

    /// TODO: probably will not work after minification as args will be renamed
    const args : any[] = [...$decoratorParams.args];
    if ($invokeParams.options.responseType === ExternResponseType.DEFAULT &&
        $decoratorParams.descriptor.value.length === args.length &&
        StringUtils.ContainsIgnoreCase($decoratorParams.descriptor.value, "$callback", "$handler", "$response")) {
        LogIt.Error("Incorrect definition of IResponse for " +
            Convert.ToType($decoratorParams.target) + "." + $decoratorParams.propertyKey +
            " marked as Extern without FULL_CONTROL option. " +
            "Falling back to FULL_CONTROL response due to detected parameter with name [$callback|$handler|$response].");
        $invokeParams.options.responseType = ExternResponseType.FULL_CONTROL;
    }
    if (!Reflection.getInstance().Implements(args[args.length - 1],
        "Io.Oidis.Localhost.Interfaces.IResponse")) {
        // this should never happen as IResponse is automatically generated by LiceContentResolver
        LogIt.Error("IResponse parameter is missing for " +
            Convert.ToType($decoratorParams.target) + "." + $decoratorParams.propertyKey +
            ". This method will never respond, but root cause needs to be investigated.");
    }

    let response : IResponse = ResponseFactory.getResponse(args[args.length - 1]);
    if ($invokeParams.options.responseType === ExternResponseType.IMPLICIT_RETURN ||
        $invokeParams.options.responseType === ExternResponseType.DEFAULT) {
        args.pop();
    }
    const retVal : any = $decoratorParams.descriptor.value.apply($self, args);
    if (!ObjectValidator.IsEmptyOrNull($invokeParams.options.responseHandler)) {
        (<any>$invokeParams.options.responseHandler).Clone(response);
        response = $invokeParams.options.responseHandler;
    }
    const sendResponse : any = ($retVal : any) : void => {
        $retVal = $invokeParams.options.outputFormatter.format($retVal);
        if ($invokeParams.options.responseType !== ExternResponseType.FULL_CONTROL) {
            if (!ObjectValidator.IsSet($retVal)) {
                $retVal = $decoratorParams.noVoidSymbol ? "" : LiveContentArgumentType.RETURN_VOID;
            } else if ($retVal === null) {
                $retVal = null;
            } else if (typeof $retVal === "object" && $retVal.constructor.name === "Buffer") {
                $retVal = $retVal.toString();
            }
            response.Send($retVal);
            $retVal = null;
        } else if (ObjectValidator.IsSet($retVal)) {
            LogIt.Error("Method " + Convert.ToType($decoratorParams.target) + "." + $decoratorParams.propertyKey +
                " is not void even that response is in FULL_CONTROL mode. Return value will never be send " +
                "in this use case and must be handle by IResponse manually in method body.");
        } else {
            // returned value should be processed by autoResponse or IResponse in method body,
            // however some processing of retVal could be inserted also here in future
        }
    };
    if (retVal instanceof Promise ||
        !ObjectValidator.IsEmptyOrNull(retVal) && retVal.constructor.name === "Promise") {
        try {
            retVal.then(($retVal : any) : void => {
                sendResponse($retVal);
            }).catch(($error : Error) : void => {
                response.OnError($error);
            });
        } catch (ex) {
            response.OnError(ex);
        }
    } else {
        sendResponse(retVal);
    }
}

// generated-code-start
export const IIntegrityCheckOption = globalThis.RegisterInterface(["name"]);
// generated-code-end

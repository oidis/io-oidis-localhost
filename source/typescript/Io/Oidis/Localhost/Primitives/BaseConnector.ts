/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { ResponseFactory } from "../HttpProcessor/ResponseApi/ResponseFactory.js";
import { IConnector } from "../Interfaces/IConnector.js";
import { IResponse } from "../Interfaces/IResponse.js";

export class BaseConnector extends BaseObject implements IConnector {
    protected isExternCall : boolean;
    private isInvocable : boolean;

    public static getInstance<TConnector>() : TConnector {
        const singleton : string = "__Singleton." + this.ClassName();
        if (this.hasOwnProperty(singleton)) {
            return this[singleton];
        }
        return this[singleton] = <any>(new this());
    }

    /**
     * @returns {string} Returns token from LCP by which the @Extern method has been invoked
     */
    protected static currentToken() : string {
        // this is placeholder for token from LCP by which the @Extern method has been invoked
        return "";
    }

    protected static pipe<TConnector>($callback : IResponse) : TConnector {
        const instance : any = this.getInstance();
        const wrapper : any = {};
        instance.getMethods().forEach(($method : string) : void => {
            wrapper[$method] = (...$args : any[]) : any => {
                // eslint-disable-next-line prefer-spread
                ResponseFactory.getResponse($callback).Send(instance[$method].apply(instance, $args));
            };
        });
        return wrapper;
    }

    constructor() {
        super();
        this.isInvocable = true;
        this.isExternCall = false;
    }

    public ExternInvokeEnabled($value? : boolean) : boolean {
        return this.isInvocable = Property.Boolean(this.isInvocable, $value);
    }

    public async Destroy() : Promise<void> {
        LogIt.Debug("Removing instance of " + this.getClassName() + " for token: " + this.getCurrentToken());
        const singleton : string = "__Singleton." + this.getClassName();
        if (this.hasOwnProperty(singleton)) {
            delete this[singleton];
        }
        await this.terminate();
    }

    /**
     * @returns {string} Returns token from LCP by which the @Extern method has been invoked
     */
    protected getCurrentToken() : string {
        // this is placeholder for token from LCP by which the @Extern method has been invoked
        return "";
    }

    /**
     * @returns {string} Returns LCP version by which the @Extern method has been invoked
     */
    protected getProtocolVersion() : string {
        // this is placeholder for LCP version by which the @Extern method has been invoked
        return "";
    }

    protected async terminate() : Promise<void> {
        // override this if some routine should be executed before connector will be removed from register
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IFileTransferProtocol, IRemoteFileTransferProtocol } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { EventsManager } from "../Events/EventsManager.js";
import { ResponseFactory } from "../HttpProcessor/ResponseApi/ResponseFactory.js";
import { IFileInfo, IRemoteFileInfo } from "../Interfaces/IFileInfo.js";
import { IResponse } from "../Interfaces/IResponse.js";
import { Loader } from "../Loader.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { FileSystemHandler } from "./FileSystemHandler.js";

export class FileTransferHandler extends BaseConnector {
    private readonly downloadAwait : any[];
    private readonly downloadAwaitTimeouts : any[];
    private readonly ackTimeoutMs : number;

    public static TransferFile($fileInfo : IFileInfo, $sourcePath : string, $callbacks : IFileTransferCallbacks) : void {
        EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
            $callbacks.onMessage = $callbacks.onMessage ||
                (() : any => {
                    // empty handler
                });
            $callbacks.onError = $callbacks.onError ||
                (() : any => {
                    // empty handler
                });
            $callbacks.onComplete = $callbacks.onComplete ||
                (() : any => {
                    // empty handler
                });

            let chunkTimeout : number = 60;  // timeout in seconds
            if (!ObjectValidator.IsEmptyOrNull($fileInfo.chunkTimeout)) {
                chunkTimeout = $fileInfo.chunkTimeout;
            }
            const maxChunkSize : number = ($fileInfo.maxChunkSizeKb > 0 ? $fileInfo.maxChunkSizeKb : 1024 * 1.5) * 1024;
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            if (fileSystem.Exists($sourcePath)) {
                const {Transform} = require("stream");
                const fs : any = require("fs");
                const contentSize : number = fs.statSync($sourcePath).size;
                const filePath : string = fileSystem.NormalizePath($fileInfo.path);

                const chunk : any = {
                    data : "",
                    end  : maxChunkSize,
                    id   : StringUtils.getSha1("" + filePath + new Date().getTime() + Math.random()),
                    index: 0,
                    path : filePath,
                    size : contentSize,
                    start: 0
                };

                let timerHandler;
                const tearDownTimeout : any = () => {
                    if (ObjectValidator.IsSet(timerHandler)) {
                        clearTimeout(timerHandler);
                    }
                };
                try {
                    const transfer : any = () : void => {
                        let buffer : any = null; // eslint-disable-line @typescript-eslint/no-unused-vars
                        const pipeTransform : typeof Transform = new Transform({
                            transform: ($data : any, $encoding : string, $callback : any) : void => {
                                sendChunk(Buffer.from($data).toString("base64"), $callback);
                            }
                        });
                        const sendChunk : any = ($data : string, $callback : any) : void => {
                            chunk.data = $data;
                            chunk.end = chunk.start + maxChunkSize;
                            if (chunk.end > chunk.size) {
                                chunk.end = chunk.size;
                            }

                            if (ObjectValidator.IsSet($fileInfo.metadata)) {
                                chunk.metadata = JSON.stringify($fileInfo.metadata);
                            } else {
                                chunk.metadata = undefined;
                            }

                            if (chunkTimeout > 0) {
                                timerHandler = setTimeout(() => {
                                    stream.close();
                                    $callbacks.onError(chunk.id, "Transfer of chunk failed: timeout limit exceeded.");
                                }, chunkTimeout * 1000);
                            }
                            $callbacks.sendData(chunk, ($success : boolean) : void => {
                                tearDownTimeout();
                                if ($success) {
                                    $callbacks.onMessage(FileTransferHandler.getTransferMessage(chunk));
                                    chunk.index++;
                                    chunk.start = chunk.end;
                                    buffer = null;
                                    $callback();
                                } else {
                                    stream.close();
                                    $callbacks.onError(chunk.id, "Transfer of chunk failed.");
                                }
                            });
                        };
                        const stream : any = fs.createReadStream($sourcePath, <any>{
                            highWaterMark: maxChunkSize
                        });
                        stream
                            .on("error", ($error : Error) : void => {
                                tearDownTimeout();
                                $callbacks.onError(chunk.id, $error.message);
                            })
                            .pipe(pipeTransform)
                            .on("finish", () : void => {
                                $callbacks.onComplete(chunk.id);
                            });
                    };
                    transfer();
                } catch (ex) {
                    tearDownTimeout();
                    $callbacks.onError(chunk.id, ex);
                }
            } else {
                $callbacks.onError(null, "Transfer failed. File " + $fileInfo.path + " doesn't exists.");
            }
        });
    }

    public static getTransferMessage($chunk : IFileTransferProtocol) : string {
        return "Transferred chunk #" + ($chunk.index + 1) + " (" +
            Convert.IntegerToSize($chunk.end) + "/" + Convert.IntegerToSize($chunk.size) + ")";
    }

    constructor() {
        super();
        this.downloadAwait = <any>{};
        this.downloadAwaitTimeouts = <any>{};
        this.ackTimeoutMs = 5000;
    }

    public UploadFile($data : IRemoteFileTransferProtocol, $callback : ($status : boolean) => void | IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const cwd : string = ObjectValidator.IsSet($data.cwd) ? $data.cwd : this.getDefaultPath();
        const path : string = fileSystem.NormalizePath(cwd + "/" + $data.path);

        LogIt.Info("Print file chunk " + $data.index + " to " + path);
        if (ObjectValidator.IsString($data.data)) {
            $data.data = <any>Buffer.from($data.data, "base64");
        }
        if (!(<any>$data.data instanceof Buffer)) {
            $data.data = <any>Buffer.from($data.data);
        }
        const status : boolean = fileSystem.Write(path, $data.data, $data.index !== 0);
        $data.data = null;
        response.Send(status);
    }

    public DownloadFile($fileInfo : IRemoteFileInfo, $response : IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($response);
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const cwd : string = ObjectValidator.IsSet($fileInfo.cwd) ? $fileInfo.cwd : this.getDefaultPath();
        const path : string = fileSystem.NormalizePath(cwd + "/" + $fileInfo.path);

        FileTransferHandler.TransferFile($fileInfo, path, {
            onComplete: ($chunkId : string) : void => {
                delete this.downloadAwait[$chunkId]; // eslint-disable-line @typescript-eslint/no-array-delete
                this.clearAwaitTimeout($chunkId);
                response.OnComplete();
            },
            onError   : ($chunkId : string, $message : string) : void => {
                delete this.downloadAwait[$chunkId]; // eslint-disable-line @typescript-eslint/no-array-delete
                this.clearAwaitTimeout($chunkId);
                response.OnError($message);
            },
            sendData  : ($data : IFileTransferProtocol, $callback : ($success : boolean) => void) : void => {
                response.Send($data);
                if ($data.end > $data.size) {
                    this.downloadAwait[$data.id] = () : void => {
                        delete this.downloadAwait[$data.id]; // eslint-disable-line @typescript-eslint/no-array-delete
                        $callback(true);
                    };
                    this.downloadAwaitTimeouts[$data.id] =
                        EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                            $callback(false);
                        }, false, this.ackTimeoutMs);
                } else {
                    $callback(true);
                }
            }
        });
    }

    public AcknowledgeChunk($chunkId : string) : void {
        const downloadAwait : any = this.downloadAwait[$chunkId];
        if (!ObjectValidator.IsEmptyOrNull(downloadAwait)) {
            this.clearAwaitTimeout($chunkId);
            downloadAwait(true);
        }
    }

    public getDefaultPath() : string {
        return Loader.getInstance().getProgramArgs().AppDataPath() +
            "/resource/data/" + StringUtils.Replace(this.getClassName(), ".", "/") + "/";
    }

    private clearAwaitTimeout($chunkId : string) : void {
        const downloadAwaitTimeout : any = this.downloadAwaitTimeouts[$chunkId];
        if (!ObjectValidator.IsEmptyOrNull(downloadAwaitTimeout)) {
            clearTimeout(downloadAwaitTimeout);
            delete this.downloadAwaitTimeouts[$chunkId]; // eslint-disable-line @typescript-eslint/no-array-delete
        }
    }
}

export interface IFileTransferCallbacks {
    sendData : ($data : IFileTransferProtocol, $callback : ($success : boolean) => void) => void;
    onMessage? : ($message : string) => void;
    onError? : ($chunkId : string, $message : string) => void;
    onComplete? : ($chunkId : string) => void;
}

// generated-code-start
export const IFileTransferCallbacks = globalThis.RegisterInterface(["sendData", "onMessage", "onError", "onComplete"]);
// generated-code-end

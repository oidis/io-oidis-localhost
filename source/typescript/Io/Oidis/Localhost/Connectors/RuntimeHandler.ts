/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IModelListFilter, IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { IConnectorInfo } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IConnectorInfo.js";
import { Loader } from "../Loader.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { Extern, Route } from "../Primitives/Decorators.js";

export class RuntimeHandler extends BaseConnector {

    @Extern()
    public Exit($code : number = 0) : void {
        Loader.getInstance().Exit($code);
    }

    @Extern()
    @Route("GET /application/buildTime")
    public getBuildTime() : number {
        return Loader.getInstance().getAppConfiguration().build.timestamp;
    }

    @Extern()
    @Route("GET /application/upTime")
    public getUpTime() : number {
        return Loader.getInstance().getUptime();
    }

    @Extern()
    @Route("GET /application/version")
    public getVersion() : string {
        return Loader.getInstance().getAppConfiguration().version;
    }

    @Extern()
    public async getConnectorsInfo($filter? : string, $options? : IModelListFilter) : Promise<IModelListResult<IConnectorInfo>> {
        return Loader.getInstance().getConnectorsRegister().getList($filter, $options);
    }
}

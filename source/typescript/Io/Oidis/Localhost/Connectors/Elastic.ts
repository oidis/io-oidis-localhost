/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Resources } from "@io-oidis-services/Io/Oidis/Services/DAO/Resources.js";
import { IElasticConfiguration } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";

export class Elastic extends BaseConnector {
    private client : any;
    private index : string;

    constructor() {
        super();
    }

    public Process($data : any[]) : void {
        this.process($data)
            .then(() : void => {
                LogIt.Info("Elastic processed");
            })
            .catch(($error : Error) : void => {
                LogIt.Error($error.message, $error);
            });
    }

    protected async init() : Promise<void> {
        const {Client} = require("@elastic/elasticsearch");
        const fs = require("fs");
        const config : IElasticConfiguration = <IElasticConfiguration>{
            cert    : "",
            index   : "data",
            location: "https://elastic.oidis.io",
            mapping : "resource/configs/elastic.mapping.json",
            pass    : "",
            user    : ""
        };
        Resources.Extend(config, Loader.getInstance().getAppConfiguration().elastic);

        Loader.getInstance().getAppConfiguration().elastic = config;

        let ssl : any;
        if (ObjectValidator.IsEmptyOrNull(config.cert)) {
            LogIt.Info("CA certificate missing - continuing without unauthorized rejection. To use certificate, set " +
                "elastic.cert property.");
            ssl = {
                rejectUnauthorized: false
            };
        } else {
            ssl = {
                ca: fs.readFileSync(config.cert),
                checkServerIdentity() {
                    return undefined;
                },
                rejectUnauthorized: true
            };
        }

        const options : any = {
            node: config.location,
            ssl
        };
        if (!ObjectValidator.IsEmptyOrNull(config.user)) {
            options.auth = {
                username: config.user
            };
            if (!ObjectValidator.IsEmptyOrNull(config.pass)) {
                options.auth.password = config.pass;
            }
        }
        this.client = new Client(options);

        this.index = config.index;
        if (ObjectValidator.IsEmptyOrNull(this.index) || (this.index.length < 4)) {
            throw new Error("Index property for elastic has to been specified and name length has to be greater than 3.");
        }
    }

    protected async createMapping() : Promise<void> {
        const mappingPath : string = Loader.getInstance().getAppConfiguration().elastic.mapping;
        const mappingData : string = Loader.getInstance().getFileSystemHandler().Read(
            Loader.getInstance().getProgramArgs().AppDataPath() + "/" + mappingPath).toString();
        if (!ObjectValidator.IsEmptyOrNull(mappingData)) {
            let mappings : any = {};
            // eslint-disable-next-line no-useless-catch
            try {
                mappings = JSON.parse(mappingData);
            } catch ($e) {
                throw $e;
            }

            // TODO(mkelnar) mapping generation ?
            if (await this.mappingExists()) {
                LogIt.Warning("Attempting to modify mapping for elastic data. " +
                    "It can damage already existing elastic data or data viewers be aware of what you are doing...");
                const result : IElasticResponse = await this.client.indices.putMapping({body: mappings, index: this.index});
                this.elasticErrorHandler(result);
            } else {
                LogIt.Info("Creating mapping.");
                const result : IElasticResponse = await this.client.indices.create({
                    body : {
                        mappings
                    },
                    index: this.index
                });
                this.elasticErrorHandler(result);
            }
        } else {
            LogIt.Warning("Mapping file was not found so operation skipped.");
        }
    }

    protected async deleteMapping() : Promise<void> {
        LogIt.Warning("Attempting to delete mapping for elastic data. " +
            "It can damage already existing elastic data or data viewers be aware of what you are doing...");
        try {
            const result : IElasticResponse = await this.client.indices.delete({index: this.index});
            this.elasticErrorHandler(result);
        } catch ($e) {
            if (!ObjectValidator.IsEmptyOrNull($e.meta) && !ObjectValidator.IsEmptyOrNull($e.meta.body) &&
                $e.meta.body.error.type === "index_not_found_exception") {
                LogIt.Info("Mapping is not exist so delete were not succeed.");
                LogIt.Debug("Mapping delete: " + JSON.stringify($e));
            } else {
                throw $e;
            }
        }
    }

    protected async mappingExists() : Promise<boolean> {
        const result : IElasticResponse = await this.client.indices.exists({index: this.index});
        return result.statusCode === 200 && ObjectValidator.IsBoolean(result.body) && result.body === true;
    }

    protected async process($data : any[]) : Promise<string[]> {
        if (ObjectValidator.IsEmptyOrNull(this.client)) {
            await this.init();
        }
        if (Loader.getInstance().getProgramArgs().ElasticMapping() || !await this.mappingExists()) {
            await this.createMapping();
        }

        const promises : Array<Promise<string>> = [];
        $data.forEach(($item : any) : void => {
            promises.push(this.pushData($item));
        });

        return Promise.all(promises);
    }

    protected async exists($id : string) : Promise<boolean> {
        LogIt.Debug("Checking existence of: " + $id);
        const result : IElasticResponse = await this.client.exists({index: this.index, id: $id});
        return result.statusCode === 200 && ObjectValidator.IsBoolean(result.body) && result.body === true;
    }

    protected async pushData($data : any) : Promise<string> {
        if (this.pushCondition($data)) {
            let id : string = this.getUID();
            if (!ObjectValidator.IsEmptyOrNull($data.id)) {
                id = $data.id;
                if (await this.exists(id)) {
                    LogIt.Debug("Updating item: " + id);
                    await this.client.update({
                        body : await this.preprocessData($data),
                        id,
                        index: this.index
                    });
                    return id;
                }
            }

            LogIt.Debug("Creating new item: " + id);
            await this.client.create({
                body : await this.preprocessData($data),
                id,
                index: this.index
            });
            return id;
        }
        return "";
    }

    protected async deleteItem($id : string) : Promise<void> {
        try {
            const result : IElasticResponse = await this.client.delete({index: this.index, id: $id});
            this.elasticErrorHandler(result);
        } catch ($e) {
            if (!ObjectValidator.IsEmptyOrNull($e.meta) && !ObjectValidator.IsEmptyOrNull($e.meta.body) &&
                $e.meta.body.error.type === "index_not_found_exception") {
                LogIt.Info("Item is not exist in elastic so delete were not succeed.");
                LogIt.Debug("Item delete failed: " + JSON.stringify($e));
            } else {
                throw $e;
            }
        }
    }

    protected async deleteByQuery($query : any) : Promise<void> {
        LogIt.Debug("Delete items by query");
        return this.client.deleteByQuery({
            body : $query,
            index: this.index
        });
    }

    protected async findById($id : string) : Promise<any> {
        const found : any[] = await this.find(
            {
                query: {
                    ids: {
                        values: [$id]
                    }
                }
            });
        if (!ObjectValidator.IsEmptyOrNull(found)) {
            if (found.length > 1 || found.length < 1) {
                throw new Error("Multiple items found by findById function is not expected result.");
            }
            return found[0];
        }
    }

    protected async find($dsl : any) : Promise<any[]> {
        const response : any = await this.client.search({
            body : $dsl,
            index: this.index
        });
        const retVal : any[] = [];
        if (!ObjectValidator.IsEmptyOrNull(response.body) && !ObjectValidator.IsEmptyOrNull(response.body.hits) &&
            !ObjectValidator.IsEmptyOrNull(response.body.hits.hits)) {
            response.body.hits.hits.forEach(($item : any) : void => {
                retVal.push(this.toObject($item));
            });
        }
        return retVal;
    }

    protected async preprocessData($data : any) : Promise<any> {
        if (ObjectValidator.IsEmptyOrNull($data.timestamp)) {
            $data.timestamp = await this.acquireRealTimestamp();
        }
        if (!ObjectValidator.IsEmptyOrNull($data.id)) {
            delete $data.id;
        }
        return $data;
    }

    protected toObject($eData : any) : any {
        if (!ObjectValidator.IsEmptyOrNull($eData._source)) {
            return JsonUtils.Clone($eData._source);
        }
        return {};
    }

    protected pushCondition($data : any) : boolean {
        return !ObjectValidator.IsEmptyOrNull($data);
    }

    protected async acquireRealTimestamp() : Promise<string> {
        const date : Date = new Date();
        return date.toISOString();
    }

    protected async delay($ms? : number) : Promise<void> {
        return new Promise<void>(($resolve : () => void) : void => {
            setTimeout($resolve, ObjectValidator.IsEmptyOrNull($ms) ? 2000 : $ms);
        });
    }

    private elasticErrorHandler($response : IElasticResponse) : void {
        if ($response.statusCode !== 200) {
            if (ObjectValidator.IsEmptyOrNull($response.body)) {
                throw new Error(this.getClassNameWithoutNamespace() + ": " + $response.toString());
            } else {
                if (ObjectValidator.IsEmptyOrNull($response.body.error)) {
                    throw new Error($response.body.error.root_cause[0].type + ": " + $response.body.error.root_cause[0].reason);
                } else {
                    throw new Error(this.getClassNameWithoutNamespace() + ": Unknown error");
                }
            }
        }
    }
}

export interface IElasticResponse {
    body : any;
    statusCode : number;
    warnings : any;
}

// generated-code-start
export const IElasticResponse = globalThis.RegisterInterface(["body", "statusCode", "warnings"]);
// generated-code-end

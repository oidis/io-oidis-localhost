/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IArchiveOptions, IFileSystemDownloadOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";
import { Resources } from "@io-oidis-services/Io/Oidis/Services/DAO/Resources.js";
import { SpawnSyncReturns } from "child_process";
import { IFetchResolver } from "../HttpProcessor/HttpResolver.js";
import { CallbackResponse } from "../HttpProcessor/ResponseApi/Handlers/CallbackResponse.js";
import { ResponseFactory } from "../HttpProcessor/ResponseApi/ResponseFactory.js";
import { IResponse } from "../Interfaces/IResponse.js";
import { Loader } from "../Loader.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { IntegrityCheck } from "../Primitives/Decorators.js";
import { EnvironmentHelper } from "../Utils/EnvironmentHelper.js";
import { Terminal } from "./Terminal.js";

export class FileSystemHandler extends BaseConnector {
    protected downloadedFileCheckCount : number;
    protected downloadedFileCheckPeriod : number;
    private readonly lockCheckCount : number;
    private readonly lockCheckPeriod : number;
    private responseRegister : IResponsesRegister;
    private terminal : Terminal;
    private useOldRm : boolean;

    @IntegrityCheck()
    private static async fileSystemHandler() : Promise<void> {
        if (StringUtils.IsEmpty(Loader.getInstance().getFileSystemHandler().getTempPath())) {
            throw new Error("Basic validation of FileSystemHandler failed");
        }
    }

    constructor() {
        super();
        this.responseRegister = <IResponsesRegister>{
            abort  : [],
            running: {}
        };
        this.downloadedFileCheckCount = 10;
        this.downloadedFileCheckPeriod = 10;
        this.lockCheckCount = 10;
        this.lockCheckPeriod = 10;
        this.terminal = Loader.getInstance().getTerminal();
        this.useOldRm = false;
    }

    public Exists($path : string) : boolean {
        const status : boolean = this.exists($path);
        LogIt.Info("> validate path \"" + $path + "\" existence: " + status, LogSeverity.MEDIUM);
        return status;
    }

    public IsEmpty($path : string) : boolean {
        const fs : any = require("fs");
        LogIt.Info("> validate if \"" + $path + "\" is empty", LogSeverity.LOW);
        let status : boolean = true;
        if (!ObjectValidator.IsEmptyOrNull($path)) {
            try {
                if (fs.existsSync($path)) {
                    if (fs.statSync($path).isDirectory()) {
                        status = this.Expand($path + "/*").length === 0;
                    } else {
                        status = fs.readFileSync($path).length === 0;
                    }
                }
            } catch (ex) {
                LogIt.Warning(ex.message);
            }
        }
        return status;
    }

    public IsFile($path : string) : boolean {
        return this.exists($path) && require("fs").lstatSync(this.NormalizePath($path)).isFile();
    }

    public IsDirectory($path : string) : boolean {
        return this.exists($path) && require("fs").lstatSync(this.NormalizePath($path)).isDirectory();
    }

    public IsSymbolicLink($path : string) : boolean {
        return this.exists($path) && require("fs").lstatSync(this.NormalizePath($path)).isSymbolicLink();
    }

    public IsFileLocked($path : string) : boolean {
        if (this.IsFile($path)) {
            const fs : any = require("fs");
            try {
                fs.closeSync(fs.openSync(this.NormalizePath($path), "r+"));
            } catch (ex) {
                if (ex.code === "EBUSY") {
                    return true;
                }
            }
        }
        return false;
    }

    public getTempPath() : string {
        let value : string;
        if (this.exists(Loader.getInstance().getAppConfiguration().target.tmpPath)) {
            value = this.NormalizePath(Loader.getInstance().getAppConfiguration().target.tmpPath);
        } else {
            if (EnvironmentHelper.IsWindows()) {
                const spawnSync : any = require("child_process").spawnSync;
                let retVal : SpawnSyncReturns<Buffer> = spawnSync(
                    "reg query \"HKCU\\Environment\" /v \"TEMP\"", {
                        shell      : true,
                        windowsHide: true
                    });
                if (retVal.status === 0) {
                    let regValue : string = retVal.stdout.toString();
                    regValue = StringUtils.Substring(regValue, StringUtils.IndexOf(regValue, "REG_EXPAND_SZ") + 13).trim();
                    if (!ObjectValidator.IsEmptyOrNull(regValue)) {
                        retVal = spawnSync(
                            "echo " + regValue, {
                                shell      : true,
                                windowsHide: true
                            });
                        if (retVal.status === 0) {
                            regValue = retVal.stdout.toString().trim();
                            if (!ObjectValidator.IsEmptyOrNull(regValue)) {
                                if (require("fs").existsSync(regValue)) {
                                    value = this.NormalizePath(regValue);
                                    this.getTempPath = () : string => {
                                        return value;
                                    };
                                }
                            }
                        }
                    }
                } else {
                    LogIt.Warning("> " + retVal.stdout + retVal.stderr);
                }
            }
            if (ObjectValidator.IsEmptyOrNull(value)) {
                const os : any = require("os");
                value = os.tmpdir();
            }
        }
        return value;
    }

    public getProjectBasePath() : string {
        return Loader.getInstance().getProgramArgs().ProjectBase();
    }

    public getAppDataPath() : string {
        return Loader.getInstance().getProgramArgs().AppDataPath();
    }

    public getLocalAppDataPath() : string {
        const spawnSync : any = require("child_process").spawnSync;
        let value : string = this.getTempPath();
        const retVal : SpawnSyncReturns<Buffer> = spawnSync(
            EnvironmentHelper.IsWindows() ? "cmd /c echo %USERPROFILE%" : "echo \"$HOME\"", {
                shell      : true,
                windowsHide: true
            });
        if (retVal.status === 0) {
            const rawOutput : string = StringUtils.Remove(retVal.stdout.toString(), "\r\n", "\n", "\\\"").trim();
            if (!ObjectValidator.IsEmptyOrNull(rawOutput)) {
                const path : string = this.NormalizePath(rawOutput);
                if (this.exists(path) && require("path").isAbsolute(path)) {
                    value = path;
                    if (EnvironmentHelper.IsWindows()) {
                        value += "/AppData/Local";
                    }
                } else {
                    LogIt.Warning("System local app path not exists or is not absolute. => Using temporary path.");
                }
            } else {
                LogIt.Warning("System local app path is empty. => Using temporary path.");
            }
        } else {
            LogIt.Warning("> " + retVal.stdout + retVal.stderr + " => Using temporary path.");
        }
        return value;
    }

    public Expand($pattern : string | string[]) : string[] {
        LogIt.Info("> search for pattern \"" + $pattern + "\"", LogSeverity.MEDIUM);
        if (ObjectValidator.IsString(<string>$pattern)) {
            $pattern = [<string>$pattern];
        }
        const glob : any = require("glob");
        let output : string[] = [];
        const input : string[] = [];
        const ignore : string[] = [];
        (<string[]>$pattern).forEach(($pattern : string) : void => {
            // eslint-disable-next-line @typescript-eslint/no-unused-expressions
            StringUtils.StartsWith($pattern, "!") ? ignore.push(StringUtils.Substring($pattern, 1)) : input.push($pattern);
        });
        let cwd : string = Loader.getInstance().getProgramArgs().TargetBase();
        if (cwd !== process.cwd()) {
            cwd = this.NormalizePath(process.cwd());
        }
        input.forEach(($pattern : string) : void => {
            $pattern = StringUtils.Replace($pattern, "\\", "/");
            try {
                output = output.concat(glob.globSync($pattern, {
                    cwd,
                    dot: true,
                    ignore
                }));
            } catch (ex) {
                LogIt.Warning("> " + ex.message);
            }
        });
        return output;
    }

    public CreateDirectory($path : string) : boolean {
        LogIt.Info("> create directory at \"" + $path + "\"", LogSeverity.MEDIUM);
        let status : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull($path)) {
            try {
                this.pmkdir($path);
                status = true;
            } catch (ex) {
                LogIt.Warning(ex.message);
            }
        }
        return status;
    }

    public Rename($oldPath : string, $newPath : string) : boolean {
        const fs : any = require("fs");
        LogIt.Info("> rename from \"" + $oldPath + "\" to \"" + $newPath + "\"", LogSeverity.MEDIUM);
        let status : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull($oldPath) && !ObjectValidator.IsEmptyOrNull($newPath)) {
            try {
                if ($oldPath === $newPath) {
                    status = true;
                } else if (this.exists($oldPath)) {
                    fs.renameSync($oldPath, $newPath);
                    status = true;
                }
            } catch (ex) {
                LogIt.Warning(ex.message);
            }
        }
        return status;
    }

    public Read($path : string) : string | Buffer {
        const fs : any = require("fs");
        let data : string | Buffer = "";

        if (!ObjectValidator.IsEmptyOrNull($path)) {
            $path = this.NormalizePath($path);
            if (!StringUtils.StartsWith($path, "/snapshot/") && (
                (EnvironmentHelper.IsWindows() && !StringUtils.Contains($path, ":/")) ||
                (!EnvironmentHelper.IsWindows() && ($path[0] !== "/")))) {
                let cwd : string = Loader.getInstance().getProgramArgs().TargetBase();
                if (cwd !== process.cwd()) {
                    cwd = this.NormalizePath(process.cwd());
                }
                $path = cwd + "/" + $path;
            }
            $path = this.NormalizePath($path);

            LogIt.Info("> read content from \"" + $path + "\"", LogSeverity.MEDIUM);
            try {
                if (this.IsFile($path)) {
                    data = fs.readFileSync($path);
                }
            } catch (ex) {
                LogIt.Warning(ex.message);
            }
        }
        return data;
    }

    public Write($path : string, $data : any, $append : boolean = false) : boolean {
        let status : boolean = false;

        if (!ObjectValidator.IsEmptyOrNull($path)) {
            $path = this.NormalizePath($path);

            LogIt.Info("> write content to \"" + $path + "\"", LogSeverity.MEDIUM);
            try {
                this.pmkdir(StringUtils.Substring($path, 0, StringUtils.IndexOf($path, "/", false)));
                if ($append) {
                    require("fs").appendFileSync($path, $data);
                } else {
                    require("fs").writeFileSync($path, $data);
                }
                status = true;
            } catch (ex) {
                LogIt.Warning(ex.message);
            }
        }
        return status;
    }

    public Copy($sourcePath : string, $destinationPath : string, $callback? : (($status : boolean) => void) | IResponse) : void {
        const fs : any = require("fs");
        const path : any = require("path");
        const response : IResponse = ResponseFactory.getResponse($callback);
        const onComplete : any = ($status : boolean) : void => {
            response.Send($status);
        };
        try {
            if (this.exists($sourcePath) && !ObjectValidator.IsEmptyOrNull($destinationPath)) {
                const fileStats : any = fs.lstatSync($sourcePath);
                if (fileStats.isFile() || fileStats.isSymbolicLink()) {
                    LogIt.Info("> copy file from \"" + $sourcePath + "\" to \"" + $destinationPath + "\"", LogSeverity.MEDIUM);
                    const permissions : number = fs.lstatSync($sourcePath).mode & parseInt("777", 8);
                    this.pmkdir(path.dirname($destinationPath));
                    if (this.IsDirectory($destinationPath)) {
                        $destinationPath = $destinationPath + "/" + path.basename($sourcePath);
                    }
                    const write : any = fs.createWriteStream($destinationPath, {mode: permissions});
                    write.on("close", () : void => {
                        onComplete(true);
                    });
                    write.on("error", ($error : Error) : void => {
                        response.OnError($error);
                    });
                    fs.createReadStream($sourcePath).pipe(write);
                } else {
                    let cmd : string;
                    let args : string[];
                    $sourcePath = this.NormalizePath($sourcePath, true);
                    $destinationPath = this.NormalizePath($destinationPath, true);
                    if (EnvironmentHelper.IsWindows()) {
                        cmd = "Robocopy";
                        args = ["\"" + $sourcePath + "\"", "\"" + $destinationPath + "\"", "/E", "/MT", "/NP", "/NFL"];
                    } else {
                        cmd = "cp";
                        args = ["-Rp", "\"" + $sourcePath + "\"/*", "\"" + $destinationPath + "\""];
                        this.pmkdir($destinationPath);
                    }
                    this.terminal.Spawn(cmd, args, "", ($exitCode : number) : void => {
                        if (EnvironmentHelper.IsWindows()) {
                            onComplete($exitCode === 1 || $exitCode === 3);
                        } else {
                            onComplete($exitCode === 0);
                        }
                    });
                }
            } else {
                LogIt.Info("> copy from \"" + $sourcePath + "\" to \"" + $destinationPath +
                    "\" skipped: source does not exist or destination is empty");
                onComplete(false);
            }
        } catch (ex) {
            response.OnError(ex);
        }
    }

    public async CopyAsync($sourcePath : string, $destinationPath : string) : Promise<void> {
        return new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) : void => {
            this.Copy($sourcePath, $destinationPath, ($status : boolean) : void => {
                if ($status) {
                    $resolve();
                } else {
                    $reject(new Error("copy from \"" + $sourcePath + "\" to \"" + $destinationPath + "\" failed."));
                }
            });
        });
    }

    public Download($urlOrOptions : string | IFileSystemDownloadOptions,
                    $callback? : (($headers : string, $bodyOrPath : string) => void) | IResponse,
                    $onHttpError? : ($httpResponse : any, $callback : IResponse) => boolean) : void {
        const fs : any = require("fs");
        const url : any = require("url");
        const options : any = {
            body               : "",
            headers            : {},
            maxReconnection    : 5,
            method             : HttpMethodType.GET,
            streamOutput       : false,
            streamReturnsBuffer: false,
            strictSSL          : true,
            url                : "",
            verbose            : true
        };
        if (ObjectValidator.IsString($urlOrOptions)) {
            options.url = $urlOrOptions;
        } else if (ObjectValidator.IsObject($urlOrOptions)) {
            let parameter : string;
            for (parameter in <any>$urlOrOptions) {
                if ($urlOrOptions.hasOwnProperty(parameter)) {
                    options[parameter] = $urlOrOptions[parameter];
                }
            }
        }
        if (ObjectValidator.IsString(options.url)) {
            options.url = url.parse(options.url);
            options.url.method = options.method;
            options.url.headers = options.headers;
        }
        if (options.method === HttpMethodType.GET) {
            options.body = "";
        }
        if (!ObjectValidator.IsEmptyOrNull(options.proxy) && options.proxy === false) {
            if (ObjectValidator.IsEmptyOrNull(options.proxyConfig)) {
                options.proxyConfig = {};
            }
            options.proxyConfig.noProxy = "*";
        }
        const response : IResponse = ResponseFactory.getResponse($callback);

        let httpStatusCodeHandler : any = ($httpResponse : any, $callback : IResponse) : boolean => {
            $callback.OnError("Bad response status code: " + $httpResponse.statusCode + " (" +
                require("http").STATUS_CODES[$httpResponse.statusCode] + ")");
            return true;
        };
        if (!ObjectValidator.IsEmptyOrNull($onHttpError)) {
            httpStatusCodeHandler = $onHttpError;
        }
        const utils : DownloadUtils = new DownloadUtils(this, options);
        if (options.verbose) {
            LogIt.Info("> download (" + options.method + ") from: " + url.format(options.url));
            const headers : string = JSON.stringify(options.url.headers);
            if (headers !== "{}") {
                LogIt.Info("> with headers: " + headers);
            }
            if (options.body !== "") {
                LogIt.Info("> with body: " + options.body);
            }
        }

        if (options.url.protocol === "http:" || options.url.protocol === "https:") {
            const makeRequest : any = ($client : any, $options : any) : void => {
                const httpRequest : any = $client.request($options.url, ($httpResponse : any) : void => {
                    if ($httpResponse.statusCode === 200 || $httpResponse.statusCode === 201) {
                        const meta : any = utils.ParseHeaders($httpResponse.headers);
                        const length : number = meta.length;
                        const isChunked : boolean = meta.isChunked || $options.method === HttpMethodType.HEAD;
                        let currentLength : number = 0;
                        try {
                            let fileName : string = "";
                            let streamBuffers : any[] = [];
                            let fsStream : any = null;
                            let completed : boolean = false;
                            const onComplete : any = () : void => {
                                if (!completed) {
                                    completed = true;
                                    if ($options.streamOutput) {
                                        const data : any = Buffer.concat(streamBuffers);
                                        response.OnComplete($httpResponse.headers,
                                            $options.streamReturnsBuffer ? data : data.toString());
                                        // eslint-disable-next-line @typescript-eslint/no-array-delete
                                        delete this.responseRegister.running[response.getId()];
                                    } else {
                                        this.downloadFileCheck(fileName, isChunked ? currentLength : length,
                                            ($status : boolean) : void => {
                                                if ($status) {
                                                    this.filesUnlockCheck([fileName], ($status : boolean) : void => {
                                                        if ($status) {
                                                            response.OnComplete($httpResponse.headers, fileName);
                                                            // eslint-disable-next-line @typescript-eslint/no-array-delete
                                                            delete this.responseRegister.running[response.getId()];
                                                        } else {
                                                            response.OnError("Downloaded file lock check failed.");
                                                        }
                                                    }, $options.verbose);
                                                } else {
                                                    response.OnError("Downloaded file size check failed.");
                                                }
                                            }, $options.verbose);
                                    }
                                }
                            };

                            if (!$options.streamOutput) {
                                fileName = meta.fileName;
                                fileName = utils.PrepareTmp(fileName);
                                fsStream = fs.createWriteStream(fileName);
                                fsStream.on("end", () : void => {
                                    onComplete();
                                });
                                $httpResponse.pipe(fsStream);
                            }

                            let counter : number = 0;
                            $httpResponse.on("data", ($chunk : string) : void => {
                                currentLength += $chunk.length;
                                if ($options.streamOutput) {
                                    streamBuffers.push($chunk);
                                }
                                if ($options.verbose) {
                                    if (counter < 100) {
                                        Echo.Print(".");
                                    } else {
                                        if (!isNaN(length)) {
                                            Echo.Println(" (" + Math.floor(100 / length * currentLength) + "%)");
                                        } else {
                                            Echo.Println(".");
                                        }
                                        counter = 0;
                                    }
                                    counter++;
                                }
                                response.OnChange({
                                    currentValue: currentLength,
                                    rangeEnd    : length,
                                    rangeStart  : 0
                                });
                            });
                            let connectionClosed : boolean = false;
                            $httpResponse.on("close", () : void => {
                                if (currentLength !== length && !isChunked) {
                                    connectionClosed = true;
                                    if (!$options.streamOutput) {
                                        this.Delete(fileName);
                                    } else {
                                        streamBuffers = [];
                                    }
                                    const abortIndex : number =
                                        this.responseRegister.abort.indexOf(response.getId() + "");
                                    if (abortIndex !== -1) {
                                        this.responseRegister.abort.splice(abortIndex, 1);
                                        if ($options.verbose) {
                                            LogIt.Info("Connection has been aborted.");
                                        }
                                    } else {
                                        if (this.responseRegister.running.hasOwnProperty(response.getId()) &&
                                            !ObjectValidator.IsEmptyOrNull(this.responseRegister.running[response.getId()])) {
                                            if (this.responseRegister.running[response.getId()]
                                                .reconnectCounter < $options.maxReconnection) {
                                                if ($options.verbose) {
                                                    LogIt.Warning("Reconnecting ...");
                                                }
                                                httpRequest.end();
                                                this.Download($urlOrOptions, response, $onHttpError);
                                            } else {
                                                response.OnError("Connection has been lost.");
                                            }
                                            this.responseRegister.running[response.getId()].reconnectCounter++;
                                        } else {
                                            response.OnError("Connection has been lost.");
                                        }
                                    }
                                } else {
                                    onComplete();
                                }
                            });

                            $httpResponse.on("end", () : void => {
                                if (!connectionClosed) {
                                    if ($options.verbose) {
                                        Echo.Println(" (100%)");
                                    }
                                    if (!$options.streamOutput) {
                                        onComplete();
                                    }
                                }
                            });
                        } catch (ex) {
                            response.OnError(ex);
                        }
                    } else if ($httpResponse.statusCode === 301 || $httpResponse.statusCode === 302) {
                        $options.url = $httpResponse.headers.location;
                        this.Download($options, response, $onHttpError);
                    } else if (httpStatusCodeHandler($httpResponse, response)) {
                        delete this.responseRegister.running[response.getId()]; // eslint-disable-line @typescript-eslint/no-array-delete
                    }
                });

                httpRequest.on("error", ($ex : Error) : void => {
                    if (this.responseRegister.running.hasOwnProperty(response.getId()) &&
                        !ObjectValidator.IsEmptyOrNull(this.responseRegister.running[response.getId()])) {
                        if (this.responseRegister.running[response.getId()].reconnectCounter < $options.maxReconnection) {
                            if ($options.verbose) {
                                Echo.Println($ex.message);
                            }
                            this.Download($urlOrOptions, response, $onHttpError);
                        } else {
                            if ($options.verbose) {
                                Echo.Println("");
                            }
                            response.OnError($ex);
                        }
                        this.responseRegister.running[response.getId()].reconnectCounter++;
                    } else {
                        response.OnError($ex);
                    }
                });

                if ($options.body !== "") {
                    httpRequest.write($options.body);
                }

                httpRequest.end();

                if (!this.responseRegister.running.hasOwnProperty(response.getId())) {
                    response.OnStart();
                    this.responseRegister.running[response.getId()] = {
                        httpRequest,
                        reconnectCounter: 0
                    };
                } else {
                    this.responseRegister.running[response.getId()].httpRequest = httpRequest;
                }
            };
            Loader.getInstance().getHttpResolver().FetchHelper(options)
                .then(($environment : IFetchResolver) : void => {
                    makeRequest($environment.client, $environment.options);
                })
                .catch(($error : Error) : void => {
                    response.OnError($error);
                });
        } else if (options.url.protocol === "file:") {
            try {
                let urlTemp : string = options.url.href;
                if (ObjectValidator.IsString($urlOrOptions)) {
                    urlTemp = <string>$urlOrOptions;
                }
                if (options.streamOutput) {
                    const data : Buffer = <Buffer>this.Read(utils.FormatUrl(urlTemp));
                    response.OnComplete({}, data);
                } else {
                    const tmpPath : string = utils.PrepareTmp();
                    this.Copy(utils.FormatUrl(urlTemp), tmpPath, () : void => {
                        response.OnComplete({}, tmpPath);
                    });
                }
            } catch (ex) {
                response.OnError(ex);
            }
        } else {
            response.OnError("Selected protocol \"" + options.url.protocol + "\" is not supported." +
                " Please use http://, https:// or file:// protocol instead.");
        }
    }

    public async DownloadAsync($urlOrOptions : string | IFileSystemDownloadOptions) : Promise<IDownloadResult> {
        return new Promise(($resolve : ($result : IDownloadResult) => void, $reject : ($error : Error) => void) : void => {
            const callback : CallbackResponse = new CallbackResponse(($headers : string, $bodyOrPath : string) : void => {
                    $resolve({bodyOrPath: $bodyOrPath, headers: $headers});
                }
            );
            callback.OnError = ($message : string | Error, ...$args : any[]) : void => {
                if (ObjectValidator.IsString($message)) {
                    $reject(new Error(<string>$message));
                } else {
                    $reject(<Error>$message);
                }
            };
            this.Download($urlOrOptions, callback, ($httpResponse) : boolean => {
                $reject(new Error($httpResponse.statusMessage + " (" + $httpResponse.statusCode + ")"));
                return true;
            });
        });
    }

    public AbortDownload($id : number) : boolean {
        const id : string = $id + "";
        let status : boolean = false;
        if (this.responseRegister.running.hasOwnProperty(id)) {
            try {
                this.responseRegister.abort.push(id);
                this.responseRegister.running[id].httpRequest.abort();
                delete this.responseRegister.running[id]; // eslint-disable-line @typescript-eslint/no-array-delete
                status = true;
                LogIt.Info("Download with connection id \"" + $id + "\" has been abort.");
            } catch (ex) {
                LogIt.Warning(ex.message);
            }
        }
        return status;
    }

    public Delete($pathOrOptions : string | IFSDeleteOptions, $callback? : (($status : boolean) => void) | IResponse) : boolean {
        const options : IFSDeleteOptions = {
            ignoreLocked : false,
            path         : "",
            statusHandler: ($message : string, $type : LogLevel | string, $severity? : LogSeverity) : void => {
                if ($type === "print") {
                    Echo.Print($message);
                } else if ($type === "println") {
                    Echo.Println($message);
                } else if ($type === LogLevel.WARNING) {
                    LogIt.Warning($message);
                } else if ($type === LogLevel.INFO) {
                    LogIt.Info($message, $severity);
                } else if ($type === LogLevel.ERROR) {
                    LogIt.Error($message);
                } else {
                    LogIt.Debug($message);
                }
            },
            useRecursive : this.useOldRm
        };
        if (ObjectValidator.IsString($pathOrOptions)) {
            options.path = <string>$pathOrOptions;
        } else {
            JsonUtils.Extend(options, $pathOrOptions);
        }
        const fs : any = require("fs");
        let status : boolean = false;
        const response : IResponse = ResponseFactory.getResponse($callback);
        response.OnStart();
        if (!ObjectValidator.IsEmptyOrNull($pathOrOptions)) {
            try {
                if (options.ignoreLocked) {
                    options.statusHandler(
                        "> running insecurely with ignoreLocked option. Some files or dirs may not be deleted.",
                        LogLevel.WARNING);
                }
                let counter : number = 0;
                const failedDelete : string[] = [];
                const deleteRecursive : any = ($path : string) : void => {
                    if (this.exists($path)) {
                        try {
                            if (fs.lstatSync($path).isDirectory()) {
                                fs.readdirSync($path).forEach(($file : string) : void => {
                                    deleteRecursive($path + "/" + $file);
                                });
                                fs.rmdirSync($path);
                            } else {
                                fs.unlinkSync($path);
                            }
                        } catch (ex) {
                            try {
                                options.statusHandler(
                                    "Trying to remove file as directory because of:" + ex.message,
                                    LogLevel.WARNING);
                                fs.rmdirSync($path);
                            } catch (ex) {
                                if (options.ignoreLocked) {
                                    options.statusHandler(ex.message, LogLevel.INFO, LogSeverity.MEDIUM);
                                    failedDelete.push($path);
                                } else {
                                    throw ex;
                                }
                            }
                        }

                        response.OnChange({
                            currentValue: counter,
                            rangeEnd    : 100,
                            rangeStart  : 0
                        });
                        if (counter < 100) {
                            options.statusHandler(".", "print");
                        } else {
                            options.statusHandler(".", "println");
                            counter = 0;
                        }
                        counter++;
                    }
                };
                const oldRm : any = ($path : string) : void => {
                    if (this.exists($path)) {
                        if (fs.lstatSync($path).isDirectory()) {
                            deleteRecursive($path);
                            options.statusHandler("", "println");
                        } else {
                            fs.unlinkSync($path);
                        }
                    }
                };
                options.path = this.NormalizePath(options.path);
                options.statusHandler("> delete path \"" + options.path + "\"", LogLevel.INFO, LogSeverity.MEDIUM);
                if (!options.useRecursive) {
                    // progress like setInterval() in execute can not be there because it will need to be async but delete is used
                    // as sync on some places so no prints here but it is ok for low files count dirs but i.e. nodejs dir in deps
                    // rm takes some tens second
                    try {
                        fs.rmSync(options.path, {force: true, recursive: true, maxRetries: 20, retryDelay: 500});
                    } catch (ex) {
                        if (options.ignoreLocked) {
                            options.statusHandler("> rmSync failed, falling back to recursive delete",
                                LogLevel.INFO, LogSeverity.LOW);
                            oldRm();
                        } else {
                            throw ex;
                        }
                    }
                } else {
                    oldRm();
                }
                status = true;
                if (!ObjectValidator.IsEmptyOrNull(failedDelete)) {
                    options.statusHandler("> not deleted: " + failedDelete.join(","), LogLevel.WARNING);
                }
            } catch (ex) {
                options.statusHandler(ex.message, LogLevel.WARNING);
            }
        }
        response.OnComplete(status);
        return status;
    }

    public async DeleteAsync($pathOrOptions : string | IFSDeleteOptions) : Promise<void> {
        const options : IFSDeleteOptions = {
            ignoreLocked : false,
            path         : "",
            statusHandler: ($message : string, $type : LogLevel | string, $severity? : LogSeverity) : void => {
                if ($type === "print") {
                    Echo.Print($message);
                } else if ($type === "println") {
                    Echo.Println($message);
                } else if ($type === LogLevel.WARNING) {
                    LogIt.Warning($message);
                } else if ($type === LogLevel.INFO) {
                    LogIt.Info($message, $severity);
                } else if ($type === LogLevel.ERROR) {
                    LogIt.Error($message);
                } else {
                    LogIt.Debug($message);
                }
            }
        };
        if (ObjectValidator.IsString($pathOrOptions)) {
            options.path = <string>$pathOrOptions;
        } else {
            JsonUtils.Extend(options, $pathOrOptions);
        }
        const fs : any = require("fs/promises");

        if (ObjectValidator.IsEmptyOrNull($pathOrOptions)) {
            return;
        }
        try {
            if (options.ignoreLocked) {
                options.statusHandler(
                    "> running insecurely with ignoreLocked option. Some files or dirs may not be deleted.",
                    LogLevel.WARNING);
            }
            options.path = this.NormalizePath(options.path);
            options.statusHandler("> delete path \"" + options.path + "\"", LogLevel.INFO, LogSeverity.MEDIUM);

            await fs.rm(options.path, {force: true, recursive: true, maxRetries: 20, retryDelay: 500});
        } catch (ex) {
            throw new Error("File or folder at path \"" + options.path + "\" can not be deleted.");
        }
    }

    public Pack($path : string | string[], $options? : IArchiveOptions, $callback? : (($tmpPath : string) => void) | IResponse) : void {
        const path : any = require("path");
        const fs : any = require("fs");
        const archiver : any = require("archiver");

        let cwd : string = "";
        if (ObjectValidator.IsString($path)) {
            if (this.IsFile(<string>$path)) {
                $path = path.dirname(<string>$path);
            }
            if (!StringUtils.Contains(<string>$path, "*")) {
                $path += "/**/*";
            }
            cwd = StringUtils.Substring(<string>$path, 0, StringUtils.IndexOf(<string>$path, "/*", false));
        }
        const paths : string[] = this.Expand($path);
        let notFound : boolean = false;

        const response : IResponse = ResponseFactory.getResponse($callback === undefined ? <any>$options : $callback);
        if (paths.length > 0) {
            paths.forEach(($element : string, $index : number) : void => {
                if (!this.exists($element)) {
                    response.OnError("Unable to locate file \"" + $element + "\" for archive creation.");
                    notFound = true;
                } else {
                    if (StringUtils.Contains($element, " ")) {
                        $element = "\"" + $element + "\"";
                    }
                    paths[$index] = this.NormalizePath($element, true);
                }
            });
        } else {
            notFound = true;
            response.OnError("Unable to create archive from empty source \"" + $path + "\"");
        }

        if (!notFound) {
            const options : IArchiveOptions = {
                autoStrip: false,
                cwd,
                output   : "",
                override : true,
                type     : EnvironmentHelper.IsWindows() ? "zip" : "tar.bz2"
            };
            if (ObjectValidator.IsObject($options)) {
                Resources.Extend(options, $options);
            }
            if (ObjectValidator.IsEmptyOrNull(options.output)) {
                let tmpOutput : string;
                if (ObjectValidator.IsString($path)) {
                    tmpOutput = path.basename(this.NormalizePath(StringUtils.Remove(<string>$path, "*")));
                } else {
                    tmpOutput = "archive";
                }
                options.output = this.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() + "/" +
                    tmpOutput + "." + options.type;
            } else if (StringUtils.Contains(options.output, ".tar")) {
                options.type = StringUtils.Substring(options.output, StringUtils.IndexOf(options.output, ".tar"));
            }
            if (StringUtils.StartsWith(options.type, ".")) {
                options.type = StringUtils.Substring(options.type, 1);
            }
            if (["zip", "tar", "tar.gz", "tar.bz2"].indexOf(options.type) === -1) {
                response.OnError("Failed to create archive: required format \"" + options.type + "\" does not match " +
                    "any of supported formats (zip, tar, tar.gz and tar.bz2)");
            } else {
                options.output = this.NormalizePath(options.output);
                if (StringUtils.Contains(options.output, ".")) {
                    options.output = StringUtils.Substring(options.output, 0, StringUtils.IndexOf(options.output, "."));
                }
                options.output += "." + options.type;
                options.cwd = this.NormalizePath(options.cwd);
                LogIt.Info("> pack \"" + options.cwd + "\" to \"" + options.output + "\"");

                options.output = StringUtils.Remove(options.output, ".bz2");
                if (StringUtils.OccurrenceCount(options.cwd, "/") > 1) {
                    options.cwd = path.dirname(options.cwd);
                }
                try {
                    if (!options.override && this.Exists(options.output)) {
                        response.OnError("Failed to create archive: destination file already exists");
                    } else {
                        this.Delete(options.output);
                        this.pmkdir(path.dirname(options.output));
                        const output : any = fs.createWriteStream(options.output);
                        const archive : any = archiver(StringUtils.Contains(options.type, "tar") ? "tar" : options.type, {
                            gzip: StringUtils.Contains(options.type, ".gz"),
                            zlib: {level: 9}
                        });
                        output.on("close", () : void => {
                            if (StringUtils.Contains(options.type, ".bz2")) {
                                LogIt.Info("> compression to bzip2 format");
                                let compressjs : any = null;
                                try {
                                    compressjs = require("compressjs");
                                } catch (ex) {
                                    // handle optional module
                                }
                                if (!ObjectValidator.IsEmptyOrNull(compressjs)) {
                                    this.Write(options.output + ".bz2", compressjs.Bzip2.compressFile(this.Read(options.output)));
                                    this.Delete(options.output);
                                    response.Send(options.output + ".bz2");
                                } else {
                                    LogIt.Error("Missing optional dependency \"compressjs\" for ability to support .bz2 format. " +
                                        "Please specify required include in project configuration.");
                                    response.OnError("Failed to compress in bz2 format due to missing module.");
                                }
                            } else {
                                response.Send(options.output);
                            }
                        });
                        archive.on("warning", ($error : any) : void => {
                            if ($error.code === "ENOENT") {
                                LogIt.Warning($error);
                            } else {
                                response.OnError($error);
                            }
                        });
                        archive.on("error", ($error : Error) : void => {
                            response.OnError($error);
                        });

                        let rootFolder : string = "";
                        if (!options.autoStrip) {
                            rootFolder = path.basename(options.output);
                            rootFolder = StringUtils.Substring(rootFolder, 0, StringUtils.IndexOf(rootFolder, "."));
                            rootFolder += "/";
                        }
                        paths.forEach(($file : string) : void => {
                            $file = StringUtils.Remove($file, "\"");
                            archive.file($file, {
                                name: rootFolder + StringUtils.Remove(this.NormalizePath($file), options.cwd + "/")
                            });
                        });

                        archive.pipe(output);
                        archive.finalize();
                    }
                } catch (ex) {
                    response.OnError(ex);
                }
            }
        }
    }

    public async PackAsync($path : string | string[], $options? : IArchiveOptions) : Promise<string> {
        return new Promise(($resolve : ($tmpPath : string) => void, $reject : ($error : Error) => void) : void => {
            this.Pack($path, $options, ($tmpPath : string) : void => {
                if (this.exists($tmpPath)) {
                    $resolve($tmpPath);
                } else {
                    $reject(new Error("Packaging failed."));
                }
            });
        });
    }

    public Unpack($path : string, $options? : IArchiveOptions, $callback? : (($tmpPath : string) => void) | IResponse) : void {
        const path : any = require("path");
        const fs : any = require("fs");
        const decompress : any = require("decompress");

        let pathExtension : string = path.extname($path);
        const pathMatch : RegExpMatchArray = $path.match(/\.tar\.[a-zA-Z0-9]+$/g);
        if (!ObjectValidator.IsEmptyOrNull(pathMatch) && !ObjectValidator.IsEmptyOrNull(pathMatch[0])) {
            pathExtension = pathMatch[0];
        }

        const response : IResponse = ResponseFactory.getResponse(!ObjectValidator.IsSet($callback) ? <any>$options : $callback);
        const options : IArchiveOptions = {
            autoStrip: true,
            output   : "",
            override : true
        };
        if (ObjectValidator.IsObject($options)) {
            Resources.Extend(options, $options);
        }

        if (options.output === "") {
            options.output = this.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() + "/" +
                path.basename($path).replace(pathExtension, "");
        }
        options.output = this.NormalizePath(options.output);
        $path = this.NormalizePath($path);

        LogIt.Info("> unpack \"" + $path + "\" to \"" + options.output + "\"");

        const files : string[] = [];
        const onComplete : any = () : void => {
            this.filesUnlockCheck(files, ($status : boolean) : void => {
                if ($status) {
                    response.OnComplete(options.output);
                } else {
                    response.OnError("Unable to unlock extracted files.");
                }
            });
        };
        if (!options.override && fs.existsSync(options.output)) {
            onComplete();
        } else {
            this.Delete(options.output);
            try {
                decompress($path, options.output, {
                    map    : ($file : any) : any => {
                        if ($file.type === "symlink") {
                            if (EnvironmentHelper.IsWindows() && !fs.existsSync($file.linkname)) {
                                $file.linkname = this.NormalizePath(options.output + "/" + $file.linkname);
                            }
                        } else if ($file.type === "file") {
                            files.push(options.output + "/" + $file.path);
                        }
                        return $file;
                    },
                    plugins: [
                        require("decompress-unzip")(),
                        require("decompress-tar")(),
                        require("decompress-tarbz2")(),
                        require("decompress-targz")()
                    ],
                    strip  : options.autoStrip ? 1 : 0
                }).then(() : void => {
                    onComplete();
                }).catch(($error : any) : void => {
                    if ($error.code === "EPERM" || $error.code === "ENOENT") {
                        LogIt.Warning($error.stack);
                        onComplete();
                    } else {
                        response.OnError($error);
                    }
                });
            } catch (ex) {
                response.OnError(ex);
            }
        }
    }

    public async UnpackAsync($path : string, $options? : IArchiveOptions) : Promise<string> {
        return new Promise<string>(($resolve : ($tmpPath : string) => void, $reject : ($error : Error) => void) : void => {
            this.Unpack($path, $options, ($tmpPath : string) : void => {
                if (this.exists($tmpPath)) {
                    $resolve($tmpPath);
                } else {
                    $reject(new Error("Package extraction failed."));
                }
            });
        });
    }

    public NormalizePath($source : string, $osSeparator : boolean = false) : string {
        const path : any = require("path");
        return path.normalize($source).replace(/[\\/]+/g, $osSeparator ? path.sep : "/");
    }

    protected downloadFileCheck($path : string, $demandSize : number, $callback : ($status : boolean) => void,
                                $verbose : boolean = true) : void {
        if ($verbose) {
            LogIt.Info("> check downloaded file existence", LogSeverity.LOW);
        }
        const fs : any = require("fs");
        let iterations : number = 0;
        const check : any = () : void => {
            let success : boolean = false;
            let size : number = 0;
            try {
                const stats : any = fs.lstatSync($path);
                size = stats.size;
                success = size === $demandSize;
            } catch {
                // dummy
                size = -1;
            }
            if ($verbose) {
                LogIt.Debug("[" + (iterations + 1) + "/" + this.downloadedFileCheckCount +
                    "] Checking size of downloaded file {0}: [" + size + "/" + $demandSize + "] = " + success, $path);
            }
            if (success) {
                $callback(true);
            } else if (iterations++ < this.downloadedFileCheckCount) {
                setTimeout(check, this.downloadedFileCheckPeriod); // eslint-disable-line @typescript-eslint/no-implied-eval
            } else {
                $callback(false);
            }
        };
        check();
    }

    private filesUnlockCheck($files : string[], $callback : ($status : boolean) => void, $verbose : boolean = true) : void {
        if ($verbose) {
            LogIt.Info("> check " + $files.length + " files lock", LogSeverity.LOW);
        }
        let iterations : number = 0;
        const check : any = () : void => {
            let locked : boolean = false;
            for (const path of $files) {
                if ($verbose) {
                    LogIt.Debug("[" + (iterations + 1) + "/" + this.lockCheckCount + "] Checking file lock: {0}", path);
                }
                if (this.IsFileLocked(path)) {
                    locked = true;
                    break;
                }
            }
            if (!locked) {
                $callback(true);
            } else if (iterations++ < this.lockCheckCount) {
                setTimeout(check, this.lockCheckPeriod); // eslint-disable-line @typescript-eslint/no-implied-eval
            } else {
                $callback(false);
            }
        };
        check();
    }

    private exists($path : string) : boolean {
        let status : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull($path)) {
            $path = this.NormalizePath($path);
            const fs : any = require("fs");
            const isLink : any = () : boolean => {
                let retVal : boolean = false;
                try {
                    retVal = fs.lstatSync($path).isSymbolicLink();
                    LogIt.Info("> is existing symbolic link", LogSeverity.LOW);
                } catch (e) {
                    // leave this block empty
                }
                return retVal;
            };
            status = fs.existsSync($path) || isLink();
        }
        return status;
    }

    private pmkdir($path : string) : void {
        const fs : any = require("fs");
        if (!fs.existsSync($path)) {
            $path.split("/").reduce(($currentPath : string, $folder : string) : string => {
                $currentPath += $folder + "/";
                if (!fs.existsSync($currentPath)) {
                    LogIt.Info("> create dir: " + $currentPath, LogSeverity.LOW);
                    fs.mkdirSync($currentPath);
                }
                return $currentPath;
            }, "");
        }
    }
}

export class DownloadUtils extends BaseObject {
    private owner : FileSystemHandler;
    private path : any;
    private fs : any;
    private options : any;

    constructor($owner : FileSystemHandler, $options : any) {
        super();
        this.owner = $owner;
        this.path = require("path");
        this.fs = require("fs");
        this.options = $options;
    }

    public PrepareTmp($fileName? : string) : string {
        let tmpPath : string = this.owner.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() + "/downloads";
        (<any>this.owner).pmkdir(tmpPath);
        if (!ObjectValidator.IsEmptyOrNull($fileName)) {
            tmpPath += "/" + $fileName;
        } else {
            if (this.options.url.pathname.indexOf("/") !== -1) {
                const name : string = this.options.url.pathname.substr(this.options.url.pathname.lastIndexOf("/"));
                if (name !== "") {
                    tmpPath += "/" + name;
                } else {
                    tmpPath += "/" + new Date().getTime();
                }
            } else {
                tmpPath += "/" + new Date().getTime();
            }
        }
        tmpPath = this.owner.NormalizePath(tmpPath);

        if (this.fs.existsSync(tmpPath)) {
            let index : number = 2;
            let pathExtension : string = this.path.extname(tmpPath);
            const pathMatch : RegExpMatchArray = tmpPath.match(/.tar.[a-zA-Z0-9]+$/g);
            if (pathMatch !== null && pathMatch.length >= 0 && pathMatch[0] !== undefined) {
                pathExtension = pathMatch[0];
            }

            let newPath : string;
            do {
                if (pathExtension !== "") {
                    newPath = StringUtils.Replace(tmpPath, pathExtension, "_" + index + pathExtension);
                } else {
                    newPath = tmpPath + "_" + index;
                }
                index++;
            }
            while (this.fs.existsSync(newPath));
            tmpPath = newPath;
        }

        return tmpPath;
    }

    public FormatUrl($url : string) : string {
        if (EnvironmentHelper.IsWindows()) {
            $url = StringUtils.Remove($url, "file:///");
        }
        return StringUtils.Remove($url, "file://");
    }

    public ParseHeaders($headers : any) : any {
        const length : number = StringUtils.ToInteger($headers["content-length"]);
        const isChunked : boolean = !ObjectValidator.IsEmptyOrNull($headers["transfer-encoding"]) &&
            StringUtils.ContainsIgnoreCase($headers["transfer-encoding"], "chunked");

        let fileName : string = "";
        const disposition : string = "content-disposition";
        if ($headers.hasOwnProperty(disposition)) {
            const filenameKey : string = "filename=";
            const dispositionValue : string = $headers[disposition];
            if (StringUtils.Contains(dispositionValue, "attachment;") &&
                StringUtils.Contains(dispositionValue, filenameKey)) {
                const parts : string[] = StringUtils.Split(dispositionValue, ";");
                parts.forEach(($part : string) : void => {
                    if (StringUtils.Contains($part, filenameKey) && ObjectValidator.IsEmptyOrNull(fileName)) {
                        $part = $part.trim();
                        $part = StringUtils.Remove($part, filenameKey);
                        $part = StringUtils.Remove($part, "\"");
                        fileName = ObjectDecoder.Url($part.trim());
                    }
                });
            }
        }
        return {
            fileName,
            isChunked,
            length
        };
    }
}

export interface IResponsesRegister {
    running : any[];
    abort : string[];
}

export interface IDownloadResult {
    headers : string;
    bodyOrPath : string;
}

export interface IFSDeleteOptions {
    path : string;
    ignoreLocked? : boolean;
    useRecursive? : boolean;
    statusHandler? : ($message : string, $type : LogLevel | string, $severity? : LogSeverity) => void;
}

// generated-code-start
export const IResponsesRegister = globalThis.RegisterInterface(["running", "abort"]);
export const IDownloadResult = globalThis.RegisterInterface(["headers", "bodyOrPath"]);
export const IFSDeleteOptions = globalThis.RegisterInterface(["path", "ignoreLocked", "useRecursive", "statusHandler"]);
// generated-code-end

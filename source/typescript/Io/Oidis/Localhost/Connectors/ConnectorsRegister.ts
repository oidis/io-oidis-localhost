/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IModelListFilter, IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { IConnectorInfo } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IConnectorInfo.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";

export class ConnectorsRegister extends BaseObject {
    private static connectorsRegister : IConnectorMeta[] = [];
    private readonly symbolsMap : any;

    constructor() {
        super();
        this.symbolsMap = {};
    }

    public getList($filter? : string, $options? : IModelListFilter) : IModelListResult<IConnectorInfo> {
        const output : IModelListResult<IConnectorInfo> = {
            data  : [],
            limit : -1,
            offset: 0,
            size  : 0
        };
        JsonUtils.Extend(output, $options);

        ConnectorsRegister.connectorsRegister.filter(($info : IConnectorMeta) : boolean => {
            if (ObjectValidator.IsEmptyOrNull($filter)) {
                return true;
            }
            return StringUtils.ContainsIgnoreCase($info.namespace + $info.requester, $filter) || $info.owner === $filter;
        }).forEach(($info : IConnectorMeta, $index : number) : void => {
            if ($index >= output.offset && ($index < output.limit || output.limit === -1)) {
                output.data.push({namespace: $info.namespace, owner: $info.owner, requester: $info.requester, created: $info.created});
            }
        });
        output.size = output.data.length;
        return output;
    }

    public async Destroy($token : string) : Promise<void> {
        const register : IConnectorMeta[] = [];
        for await (const info of ConnectorsRegister.connectorsRegister) {
            if (info.token !== $token) {
                register.push(info);
            } else {
                await info.connector.Destroy();
            }
        }
        ConnectorsRegister.connectorsRegister = register;
    }

    public getConnectorForToken($token : string, $namespace : string, $requester : string) : BaseConnector {
        $namespace = this.ResolveSymbol($namespace);
        let instance : BaseConnector = null;
        if (!ObjectValidator.IsEmptyOrNull($token)) {
            ConnectorsRegister.connectorsRegister.forEach(($info : IConnectorMeta) : void => {
                if ($info.token === $token && $info.namespace === $namespace) {
                    instance = $info.connector;
                }
            });
            if (ObjectValidator.IsEmptyOrNull(instance)) {
                LogIt.Debug("Constructing new connector {0} for {1}", $namespace, $token);
                const prototype : any = Reflection.getInstance().getClass($namespace);
                instance = new prototype();
                ConnectorsRegister.connectorsRegister.push({
                    owner    : StringUtils.getSha1($token),
                    created  : new Date().getTime(),
                    requester: $requester,
                    token    : $token,
                    namespace: $namespace,
                    connector: instance
                });
            }
        } else {
            instance = Reflection.getInstance().getClass($namespace).getInstance();
            if (instance.getClassName() !== $namespace) {
                const wantedNamespace : string = Reflection.NormalizeNamespace($namespace);
                const returnedNamespace : string = Reflection.NormalizeNamespace(instance.getClassName());
                if (wantedNamespace !== returnedNamespace) {
                    LogIt.Debug("Wants to create class {0} but gets {1}.", wantedNamespace, returnedNamespace);
                    instance = null;
                }
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(instance)) {
            (<any>instance).isExternCall = true;
        }

        return instance;
    }

    public setSymbolMapping(...$definitions : ISymbolDefinition[]) : void {
        $definitions.forEach(($definition : ISymbolDefinition) : void => {
            let symbols : string[] = [];
            if (ObjectValidator.IsString($definition.symbol)) {
                symbols.push(<string>$definition.symbol);
            } else {
                symbols = <string[]>$definition.symbol;
            }
            symbols.forEach(($symbol : string) : void => {
                this.symbolsMap[$symbol] = $definition.value;
            });
        });
    }

    public ResolveSymbol($symbol : string) : string {
        let output : string = $symbol;
        for (const symbol in this.symbolsMap) {
            if (this.symbolsMap.hasOwnProperty(symbol)) {
                if (StringUtils.ContainsIgnoreCase($symbol, symbol) ||
                    StringUtils.PatternMatched(symbol, $symbol)) {
                    output = this.symbolsMap[symbol];
                    // LogIt.Debug("Mapping namespace symbol: {0} to: {1}", $symbol, output);
                }
            }
        }
        return output;
    }
}

export interface ISymbolDefinition {
    symbol : string | string[];
    value : string;
}

export interface IConnectorMeta extends IConnectorInfo {
    token : string;
    connector : BaseConnector;
}

// generated-code-start
export const ISymbolDefinition = globalThis.RegisterInterface(["symbol", "value"]);
export const IConnectorMeta = globalThis.RegisterInterface(["token", "connector"], <any>IConnectorInfo);
// generated-code-end

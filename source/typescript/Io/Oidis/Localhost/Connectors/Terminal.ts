/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ITerminalOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/TerminalConnector.js";
import { ChildProcess } from "child_process";
import { StdinType } from "../Enums/StdinType.js";
import { ResponseFactory } from "../HttpProcessor/ResponseApi/ResponseFactory.js";
import { IResponse } from "../Interfaces/IResponse.js";
import { Loader } from "../Loader.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { IntegrityCheck } from "../Primitives/Decorators.js";
import { EnvironmentHelper } from "../Utils/EnvironmentHelper.js";
import { FileSystemHandler } from "./FileSystemHandler.js";

export class Terminal extends BaseConnector {

    @IntegrityCheck()
    private static async terminalHandler() : Promise<void> {
        const result : IExecuteResult = await Loader.getInstance().getTerminal().ExecuteAsync(
            "echo",
            [EnvironmentHelper.IsWindows() ? "%OIDIS_INTEGRITY_CHECK_ENV%" : "$OIDIS_INTEGRITY_CHECK_ENV"],
            {
                env    : {OIDIS_INTEGRITY_CHECK_ENV: "oidis-integrity-check-env-value"},
                verbose: false
            });
        if (!StringUtils.Contains(result.std.join(), "oidis-integrity-check-env-value")) {
            throw new Error("Basic validation of TerminalHandler failed");
        }
    }

    public Execute($cmd : string, $args : string[], $cwdOrOptions : string | ITerminalOptions,
                   $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
        const execProcess : any = require("child_process").exec;
        const path : any = require("path");
        const fs : any = require("fs");
        const response : IResponse = ResponseFactory.getResponse($callback);
        let cwd : string = <string>$cwdOrOptions;
        const cmdOptions : ITerminalOptions = <any>{
            advanced   : {
                colored       : false,
                noExitOnStderr: false,
                noTerminalLog : false
            },
            shell      : true,
            verbose    : true,
            windowsHide: true
        };
        if (!ObjectValidator.IsSet(cwd) || ObjectValidator.IsEmptyOrNull(cwd)) {
            cwd = process.cwd();
        } else if (ObjectValidator.IsObject($cwdOrOptions)) {
            const options : ITerminalOptions = <ITerminalOptions>$cwdOrOptions;
            if (ObjectValidator.IsSet(options.cwd)) {
                cwd = options.cwd;
            } else {
                cwd = process.cwd();
            }
            if (ObjectValidator.IsSet(options.env)) {
                cmdOptions.env = options.env;
            }
            if (ObjectValidator.IsSet(options.verbose)) {
                cmdOptions.verbose = options.verbose;
            }
            if (ObjectValidator.IsSet(options.detached)) {
                LogIt.Warning("Detached option is not supported by Execute method use Spawn instead.");
            }
            if (ObjectValidator.IsSet(options.shell)) {
                cmdOptions.shell = options.shell;
            }
            if (ObjectValidator.IsSet(options.advanced)) {
                if (ObjectValidator.IsSet(options.advanced.colored)) {
                    cmdOptions.advanced.colored = options.advanced.colored;
                }
                if (ObjectValidator.IsSet(options.advanced.noTerminalLog)) {
                    cmdOptions.advanced.noTerminalLog = options.advanced.noTerminalLog;
                }
                if (ObjectValidator.IsSet(options.advanced.noExitOnStderr)) {
                    cmdOptions.advanced.noExitOnStderr = options.advanced.noExitOnStderr;
                }
            }
        }
        if (cmdOptions.hasOwnProperty("env") && cmdOptions.env.hasOwnProperty("PATH")) {
            cmdOptions.env.PATH = this.NormalizeEnvironmentPath(cmdOptions.env.PATH);
        }
        if (!ObjectValidator.IsSet(cmdOptions.env)) {
            cmdOptions.env = process.env;
        } else {
            cmdOptions.env.HTTP_PROXY = process.env.HTTP_PROXY;
            cmdOptions.env.http_proxy = process.env.http_proxy;
            cmdOptions.env.HTTPS_PROXY = process.env.HTTPS_PROXY;
            cmdOptions.env.https_proxy = process.env.https_proxy;
            cmdOptions.env.NO_PROXY = process.env.NO_PROXY;
            cmdOptions.env.no_proxy = process.env.no_proxy;
        }

        if (EnvironmentHelper.IsDetached() || cmdOptions.detached) {
            if (!ObjectValidator.IsSet(cmdOptions.env)) {
                cmdOptions.env = process.env;
            }
            cmdOptions.env.IS_OIDIS_DETACHED_PROCESS = "1";
        }

        if (!ObjectValidator.IsSet($args) || ObjectValidator.IsEmptyOrNull($args)) {
            $args = [];
        }
        if (cmdOptions.verbose) {
            if (!cmdOptions.advanced.noTerminalLog) {
                LogIt.Info(cwd + " > " + $cmd + " " + $args.join(" "));
            }
        } else {
            LogIt.Info("Execute process in silent mode at: " + cwd);
        }

        let counter : number = 0;
        const progress : any = setInterval(() : void => {
            if (counter < 100) {
                if (cmdOptions.verbose && !cmdOptions.advanced.noTerminalLog) {
                    Echo.Print(".");
                }
            } else {
                if (cmdOptions.verbose && !cmdOptions.advanced.noTerminalLog) {
                    Echo.Println(".");
                }
                counter = 0;
            }
            response.OnChange(".");
            counter++;
        }, 250);

        let exitCode : number = -1;
        const onExitHandler : any = ($exitCode : number, $stdout : string, $stderr : string) : void => {
            clearInterval(progress);
            $stdout = this.normalizeStdout($stdout, cmdOptions.advanced.colored);
            $stderr = this.normalizeStdout($stderr, cmdOptions.advanced.colored);
            if (cmdOptions.verbose) {
                Echo.Println("");
                LogIt.Info($stdout + $stderr);
            }
            if (!cmdOptions.advanced.noTerminalLog) {
                LogIt.Info("exit code: " + $exitCode);
            }
            response.OnComplete($exitCode, [$stdout, $stderr]);
        };

        response.OnStart();
        try {
            cmdOptions.cwd = cwd.replace(/[/\\]/g, path.sep);

            let tmpCmd : string = path.normalize(StringUtils.Remove($cmd, "\""));
            if (fs.existsSync(tmpCmd)) {
                if (this.checkWuiApp(tmpCmd)) {
                    $args.push("--disable-console");
                }
            } else {
                tmpCmd = path.normalize(cmdOptions.cwd + "/" + tmpCmd);
                if (fs.existsSync(tmpCmd)) {
                    if (this.checkWuiApp(tmpCmd)) {
                        $args.push("--disable-console");
                    }
                }
            }
            const child : ChildProcess = execProcess($cmd + " " + $args.join(" "), <any>cmdOptions,
                ($error : Error, $stdout : string, $stderr : string) : void => {
                    onExitHandler(exitCode, $stdout, $stderr);
                });
            child.on("exit", ($exitCode : number) : void => {
                if (!ObjectValidator.IsEmptyOrNull($exitCode)) {
                    exitCode = $exitCode;
                }
            });
            response.AddAbortHandler(() : void => {
                const process : string = cmdOptions.verbose ? ($cmd + " " + $args.join(" ")) : "";
                LogIt.Warning(cmdOptions.cwd + ">" + process + " Process kill requested by abort.");
                child.kill("SIGINT");
            });
            return child.pid;
        } catch (ex) {
            onExitHandler(-1, "", ex.message);
            LogIt.Info(ex.stack);
        }
        return -1;
    }

    public async ExecuteAsync($cmd : string, $args : string[], $cwdOrOptions : string | ITerminalOptions) : Promise<IExecuteResult> {
        return new Promise(($resolve : ($result : IExecuteResult) => void) : void => {
            this.Execute($cmd, $args, $cwdOrOptions, ($exitCode : number, $std : string[]) : void => {
                $resolve({exitCode: $exitCode, std: $std});
            });
        });
    }

    public Spawn($cmd : string, $args : string[], $cwdOrOptions : string | ITerminalOptions,
                 $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
        const spawn : any = require("child_process").spawn;
        const pathNormalize : any = require("path").normalize;
        const fs : any = require("fs");
        const response : IResponse = ResponseFactory.getResponse($callback);
        const cmdOptions : ITerminalOptions = <any>{
            advanced   : {
                colored       : false,
                noExitOnStderr: false,
                noTerminalLog : false,
                useStdIn      : false
            },
            detached   : false,
            shell      : true,
            verbose    : true,
            windowsHide: true
        };
        let cwd : string = <string>$cwdOrOptions;
        const colored : boolean = false;
        if (!ObjectValidator.IsSet($cwdOrOptions) || ObjectValidator.IsEmptyOrNull($cwdOrOptions)) {
            cwd = process.cwd();
        } else if (ObjectValidator.IsObject($cwdOrOptions)) {
            const options : ITerminalOptions = <ITerminalOptions>$cwdOrOptions;
            if (ObjectValidator.IsSet(options.cwd)) {
                cwd = options.cwd;
            } else {
                cwd = process.cwd();
            }
            if (ObjectValidator.IsSet(options.env)) {
                cmdOptions.env = options.env;
            }
            if (ObjectValidator.IsSet(options.verbose)) {
                cmdOptions.verbose = options.verbose;
            }
            if (ObjectValidator.IsSet(options.detached)) {
                cmdOptions.detached = options.detached;
            }
            if (ObjectValidator.IsSet(options.shell)) {
                cmdOptions.shell = options.shell;
            }
            if (ObjectValidator.IsSet(options.advanced)) {
                if (ObjectValidator.IsSet(options.advanced.colored)) {
                    cmdOptions.advanced.colored = options.advanced.colored;
                }
                if (ObjectValidator.IsSet(options.advanced.noTerminalLog)) {
                    cmdOptions.advanced.noTerminalLog = options.advanced.noTerminalLog;
                }
                if (ObjectValidator.IsSet(options.advanced.noExitOnStderr)) {
                    cmdOptions.advanced.noExitOnStderr = options.advanced.noExitOnStderr;
                }
                if (ObjectValidator.IsSet(options.advanced.useStdIn)) {
                    cmdOptions.advanced.useStdIn = options.advanced.useStdIn;
                }
            }
        }
        cmdOptions.cwd = cwd;
        if (cmdOptions.hasOwnProperty("env") && cmdOptions.env.hasOwnProperty("PATH")) {
            cmdOptions.env.PATH = this.NormalizeEnvironmentPath(cmdOptions.env.PATH);
        }
        if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getAppConfiguration().httpProxy)) {
            if (!ObjectValidator.IsSet(cmdOptions.env)) {
                cmdOptions.env = process.env;
            }
            cmdOptions.env.HTTP_PROXY = process.env.HTTP_PROXY;
            cmdOptions.env.http_proxy = process.env.http_proxy;
            cmdOptions.env.HTTPS_PROXY = process.env.HTTPS_PROXY;
            cmdOptions.env.https_proxy = process.env.https_proxy;
        }

        if (!ObjectValidator.IsSet($args) || ObjectValidator.IsEmptyOrNull($args)) {
            $args = [];
        }
        if (cmdOptions.verbose) {
            if (!cmdOptions.advanced.noTerminalLog) {
                LogIt.Info(cwd + " > " + $cmd + " " + $args.join(" "));
            }
        } else {
            LogIt.Info("Spawn process in silent mode at: " + cwd);
        }

        let fired : boolean = false;
        let stdout : string = "";
        let stderr : string = "";
        const onExitHandler : any = ($exitCode : number) : void => {
            if (!fired) {
                fired = true;
                if (!cmdOptions.advanced.noTerminalLog) {
                    LogIt.Info("exit code: " + $exitCode);
                }
                response.OnComplete($exitCode, [stdout, stderr]);
            }
        };
        const onErrorHandler : any = ($error : Error) : void => {
            stderr += $error.message;
            LogIt.Info((<any>$error).stack);
            onExitHandler(-1);
        };
        try {
            let tmpCmd : string = pathNormalize(StringUtils.Remove($cmd, "\""));
            if (fs.existsSync(tmpCmd)) {
                if (this.checkWuiApp(tmpCmd)) {
                    $args.push("--disable-console");
                }
            } else {
                tmpCmd = pathNormalize(cmdOptions.cwd + "/" + tmpCmd);
                if (fs.existsSync(tmpCmd)) {
                    if (this.checkWuiApp(tmpCmd)) {
                        $args.push("--disable-console");
                    }
                }
            }
            if (cmdOptions.detached) {
                (<any>cmdOptions).stdio = ["pipe", "pipe", "pipe", "ipc"];
            } else if (cmdOptions.advanced.useStdIn) {
                (<any>cmdOptions).stdio = ["inherit", "pipe", "pipe"];
            }
            if (EnvironmentHelper.IsDetached() || cmdOptions.detached) {
                if (!ObjectValidator.IsSet(cmdOptions.env)) {
                    cmdOptions.env = process.env;
                }
                cmdOptions.env.IS_OIDIS_DETACHED_PROCESS = "1";
            }
            response.OnStart();

            const child : ChildProcess = spawn($cmd, $args, <any>cmdOptions);
            child.on("exit", onExitHandler);
            child.on("close", onExitHandler);
            child.on("error", onErrorHandler);
            child.stdout.on("data", ($data : string) : void => {
                $data = this.normalizeStdout($data, colored);
                if ($data.indexOf(StdinType.STRING) !== -1 || $data.indexOf(StdinType.PASS) !== -1) {
                    response.OnChange($data, child.stdin);
                } else {
                    stdout += $data;
                    if (cmdOptions.verbose) {
                        Echo.Print($data);
                    }
                    response.OnChange($data);
                }
            });
            child.stderr.on("data", ($data : string) : void => {
                $data = this.normalizeStdout($data, colored);
                stderr += $data;
                if (cmdOptions.verbose) {
                    Echo.Print($data);
                }
                response.OnChange($data);
                if (cmdOptions.advanced.noExitOnStderr) {
                    if ($data.toLowerCase().indexOf("fatal:") !== -1 ||
                        $data.toLowerCase().indexOf("error:") !== -1) {
                        onExitHandler(-1);
                    }
                }
            });
            if (cmdOptions.detached) {
                child.on("message", ($data : string) : void => {
                    $data = this.normalizeStdout($data, colored);
                    stdout += $data;
                    if (cmdOptions.verbose) {
                        Echo.Print($data);
                    }
                    response.OnChange($data);
                });
            }
            response.AddAbortHandler(() : void => {
                const process : string = cmdOptions.verbose ? ($cmd + " " + $args.join(" ")) : "";
                LogIt.Warning(cmdOptions.cwd + ">" + process + " Process kill requested by abort.");
                child.kill("SIGINT");
            });
            return child.pid;
        } catch (ex) {
            onErrorHandler(ex);
        }
        return -1;
    }

    public async SpawnAsync($cmd : string, $args : string[], $cwdOrOptions : string | ITerminalOptions) : Promise<IExecuteResult> {
        return new Promise(($resolve : ($result : IExecuteResult) => void) : void => {
            this.Spawn($cmd, $args, $cwdOrOptions, ($exitCode : number, $std : string[]) : void => {
                $resolve({exitCode: $exitCode, std: $std});
            });
        });
    }

    public Open($path : string, $callback? : (($exitCode : number) => void) | IResponse) : number {
        const exec : any = require("child_process").exec;
        const onOpenHandler : any = ($error : number | Error, $data : string) : void => {
            if ($error !== null) {
                LogIt.Info(<any>$error);
            }
            LogIt.Info($data.toString());
            ResponseFactory.getResponse($callback).Send($error === null ? 0 : -1);
        };
        LogIt.Info("open > " + $path);
        if (ObjectValidator.IsEmptyOrNull($path)) {
            onOpenHandler(-1, "Undefined or empty path for open.");
        } else {
            return exec(FileSystemHandler.getInstance<FileSystemHandler>()
                .NormalizePath($path, true), {windowsHide: true}, onOpenHandler).pid;
        }
        return -1;
    }

    public async OpenAsync($path : string) : Promise<number> {
        return new Promise(($resolve : ($exitCode : number) => void) : void => {
            this.Open($path, ($exitCode : number) : void => {
                $resolve($exitCode);
            });
        });
    }

    public Elevate($cmd : string, $args : string[], $cwd : string,
                   $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
        const pathSeparator : string = require("path").sep;
        let cmd : string;
        let args : string[];
        if (EnvironmentHelper.IsWindows()) {
            cmd = "powershell.exe";
            args = [
                "-Command \"(Start-Process " +
                "-WindowStyle hidden '" + $cmd + "' " +
                (ObjectValidator.IsEmptyOrNull($args) ? "" : "-ArgumentList '" + $args.join(" ") + "' ") +
                "-Verb RunAs " +
                "-PassThru).Id\""
            ];
        } else {
            cmd = "sudo";
            args = [$cmd];
            if (!ObjectValidator.IsEmptyOrNull($args)) {
                $args.forEach(($arg : string) : void => {
                    args.push($arg);
                });
            }
        }
        if (ObjectValidator.IsEmptyOrNull($cwd)) {
            cmd = "cd \"" + $cwd.replace(/\//gi, pathSeparator) + "\" && " + cmd;
        }
        return this.Execute(cmd, args, null, ($exitCode : number, $std : string[]) : void => {
            ResponseFactory.getResponse($callback).Send($exitCode, $std);
        });
    }

    public async ElevateAsync($cmd : string, $args : string[], $cwd : string) : Promise<IExecuteResult> {
        return new Promise(($resolve : ($result : IExecuteResult) => void) : void => {
            this.Elevate($cmd, $args, $cwd, ($exitCode : number, $std : string[]) : void => {
                $resolve({exitCode: $exitCode, std: $std});
            });
        });
    }

    public NormalizeEnvironmentPath($input : string) : string {
        const envSeparator = EnvironmentHelper.IsWindows() ? ";" : ":";
        if (EnvironmentHelper.IsWindows()) {
            $input = StringUtils.Replace($input, "/", "\\");
        } else {
            $input = StringUtils.Replace($input, "\\", "/");
        }
        if (!StringUtils.EndsWith($input, envSeparator)) {
            $input += envSeparator;
        }
        let systemPath : string = process.env.PATH;
        $input = StringUtils.Replace($input, envSeparator + envSeparator, envSeparator);
        StringUtils.Split($input, envSeparator).forEach(($path : string) : void => {
            if ($path !== "") {
                systemPath = StringUtils.Replace(systemPath, $path + envSeparator, "");
            }
        });
        if (!StringUtils.EndsWith($input, envSeparator)) {
            $input += envSeparator;
        }
        return StringUtils.Replace($input + systemPath, envSeparator + envSeparator, envSeparator);
    }

    public Kill($pathOrName : string, $callback? : (($status : boolean) => void) | IResponse) : void {
        const path : any = require("path");
        const response : IResponse = ResponseFactory.getResponse($callback);
        let binPath = StringUtils.Replace($pathOrName, "\\", "/");
        binPath = StringUtils.Replace(binPath, "/", path.sep);
        const binName : string = path.basename(binPath);
        const killAll : boolean = binPath === binName;
        if (EnvironmentHelper.IsWindows()) {
            this.Execute("wmic", [
                "process", "where name=\"" + binName + "\" get processid, executablepath"
            ], {
                verbose: false
            }, ($exitCode : number, $std : string[]) : void => {
                if ($exitCode === 0) {
                    const outList : string = (<any>$std[0]).trim();
                    if (!ObjectValidator.IsEmptyOrNull(outList)) {
                        const entries : string[] = StringUtils.Split(StringUtils.Remove(outList, "\r"), "\n");
                        let status : boolean = true;
                        entries.forEach(($item : any) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($item)) {
                                const parts : any[] = $item.trim().replace(/\s+/g, "*").split("*");
                                if (parts.length >= 2 && StringUtils.ContainsIgnoreCase(parts[0], killAll ? binPath : binName)) {
                                    const pid : number = StringUtils.ToInteger(parts[1]);
                                    if (pid !== process.pid) {
                                        try {
                                            process.kill(pid);
                                        } catch (ex) {
                                            status = false;
                                            LogIt.Warning("Failed to kill process with PID " + parts[1] + ": " + ex.message);
                                        }
                                    }
                                }
                            }
                        });
                        response.Send(status);
                    } else {
                        response.Send(true);
                    }
                } else {
                    LogIt.Warning($std[0] + $std[1]);
                    response.Send(false);
                }
            });
        } else if (EnvironmentHelper.IsMac()) {
            // TODO(mkelnar) add kill implementation for macos (/proc folder not exist on macos)
            LogIt.Warning("Kill is not implemented on MacOS.");
            response.Send(false);
        } else {
            let pids : string[] = [];
            let status : boolean = true;
            const killProcess : any = ($index : number) : void => {
                if ($index < pids.length) {
                    if (!ObjectValidator.IsEmptyOrNull(pids[$index])) {
                        this.Execute("ls", ["-l", "/proc/" + pids[$index] + "/exe"], {
                            verbose: false
                        }, ($exitCode : number, $std : string[]) : void => {
                            if ($exitCode === 0) {
                                const processInfo : string = (<any>$std[0]).trim();
                                if (!ObjectValidator.IsEmptyOrNull(processInfo)) {
                                    const parts : string[] = StringUtils.Split(processInfo, " -> ");
                                    if (parts.length >= 2 && StringUtils.ContainsIgnoreCase(parts[1], killAll ? binPath : binName)) {
                                        const pid : number = StringUtils.ToInteger(pids[$index]);
                                        if (pid !== process.pid) {
                                            try {
                                                process.kill(pid);
                                            } catch (ex) {
                                                LogIt.Warning("Failed to kill process with PID " + pids[$index] + ": " + ex.message);
                                                status = false;
                                            }
                                        }
                                    }
                                }
                            } else {
                                LogIt.Warning($std[0] + $std[1]);
                                status = false;
                            }
                            killProcess($index + 1);
                        });
                    } else {
                        killProcess($index + 1);
                    }
                } else {
                    response.Send(status);
                }
            };
            this.Execute("pgrep", ["\"" + binName + "\""], {
                verbose: false
            }, ($exitCode : number, $std : string[]) : void => {
                if ($exitCode === 0 || $exitCode === 1 && ObjectValidator.IsEmptyOrNull($std[0] + $std[1])) {
                    const outList : string = (<any>$std[0]).trim();
                    if (!ObjectValidator.IsEmptyOrNull(outList)) {
                        pids = StringUtils.Split(outList, "\n");
                        killProcess(0);
                    } else {
                        response.Send(true);
                    }
                } else {
                    LogIt.Warning($std[0] + $std[1]);
                    response.Send(false);
                }
            });
        }
    }

    public async KillAsync($pathOrName : string) : Promise<void> {
        return new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) : void => {
            this.Kill($pathOrName, ($status : boolean) : void => {
                if ($status) {
                    $resolve();
                } else {
                    $reject(new Error("Process kill failed."));
                }
            });
        });
    }

    private normalizeStdout($value : string, $colorized : boolean = false) : string {
        const os : any = require("os");
        $value = Buffer.from($value + "").toString("utf8")
            .replace(/█/gm, "[__BAR__]")
            .replace(/▇/gm, "[__BAR__]")
            .replace(/✓/gm, "[__OK__]")
            .replace(/×/gm, "[__FAIL__]")
            .replace(/→/gm, "[__ARROW__]")
            .replace(/\r\n/gm, "\n")
            .replace(/\n/gm, "[__EOL__]");
        if (!$colorized) {
            try {
                $value = $value.replace( // eslint-disable-next-line no-control-regex
                    /[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, "");
            } catch ($ex) {
                // let std as is
            }
            $value = $value.replace(/[^ -~]+/g, "");
        }
        $value = $value.replace(/\[__EOL__\]/gm, os.EOL);
        if ($value.match(/(\(.*\d+\/\d+\))?.*\[[#-]+\].*%/gm) !== null) {
            $value = $value.replace(/%/gm, "%" + os.EOL);
        }
        return $value;
    }

    private checkWuiApp($path : string) : boolean {
        let status : boolean = false;
        if (EnvironmentHelper.IsWindows()) {
            try {
                const filePath : string = require("path").normalize(StringUtils.Remove($path, "\""));
                if (require("fs").existsSync(filePath)) {
                    const verInfo : any = require("win-version-info")(filePath);
                    if (!ObjectValidator.IsEmptyOrNull(verInfo) &&
                        verInfo.hasOwnProperty("BaseProjectName") && (
                            StringUtils.StartsWith(verInfo.BaseProjectName, "io-oidis-") ||
                            StringUtils.StartsWith(verInfo.BaseProjectName, "com-wui-framework-"))) {
                        LogIt.Info("Executing command has been found as Oidis Framework application.");
                        status = true;
                    }
                }
            } catch (ex) {
                // Can not read version info. Executable will be processed as default.
            }
        } else {
            this.checkWuiApp = ($path : string) : boolean => {
                return false;
            };
        }
        return status;
    }
}

export interface IExecuteResult {
    exitCode : number;
    std : string[];
}

// generated-code-start
export const IExecuteResult = globalThis.RegisterInterface(["exitCode", "std"]);
// generated-code-end

/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { BrowserErrorPage as Parent } from "@io-oidis-gui/Io/Oidis/Gui/ErrorPages/BrowserErrorPage.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { HttpRequestParser } from "../HttpProcessor/HttpRequestParser.js";
import { HttpServer } from "../HttpProcessor/HttpServer.js";

export class BrowserErrorPage extends Parent {
    public Process() : void {
        super.Process();
        HttpServer.SendData(<HttpRequestParser>this.getRequest(), this.RequestArgs().POST().getItem("Connector"),
            HttpStatusType.FORBIDDEN, {}, StaticPageContentManager.ToString());
    }
}

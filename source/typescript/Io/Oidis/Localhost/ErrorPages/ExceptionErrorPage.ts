/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { ExceptionErrorPage as Parent } from "@io-oidis-gui/Io/Oidis/Gui/ErrorPages/ExceptionErrorPage.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { HttpRequestParser } from "../HttpProcessor/HttpRequestParser.js";
import { HttpServer } from "../HttpProcessor/HttpServer.js";

export class ExceptionErrorPage extends Parent {
    public Process() : void {
        super.Process();
        HttpServer.SendData(<HttpRequestParser>this.getRequest(), this.RequestArgs().POST().getItem("Connector"),
            HttpStatusType.ERROR, {}, StaticPageContentManager.ToString());
    }
}

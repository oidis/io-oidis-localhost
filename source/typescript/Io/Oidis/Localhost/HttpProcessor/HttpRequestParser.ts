/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpRequestParser as Parent } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpRequestParser.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Loader } from "../Loader.js";

export class HttpRequestParser extends Parent {

    public getUrl() : string {
        if (this.isOnBackend()) {
            const url : string = this.getOwner().url;
            if (ObjectValidator.IsEmptyOrNull(url)) {
                return "/";
            }
            return url;
        }
        return super.getUrl();
    }

    public getScheme() : string {
        if (this.isOnBackend()) {
            return this.getOwner().protocol;
        }
        return this.parsedInfo.scheme;
    }

    public getClientIP() : string {
        if (this.isOnBackend()) {
            const request : any = this.getOwner();
            try {
                if (!ObjectValidator.IsEmptyOrNull(request) && request !== window) {
                    const ip = request.headers["x-forwarded-for"] ||
                        request.connection.remoteAddress ||
                        request.socket.remoteAddress ||
                        request.connection.socket.remoteAddress;
                    return ip.split(",")[0];
                }
            } catch (ex) {
                // unable to read or request properties correctly
            }
        }
        return super.getClientIP();
    }

    public getServerIP() : string {
        if (this.isOnBackend()) {
            const interfaces : any = require("os").networkInterfaces();
            let output : string = this.parsedInfo.hostIpAddress;
            Object.keys(interfaces).forEach(($name : string) : void => {
                interfaces[$name].forEach(($networkInterface : any) : void => {
                    if ("IPv4" !== $networkInterface.family || $networkInterface.internal !== false) {
                        return;
                    }
                    output = $networkInterface.address;
                });
            });
            return output;
        }
        return this.parsedInfo.hostIpAddress;
    }

    protected getServerLocation() : Location {
        if (this.isOnBackend()) {
            if (this.getOwner() === window) {
                return require("url").parse("http://" + Loader.getInstance().getAppConfiguration().domain.location);
            }
            return require("url").parse(this.getUrl());
        }
        return super.getServerLocation();
    }

    protected getNavigator() : Navigator {
        if (this.isOnBackend()) {
            return <any>{
                cookieEnabled: false,
                language     : "en-US",
                onLine       : true,
                platform     : "win64",
                userAgent    : "Node.js Chrome/52.0.2743.116"
            };
        }
        return super.getNavigator();
    }

    protected getRawHeaders() : ArrayList<string> {
        if (this.isOnBackend()) {
            const headers : ArrayList<string> = new ArrayList<string>();
            try {
                if (!ObjectValidator.IsEmptyOrNull(this.getOwner()) && this.getOwner() !== window) {
                    let key : string;
                    for (key in this.getOwner().headers) {
                        if (this.getOwner().headers.hasOwnProperty(key)) {
                            headers.Add(this.getOwner().headers[key], StringUtils.ToLowerCase(key));
                        }
                    }
                }
            } catch (ex) {
                // unable to read or parse headers
            }
            return headers;
        }
        return new ArrayList<string>();
    }

    private isOnBackend() : boolean {
        const status : boolean = !Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed();
        this.isOnBackend = () : boolean => {
            return status;
        };
        return status;
    }
}

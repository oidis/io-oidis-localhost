/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { HttpRequestConstants } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpRequestConstants.js";
import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { IHttpResolverContext } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { isBrowser } from "@io-oidis-commons/Io/Oidis/Commons/Utils/EnvironmentHelper.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IFileSystemDownloadOptions, IProxyConfig } from "@io-oidis-services/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";
import { HttpResolver as Parent } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/HttpResolver.js";
import { UrlWithStringQuery } from "url";
import { EventsManager } from "../Events/EventsManager.js";
import { Loader } from "../Loader.js";
import { EnvironmentHelper } from "../Utils/EnvironmentHelper.js";
import { HttpManager } from "./HttpManager.js";
import { HttpRequestParser } from "./HttpRequestParser.js";

export class HttpResolver extends Parent {

    public CreateRequest($baseUrl? : string) : HttpRequestParser {
        return <HttpRequestParser>super.CreateRequest($baseUrl);
    }

    public getEvents() : EventsManager {
        return <EventsManager>super.getEvents();
    }

    public async FetchHelper($options : IFileSystemDownloadOptions) : Promise<IFetchAttributes> {
        if (isBrowser) {
            /// TODO: FE implementation can be reused from VIT and used for async download with headers etc.
            //        > options will be able to resolve filename by DownloadUtils
            //        > client will be returned based on CORS conditions
            throw new Error("Only back-end calls are supported for FetchHelper");
        }
        const config : IProxyAttributes = this.detectProxy($options.url, $options.proxyConfig);

        if (process.env.hasOwnProperty("DISABLE_STRICT_SSL") || Loader.getInstance().getAppConfiguration().disableStrictSSL ||
            !$options.strictSSL) {
            $options.url.strictSSL = false;
            $options.url.rejectUnauthorized = false;
        }

        if (($options.method === HttpMethodType.DELETE ||
            $options.method === HttpMethodType.CONNECT) && !ObjectValidator.IsEmptyOrNull($options.body)) {
            JsonUtils.Extend($options.headers, {
                "content-length": $options.body.length
            });
        }

        return new Promise<IFetchAttributes>(($resolve : any, $reject : any) : void => {
            const request : IFetchResolver = this.fetchOptionsResolver($options);
            if (config.noProxy || (config.client === "http" && ObjectValidator.IsEmptyOrNull(config.agent))) {
                $resolve({client: request.client, options: request.options});
            } else {
                require("http")
                    .request(request.proxy)
                    .on("connect", ($httpResponse : any, $socket : any) : void => {
                        if ($httpResponse.statusCode === 200) {
                            request.options.url.agent = new (request.agent)({socket: $socket});
                            $resolve({client: request.client, options: $options});
                        }
                    })
                    .on("error", ($error : Error) : void => {
                        $reject($error);
                    })
                    .end();
            }
        });
    }

    protected async getStartupResolvers($context : IHttpResolverContext) : Promise<any> {
        await this.registerResolver(async () => (await import("./Resolvers/FrontEndLoader.js")).FrontEndLoader,
            "/");
        await this.registerResolver(async () => (await import("../ErrorPages/ExceptionErrorPage.js")).ExceptionErrorPage,
            "/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}");
        await this.registerResolver(async () =>
                (await import("@io-oidis-services/Io/Oidis/Services/ErrorPages/AuthHttpResolver.js")).AuthHttpResolver,
            "/ServerError/Http/NotAuthorized");
        await this.registerResolver(async () =>
                (await import("@io-oidis-services/Io/Oidis/Services/ErrorPages/ConnectionLostPage.js")).ConnectionLostPage,
            "/ServerError/Http/ConnectionLost");
        await this.registerResolver(async () => (await import("../ErrorPages/BrowserErrorPage.js")).BrowserErrorPage,
            "/ServerError/Browser");
        await this.registerResolver(async () => (await import("../ErrorPages/Http404NotFoundPage.js")).Http404NotFoundPage,
            "/ServerError/Http/NotFound");
        await this.registerResolver(async () => (await import("../ErrorPages/Http301MovedPage.js")).Http301MovedPage,
            "/ServerError/Http/Moved");
        await this.registerResolver(async () => (await import("../ErrorPages/Http403ForbiddenPage.js")).Http403ForbiddenPage,
            "/ServerError/Http/Forbidden");
        await this.registerResolver(async () => (await import("./Resolvers/XOriginInterface.js")).XOriginInterface,
            "/wuihost/", "/xorigin/", "/xorigin/Request");
        await this.registerResolver(async () => (await import("./Resolvers/ConnectorResponse.js")).ConnectorResponse,
            "/wuihost/CloseClient", "/{clientId}/response.jsonp", "connector.config.jsonp", "connector.config.js");
        await this.registerResolver(async () => (await import("./Resolvers/XOriginServices.js")).XOriginServices,
            "/resource/javascript/xservices.js");
        await this.registerResolver(async () => (await import("./Resolvers/FileRequestResolver.js")).FileRequestResolver,
            "/favicon.ico");
        await this.registerResolver(async () => (await import("./Resolvers/LiveContentResolver.js")).LiveContentResolver,
            "/liveContent/");
        await this.registerResolver(async () => (await import("./Resolvers/AcmeResolver.js")).AcmeResolver,
            "/.well-known/acme-challenge/{token}");
        await this.registerResolver(async () => (await import("./Resolvers/LoaderResolver.js")).LoaderResolver,
            "/resource/javascript/loader.min.js");
        await this.registerResolver(async () => (await import("./Resolvers/CSPReportResolver.js")).CSPReportResolver,
            "/cspReport");

        const fileExtensions : string[] = [
            "*.htm", "*.html", "*.txt", "*.log", "*.pdf",
            "*.min.css", "*.css", "*.scss", "*.sass",
            "*.min.js", "*.js", "*.json", "*.jsonp", "*.map", "*.ts", "*.tsx",
            "*.bmp", "*.jpg", "*.jpe", "*.jpeg", "*.png", "*.gif", "*.ico", "*.icon", "*.svg",
            "*.woff", "*.woff2", "*.eot", "*.otf", "*.ttf",
            "*.xml", "*.xhml"
        ];
        for await (const $extension of fileExtensions) {
            await this.registerResolver(async () => (await import("./Resolvers/FileRequestResolver.js")).FileRequestResolver,
                "/**/" + $extension);
        }

        if ($context.isProd) {
            const maps : string[] = ["*.min.js.map", "*.min.css.map"];
            for await (const url of maps) {
                await this.registerResolver(async () => (await import("./Resolvers/MapFilesPatchResolver.js")).MapFilesPatchResolver,
                    "/**/" + url);
            }
        }

        await this.registerResolver(async () => (await import("./Resolvers/RESTResolver.js")).RESTResolver,
            "/api/{section}/{subsection1}/{subsection2}/{subsection3}/{subsection4}/{subsection5}",
            "/api/{section}/{subsection1}/{subsection2}/{subsection3}/{subsection4}",
            "/api/{section}/{subsection1}/{subsection2}/{subsection3}",
            "/api/{section}/{subsection1}/{subsection2}",
            "/api/{section}/{subsection1}",
            "/api/{section}");

        return super.getStartupResolvers($context);
    }

    protected getRequestParserClass() : any {
        return HttpRequestParser;
    }

    protected getHttpManagerClass() : any {
        return HttpManager;
    }

    protected getEventsManagerClass() : any {
        return EventsManager;
    }

    protected detectProxy($url : string | UrlWithStringQuery, $config? : IProxyConfig) : IProxyAttributes {
        if (ObjectValidator.IsEmptyOrNull($config)) {
            $config = EnvironmentHelper.NormalizeProxySetting({});
        }
        // returns true is proxy should not be applied
        const checkUrlPattern : any = ($hostname : string, $port : number, $defaultPort : number) : boolean => {
            if (ObjectValidator.IsEmptyOrNull($config.noProxy)) {
                return true;
            }
            if ($config.noProxy === "*") {
                return true;
            }
            return $config.noProxy.split(",").some(($pattern : string) : boolean => {
                if (StringUtils.StartsWith($pattern, ".")) {
                    $pattern = StringUtils.Substring($pattern, 1);
                }
                if (!StringUtils.StartsWith($pattern, "*")) {
                    $pattern = "*" + $pattern;
                }
                return StringUtils.PatternMatched($pattern, $hostname) || StringUtils.PatternMatched($pattern, $hostname + ":" +
                    (ObjectValidator.IsEmptyOrNull($port) ? $defaultPort : $port));
            });
        };
        const retVal : IProxyAttributes = {
            agent   : "",  // when agent is specified then proxy is required
            client  : "",  // client without agent means direct http/https/ws/wss connection
            excluded: false,  // just for tests for now
            noProxy : false,  // identify if proxy should be used or not
            proxyUrl: ""  // return defined proxy url which should be used for next processing
        };
        const destUrl : UrlWithStringQuery = ObjectValidator.IsString($url) ? new (require("url").URL)($url) : $url;
        switch (destUrl.protocol) {
        case "ws:":
        case "http:":
            retVal.client = StringUtils.Remove(destUrl.protocol, ":");
            if (!ObjectValidator.IsEmptyOrNull($config.httpProxy)) {
                if (!ObjectValidator.IsEmptyOrNull($config.noProxy)) {
                    if (checkUrlPattern(destUrl.hostname, destUrl.port, 80)) {
                        // excluded from proxy
                        retVal.excluded = true;
                        retVal.noProxy = true;
                    }
                }
                if (!retVal.noProxy) {
                    retVal.proxyUrl = $config.httpProxy;
                    // proxy enabled so update WS
                    if (retVal.client === "ws") {
                        retVal.client = "http";  // forced use of http client as entrypoint because secured proxy is not supported
                        retVal.agent = "ws";  // this also turn on http agent pool type
                    }
                }
            } else {
                // no proxy defined for dest url continue as normal for non-secure
                retVal.noProxy = true;
            }
            break;
        case "wss:":
        case "https:":
            retVal.client = StringUtils.Remove(destUrl.protocol, ":");
            if (!ObjectValidator.IsEmptyOrNull($config.httpsProxy)) {
                if (!ObjectValidator.IsEmptyOrNull($config.noProxy)) {
                    if (checkUrlPattern(destUrl.hostname, destUrl.port, 443)) {
                        // excluded from proxy
                        retVal.excluded = true;
                        retVal.noProxy = true;
                    }
                }
                if (!retVal.noProxy) {
                    retVal.proxyUrl = $config.httpsProxy;
                    if (retVal.client === "wss") {
                        retVal.agent = "wss";  // this also turn on https agent pool type
                    } else {
                        retVal.agent = "https";
                    }
                    retVal.client = "http";  // forced use of http client as entrypoint because secured proxy is not supported
                }
            } else {
                // no proxy defined for dest url
                retVal.noProxy = true;
            }
            break;
        default:
            throw new Error("Unsupported protocol");
        }
        LogIt.Info("> detected proxy: " + JSON.stringify(retVal), LogSeverity.LOW);
        return retVal;
    }

    protected fetchOptionsResolver($options : IFileSystemDownloadOptions) : IFetchResolver {
        $options = JsonUtils.Extend({
            body   : "",
            headers: {},
            method : HttpMethodType.GET
        }, $options);
        const config : IProxyAttributes = this.detectProxy($options.url, $options.proxyConfig);
        let client : any;
        if (config.client === "ws" || config.client === "wss" || config.agent === "ws" || config.agent === "wss") {
            client = require("websocket").w3cwebsocket;
        } else {
            client = require(config.client);
        }
        if (config.noProxy) {
            return {client, options: $options};
        } else {
            let proxy : any = config.proxyUrl;
            let target : any = $options.url;
            // TODO(mkelnar) parse should be replaced by WHATWG URL, could be not stable or not supported in coming versions.
            //  Warning: kuba mentioned some "undefined" issues for usage of new URL() here which was not discovered in detectProxy().
            if (ObjectValidator.IsString(config.proxyUrl)) {
                proxy = require("url").parse(config.proxyUrl);
            }
            if (ObjectValidator.IsString($options.url)) {
                target = require("url").parse($options.url);
            }

            if (!ObjectValidator.IsEmptyOrNull(proxy.auth)) {
                proxy.headers = JsonUtils.Extend(proxy.headers, {
                    "Proxy-Authorization": "Basic " + ObjectEncoder.Base64(proxy.auth)
                });
            }
            if (!ObjectValidator.IsEmptyOrNull($options.strictSSL)) {
                proxy.strictSSL = $options.url.strictSSL;
                proxy.rejectUnauthorized = $options.url.rejectUnauthorized;
            }

            let agent : any = null;
            if (config.client === "http" && config.agent === "") {
                $options.url = proxy;
                $options.url.method = $options.method;
                $options.url.headers = $options.headers;
                $options.url.headers.Host = target.hostname;
                $options.url.path = target.href;
            } else {
                if (config.agent === "http" || config.agent === "https") {
                    client = require(config.agent);
                }
                proxy.method = HttpMethodType.CONNECT;
                proxy.path = target.hostname + ":";
                if (ObjectValidator.IsEmptyOrNull(target.port)) {
                    proxy.path += (config.agent === "ws" || config.agent === "http" ? 80 : 443);
                } else {
                    proxy.path += target.port;
                }
                agent = require(config.agent === "wss" || config.agent === "https" ? "https" : "http").Agent;
            }
            return {client, options: $options, proxy, agent};
        }
    }
}

export interface IProxyAttributes {
    client : string;
    excluded : boolean;
    noProxy : boolean;
    agent : string;
    proxyUrl : string;
}

export interface IFetchAttributes {
    client : any;
    options : IFileSystemDownloadOptions;
}

export interface IFetchResolver extends IFetchAttributes {
    proxy? : any;
    agent? : any;
}

// generated-code-start
export const IProxyAttributes = globalThis.RegisterInterface(["client", "excluded", "noProxy", "agent", "proxyUrl"]);
export const IFetchAttributes = globalThis.RegisterInterface(["client", "options"]);
export const IFetchResolver = globalThis.RegisterInterface(["proxy", "agent"], <any>IFetchAttributes);
// generated-code-end

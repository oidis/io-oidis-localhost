/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpManager as Parent } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/HttpManager.js";
import { HttpRequestParser } from "./HttpRequestParser.js";

export class HttpManager extends Parent {

    public getRequest() : HttpRequestParser {
        return <HttpRequestParser>super.getRequest();
    }
}

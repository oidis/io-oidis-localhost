/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { IResponseProtocol } from "../../Interfaces/IResponse.js";
import { Loader } from "../../Loader.js";
import { CertsManager } from "../../Utils/CertsManager.js";
import { BaseHttpResolver } from "./BaseHttpResolver.js";

export class AcmeResolver extends BaseHttpResolver {
    private token : string;

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        if ($GET.KeyExists("token")) {
            this.token = $GET.getItem("token");
        }
        super.argsHandler($GET, $POST);
    }

    protected resolver() : void {
        const dataPath : string = CertsManager.getInstance().getStoragePath() + "/" + this.token;
        if (Loader.getInstance().getFileSystemHandler().Exists(dataPath)) {
            this.getConnector().Send(<IResponseProtocol>{
                body   : require("fs").createReadStream(dataPath),
                headers: {"content-type": "text/plain"},
                status : HttpStatusType.SUCCESS
            });
        } else {
            this.getConnector().Send(<IResponseProtocol>{
                body  : "Data for ACME challenge has not been found.",
                status: HttpStatusType.NOT_FOUND
            });
        }
    }
}

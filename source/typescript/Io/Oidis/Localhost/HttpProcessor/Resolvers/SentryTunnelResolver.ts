/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { ISentryConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IProject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IDownloadResult } from "../../Connectors/FileSystemHandler.js";
import { IHttpResponse } from "../../Interfaces/IRoute.js";
import { Loader } from "../../Loader.js";
import { BaseConnector } from "../../Primitives/BaseConnector.js";
import { Route } from "../../Primitives/Decorators.js";
import { BaseResponseFormatter } from "../ResponseApi/Formatters/BaseResponseFormatter.js";
import { StreamResponse } from "../ResponseApi/Handlers/StreamResponse.js";

export class SentryTunnelResolver extends BaseConnector {

    @Route("POST /sentry", {responseHandler: new StreamResponse(), inputFormatter: new BaseResponseFormatter()})
    public async Proxy($data : any, $isSecurityReport : boolean = false) : Promise<IHttpResponse> {
        try {
            const config : ISentryConfiguration = Loader.getInstance().getEnvironmentArgs().getProjectConfig().sentry;
            if (!$isSecurityReport && !$data.includes(config.dsn)) {
                return {body: "Invalid sentry dsn", status: HttpStatusType.FORBIDDEN};
            }
            const reports : any[] = [];
            const projectConfig : URL = new URL(config.dsn);
            let url : string = projectConfig.protocol + "//" + projectConfig.host + "/api" + projectConfig.pathname;
            if (!$isSecurityReport) {
                url += "/envelope/";
                reports.push({
                    url,
                    body: $data
                });
            } else {
                url += "/security/?sentry_key=" + projectConfig.username +
                    "&sentry_release=" + ObjectEncoder.Url(Loader.getInstance().getEnvironmentArgs().getProjectVersion());
                if (!ObjectValidator.IsEmptyOrNull(config.environment) && config.environment !== "none") {
                    url += "&sentry_environment=" + ObjectEncoder.Url(config.environment);
                }
                /// TODO: this data normalization to deprecated serialization of violation report can be removed when
                //        sentry will support this content-type: "application/reports+json"
                //        more details at: https://w3c.github.io/webappsec-csp/#deprecated-serialize-violation and
                //                         https://w3c.github.io/webappsec-csp/#report-violation
                if (ObjectValidator.IsString($data)) {
                    $data = JSON.parse($data);
                }
                if (!ObjectValidator.IsArray($data)) {
                    $data = [$data];
                }
                if (ObjectValidator.IsArray($data)) {
                    $data.forEach(($report : any) : void => {
                        if ($report.type === "csp-violation") {
                            reports.push({
                                url,
                                body   : JSON.stringify({
                                    "csp-report": {
                                        "document-uri": $report.body.documentURL,
                                        "blocked-uri": $report.body.blockedURL,
                                        "referrer": $report.body.referrer,
                                        "effective-directive": $report.body.effectiveDirective,
                                        "violated-directive": $report.body.violatedDirective,
                                        "original-policy": $report.body.originalPolicy,
                                        "disposition": $report.body.disposition,
                                        "status-code": $report.body.statusCode,
                                        "source-file": $report.body.sourceFile,
                                        "script-sample": $report.body.sample,
                                        "line-number": $report.body.lineNumber,
                                        "column-number": $report.body.columnNumber
                                    }
                                }),
                                headers: {
                                    "Content-Type": "application/csp-report"
                                }
                            });
                        } else {
                            LogIt.Warning("Unrecognized violation report type: " + $report.type + "\n" + JSON.stringify($report));
                        }
                    });
                } else {
                    LogIt.Warning("Invalid csp report format: " + JSON.stringify($data));
                }
            }
            for await (const report of reports) {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const result : IDownloadResult = await Loader.getInstance().getFileSystemHandler().DownloadAsync(JsonUtils.Extend({
                    method      : HttpMethodType.POST,
                    streamOutput: true,
                    verbose     : false,
                    strictSSL   : config.useStrictSSL
                }, report));
                // LogIt.Debug(result.bodyOrPath);
            }
        } catch (ex) {
            LogIt.Error("Failed to tunnel to sentry", ex);
            return {body: "Failed to tunnel to sentry", status: HttpStatusType.ERROR};
        }
        return {body: "Data successfully sent to sentry", status: HttpStatusType.SUCCESS};
    }
}

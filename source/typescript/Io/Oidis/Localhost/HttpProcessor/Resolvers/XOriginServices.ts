/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Readable } from "stream";
import { Loader } from "../../Loader.js";
import { FileRequestResolver, IFilePath } from "./FileRequestResolver.js";

export class XOriginServices extends FileRequestResolver {
    private static origin : string;
    private static schema : string;

    protected getEtag($file : IFilePath, $lastModifiedTime : number) : string {
        let origin : string = Loader.getInstance().getAppConfiguration().domain.location;
        const headers : ArrayList<string> = this.getRequest().getHeaders();
        if (headers.KeyExists("host")) {
            if (!ObjectValidator.IsEmptyOrNull(headers.getItem("x-forwarded-host"))) {
                origin = headers.getItem("x-forwarded-host");
            } else {
                origin = headers.getItem("host");
            }
        }
        let schema : string = "http://";
        if (headers.KeyExists("referer")) {
            if (StringUtils.StartsWith(headers.getItem("referer"), "https://")) {
                schema = "https://";
            }
        } else {
            schema = this.getRequest().getScheme();
        }
        if (XOriginServices.origin !== origin) {
            XOriginServices.origin = origin;
        }
        if (XOriginServices.schema !== schema) {
            XOriginServices.schema = schema;
        }
        return StringUtils.getSha1($lastModifiedTime + JSON.stringify({
            origin: XOriginServices.origin,
            schema: XOriginServices.schema
        }) + $file.path);
    }

    protected streamProcessor($filePath : string) : any {
        const fs : any = require("fs");
        const ReadableClass : any = require("stream").Readable;

        let content : string = fs.readFileSync($filePath).toString();
        content = StringUtils.Replace(content, "<? @var build.year ?>",
            new Date(Loader.getInstance().getEnvironmentArgs().getBuildTime()).getFullYear() + "");
        content = StringUtils.Replace(content, "<? @var xservices.origin ?>", XOriginServices.origin + "/xorigin/");
        content = StringUtils.Replace(content, "<? @var xservices.request ?>",
            XOriginServices.schema + XOriginServices.origin + "/xorigin/");

        const stream : Readable = new ReadableClass();
        stream.push(content);
        stream.push(null);
        return stream;
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2022 NXP
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { BaseHttpResolver } from "./BaseHttpResolver.js";

export class MapFilesPatchResolver extends BaseHttpResolver {
    public Process() : void {
        if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
            this.getConnector().Send({
                body  : JSON.stringify({
                    version : 3,
                    sources : [],
                    names   : [],
                    mappings: "",
                    file    : ""
                }),
                status: HttpStatusType.SUCCESS
            });
        } else {
            super.Process();
        }
    }
}

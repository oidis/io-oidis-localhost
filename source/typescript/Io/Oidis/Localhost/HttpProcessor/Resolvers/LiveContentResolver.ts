/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ILiveContentProtocol, IWebServiceProtocol } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceProtocol.js";
import { IConnector } from "../../Interfaces/IConnector.js";
import { IRequestConnector } from "../../Interfaces/IRequestConnector.js";
import { IAuthPromise, IResponse } from "../../Interfaces/IResponse.js";
import { Loader } from "../../Loader.js";
import { Metrics } from "../../Utils/Metrics.js";
import { LiveContentResponse } from "../ResponseApi/Handlers/LiveContentResponse.js";
import { BaseHttpResolver } from "./BaseHttpResolver.js";

export class LiveContentResolver extends BaseHttpResolver {
    private static eventSubscribers : IEventSubscriber[][];
    private requestProtocol : IWebServiceProtocol;
    private response : IResponse;

    public static FireEvent($name : string, $args? : any) : void {
        if (ObjectValidator.IsEmptyOrNull($args)) {
            $args = "";
        }
        if (ObjectValidator.IsEmptyOrNull(LiveContentResolver.eventSubscribers)) {
            LiveContentResolver.eventSubscribers = <any>{};
        }
        if (LiveContentResolver.eventSubscribers.hasOwnProperty($name)) {
            let index : number;
            for (index = 0; index < LiveContentResolver.eventSubscribers[$name].length; index++) {
                const subscriber : IEventSubscriber = LiveContentResolver.eventSubscribers[$name][index];
                subscriber.protocol.status = HttpStatusType.SUCCESS;
                subscriber.protocol.type = "FireEvent";
                subscriber.protocol.data = Buffer.from(JSON.stringify({
                    args: Buffer.from(JSON.stringify($args)).toString("base64"),
                    name: $name
                })).toString("base64");
                subscriber.connector.Send(subscriber.protocol);
            }
        }
    }

    public Process() : void {
        this.resolver();
    }

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        super.argsHandler($GET, $POST);
        if ($POST.KeyExists("LiveProtocol")) {
            this.requestProtocol = $POST.getItem("LiveProtocol");
        }
        if ($POST.KeyExists("LiveResponse")) {
            this.response = $POST.getItem("LiveResponse");
        }
    }

    protected resolver() : void {
        const metrics : Metrics = Metrics.getInstance();
        let data : any = null;
        try {
            data = JSON.parse(Buffer.from(<string>this.requestProtocol.data, "base64").toString("utf8"));
        } catch (ex) {
            LogIt.Info("Received message in incorrect format: " + this.requestProtocol);
            this.response.OnError(ex);
        }
        if (data !== null) {
            try {
                let requestData : ILiveContentProtocol = data;
                metrics.Start(requestData.name, "LiveContent");
                if (StringUtils.StartsWith(this.requestProtocol.type, "LiveContentWrapper.")) {
                    let namespaceName : string = requestData.name.substr(0, requestData.name.lastIndexOf("."));
                    let memberName : string = requestData.name.replace(namespaceName + ".", "");
                    let args : any[] = JSON.parse(Buffer.from(requestData.args, "base64").toString("utf8"));

                    let classPrototype : any = null;
                    try {
                        classPrototype = Reflection.getInstance().getClass(namespaceName);
                    } catch (ex) {
                        LogIt.Warning("Reflection for required class failed: " + ex.message);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(classPrototype)) {
                        try {
                            if (Reflection.getInstance().ClassHasInterface(classPrototype, IConnector)) {
                                let liveResponse : LiveContentResponse = new LiveContentResponse(this.response);
                                let instance : any = Loader.getInstance().getConnectorsRegister()
                                    .getConnectorForToken(this.requestProtocol.token, namespaceName, this.requestProtocol.type);
                                if (!ObjectValidator.IsEmptyOrNull(instance)) {
                                    switch (this.requestProtocol.type) {
                                    case "LiveContentWrapper.InvokeMethod": {
                                        memberName += "_Extern";
                                        if (ObjectValidator.IsFunction(classPrototype[memberName])) {
                                            this.invokeMethod(classPrototype, args, liveResponse, memberName);
                                        } else if (ObjectValidator.IsFunction(instance[memberName])) {
                                            this.invokeMethod(instance, args, liveResponse, memberName);
                                        } else {
                                            liveResponse.OnError("Required method \"" + memberName + "\" " +
                                                "does not exist in class \"" + namespaceName + "\".");
                                        }
                                        break;
                                    }
                                    case "LiveContentWrapper.setProperty": {
                                        if (ObjectValidator.IsSet(classPrototype[memberName])) {
                                            classPrototype[memberName] = args[0];
                                            liveResponse.Send(true);
                                        } else {
                                            liveResponse.OnError("Required property \"" + memberName + "\" " +
                                                "does not exist in class \"" + namespaceName + "\".");
                                        }
                                        break;
                                    }
                                    case "LiveContentWrapper.getProperty": {
                                        if (ObjectValidator.IsSet(classPrototype[memberName])) {
                                            liveResponse.Send(classPrototype[memberName]);
                                        } else {
                                            liveResponse.OnError("Required property \"" + memberName + "\" " +
                                                "does not exist in class \"" + namespaceName + "\".");
                                        }
                                        break;
                                    }
                                    case "LiveContentWrapper.AddEventHandler": {
                                        liveResponse.OnError("Unsupported \"AddEventHandler\" request.");
                                        break;
                                    }
                                    case "LiveContentWrapper.getEvent": {
                                        liveResponse.OnError("Unsupported \"getEvent\" request.");
                                        break;
                                    }
                                    default: {
                                        liveResponse.OnError("Unsupported live content request type: " +
                                            "\"" + this.requestProtocol.type + "\"");
                                    }
                                    }
                                    metrics.End(HttpStatusType.SUCCESS, true);
                                } else {
                                    metrics.End(HttpStatusType.ERROR, true);
                                    this.response.OnError("Unable to load class \"" + namespaceName + "\"");
                                }
                                liveResponse = null;
                                instance = null;
                            } else {
                                metrics.End(HttpStatusType.NOT_AUTHORIZED, true);
                                this.response.OnError("Unable to call any method from required class \"" + namespaceName +
                                    "\" due to security restrictions.");
                            }
                        } catch (ex) {
                            metrics.Error(ex);
                            this.response.OnError(ex);
                        }
                    } else {
                        metrics.End(HttpStatusType.ERROR, true);
                        this.response.OnError("Required class \"" + namespaceName + "\" does not exist.");
                    }
                    namespaceName = "";
                    memberName = "";
                    args = null;
                    classPrototype = null;
                } else {
                    if (this.requestProtocol.type !== "FireEvent") {
                        if (this.requestProtocol.type === "AddEventListener") {
                            LogIt.Info("Events hook for: " + (<ILiveContentProtocol>data).name);
                        } else {
                            LogIt.Info("Resolving request: " + this.requestProtocol.type);
                        }
                    }

                    switch (this.requestProtocol.type) {
                    case "AddEventListener":
                        if (ObjectValidator.IsEmptyOrNull(LiveContentResolver.eventSubscribers)) {
                            LiveContentResolver.eventSubscribers = <any>{};
                        }
                        if (!LiveContentResolver.eventSubscribers.hasOwnProperty(requestData.name)) {
                            LiveContentResolver.eventSubscribers[requestData.name] = [];
                        }
                        LiveContentResolver.eventSubscribers[requestData.name].push({
                            connector: this.response,
                            protocol : this.requestProtocol
                        });
                        this.response.Send(true);
                        break;

                    case "FireEvent":
                        LiveContentResolver.FireEvent(requestData.name,
                            JSON.parse(Buffer.from(requestData.args, "base64").toString("utf8")));
                        this.response.Send(true);
                        break;

                    default:
                        this.resolveCustomProtocol(this.requestProtocol.type, data, this.response);
                        break;
                    }
                    metrics.End(HttpStatusType.SUCCESS, true);
                }
                requestData = null;
            } catch (ex) {
                metrics.Error(ex);
                this.response.OnError(ex);
            }
        } else if (ObjectValidator.IsEmptyOrNull(this.requestProtocol.type)) {
            const message : string = "Received message with incorrect protocol: " + this.requestProtocol;
            metrics.Error(message);
            LogIt.Info(message);
            this.response.OnError("Incorrect request data.");
        }
        data = null;
    }

    protected isAuthorizedFor($token : string, $method : string, $callback : ($status : boolean) => void) : void {
        // override this method if token authorization should be implemented
        $callback(true);
    }

    protected resolveCustomProtocol($type : string, $args : any, $response : IResponse) : void {
        LogIt.Info("Received undefined protocol: " + this.requestProtocol);
        this.response.OnError("Requested protocol \"" + $type + "\" has not been found.");
    }

    private invokeMethod($entity : any, $args : any, $liveResponse : any, $memberName : any) : void {
        $args.push($liveResponse);
        let promise : IAuthPromise = $entity[$memberName].apply($entity, $args); // eslint-disable-line prefer-spread
        if (!ObjectValidator.IsEmptyOrNull(promise) &&
            !ObjectValidator.IsEmptyOrNull(promise.auth) &&
            !ObjectValidator.IsEmptyOrNull(promise.resolve)) {
            this.isAuthorizedFor(this.requestProtocol.token, promise.auth,
                ($status : boolean) : void => {
                    if ($status) {
                        try {
                            promise.resolve.apply($entity, [this.requestProtocol.token, this.requestProtocol.version]);
                        } catch (ex) {
                            Metrics.getInstance().Error(ex);
                            this.response.OnError(ex);
                        }
                        promise = null;
                    } else {
                        this.requestProtocol.status = HttpStatusType.NOT_AUTHORIZED;
                        $liveResponse.SendObject(this.requestProtocol);
                    }
                });
        } else {
            promise = null;
        }
    }
}

export interface IEventSubscriber {
    protocol : IWebServiceProtocol;
    connector : IRequestConnector;
}

// generated-code-start
export const IEventSubscriber = globalThis.RegisterInterface(["protocol", "connector"]);
// generated-code-end

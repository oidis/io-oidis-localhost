/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { LanguageType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LanguageType.js";
import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IAuthPromise } from "../../Interfaces/IResponse.js";
import { BaseRouteArgument, IRoute } from "../../Interfaces/IRoute.js";
import { Loader } from "../../Loader.js";
import { RESTResponse } from "../ResponseApi/Handlers/RESTResponse.js";
import { BaseHttpResolver } from "./BaseHttpResolver.js";

export class RESTResolver extends BaseHttpResolver {
    protected readonly options : IRESTResolverOptions;
    private token : string;

    public static getRoutes() : IRoute[] {
        const routes : IRoute[] = globalThis.oidisAPIRoutes.map(($function) => $function());
        const loaderNamespaces : string[] = Loader.getInstance().getNamespaceHierarchy();
        loaderNamespaces.pop();
        loaderNamespaces.reverse();
        let sorted : IRoute[] = [];
        for (const loaderNamespace of loaderNamespaces) {
            for (const route of routes) {
                if (StringUtils.StartsWith(route.ClassName(), loaderNamespace)) {
                    const register : IRoute = sorted.find(($route : IRoute) =>
                        $route.route === route.route && $route.options.httpMethod === route.options.httpMethod);
                    if (ObjectValidator.IsEmptyOrNull(register)) {
                        sorted.push(route);
                    } else {
                        LogIt.Info("Route " + route.schema + " from " + register.ClassName() + " overrides " + route.ClassName(),
                            LogSeverity.LOW);
                        sorted[sorted.indexOf(register)] = route;
                    }
                }
            }
        }
        sorted = sorted.sort(($a : any, $b : any) : number => {
            return ($a.options.priority < $b.options.priority) ? -1 : ($a.options.priority > $b.options.priority) ? 1 : 0;
        });
        RESTResolver.getRoutes = () : IRoute[] => {
            return sorted;
        };
        return sorted;
    }

    constructor() {
        super();
        this.token = null;
        this.options = {
            created    : new Date().getTime(),
            section    : "",
            subsections: [],
            method     : HttpMethodType.GET,
            lang       : LanguageType.EN,
            force      : false,
            requestType: "",
            acceptType : "",
            etags      : new ArrayList<string>(),
            keys       : new ArrayList<string>()
        };
    }

    protected async accessValidator() : Promise<boolean> {
        this.token = null;
        if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
            this.token = StringUtils.Remove(this.getRequest().getHeaders().getItem("authorization"), "Bearer ");
        } else {
            LogIt.Error("REST resolver is suitable only for back-end calls.");
            return false;
        }
        return super.accessValidator();
    }

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        super.argsHandler($GET, $POST);
        this.options.section = "/" + $GET.getItem("section");
        this.options.subsections = [];
        for (let index : number = 1; index <= 5; index++) {
            if ($GET.KeyExists("subsection" + index)) {
                this.options.subsections.push($GET.getItem("subsection" + index));
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(this.options.subsections)) {
            this.options.section = this.options.section + "/" + this.options.subsections.join("/");
        }
        this.options.force = $GET.KeyExists("force");
        /// TODO: $GET values are sometimes overridden by concurrent request, so getUrlArgs are more stable - root cause is unknown
        this.options.keys = this.getRequest().getUrlArgs();

        const headers : ArrayList<string> = this.getRequest().getHeaders();
        this.options.method = this.getRequest().getOwner().method;
        if (this.options.method === HttpMethodType.POST) {
            const methodOverride : string = headers.getItem("x-http-method-override");
            if (!ObjectValidator.IsEmptyOrNull(methodOverride)) {
                this.options.method = methodOverride;
            }
        }

        this.options.requestType = "json";
        if (headers.KeyExists("content-type")) {
            if (StringUtils.Contains(headers.getItem("content-type"), "/xml")) {
                this.options.requestType = "xml";
            } else if (StringUtils.Contains(headers.getItem("content-type"), "x-www-form-urlencoded")) {
                this.options.requestType = "urlencoded";
            }
        }
        this.options.acceptType = headers.getItem("accept");
        this.options.etags = this.getRequest().getEtags();
    }

    protected async resolver() : Promise<void> {
        this.options.lang = this.getRequest().getHeaders().getItem("accept-language");
        if (ObjectValidator.IsEmptyOrNull(this.options.lang)) {
            this.options.lang = LanguageType.EN;
        }
        this.options.lang = StringUtils.ToLowerCase(this.options.lang);
        if (this.options.lang === "cs") {
            this.options.lang = "cz";
        }

        const data : any = await this.formatRequest();
        const response : RESTResponse = this.getConnector();
        response.setOptions(this.options);

        if (ObjectValidator.IsEmptyOrNull(this.options.section) ||
            this.options.section === "/doc" || this.options.section === "/documentation") {
            response.Send(this.getServerLocation() + " REST API documentation: https://whoknows.where.com");
        } else {
            try {
                const route : IRoute = RESTResolver.getRoutes().find(
                    ($route : IRoute) => $route.route === this.options.section && $route.options.httpMethod === this.options.method);
                if (ObjectValidator.IsEmptyOrNull(route)) {
                    response.SendError("Unsupported method " + this.options.method + " for section " + this.options.section,
                        HttpStatusType.NOT_FOUND);
                } else {
                    const args : any[] = [];
                    // TODO(mkelnar) get arg values from $GET based on args array[*].Key, while array[*].Value is default value
                    const missingKeys : string[] = [];
                    const reservedKeys : string[] = [
                        "force", "section", "subsection1", "subsection2", "subsection3", "subsection4", "subsection5"
                    ];
                    this.options.keys.getKeys().forEach(($key : string) : void => {
                        if (!reservedKeys.includes($key)) {
                            let keyFound : boolean = false;
                            route.options.args.forEach(($value : BaseRouteArgument) : void => {
                                if ($value.Key === $key) {
                                    keyFound = true;
                                }
                            });
                            if (!keyFound) {
                                missingKeys.push($key);
                            }
                        }
                    });
                    if (!ObjectValidator.IsEmptyOrNull(missingKeys)) {
                        throw new Error("Passed parameters [" + missingKeys.join(", ") + "] " +
                            "not declared in route schema: " + route.schema);
                    }
                    route.options.args.forEach(($value : string | BaseRouteArgument) : void => {
                        const routeArg : BaseRouteArgument = <BaseRouteArgument>$value;
                        const keyValue : string = this.options.keys.getItem(routeArg.Key);
                        if (!ObjectValidator.IsEmptyOrNull(keyValue)) {
                            args.push(routeArg.ResolveValue(keyValue));
                        } else if (reservedKeys.includes(routeArg.Key) || !ObjectValidator.IsEmptyOrNull(routeArg.Value)) {
                            args.push(routeArg.Value);
                        } else {
                            args.push(undefined);
                        }
                    });
                    if (this.options.method === HttpMethodType.POST || this.options.method === HttpMethodType.PUT) {
                        args.push(route.options.inputFormatter.format(data));
                    }
                    const methodName : string = route.methodName();
                    await this.invokeMethod(Loader.getInstance().getConnectorsRegister().getConnectorForToken(this.getCurrentToken(),
                            methodName.slice(0, StringUtils.IndexOf(methodName, ".", false)),
                            route.schema),
                        args,
                        response,
                        route);
                }
            } catch (ex) {
                LogIt.Error(ex.message);
                // LogIt.Debug(ex.stack);
                response.OnError(ex.message, ex);
            }
        }
    }

    protected getProtocol() : any {
        let data : any = super.getProtocol();
        if (this.options.method === HttpMethodType.GET) {
            // suppress web response data for GET to propagate into invoker
            data = [];
        }
        return data;
    }

    protected convertValue($value : any, $key : string) : any {
        return $value;
    }

    protected normalizeRequestBody($value : any) : any {
        return $value;
    }

    protected getServerLocation() : string {
        let location : string = this.getRequest().getUrl();
        if (!StringUtils.Contains(location, "http://", "https://")) {
            let host : string = Loader.getInstance().getAppConfiguration().domain.location;
            const requestHeaders : ArrayList<string> = this.getRequest().getHeaders();
            if (requestHeaders.KeyExists("host")) {
                if (!ObjectValidator.IsEmptyOrNull(requestHeaders.getItem("x-forwarded-host"))) {
                    host = requestHeaders.getItem("x-forwarded-host");
                } else {
                    host = requestHeaders.getItem("host");
                }
            }
            location = host + location;
        }
        return location;
    }

    protected async isAuthorizedFor($token : string, $method : string) : Promise<boolean> {
        return true;
    }

    protected getCurrentToken() : string {
        return this.token;
    }

    protected getConnector() : RESTResponse {
        return new RESTResponse(super.getConnector());
    }

    protected browserValidator() : boolean {
        return !this.getRequest().IsSearchBot();
    }

    protected afterLoad() {
        // suppress static page served by afterload default implementation
    }

    protected async formatRequest() : Promise<any> {
        let data : any = <any>super.getProtocol();
        /// TODO: xml etc. should be done in import way, supported by format adapters
        if (this.options.requestType === "xml") {
            try {
                const result : any = await require("xml2js").parseStringPromise(Buffer.from(<string>data).toString("utf-8"), {
                    explicitArray  : false,
                    explicitRoot   : false,
                    parseBooleans  : true,
                    parseNumbers   : true,
                    strict         : true,
                    valueProcessors: [
                        ($value : any, $name : string) : any => {
                            return this.convertValue($value, $name);
                        }
                    ]
                });
                data = this.normalizeRequestBody(JSON.parse(JSON.stringify(result)));
            } catch (ex) {
                LogIt.Warning(ex.stack);
            }
        } else if (this.options.requestType === "urlencoded") {
            let lastKey : string = "";
            data = this.normalizeRequestBody(require("qs").parse(data, {
                decoder: ($value : any, $defaultEncoder : any, $charset : string, $type : string) : any => {
                    $value = $defaultEncoder($value);
                    if ($type === "key") {
                        lastKey = StringUtils.Replace($value, "[", ".");
                        lastKey = StringUtils.Remove(lastKey, "]");
                        lastKey = StringUtils.Substring(lastKey, StringUtils.IndexOf(lastKey, ".", false) + 1);
                    } else if ($type === "value") {
                        $value = this.convertValue($value, lastKey);
                    }
                    return $value;
                }
            }));
        }
        return data;
    }

    private async invokeMethod($entity : any, $args : any[], $response : RESTResponse, $memberName : any) : Promise<void> {
        $args.push($response);
        let promise : IAuthPromise = $memberName.method.apply($entity, $args);
        if (!ObjectValidator.IsEmptyOrNull(promise) &&
            !ObjectValidator.IsEmptyOrNull(promise.auth) &&
            !ObjectValidator.IsEmptyOrNull(promise.resolve)) {
            if (await this.isAuthorizedFor(this.getCurrentToken(), promise.auth)) {
                promise.resolve.apply($entity, [this.getCurrentToken()]);
                promise = null;
            } else {
                $response.SendError("Not authorized", HttpStatusType.NOT_AUTHORIZED);
            }
        } else {
            promise = null;
        }
    }
}

export interface IRESTResolverOptions {
    created : number;
    section : string;
    subsections : string[];
    method : HttpMethodType;
    lang : string;
    force : boolean;
    etags : ArrayList<string>;
    requestType : string;
    acceptType : string;
    keys : ArrayList<string>;
}

// generated-code-start
/* eslint-disable */
export const IRESTResolverOptions = globalThis.RegisterInterface(["created", "section", "subsections", "method", "lang", "force", "etags", "requestType", "acceptType", "keys"]);
/* eslint-enable */
// generated-code-end

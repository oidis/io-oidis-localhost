/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { PageContentBundles, PageContentBundleType } from "@io-oidis-gui/Io/Oidis/Gui/Utils/PageContentBundles.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { BaseHttpResolver } from "./BaseHttpResolver.js";

export class FrontEndLoader extends BaseHttpResolver {

    public Process() : void {
        if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
            StaticPageContentManager.Nonce(this.getNonce());
            StaticPageContentManager.Clear(true);
            this.setContent();
            this.getConnector().Send({
                body  : StaticPageContentManager.ToString(),
                status: this.getResponseStatus()
            });
        } else {
            LogIt.Error(this.getClassName() + " can be used only as front-end loader. " +
                "Validate that HttpResolvers are configured correctly for front-end section.");
        }
    }

    protected setContent() : void {
        StaticPageContentManager.Title(this.getEnvironmentArgs().getProjectName());
        StaticPageContentManager.FaviconSource("resource/graphics/icon.ico");
        StaticPageContentManager.License(this.getLicense());
        PageContentBundles.getInstance().Apply(PageContentBundleType.FONTAWESOME);
        PageContentBundles.getInstance().Apply(PageContentBundleType.SENTRY);

        const buildTime : number = new Date(this.getEnvironmentArgs().getBuildTime()).getTime();

        StaticPageContentManager.BodyEventsEnabled(false);
        StaticPageContentManager.BodyAppend(this.getNoScript());
        StaticPageContentManager.BodyAppend(`
                    <section data-oidis-bind="PageLoader" style="display: none;">
                        ${this.getPageLoader()}
                    </section>
                    <section data-oidis-bind="PageContentHolder" style="display: none;"></section>
                    <script nonce="${this.getNonce()}" type="text/javascript">
                        globalThis.nonce = "${this.getNonce()}";
                        var loader = document.querySelector("[data-oidis-bind=\\"PageLoader\\"]");
                        if (loader) {
                            loader.style.display = "block";
                        }
                    </script>
                    <script nonce="${StaticPageContentManager.Nonce()}" type="module" src="resource/javascript/loader.min.js?v=${buildTime}"></script>
                `);
    }

    protected getLicense() : string {
        return `
<!--

Copyright 2010-2013 Jakub Cieslar
Copyright 2014-2016 Freescale Semiconductor, Inc.
Copyright 2017-2019 NXP
Copyright ${StringUtils.YearFrom(2019)} Oidis

SPDX-License-Identifier: BSD-3-Clause
The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution

or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText

-->
<!-- version: ${this.getEnvironmentArgs().getProjectVersion()}, build: ${this.getEnvironmentArgs().getBuildTime()} -->
`;
    }

    protected getNoScript() : string {
        return `
                    <noscript>
                    This library requires enabled JavaScript in the browser. See link below for more information:
                    <br>
                    <a href="http://www.enable-javascript.com/" target="_blank">How to enable JavaScript?</a>
                    </noscript>
                `;
    }

    protected getPageLoader() : string {
        return `
            <div class="row g-0" style="background: var(--bs-white);height: 100%;">
                <div class="col text-center align-self-center">
                    <span role="status" class="spinner-grow textsecondary text-secondary"></span>
                    <span role="status" class="spinner-grow textsecondary text-secondary"></span>
                    <span role="status" class="spinner-grow textsecondary text-secondary"></span>
                </div>
            </div>`;
    }

    protected getResponseStatus() : number {
        return HttpStatusType.SUCCESS;
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Readable } from "stream";
import { Loader } from "../../Loader.js";
import { FileRequestResolver, IFilePath } from "./FileRequestResolver.js";

export class LoaderResolver extends FileRequestResolver {
    private static clientConfig : any;

    protected getEtag($file : IFilePath, $lastModifiedTime : number) : string {
        const config : any = this.resolveClientConfig();
        const currentSha : string = StringUtils.getSha1($lastModifiedTime + JSON.stringify(LoaderResolver.clientConfig) + $file.path);
        const newSha : string = StringUtils.getSha1($lastModifiedTime + JSON.stringify(config) + $file.path);
        if (currentSha !== newSha) {
            LoaderResolver.clientConfig = config;
        }
        return newSha;
    }

    protected streamProcessor($filePath : string) : any {
        const ReadableClass : any = require("stream").Readable;

        let content : string = Loader.getInstance().getFileSystemHandler().Read(
            Loader.getInstance().getProgramArgs().ProjectBase() + "/resource/javascript/loader.min.js").toString();
        content = StringUtils.Replace(content, JSON.stringify(globalThis.clientConfig), `"{}"`);
        content = StringUtils.Replace(content, globalThis.clientConfig, `{}`);
        const beStartMark : string = `"beLoaderStart"`;
        let beEndMark : string = `"beLoaderEnd")`;
        if (!content.includes(beEndMark)) {
            beEndMark = `"beLoaderEnd";`;
        }
        const beStart : number = content.indexOf(beStartMark) + beStartMark.length;
        const beEnd : number = content.indexOf(beEndMark) + beEndMark.length;
        const beCode : string = content.substring(beStart, beEnd);
        content = StringUtils.Remove(content, beCode);
        content = StringUtils.Replace(content, beStartMark, `""`);

        const patterns : string[] = [
            "*pass*",
            "*token*"
        ];
        let loaderContent : string = JSON.stringify(LoaderResolver.clientConfig, ($key : string, $value : any) : any => {
            let passed : boolean = true;
            patterns.forEach(($pattern : string) : void => {
                if (passed && StringUtils.PatternMatched($pattern, StringUtils.ToLowerCase($key))) {
                    passed = false;
                }
            });
            if (!passed && !ObjectValidator.IsEmptyOrNull($value)) {
                LogIt.Warning("Client config contains sensitive information. " +
                    "Please validate project configuration and settings of embedFeatures. Value removed for key: " + $key);
                return undefined;
            }
            return $value;
        });
        loaderContent = JSON.stringify(loaderContent);
        content += `
globalThis.clientConfig = JSON.parse("${loaderContent.substring(1, loaderContent.length - 1)}");
`;
        const stream : Readable = new ReadableClass();
        stream.push(content);
        stream.push(null);
        return stream;
    }

    private resolveClientConfig() : any {
        const clientConfig : any = JSON.parse(globalThis.clientConfig);
        globalThis.embedFeatures.forEach(($feature : string) : void => {
            let source : any = {
                project: JsonUtils.Clone(this.getEnvironmentArgs().getProjectConfig())
            };
            const namespaces : string[] = StringUtils.Split($feature, ".");
            let found : boolean = false;
            for (const item of namespaces) {
                if (source.hasOwnProperty(item)) {
                    source = source[item];
                    found = true;
                } else {
                    found = false;
                }
            }
            if (found) {
                const target : any = {project: {}};
                let index : number;
                let item : any = target;
                for (index = 0; index < namespaces.length; index++) {
                    if (index < namespaces.length - 1) {
                        item = item[namespaces[index]] = {};
                    } else {
                        item[namespaces[index]] = source;
                    }
                }
                JsonUtils.Extend(clientConfig, target.project);
            }
        });
        return clientConfig;
    }
}

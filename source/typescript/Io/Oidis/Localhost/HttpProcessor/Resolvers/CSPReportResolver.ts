/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { ISentryConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IProject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IResponseProtocol } from "../../Interfaces/IResponse.js";
import { Loader } from "../../Loader.js";
import { BaseHttpResolver } from "./BaseHttpResolver.js";
import { SentryTunnelResolver } from "./SentryTunnelResolver.js";

export class CSPReportResolver extends BaseHttpResolver {

    protected async resolver() : Promise<void> {
        const data : string = <any>this.getProtocol();
        const config : ISentryConfiguration = Loader.getInstance().getEnvironmentArgs().getProjectConfig().sentry;
        if (!ObjectValidator.IsEmptyOrNull(config.dsn) && config.cspReportTunnel) {
            this.getConnector().Send(await (new SentryTunnelResolver()).Proxy(data, true));
        } else {
            LogIt.Warning(JSON.stringify(JSON.parse(data), null, 2));

            this.getConnector().Send(<IResponseProtocol>{
                body  : "CSP Report received successfully.",
                status: HttpStatusType.SUCCESS
            });
        }
    }
}

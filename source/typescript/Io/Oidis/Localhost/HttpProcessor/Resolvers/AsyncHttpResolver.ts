/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { HttpRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseHttpResolver } from "./BaseHttpResolver.js";

export class AsyncHttpResolver extends BaseHttpResolver {
    public RequestArgs($value? : AsyncRequestEventArgs) : AsyncRequestEventArgs {
        return <AsyncRequestEventArgs>super.RequestArgs($value);
    }

    public Process() : void {
        if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
            super.Process();
        } else {
            if (!this.RequestArgs().IsMemberOf(AsyncRequestEventArgs)) {
                const origArgs : HttpRequestEventArgs = this.RequestArgs();
                const newArgs : AsyncRequestEventArgs = this.RequestArgs(new AsyncRequestEventArgs(origArgs.Url(), origArgs.POST()));
                this.argsHandler(newArgs.GET(), newArgs.POST());
            }
            this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_START, this.RequestArgs());
            this.resolver();
        }
    }

    protected success() : void {
        this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_SUCCESS, this.RequestArgs());
        this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE, this.RequestArgs());
    }

    protected error() : void {
        this.RequestArgs().Status(HttpStatusType.ERROR);
        this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_ERROR, this.RequestArgs());
        this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE, this.RequestArgs());
    }

    protected resolver() : void {
        if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
            super.resolver();
        } else {
            this.success();
        }
    }

    protected getHostUrl() : string {
        const headers : ArrayList<string> = this.getRequest().getHeaders();
        if (headers.KeyExists("host")) {
            let address : string = headers.getItem("host");
            let scheme : string = this.getRequest().getScheme();
            if (!ObjectValidator.IsEmptyOrNull(headers.getItem("referer"))) {
                scheme = StringUtils.Contains(headers.getItem("referer"), "https://") ? "https://" : "http://";
            }
            if (!ObjectValidator.IsEmptyOrNull(headers.getItem("x-forwarded-host"))) {
                address = headers.getItem("x-forwarded-host");
            }
            let host : string = scheme + address;
            if (!StringUtils.EndsWith(host, "/")) {
                host += "/";
            }
            return host.trim();
        }
        return this.getHttpManager().getRequest().getHostUrl();
    }
}

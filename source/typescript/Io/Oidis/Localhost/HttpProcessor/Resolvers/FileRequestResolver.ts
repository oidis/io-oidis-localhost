/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FileSystemHandler } from "../../Connectors/FileSystemHandler.js";
import { IProject } from "../../Interfaces/IProject.js";
import { ScriptHandler } from "../../IOApi/Handlers/ScriptHandler.js";
import { Loader } from "../../Loader.js";
import { BaseHttpResolver } from "./BaseHttpResolver.js";

export class FileRequestResolver extends BaseHttpResolver {
    private static patterns : string[];
    private static targetAppDataPath : string;

    public Process() : void {
        if (!this.browserValidator()) {
            LogIt.Error("Unsupported browser. User Agent: " + this.getRequest().getUserAgent());
            this.getHttpManager().ReloadTo("/ServerError/Browser", null, true);
            this.stop();
        } else {
            this.resolver();
        }
    }

    protected browserValidator() : boolean {
        return !this.getRequest().IsSearchBot();
    }

    protected async filePathResolver() : Promise<IFilePath> {
        const fs : any = require("fs");
        const path : any = require("path");
        let url : string = this.getRequest().getUrl();
        if (StringUtils.Contains(url, "?")) {
            url = StringUtils.Substring(url, 0, StringUtils.IndexOf(url, "?"));
        }
        const resourcePath : string = "/resource/";
        if (!StringUtils.StartsWith(url, resourcePath) && StringUtils.Contains(url, resourcePath) &&
            !StringUtils.Contains(url, "/sass/") ||
            StringUtils.OccurrenceCount(url, resourcePath) > 1) {
            const searchFor : string = StringUtils.Contains(url, "/test" + resourcePath) ? "/test/" + resourcePath : resourcePath;
            url = StringUtils.Substring(url, StringUtils.IndexOf(url, searchFor, false));
        }

        let filePath : string = Loader.getInstance().getProgramArgs().TargetBase() + url;
        if (!fs.existsSync(filePath)) {
            if (url === "/favicon.ico") {
                filePath = Loader.getInstance().getProgramArgs().TargetBase() + resourcePath + "graphics/icon.ico";
                if (!fs.existsSync(filePath)) {
                    filePath = Loader.getInstance().getProgramArgs().ProjectBase() + resourcePath + "graphics/icon.ico";
                }
            } else {
                filePath = FileRequestResolver.targetAppDataPath + "/" + url;
            }
        }
        const name = path.basename(filePath);
        let extension = name;
        extension = StringUtils.Substring(extension,
            StringUtils.IndexOf(name, ".", false) + 1, StringUtils.Length(name));
        extension = StringUtils.ToLowerCase(extension);
        if (extension === "scss") {
            extension = "sass";
        }
        if (extension === "ts" || extension === "tsx" || extension === "sass") {
            filePath = Loader.getInstance().getProgramArgs().TargetBase() + "/../.." + url;
        }
        filePath = path.normalize(filePath);
        return {
            extension,
            name,
            path: filePath,
            url
        };
    }

    protected getAllowedPatterns() : string[] {
        if (ObjectValidator.IsSet(FileRequestResolver.patterns)) {
            return FileRequestResolver.patterns;
        }
        let patterns : string[] = [
            "/connector.config.js*",
            "/LICENSE.txt",
            "/favicon.ico",
            "/resource/css/*",
            "/resource/graphics/*",
            "/resource/esmodules-*/*",
            "/resource/javascript/*",
            "/resource/data/Io/Oidis/Services/*",
            "/resource/libs/FontOxygen/*",
            "/test/resource/data/*",
            "*.ts",
            "*.tsx",
            "*.scss",
            "*.sass"
        ];
        const loader : Loader = Loader.getInstance();
        const properties : IProject = loader.getAppConfiguration();
        const fileSystem : FileSystemHandler = loader.getFileSystemHandler();
        const targetPath : string = loader.getProgramArgs().TargetBase();
        let appDataPath : string = loader.getProgramArgs().AppDataPath();

        let config : string[] = properties.target.requestPatterns;
        if (ObjectValidator.IsString(config)) {
            config = StringUtils.Split(<any>config, ",");
        }
        if (!ObjectValidator.IsEmptyOrNull(config)) {
            patterns = patterns.concat(config);
        }
        const configPath : string = targetPath + "/wuirunner.config.jsonp";
        if (properties.connectorProxy && fileSystem.Exists(configPath)) {
            const handler : ScriptHandler = new ScriptHandler();
            handler.Path(configPath);
            handler.SuccessHandler(() : void => {
                const runnerConfig : any = handler.Data();
                config = runnerConfig.requestPatterns;
                if (ObjectValidator.IsString(config)) {
                    config = StringUtils.Split(<any>config, ",");
                }
                if (!ObjectValidator.IsEmptyOrNull(config)) {
                    patterns = patterns.concat(config);
                }
                if (!ObjectValidator.IsEmptyOrNull(runnerConfig.appDataPath)) {
                    appDataPath = runnerConfig.appDataPath;
                }
            });
            handler.ErrorHandler(($error : Error) : void => {
                LogIt.Error("Unable to load " + configPath, $error);
            });
        }
        FileRequestResolver.patterns = patterns;
        FileRequestResolver.targetAppDataPath = appDataPath;
        this.getAllowedPatterns = () : string[] => {
            return patterns;
        };
        return patterns;
    }

    protected resolver() : void {
        const fs : any = require("fs");
        const patterns : string[] = this.getAllowedPatterns();
        this.filePathResolver().then(($filePath : IFilePath) : void => {
            const requiredUrl : string = StringUtils.Replace($filePath.url, "\\", "/");
            if (!StringUtils.Contains(requiredUrl, "/resource/esmodules-")) {
                LogIt.Debug("file request for: {0}", $filePath.path);
            }
            if (!ObjectValidator.IsEmptyOrNull($filePath.blob) ||
                ObjectValidator.IsEmptyOrNull($filePath.blob) && fs.existsSync($filePath.path)) {
                let allowedPath : boolean = false;
                patterns.forEach(($pattern : string) : void => {
                    if (StringUtils.PatternMatched($pattern, requiredUrl)) {
                        allowedPath = true;
                    }
                });
                if (allowedPath) {
                    let type : string = "text/html";
                    let lastModifiedTime : number;
                    let fileSize : number;
                    if (!ObjectValidator.IsEmptyOrNull($filePath.blob)) {
                        lastModifiedTime = $filePath.blob.mtime;
                        fileSize = $filePath.blob.size;
                    } else {
                        const stats : any = fs.statSync($filePath.path);
                        lastModifiedTime = stats.mtime.getTime();
                        fileSize = stats.size;
                    }
                    const lastModifiedTimeGMT : string = Convert.TimeToGMTformat(lastModifiedTime);
                    const eTag : string = this.getEtag($filePath, lastModifiedTime);

                    let isCached : boolean = true;
                    if (this.getRequest().getUrlArgs().KeyExists("refresh") ||
                        StringUtils.Contains(this.getRequest().getUrl(), "/forceRefresh")) {
                        isCached = false;
                    }
                    if (this.getRequest().getLastModifiedTime() !== lastModifiedTimeGMT) {
                        isCached = false;
                    }
                    if (!this.getRequest().getEtags().IsEmpty() && !this.getRequest().getEtags().Contains(eTag)) {
                        isCached = false;
                    }
                    if (!isCached) {
                        const headers : any = {
                            "pragma"       : "public",
                            "expires"      : 0,
                            "etag"         : eTag,
                            "Last-Modified": lastModifiedTimeGMT
                        };
                        switch ($filePath.extension) {
                        case "htm":
                        case "html":
                            type = "text/html";
                            break;
                        case "txt":
                        case "ts":
                        case "tsx":
                        case "sass":
                        case "log":
                        case "obj":
                            type = "text/plain";
                            break;
                        case "css":
                            headers["accept-ranges"] = "bytes";
                            type = "text/css";
                            break;
                        case "js":
                        case "jsonp":
                        case "map":
                            headers["accept-ranges"] = "bytes";
                            type = "application/javascript";
                            break;
                        case "json":
                            headers["accept-ranges"] = "bytes";
                            type = "application/json";
                            break;
                        case "pdf":
                            type = "application/pdf";
                            break;
                        case "bmp":
                            headers["accept-ranges"] = "bytes";
                            type = "image/bmp";
                            break;
                        case "jpg":
                        case "jpe":
                        case "jpeg":
                            headers["accept-ranges"] = "bytes";
                            type = "image/jpeg";
                            break;
                        case "png":
                            headers["accept-ranges"] = "bytes";
                            type = "image/png";
                            break;
                        case "gif":
                            headers["accept-ranges"] = "bytes";
                            type = "image/gif";
                            break;
                        case "ico":
                        case "icon":
                            type = "image/x-icon";
                            break;
                        case "woff":
                            type = "application/font-woff";
                            break;
                        case "woff2":
                            type = "application/font-woff2";
                            break;
                        case "eot":
                            type = "application/vnd.ms-fontobject";
                            break;
                        case "otf":
                            type = "font/opentype";
                            break;
                        case "ttf":
                            type = "application/x-font-ttf";
                            break;
                        case "svg":
                            type = "image/svg+xml";
                            break;
                        case "xhml":
                        case "xml":
                            type = "text/xml";
                            break;
                        case "wasm":
                            type = "application/wasm";
                            break;
                        default :
                            if ($filePath.extension !== "ts" && $filePath.extension !== "tsx" && $filePath.extension !== "sass") {
                                const fileName : string = ObjectEncoder.Url($filePath.name);
                                headers["cache-control"] = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0";
                                headers["accept-ranges"] = "bytes";
                                headers["content-encoding"] = "identity";
                                headers["content-transfer-encoding"] = "binary";
                                headers["content-description"] = "File Transfer";
                                headers["Content-Length"] = fileSize;
                                headers["content-range"] = "0-" + (fileSize - 1) + "/" + fileSize;
                                headers["content-disposition"] =
                                    "attachment; filename*=UTF-8''" + fileName + "; filename=\"" + fileName + "\"";
                                headers["x-xss-protection"] = "1; mode=block";
                                headers["x-content-type-options"] = "nosniff";
                                headers["x-frame-options"] = "SAMEORIGIN";
                                headers["x-robots-tag"] = "none";
                                headers["x-download-options"] = "noopen";
                                headers["x-permitted-cross-domain-policies"] = "none";
                                type = "application/octet-stream";
                            }
                        }
                        headers["content-type"] = type;
                        this.getConnector().Send({
                            body  : !ObjectValidator.IsEmptyOrNull($filePath.blob) ? $filePath.blob.data :
                                this.streamProcessor($filePath.path),
                            headers,
                            status: HttpStatusType.SUCCESS
                        });
                    } else {
                        this.getConnector().Send({
                            body  : "",
                            status: HttpStatusType.CACHED
                        });
                    }
                } else {
                    LogIt.Debug("Access denied from: {0}", $filePath.url);
                    this.getConnector().Send({
                        body  : "Access denied for: " +
                            (ObjectValidator.IsEmptyOrNull($filePath.path) ? $filePath.url : $filePath.path),
                        status: HttpStatusType.FORBIDDEN
                    });
                }
            } else {
                this.getConnector().Send({
                    body  : "Unable to locate: " + (ObjectValidator.IsEmptyOrNull($filePath.path) ? $filePath.url : $filePath.path),
                    status: HttpStatusType.NOT_FOUND
                });
            }
        });
    }

    protected streamProcessor($filePath : string) : any {
        return require("fs").createReadStream($filePath);
    }

    protected getEtag($file : IFilePath, $lastModifiedTime : number) : string {
        return StringUtils.getSha1($lastModifiedTime + $file.path);
    }
}

export interface IFilePath {
    url : string;
    readonly path : string;
    readonly name : string;
    readonly extension : string;
    readonly blob? : IFileBlob;
}

export interface IFileBlob {
    readonly size : number;
    readonly mtime : number;
    readonly data : Buffer;
}

// generated-code-start
export const IFilePath = globalThis.RegisterInterface(["url", "path", "name", "extension", "blob"]);
export const IFileBlob = globalThis.RegisterInterface(["size", "mtime", "data"]);
// generated-code-end

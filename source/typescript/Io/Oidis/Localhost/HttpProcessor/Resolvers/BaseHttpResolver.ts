/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseHttpResolver as Parent } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/GeneralEventOwner.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { IResponseProtocol } from "../../Interfaces/IResponse.js";
import { HttpRequestParser } from "../HttpRequestParser.js";
import { HttpServer } from "../HttpServer.js";
import { WebResponse } from "../ResponseApi/Handlers/WebResponse.js";

export class BaseHttpResolver extends Parent {
    private connector : WebResponse;
    private protocol : IResponseProtocol;

    public Process() : void {
        super.Process();
        if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
            this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_LOAD, () : void => {
                this.afterLoad();
            });
        }
    }

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        if ($POST.KeyExists("Connector")) {
            this.connector = $POST.getItem("Connector");
        }
        if ($POST.KeyExists("Protocol")) {
            this.protocol = $POST.getItem("Protocol");
        }
    }

    protected getRequest() : HttpRequestParser {
        return <HttpRequestParser>super.getRequest();
    }

    protected getConnector() : WebResponse {
        if (ObjectValidator.IsEmptyOrNull(this.connector) && !this.getEnvironmentArgs().HtmlOutputAllowed()) {
            this.connector = (<any>HttpServer).httpArgs.getLast().POST().getItem("Connector");
        }
        return this.connector;
    }

    protected getProtocol() : IResponseProtocol {
        return this.protocol;
    }

    protected afterLoad() : void {
        this.getConnector().Send(StaticPageContentManager.ToString());
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IHttpResolverContext } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { HttpResolver as Parent } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/HttpResolver.js";

export class GuiHttpResolver extends Parent {

    protected async getStartupResolvers($context : IHttpResolverContext) : Promise<any> {
        if ($context.isBrowser) {
            if (!$context.isProd) {
                await this.registerResolver(
                    async () => (await import("../Controllers/Pages/MainPageController.js")).MainPageController,
                    "/main");
            }
            await this.registerResolver(async () => (
                    await import("@io-oidis-gui/Io/Oidis/Gui/ErrorPages/Http404NotFoundPage.js")).Http404NotFoundPage,
                "/ServerError/Http/DefaultPage");
            await this.registerResolver(async () => {
                if (!$context.isProd) {
                    return (await import("../Index.js")).Index;
                }
                return (await import("../Controllers/Pages/MainPageController.js")).MainPageController;
            }, "/", "/index", "/index.html", "/web/");
        }
        return super.getStartupResolvers($context);
    }
}

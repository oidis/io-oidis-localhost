/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IResponse } from "../../Interfaces/IResponse.js";
import { BaseResponse } from "./Handlers/BaseResponse.js";
import { CallbackResponse } from "./Handlers/CallbackResponse.js";

export class ResponseFactory extends BaseObject {

    public static getResponse($callback : ((...$args : any[]) => void) | IResponse) : IResponse {
        let response : IResponse;
        if (!($callback instanceof BaseResponse)) {
            if (ObjectValidator.IsFunction($callback)) {
                response = new CallbackResponse(<(...$args : any[]) => void>$callback);
            } else {
                response = new BaseResponse();
            }
        } else {
            response = <IResponse>$callback;
        }
        return response;
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IWebServiceProtocol } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceProtocol.js";
import { ErrorLocalizable } from "../../../Exceptions/Type/ErrorLocalizable.js";
import { IRequestConnector } from "../../../Interfaces/IRequestConnector.js";
import { IResponse, IResponsePromise } from "../../../Interfaces/IResponse.js";
import { IRESTResponseOptions } from "../../../Interfaces/IRESTResponse.js";
import { IHttpResponse } from "../../../Interfaces/IRoute.js";
import { HttpServer } from "../../HttpServer.js";
import { WebResponse } from "./WebResponse.js";

export class RESTResponse extends WebResponse {
    protected acceptType : string;
    protected etags : ArrayList<string>;
    protected force : boolean;

    constructor($owner? : IRequestConnector | IResponse, $data? : IWebServiceProtocol) {
        super($owner, $data);
    }

    public setOptions($value : IRESTResponseOptions) : void {
        if (ObjectValidator.IsSet($value.acceptType)) {
            this.acceptType = $value.acceptType;
        }
        if (ObjectValidator.IsSet($value.etags)) {
            this.etags = $value.etags;
        }
        if (ObjectValidator.IsSet($value.force)) {
            this.force = $value.force;
        }
    }

    public Send($args : any) : IResponsePromise {
        const response = this.formatContent(this.formatResponse($args, HttpStatusType.SUCCESS));
        if (response.status === HttpStatusType.SUCCESS ||
            response.status === HttpStatusType.CONTINUE) {
            const eTag : string = StringUtils.getSha1(<string>JSON.stringify(response.body));
            if (this.etags.IsEmpty() || !this.etags.Contains(eTag) || this.force) {
                if ((this.owner.method === HttpMethodType.POST || this.owner.method === HttpMethodType.GET)) {
                    JsonUtils.Extend(response.headers, {
                        etag: eTag
                    });
                }
                this.owner.Send(response);
            } else {
                this.owner.Send({
                    body  : "",
                    status: HttpStatusType.CACHED
                });
            }
        } else {
            this.owner.Send(response);
        }
        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.onsend = $callback;
            }
        };
    }

    public SendError($message : string | Error, $status : HttpStatusType) : void {
        let message : string = <string>$message;
        if (!ObjectValidator.IsString($message)) {
            message = (<Error>$message).message;
        }
        this.owner.Send({
            body  : message,
            status: $status
        });
    }

    public OnError($message : string | Error, ...$args : any[]) : void {
        // TODO(mkelnar) should we send stack or not, I think than no and just log it as warning
        let stack : string = null;
        if (ObjectValidator.IsSet((<Error>$message).message)) {
            if (ObjectValidator.IsSet((<any>$message).stack)) {
                stack = Buffer.from((<any>$message).stack).toString("base64");
            }
            if (ObjectValidator.IsSet((<ErrorLocalizable>$message).localizations) &&
                (<ErrorLocalizable>$message).localizations.hasOwnProperty(this.data.language)) {
                $message = (<ErrorLocalizable>$message).localizations[this.data.language];
            } else {
                $message = (<Error>$message).message;
            }
        } else if (!ObjectValidator.IsString($message)) {
            $message = JSON.stringify($message);
        }
        if (ObjectValidator.IsEmptyOrNull($message)) {
            $message = "";
        }
        let response : any;
        if (!ObjectValidator.IsEmptyOrNull($args)) {
            response = this.formatResponse({
                args   : Buffer.from(JSON.stringify($args)).toString("base64"),
                message: $message,
                stack
            }, HttpStatusType.ERROR);
        } else {
            response = this.formatResponse($message, HttpStatusType.ERROR);
        }
        response = this.formatContent(response);
        response.status = HttpStatusType.ERROR;
        this.owner.Send(response);
    }

    public getRequestHeaders() : ArrayList<string> {
        const args : AsyncRequestEventArgs = HttpServer.getRequestArgs(this);
        if (!ObjectValidator.IsEmptyOrNull(args)) {
            return args.Owner().getHeaders();
        }
        return new ArrayList<string>();
    }

    protected formatResponse($data : any, $status : HttpStatusType) : IHttpResponse {
        return {
            status : $status,
            headers: {},
            body   : $data
        };
    }

    protected formatContent($response : any) : any {
        if (StringUtils.ContainsIgnoreCase(this.acceptType, "/xml")) {
            $response.headers = JsonUtils.Extend(this.owner.headers, {
                "content-type": "application/xml; charset=UTF-8"
            });
            $response.body = new (require("xml2js")).Builder().buildObject($response.body);
        } else if (StringUtils.ContainsIgnoreCase(this.acceptType, "/x-www-form-urlencoded")) {
            $response.headers = JsonUtils.Extend($response.headers, {
                "content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            });
            $response.body = require("qs").stringify($response.body);
        } else {
            $response.headers = JsonUtils.Extend($response.headers, {
                "content-type": "application/json; charset=UTF-8"
            });
            $response.body = <any>JSON.stringify($response.body);
        }
        return $response;
    }
}

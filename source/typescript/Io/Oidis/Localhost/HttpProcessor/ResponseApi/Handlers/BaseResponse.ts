/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { IResponse, IResponsePromise } from "../../../Interfaces/IResponse.js";

export class BaseResponse extends BaseObject implements IResponse {
    protected owner : any;
    protected callbacks : any;
    protected keepAlive : boolean;
    private threadsRegister : any[];

    constructor() {
        super();
        this.owner = () : void => {
            // declare owner
        };
        this.callbacks = {
            onsend(...$args : any[]) : void {
                // declare promise callback
            },
            onstart(...$args : any[]) : void {
                // declare promise callback
            },
            onchange(...$args : any[]) : void {
                // declare promise callback
            },
            oncomplete(...$args : any[]) : void {
                // declare promise callback
            }
        };
        this.threadsRegister = [];
        this.keepAlive = false;
    }

    public Clone($instance : IResponse) : void {
        this.owner = (<BaseResponse>$instance).owner;
        this.callbacks = (<BaseResponse>$instance).callbacks;
        this.threadsRegister = (<BaseResponse>$instance).threadsRegister;
    }

    public Send(...$args : any[]) : IResponsePromise {
        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.onsend = $callback;
            }
        };
    }

    public OnStart(...$args : any[]) : IResponsePromise {
        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.onstart = $callback;
            }
        };
    }

    public OnChange(...$args : any[]) : IResponsePromise {
        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.onchange = $callback;
            }
        };
    }

    public OnComplete(...$args : any[]) : IResponsePromise {
        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.oncomplete = $callback;
            }
        };
    }

    public OnError($message : string | Error, ...$args : any[]) : void {
        if (ObjectValidator.IsString($message)) {
            LogIt.Error(<string>$message);
        } else {
            LogIt.Error(this.getClassName(), <Error>$message);
        }
    }

    public OnMessage($data : any) : void {
        if (!ObjectValidator.IsArray($data)) {
            $data = [$data];
        }
        this.callbacks.onsend.apply(this, $data);
        this.callbacks.onstart.apply(this, $data);
        this.callbacks.onchange.apply(this, $data);
        this.callbacks.oncomplete.apply(this, $data);
    }

    public async FireEvent($name : string, $args? : any) : Promise<void> {
        const LiveContentResolver : any = (await import("../../Resolvers/LiveContentResolver.js")).LiveContentResolver;
        LiveContentResolver.FireEvent($name, $args);
    }

    public getId() : number {
        return -1;
    }

    public getOwnerId() : string {
        return "";
    }

    public AddAbortHandler($handler : () => void) : void {
        this.KeepAlive(true);
        this.threadsRegister.push($handler);
    }

    public Abort() : void {
        this.threadsRegister.forEach(($handler : () => void) : void => {
            $handler();
        });
    }

    public KeepAlive($value? : boolean) : boolean {
        return this.keepAlive = Property.Boolean(this.keepAlive, $value);
    }

    public Dispatch() : void {
        if (ObjectValidator.IsSet(this.owner.getParentProcess) && !ObjectValidator.IsEmptyOrNull(this.owner.getParentProcess())) {
            this.owner.getParentProcess().Clear(this);
        }
    }
}

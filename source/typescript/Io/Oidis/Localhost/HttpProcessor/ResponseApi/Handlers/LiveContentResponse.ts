/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { ILiveContentProtocol, IWebServiceProtocol } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceProtocol.js";
import { IRequestConnector } from "../../../Interfaces/IRequestConnector.js";
import { IResponse, IResponsePromise } from "../../../Interfaces/IResponse.js";
import { WebResponse } from "./WebResponse.js";

export class LiveContentResponse extends WebResponse {
    private started : boolean;
    private changed : boolean;
    private completed : boolean;
    private sent : boolean;

    constructor($owner? : IRequestConnector | IResponse, $data? : IWebServiceProtocol) {
        super($owner, $data);
        this.started = false;
        this.changed = false;
        this.completed = false;
        this.sent = false;
    }

    public Send(...$args : any[]) : IResponsePromise {
        const response : ILiveContentProtocol = <ILiveContentProtocol>{
            args       : "",
            name       : (<ILiveContentProtocol>this.data.data).name,
            returnValue: null
        };
        if (ObjectValidator.IsSet($args[0])) {
            response.returnValue = $args[0];
            $args.shift();
        }
        if ($args.length > 0) {
            response.args = <any>$args;
        }
        if (!ObjectValidator.IsString(response.returnValue)) {
            if (!ObjectValidator.IsEmptyOrNull(response.returnValue) && !ObjectValidator.IsEmptyOrNull(response.returnValue.type)) {
                if (response.returnValue.type === EventType.ON_START) {
                    if (this.started) {
                        throw new Error("LCP Error: onstart notify can be send only once");
                    }
                    this.started = true;
                    if (this.completed) {
                        throw new Error("LCP Error: cannot notify onstart after oncomplete");
                    } else if (this.changed) {
                        throw new Error("LCP Error: cannot notify onstart after onchange");
                    }
                } else if (response.returnValue.type === EventType.ON_CHANGE) {
                    this.changed = true;
                    if (this.completed) {
                        throw new Error("LCP Error: cannot notify onchange after oncomplete");
                    }
                } else if (response.returnValue.type === EventType.ON_COMPLETE) {
                    if (this.completed) {
                        throw new Error("LCP Error: oncomplete notify can be send only once");
                    }
                    this.completed = true;
                } else if (this.completed) {
                    // TODO: maybe it needs to include also other type like "Request.Exception"
                    LogIt.Debug(">other:" + response.returnValue.type);
                }
            }
            response.returnValue = JSON.stringify(response.returnValue);
        } else {
            if (this.sent) {
                throw new Error("LCP Error: raw message can be send only once");
            }
            this.sent = true;
        }
        response.returnValue = Buffer.from(response.returnValue).toString("base64");

        if (!ObjectValidator.IsString(response.args)) {
            response.args = JSON.stringify(response.args);
        }
        response.args = Buffer.from(response.args).toString("base64");

        this.data.data = response;
        this.data.status = HttpStatusType.SUCCESS;

        return this.SendObject(this.data);
    }

    public SendObject($object : IWebServiceProtocol) : IResponsePromise {
        this.owner.Send($object);
        if (!this.keepAlive) {
            this.Dispatch();
        }
        return {
            Then: ($callback : ($data : string | ILiveContentProtocol) => void) : void => {
                this.callbacks.oncomplete = $callback;
            }
        };
    }
}

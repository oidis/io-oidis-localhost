/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { IRESTResponse } from "../../../Interfaces/IRESTResponse.js";
import { IHttpResponse } from "../../../Interfaces/IRoute.js";
import { RESTResponse } from "./RESTResponse.js";

export class InBodyStatusResponse extends RESTResponse {

    protected formatResponse($data : any, $status : HttpStatusType) : IHttpResponse {
        return {
            status : HttpStatusType.SUCCESS,
            headers: {},
            body   : this.formatBody($data, $status)
        };
    }

    protected formatBody($data : any, $status : HttpStatusType) : IRESTResponse {
        const body : IRESTResponse = {
            code  : $status,
            status: ""
        };
        if ($status === HttpStatusType.SUCCESS || $status === HttpStatusType.CONTINUE) {
            body.data = $data;
            body.status = "success";
        } else {
            body.message = $data;
            body.status = "error";
        }
        return body;
    }
}

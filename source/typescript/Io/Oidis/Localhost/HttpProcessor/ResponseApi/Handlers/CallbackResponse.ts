/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IResponsePromise } from "../../../Interfaces/IResponse.js";
import { BaseResponse } from "./BaseResponse.js";

export class CallbackResponse extends BaseResponse {

    constructor($owner : (...$args : any[]) => void) {
        super();
        this.owner = $owner;
    }

    public Send(...$args : any[]) : IResponsePromise {
        setTimeout(() : void => {
            this.owner.apply(this, $args); // eslint-disable-line prefer-spread
        }, 0);
        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.onsend = $callback;
            }
        };
    }

    public OnComplete(...$args : any[]) : IResponsePromise {
        setTimeout(() : void => {
            this.owner.apply(this, $args); // eslint-disable-line prefer-spread
        }, 0);
        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.oncomplete = $callback;
            }
        };
    }
}

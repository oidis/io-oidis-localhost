/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IResponsePromise } from "../../../Interfaces/IResponse.js";
import { RESTResponse as Parent } from "./RESTResponse.js";

export class StreamResponse extends Parent {

    public Send($args : any) : IResponsePromise {
        if (!ObjectValidator.IsEmptyOrNull($args.body) && !Buffer.isBuffer($args.body)) {
            if (ObjectValidator.IsObject($args.body)) {
                $args.body = JSON.stringify($args.body);
            } else {
                $args.body = JSON.stringify({message: $args.body, status: $args.status});
            }
            $args.headers = JsonUtils.Extend({
                "content-type": "application/json; charset=utf-8"
            }, $args.headers);
        }
        this.owner.Send($args);
        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.onsend = $callback;
            }
        };
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IWebServiceException, IWebServiceProtocol } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceProtocol.js";
import { ErrorLocalizable } from "../../../Exceptions/Type/ErrorLocalizable.js";
import { IRequestConnector } from "../../../Interfaces/IRequestConnector.js";
import { IResponse, IResponsePromise } from "../../../Interfaces/IResponse.js";
import { BaseResponse } from "./BaseResponse.js";

export class WebResponse extends BaseResponse {
    protected data : IWebServiceProtocol;

    constructor($owner? : IRequestConnector | IResponse, $data? : IWebServiceProtocol) {
        super();
        if (!ObjectValidator.IsEmptyOrNull($owner) && $owner instanceof WebResponse) {
            this.Clone($owner);
        } else {
            this.data = $data;
            this.owner = $owner;
        }
    }

    public Clone($instance : IResponse) : void {
        super.Clone($instance);
        if (ObjectValidator.IsSet((<WebResponse>$instance).data)) {
            this.data = (<WebResponse>$instance).data;
            try {
                this.data.data = JSON.parse(Buffer.from(<string>this.data.data, "base64").toString("utf8"));
            } catch (ex) {
                // pass data as is, because they can be already in correct format
            }
        }
    }

    public Send(...$args : any[]) : IResponsePromise {
        this.data.data = Buffer.from(JSON.stringify($args)).toString("base64");
        this.data.status = HttpStatusType.SUCCESS;
        this.owner.Send(this.data);

        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.onsend = $callback;
            }
        };
    }

    public OnStart(...$args : any[]) : IResponsePromise {
        this.KeepAlive(true);
        this.Send(this.normalizeArgs($args, "onstart"));
        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.onstart = $callback;
            }
        };
    }

    public OnChange(...$args : any[]) : IResponsePromise {
        this.KeepAlive(true);
        this.Send(this.normalizeArgs($args, "onchange"));
        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.onchange = $callback;
            }
        };
    }

    public OnComplete(...$args : any[]) : IResponsePromise {
        this.KeepAlive(false);
        this.Send(this.normalizeArgs($args, "oncomplete"));
        return {
            Then: ($callback : ($data : any) => void) : void => {
                this.callbacks.oncomplete = $callback;
            }
        };
    }

    public OnError($message : string | Error, ...$args : any[]) : void {
        let stack : string = null;
        let logMessage : string = "";
        if (ObjectValidator.IsSet((<Error>$message).message)) {
            if (ObjectValidator.IsSet((<any>$message).stack)) {
                stack = Buffer.from((<any>$message).stack).toString("base64");
            }
            logMessage = (<Error>$message).message;
            if (ObjectValidator.IsSet((<ErrorLocalizable>$message).localizations) &&
                (<ErrorLocalizable>$message).localizations.hasOwnProperty(this.data.language)) {
                $message = (<ErrorLocalizable>$message).localizations[this.data.language];
            } else {
                $message = (<Error>$message).message;
            }
        } else if (!ObjectValidator.IsString($message)) {
            $message = JSON.stringify($message);
            logMessage = $message;
        }
        if (ObjectValidator.IsEmptyOrNull($message)) {
            $message = "";
        }
        LogIt.Error(logMessage);
        $message = Buffer.from((<string>$message).replace(/[\n]/gmi, "<br/>")).toString("base64");

        this.data.status = HttpStatusType.ERROR;
        this.data.type = "Request.Exception";
        if (!ObjectValidator.IsEmptyOrNull($args)) {
            this.data.data = <IWebServiceException>{
                args   : Buffer.from(JSON.stringify($args)).toString("base64"),
                message: $message,
                stack
            };
        } else {
            this.data.data = <string>$message;
        }
        this.owner.Send(this.data);
    }

    public OnMessage($data : IWebServiceProtocol) : void {
        let data : any = Buffer.from(<string>$data.data, "base64").toString("utf8");
        try {
            data = JSON.parse(data);
        } catch (ex) {
            // provide data as is
        }
        super.OnMessage(data);
    }

    public getId() : number {
        return this.data.id;
    }

    public getOwnerId() : string {
        return this.owner.getOwnerId();
    }

    public Abort() : void {
        super.Abort();
        this.data = null;
    }

    private normalizeArgs($args : any[], $type : string) : any {
        let data : any = "";
        if ($args.length > 0) {
            data = $args.length === 1 ? $args[0] : $args;
        }
        return {
            data,
            type: $type
        };
    }
}

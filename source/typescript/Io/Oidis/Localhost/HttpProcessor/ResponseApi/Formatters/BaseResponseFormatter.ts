/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";

export interface IResponseFormatter extends IBaseObject {
    format($data : any) : any;
}

export class BaseResponseFormatter extends BaseObject implements IResponseFormatter {
    public format($data : any) : any {
        return $data;
    }
}

// generated-code-start
export const IResponseFormatter = globalThis.RegisterInterface(["format"], <any>IBaseObject);
// generated-code-end

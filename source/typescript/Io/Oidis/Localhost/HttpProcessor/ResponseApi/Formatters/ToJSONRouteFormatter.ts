/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseResponseFormatter } from "./BaseResponseFormatter.js";

export class ToJSONRouteFormatter extends BaseResponseFormatter {
    public format($data : string) : any {
        let data : any = $data;
        if (ObjectValidator.IsString(data)) {
            try {
                data = JSON.parse($data);
            } catch ($e) {
                LogIt.Warning("JSONRoute formatter JSON.parse failed.");
                data = null;
            }
        }
        return data;
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { EnvironmentArgs } from "@io-oidis-commons/Io/Oidis/Commons/EnvironmentArgs.js";
import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { IWebServiceConfigurationProtocol } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IWebServiceConfigurationProtocol.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { CertsConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/CertsConnector.js";
import { Resources } from "@io-oidis-services/Io/Oidis/Services/DAO/Resources.js";
import { IWebServiceProtocol } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceProtocol.js";
import { ClientRequest, Server } from "http";
import { AddressInfo } from "net";
import { Readable } from "stream";
import { FileSystemHandler } from "../Connectors/FileSystemHandler.js";
import { HttpServerEventType } from "../Enums/HttpServerEventType.js";
import { HttpServerEventArgs } from "../Events/Args/HttpServerEventArgs.js";
import { EventsManager } from "../Events/EventsManager.js";
import { IHttpServerEvents } from "../Interfaces/Events/IHttpServerEvents.js";
import { IContentSecurityPolicy, ICSPConfiguration, IProject, IServerConfigurationSSL } from "../Interfaces/IProject.js";
import { IRequestConnector } from "../Interfaces/IRequestConnector.js";
import { IResponse, IResponseProtocol } from "../Interfaces/IResponse.js";
import { Loader } from "../Loader.js";
import { CertsManager } from "../Utils/CertsManager.js";
import { Metrics } from "../Utils/Metrics.js";
import { HttpRequestParser } from "./HttpRequestParser.js";
import { ConnectorResponse } from "./Resolvers/ConnectorResponse.js";
import { WebResponse } from "./ResponseApi/Handlers/WebResponse.js";
import { ResponseFactory } from "./ResponseApi/ResponseFactory.js";

export class HttpServer extends BaseObject {
    private static httpArgs : ArrayList<any> = new ArrayList<any>();
    private ports : number[];
    private httpServers : any[];
    private wsServers : any[];
    private readonly responseBuffer : IResponse[];
    private environment : EnvironmentArgs;
    private readonly properties : IProject;
    private onCloseHandler : () => void;
    private responsePersistence : IPersistenceHandler;
    private events : EventsManager;
    private fileSystem : FileSystemHandler;
    private ssl : any;
    private readonly tunnels : any;
    private certsPort : number;
    private requestIndex : number;

    public static SendData($request : HttpRequestParser, $response : WebResponse, $status : HttpStatusType,
                           $headers : any, $data : string) : void {
        if (ObjectValidator.IsEmptyOrNull($response) && !ObjectValidator.IsEmptyOrNull(HttpServer.httpArgs.getLast())) {
            $response = HttpServer.httpArgs.getLast().POST().getItem("Connector");
        }
        if (!Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed() && !ObjectValidator.IsEmptyOrNull($response)) {
            $response.Send({
                body   : $data.replace(/<body(.*)>/gm, "<body>"),
                headers: $headers,
                status : $status
            });
        }
    }

    public static getRequestArgs($response : WebResponse) : AsyncRequestEventArgs {
        return this.httpArgs.getItem("arg_" + $response.getOwnerId());
    }

    constructor() {
        super();
        this.onCloseHandler = () : void => {
            // default handler
        };
        this.ports = [];
        this.httpServers = [];
        this.wsServers = [];
        this.responseBuffer = <any>{};
        this.environment = Loader.getInstance().getEnvironmentArgs();
        this.properties = Loader.getInstance().getAppConfiguration();
        this.responsePersistence = PersistenceFactory.getPersistence(ConnectorResponse.ClassName());
        this.events = new EventsManager();
        this.fileSystem = Loader.getInstance().getFileSystemHandler();
        this.ssl = {
            cert: null,
            key : null
        };
        this.tunnels = {};
        if (!ObjectValidator.IsEmptyOrNull(this.properties.reverseProxy) &&
            !ObjectValidator.IsEmptyOrNull(this.properties.reverseProxy.tunnels)) {
            this.tunnels = this.properties.reverseProxy.tunnels;
        }
        this.certsPort = -1;
        this.requestIndex = 0;
    }

    public setOnCloseHandler($handler : () => void) : void {
        this.onCloseHandler = $handler;
    }

    public Start() : void {
        if (!this.properties.reverseProxy.enabled && !(<IServerConfigurationSSL>this.properties.domain.ssl).enabled) {
            LogIt.Warning("HTTPS server has not been started due SSL server configuration.");
            this.createServer(this.properties.domain.ports.http, false, true,
                [Loader.getInstance().getProgramArgs().ProjectBase()]);
        } else {
            this.getCerts(this.properties.domain.location, ($cert : string, $key : string) : void => {
                this.ssl.cert = $cert;
                this.ssl.key = $key;
                if (this.properties.reverseProxy.enabled) {
                    this.createProxy(true, true);
                    if (this.properties.reverseProxy.withServer) {
                        let registerTunnel : boolean = true;
                        this.getEvents().OnStart(($eventArgs : HttpServerEventArgs) : void => {
                            if (registerTunnel) {
                                registerTunnel = false;
                                this.tunnels[this.properties.domain.location] = {
                                    port: $eventArgs.Port(),
                                    ssl : {
                                        cert: this.ssl.cert,
                                        key : this.ssl.key
                                    }
                                };
                            }
                        });
                        this.createServer(0, false, true,
                            [Loader.getInstance().getProgramArgs().ProjectBase()]);
                    }
                } else {
                    this.createServer(this.properties.domain.ports.http, false, true);
                    this.createServer(this.properties.domain.ports.https, true, true,
                        [Loader.getInstance().getProgramArgs().ProjectBase()]);
                }
            });
        }
    }

    public Stop() : void {
        this.wsServers.forEach(($server : any) : void => {
            try {
                $server.shutDown();
            } catch (ex) {
                LogIt.Warning("Unable to stop WS server. " + ex.stack);
            }
        });
        this.httpServers.forEach(($server : any) : void => {
            try {
                $server.close();
            } catch (ex) {
                LogIt.Warning("Unable to stop HTTP server. " + ex.stack);
            }
        });
        this.wsServers = [];
        this.httpServers = [];
        this.ports = [];
    }

    public getEvents() : IHttpServerEvents {
        return {
            OnConnect   : ($handler : any) : void => {
                this.events.setEvent(this.getClassName(), HttpServerEventType.ON_CONNECT, $handler);
            },
            OnDisconnect: ($handler : any) : void => {
                this.events.setEvent(this.getClassName(), HttpServerEventType.ON_DISCONNECT, $handler);
            },
            OnStart     : ($handler : any) : void => {
                this.events.setEvent(this.getClassName(), EventType.ON_START, $handler);
            }
        };
    }

    public Clear($response : IResponse) : void {
        const responseId : string = "res_" + ResponseFactory.getResponse($response).getId();
        try {
            if (this.responseBuffer.hasOwnProperty(responseId)) {
                // eslint-disable-next-line @typescript-eslint/no-array-delete
                delete this.responseBuffer[responseId];
            }
        } catch (ex) {
            LogIt.Error("Failed to clean up response: " + responseId, ex);
        }
    }

    protected getCerts($domain : string, $callback : ($cert : string, $key : string) => void) : void {
        $domain = StringUtils.Remove($domain, "https://", "http://");
        const handler : CertsManager = new CertsManager($domain, this.getTunnel($domain));
        const paths : IServerConfigurationSSL = handler.getCertPaths();
        if (this.fileSystem.Exists(paths.certificatePath) && this.fileSystem.Exists(paths.privateKeyPath)) {
            $callback(<string>this.fileSystem.Read(paths.certificatePath), <string>this.fileSystem.Read(paths.privateKeyPath));
        } else if (this.properties.certs.external) {
            new CertsConnector()
                .getCertsFor($domain)
                .Then(($cert : string, $key : string) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($cert)) {
                        this.fileSystem.Write(paths.certificatePath, $cert);
                    }
                    if (!ObjectValidator.IsEmptyOrNull($key)) {
                        this.fileSystem.Write(paths.privateKeyPath, $key);
                    }
                    $callback($cert, $key);
                });
        } else {
            let createServer : boolean = false;
            if (this.certsPort <= 0) {
                this.certsPort = this.properties.certs.port;
                if (this.ports.indexOf(this.certsPort) === -1) {
                    createServer = true;
                } else if (this.ports.indexOf(this.certsPort) !== -1 && !this.properties.reverseProxy.withServer) {
                    createServer = true;
                    this.certsPort = 0;
                }
            } else if (this.ports.indexOf(this.certsPort) === -1) {
                createServer = true;
            }
            let serverStarted : boolean = false;
            const handleCerts : any = () : void => {
                handler.Create(($status : boolean, $cert : any, $key : any) : void => {
                    if (serverStarted) {
                        const index : number = this.ports.indexOf(this.certsPort);
                        if (index > -1) {
                            try {
                                this.httpServers[index].close();
                                this.httpServers.splice(index, 1);
                                this.ports.splice(index, 1);
                            } catch (ex) {
                                LogIt.Warning("Unable to stop ACME server. " + ex.stack);
                            }
                        }
                    }
                    if ($status) {
                        $callback($cert, $key);
                    } else {
                        $callback(null, null);
                    }
                });
            };
            if (createServer) {
                LogIt.Debug("> create certs server at: " + this.certsPort);
                if (this.certsPort === 0) {
                    this.getEvents().OnStart(($eventArgs : HttpServerEventArgs) : void => {
                        if (this.certsPort === 0) {
                            this.certsPort = $eventArgs.Port();
                            handleCerts();
                        }
                    });
                }
                this.createServer(this.certsPort, false, false);
                serverStarted = true;
                if (this.certsPort > 0) {
                    handleCerts();
                }
            } else {
                handleCerts();
            }
        }
    }

    protected createServer($port : number, $isSecure : boolean, $includeWebSocket : boolean, $configPaths : string[] = []) : void {
        const metrics : Metrics = Metrics.getInstance();
        const url : any = require("url");
        const path : any = require("path");

        let config : IWebServiceConfigurationProtocol = <any>{};
        const protocolName : string = "wuihost";
        const token : string = "";
        const serverIP : string = Loader.getInstance().getHttpManager().getRequest().getServerIP();
        const hostName : string = this.properties.domain.location !== "localhost" ?
            Loader.getInstance().getHttpManager().getRequest().getServerIP() : this.properties.domain.location;
        let protocol : string;

        let httpServer : Server;
        try {
            if ($isSecure) {
                protocol = "https://";
                if (!ObjectValidator.IsEmptyOrNull(this.ssl.cert) && !ObjectValidator.IsEmptyOrNull(this.ssl.key)) {
                    httpServer = require("https").createServer({
                        cert: this.ssl.cert,
                        key : this.ssl.key
                    });
                } else {
                    LogIt.Warning("Failed to start HTTPS server. " +
                        "Certificates has not been found, please validate server configuration.");
                }
            } else {
                protocol = "http://";
                httpServer = require("http").createServer();
            }
        } catch (ex) {
            LogIt.Error("Failed to create server.", ex);
        }

        if (!ObjectValidator.IsEmptyOrNull(httpServer)) {
            this.httpServers.push(httpServer);

            httpServer.on("error", ($error : Error) : void => {
                LogIt.Warning($error.toString());
            });

            httpServer.on("request", ($request : any, $response : any) : void => {
                const clientId : number = this.requestIndex++;

                let requestProtocol : string = protocol;
                if (!ObjectValidator.IsEmptyOrNull($request.headers.referer)) {
                    requestProtocol = StringUtils.Contains($request.headers.referer, "https://") ? "https://" : "http://";
                }
                $request.protocol = requestProtocol;
                let origin : string = requestProtocol + config.address;
                if (config.port !== 80 && config.port !== 433) {
                    origin += ":" + config.port;
                }

                let requestUrl : string = url.parse($request.url).pathname;
                if (StringUtils.OccurrenceCount(requestUrl, "/resource/") > 1) {
                    requestUrl = StringUtils.Substring(requestUrl, StringUtils.IndexOf(requestUrl, "/resource/", false));
                }
                if (!this.isRequestAllowed($request, hostName, requestProtocol, $port, serverIP)) {
                    this.sendHttpResponse(
                        clientId,
                        $response,
                        <any>400,
                        {},
                        "Request is not allowed",
                        $request.headers["accept-encoding"]);
                    LogIt.Debug("rejected {0} request: {1}", $request.method, requestUrl);
                    return;
                } else {
                    let createLog : boolean = false;
                    if ($request.method === HttpMethodType.GET) {
                        createLog = true;
                        this.getHttpGetFilterPatterns().forEach(($pattern : string) : void => {
                            if (createLog && StringUtils.PatternMatched($pattern, requestUrl)) {
                                createLog = false;
                            }
                        });
                    } else {
                        createLog = true;
                    }
                    if (createLog) {
                        LogIt.Debug("{0} request: {1}", $request.method, requestUrl);
                        metrics.Start(origin + requestUrl, $request.method);
                    }
                }

                try {
                    if ($request.method === HttpMethodType.TRACE) {
                        this.sendHttpResponse(clientId,
                            $response,
                            HttpStatusType.NOT_ALLOWED,
                            {},
                            "",
                            $request.headers["accept-encoding"]);
                    } else if (this.isRequestWithBody($request)) {
                        let messageData : string = "";
                        $request.on("data", ($message : string) : void => {
                            messageData += $message.toString();
                        });
                        $request.on("end", () : void => {
                            this.resolveHttpRequest(requestUrl, messageData, clientId, $request, $response, origin, token);
                            messageData = "";
                            metrics.End();
                        });
                    } else if ($request.method === HttpMethodType.OPTIONS && StringUtils.StartsWith(requestUrl, "/xorigin")) {
                        this.sendHttpResponse(
                            clientId,
                            $response,
                            HttpStatusType.SUCCESS,
                            {
                                "access-control-allow-origin" : requestProtocol + hostName,
                                "access-control-allow-methods": "POST, GET, OPTIONS",
                                "access-control-allow-headers": "X-PINGOTHER, Content-Type",
                                "access-control-max-age"      : 86400,
                                "content-length"              : 0,
                                "content-type"                : "text/plain"
                            },
                            "",
                            $request.headers["accept-encoding"]);
                        metrics.End();
                    } else {
                        this.resolveHttpRequest(requestUrl, config, clientId, $request, $response, origin, token);
                        metrics.End();
                    }
                } catch (ex) {
                    this.sendHttpResponse(
                        clientId,
                        $response,
                        HttpStatusType.ERROR,
                        {},
                        "Internal server error: " + ex.stack,
                        $request.headers["accept-encoding"]);
                }
            });

            httpServer.once("error", ($error : any) : void => {
                if ($error.code === "EADDRINUSE" || $error.code === "EACCES") {
                    LogIt.Error("Cannot start server, because required port " + $port + " is already in use.");
                }
            });

            httpServer.listen($port, () : void => {
                if (ObjectValidator.IsEmptyOrNull(this.properties.domain.location)) {
                    this.properties.domain.location = hostName;
                }
                const port : number = (<AddressInfo>httpServer.address()).port;
                this.ports.push(port);
                config = <IWebServiceConfigurationProtocol>{
                    address    : this.properties.domain.location,
                    base       : "/" + protocolName,
                    location   : __dirname,
                    port,
                    responseUrl: protocol + this.properties.domain.location + ":" + port + "/{0}/response.jsonp",
                    timeout    : httpServer.timeout / 1000,
                    version    : this.properties.version
                };
                const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
                const configFileName : string =
                    "connector.config" + (Loader.getInstance().getProgramArgs().IsDebug() ? ".js" : ".jsonp");
                const createdConfigs : string[] = [];
                $configPaths.forEach(($path : string) : void => {
                    let directory : string = $path;
                    if (this.fileSystem.Exists(directory) && !this.fileSystem.IsDirectory(directory)) {
                        directory = path.dirname($path);
                    }
                    if (!this.fileSystem.Exists(directory) &&
                        StringUtils.Contains(directory, Loader.getInstance().getProgramArgs().ProjectBase())) {
                        this.fileSystem.CreateDirectory(directory);
                    }
                    if (this.fileSystem.Exists(directory)) {
                        if (!StringUtils.EndsWith(directory, "/" + configFileName)) {
                            $path = directory + "/" + configFileName;
                        }
                        if (createdConfigs.indexOf($path) === -1) {
                            if (fileSystem.Write($path, "JsonpData(" + JSON.stringify(config) + ");")) {
                                createdConfigs.push($path);
                            } else {
                                LogIt.Warning("Failed to create config file at: \"" + $path + "\"");
                            }
                        }
                    } else {
                        LogIt.Warning("Creation of config file at \"" + $path + "\" skipped: path does not exist");
                    }
                });

                LogIt.Info(($isSecure ? "SECURE server" : "Server") + " " +
                    this.environment.getAppName() + " has been started at: " +
                    protocol + ($isSecure ? this.properties.domain.location : hostName) + ":" + port);
                this.events.FireEvent(this.getClassName(), EventType.ON_START, new HttpServerEventArgs(port));
            });

            if ($includeWebSocket) {
                const targetPath : string = Loader.getInstance().getProgramArgs().TargetBase();
                let configPath : string = targetPath + "/resource/libs/OidisConnector/connector.config.jsonp";
                if (!this.fileSystem.Exists(configPath)) {
                    configPath = targetPath + "/resource/libs/WuiConnector/connector.config.jsonp";
                }
                let configData : any;
                if (this.fileSystem.Exists(configPath) && this.properties.connectorProxy) {
                    try {
                        let config : string = this.fileSystem.Read(configPath).toString();
                        config = StringUtils.Substring(config,
                            StringUtils.IndexOf(config, "({") + 1,
                            StringUtils.IndexOf(config, "});", false) + 1);
                        configData = JSON.parse(config);
                    } catch (ex) {
                        LogIt.Error("Unable to parse " + configPath, ex);
                    }
                }
                if (!ObjectValidator.IsEmptyOrNull(configData)) {
                    httpServer.on("upgrade", ($serverRequest : any, $serverResponse : any, $serverHead : any) : void => {
                        LogIt.Debug("Started target WS communication at: {0}",
                            "ws://" + configData.address + ":" + configData.port + $serverRequest.url);
                        this.createWsProxyClient($serverRequest, $serverResponse, $serverHead,
                            this.getRequestOptions($serverRequest, {
                                location: "http://" + configData.address,
                                port    : configData.port
                            }, {
                                origin: configData.address
                            }));
                    });
                } else {
                    const webSocketServer : any = require("websocket").server;
                    const wsServer : any = new webSocketServer({
                        autoAcceptConnections : false,
                        httpServer,
                        maxReceivedFrameSize  : 1024 * 1024,
                        maxReceivedMessageSize: 1024 * 1024 * 1024
                    });
                    this.wsServers.push(wsServer);

                    let wsRequestIndex : number = 0;
                    wsServer.on("request", ($request : any) : void => {
                        if (!this.isRequestAllowed($request, hostName, protocol, $port, serverIP)) {
                            $request.reject();
                            LogIt.Info((new Date()) + " Connection from origin \"" + $request.origin + "\" rejected.");
                            return;
                        }

                        const connection : any = $request.accept(protocolName, $request.origin);
                        const clientId : number = wsRequestIndex++;
                        LogIt.Debug((new Date()) + " Connection accepted.");
                        this.events.FireEvent(this.getClassName(), HttpServerEventType.ON_CONNECT,
                            new HttpServerEventArgs($port, clientId));

                        const threadsRegister : number[] = [];
                        let isChunked : boolean = false;
                        const processRawData : any = ($data : string) : void => {
                            isChunked = false;
                            let headers : any = $request.httpRequest.headers;
                            if (ObjectValidator.IsEmptyOrNull(headers)) {
                                headers = {};
                            }

                            if (!ObjectValidator.IsEmptyOrNull(headers.referer)) {
                                $isSecure = StringUtils.Contains(headers.referer, "https://");
                            }
                            let address : string = config.address;
                            let port : number = config.port;
                            if (!ObjectValidator.IsEmptyOrNull(headers.host) && !StringUtils.Contains(headers.host, hostName)) {
                                port = $isSecure ? 443 : 80;
                                if (!ObjectValidator.IsEmptyOrNull(headers["x-forwarded-host"])) {
                                    address = headers["x-forwarded-host"];
                                }
                                if (StringUtils.Contains(address, ":")) {
                                    port = StringUtils.ToInteger(StringUtils.Substring(address,
                                        StringUtils.IndexOf(address, ":") + 1));
                                    address = StringUtils.Substring(address,
                                        0, StringUtils.IndexOf(address, ":"));
                                }
                            }
                            let origin : string = (!$isSecure ? "ws://" : "wss://") + address;
                            if (port !== 80 && port !== 433) {
                                origin += ":" + port;
                            }
                            threadsRegister.push(<any>setTimeout(() : void => {
                                try {
                                    this.resolveRequest("/xorigin", $data, {
                                        Send($data : any) : void {
                                            try {
                                                if (!ObjectValidator.IsString($data)) {
                                                    $data.origin = origin;
                                                    $data.token = token;
                                                    $data = JSON.stringify($data);
                                                }
                                                connection.sendUTF($data);
                                            } catch (ex) {
                                                metrics.Error(ex);
                                                LogIt.Info(ex.stack);
                                            }
                                        },
                                        getOwnerId      : () : string => {
                                            return clientId + "";
                                        },
                                        getParentProcess: () : HttpServer => {
                                            return this;
                                        }
                                    }, $request, true);
                                    $data = null;
                                } catch (ex) {
                                    metrics.Error(ex);
                                    connection.sendUTF(JSON.stringify(this.getErrorProtocol(ex, origin, token)));
                                }
                            }));
                        };

                        let messageLength : number = -1;
                        let chunkLength : number = -1;
                        let messageData : string = "";
                        const readChunk : any = ($data : string) : void => {
                            let initStream : boolean = true;
                            if (messageLength === -1) {
                                const info : string[] = $data.split("\n");
                                messageLength = parseInt(info[0], 10);
                                chunkLength = parseInt(info[1], 10);
                                messageData = "";
                            } else if (messageLength > chunkLength && messageData.length < messageLength) {
                                messageData += $data;
                                if (messageData.length >= messageLength) {
                                    initStream = false;
                                }
                            } else {
                                messageData = $data;
                                initStream = false;
                            }

                            if (messageData.length >= messageLength) {
                                messageData = messageData.substr(0, messageLength);
                                messageLength = -1;
                                processRawData(messageData);
                            } else if (!initStream) {
                                setTimeout(() : void => {
                                    const errorMessage : string =
                                        "Received message with incorrect length: " + messageData.length;
                                    metrics.Error(errorMessage);
                                    LogIt.Info(errorMessage);
                                    connection.sendUTF(JSON.stringify(this.getErrorProtocol(errorMessage, origin, token)));
                                });
                            }
                            if (!initStream) {
                                connection.sendUTF("OK");
                            }
                        };

                        connection.on("message", ($message : any) : any => {
                            if ($message.type === "utf8") {
                                if ($message.utf8Data === "Server.Stop") {
                                    wsServer.shutDown();
                                    this.onCloseHandler();
                                } else {
                                    if (!isChunked && messageLength === -1 && $message.utf8Data.endsWith("\n35000")) {
                                        isChunked = true;
                                    }
                                    if (isChunked) {
                                        readChunk($message.utf8Data);
                                    } else {
                                        processRawData($message.utf8Data);
                                        connection.sendUTF("OK");
                                    }
                                }
                            }
                        });

                        connection.on("close", ($reasonCode : number, $description : string) : void => {
                            try {
                                threadsRegister.forEach(($threadId : number) : void => {
                                    clearTimeout($threadId);
                                });
                                let responseId : string;
                                for (responseId in this.responseBuffer) {
                                    if (this.responseBuffer.hasOwnProperty(responseId) && StringUtils.StartsWith(responseId, "res_")) {
                                        if (this.responseBuffer[responseId].getOwnerId() === clientId + "") {
                                            LogIt.Debug("Abort response: " + responseId);
                                            this.responseBuffer[responseId].Abort();
                                            // eslint-disable-next-line @typescript-eslint/no-array-delete
                                            delete this.responseBuffer[responseId];
                                        }
                                    }
                                }
                            } catch (ex) {
                                LogIt.Warning(ex.stack);
                            }
                            LogIt.Debug("Peer " + connection.remoteAddress + " disconnected. [" + $reasonCode + "]: " + $description);
                            this.events.FireEvent(this.getClassName(),
                                HttpServerEventType.ON_DISCONNECT, new HttpServerEventArgs($port, clientId));
                        });

                        connection.on("error", ($error : any) : void => {
                            if (!StringUtils.Contains($error.message, "ECONNRESET")) {
                                metrics.Error($error);
                                LogIt.Warning($error.stack);
                            }
                        });
                    });

                    wsServer.on("error", ($error : any) : void => {
                        LogIt.Warning($error);
                    });
                }
            }
        }
    }

    protected createProxy($isSecure : boolean, $includeWebSocket : boolean) : void {
        const Readable : any = require("stream").Readable;
        const http : any = require("http");
        const https : any = require("https");
        const tls : any = require("tls");

        const initServer : any = ($isSecure : boolean, $server : Server, $port : number) : void => {
            this.ports.push($port);
            this.httpServers.push($server);

            $server.on("request", ($serverRequest : any, $serverResponse : any) : void => {
                const proxy : any = ($config : any, $body? : any) : void => {
                    try {
                        let options : any = this.getRequestOptions($serverRequest, $config);
                        if (StringUtils.Contains($serverRequest.url, ".well-known/acme-challenge")) {
                            if (ObjectValidator.IsEmptyOrNull(this.properties.certs.location)) {
                                this.properties.certs.location = "http://localhost";
                            }
                            options = this.getRequestOptions($serverRequest, {
                                location: this.properties.certs.location,
                                port    : this.certsPort
                            });
                            LogIt.Debug("proxy challenge for: {0}", $config.name);
                        } else {
                            LogIt.Debug("proxy {0}: {1}", $serverRequest.method,
                                $config.location + ":" + $config.port + $serverRequest.url);
                        }

                        const client : any = (options.protocol === "https:" ? https : http)
                            .request(options, ($targetResponse : any) : void => {
                                const data : any = [];
                                $targetResponse.on("data", ($chunk : string) : void => {
                                    data.push($chunk);
                                });
                                $targetResponse.on("end", () : void => {
                                    try {
                                        const body : Readable = new Readable();
                                        body._read = () : void => {
                                            // mock _read
                                        };
                                        body.push(Buffer.concat(data));
                                        body.push(null);
                                        $serverResponse.writeHead($targetResponse.statusCode, $targetResponse.headers);
                                        body.pipe($serverResponse);
                                    } catch (ex) {
                                        LogIt.Warning(ex.stack);
                                    }
                                });
                                $targetResponse.on("close", () : void => {
                                    LogIt.Debug("Proxy request closed.");
                                });
                                $targetResponse.on("error", ($error : Error) : void => {
                                    LogIt.Warning("Proxy " + $error.stack);
                                });
                            });
                        $serverRequest.on("aborted", () : void => {
                            client.abort();
                        });
                        client.on("error", ($ex : Error) : void => {
                            LogIt.Warning("Proxy " + $ex.stack);
                        });
                        if (!ObjectValidator.IsEmptyOrNull($body)) {
                            client.write($body);
                        }
                        client.end();
                    } catch (ex) {
                        LogIt.Warning(ex.stack);
                    }
                };
                const config : any = this.getTunnel($serverRequest);
                if (!ObjectValidator.IsEmptyOrNull(config)) {
                    if (this.isRequestWithBody($serverRequest)) {
                        const data : any = [];
                        $serverRequest.on("data", ($chunk : string) : void => {
                            data.push($chunk);
                        });
                        $serverRequest.on("end", () : void => {
                            proxy(config, Buffer.concat(data));
                        });
                    } else {
                        proxy(config);
                    }
                }
            });
            if ($includeWebSocket) {
                $server.on("upgrade", ($serverRequest : any, $serverResponse : any, $serverHead : any) : void => {
                    const config : any = this.getTunnel($serverRequest);
                    if (!ObjectValidator.IsEmptyOrNull(config)) {
                        LogIt.Debug("proxy WS: " + config.location + ":" + config.port + $serverRequest.url);
                        this.createWsProxyClient($serverRequest, $serverResponse, $serverHead,
                            this.getRequestOptions($serverRequest, config));
                    }
                });
            }
            $server.once("error", ($error : any) : void => {
                if ($error.code === "EADDRINUSE" || $error.code === "EACCES") {
                    LogIt.Error("Cannot start proxy, because required port " + $port + " is already in use.");
                }
            });
            $server.listen($port, () : void => {
                LogIt.Debug("Proxy has been started at: " + ($isSecure ? "https://" : "http://") +
                    Loader.getInstance().getHttpManager().getRequest().getServerIP() + ":" + $port);
            });
        };

        initServer(false, http.createServer(), this.properties.domain.ports.http);
        if ($isSecure) {
            initServer(true, https.createServer(<any>{
                SNICallback: ($hostname : string, $callback : ($error : Error | null, $context : any) => void) : void => {
                    const config : any = this.getTunnel($hostname);
                    if (ObjectValidator.IsEmptyOrNull(config)) {
                        $callback(null, tls.createSecureContext({}));
                    } else {
                        if ((ObjectValidator.IsEmptyOrNull(config.ssl.cert) || ObjectValidator.IsEmptyOrNull(config.ssl.key)) &&
                            !config.requested) {
                            this.getCerts(config.name, ($cert : string, $key : string) : void => {
                                config.requested = true;
                                if (!ObjectValidator.IsEmptyOrNull($cert) && !ObjectValidator.IsEmptyOrNull($key)) {
                                    config.ssl.cert = $cert;
                                    config.ssl.key = $key;
                                    $callback(null, tls.createSecureContext(config.ssl));
                                } else {
                                    LogIt.Warning("Certificates for " + config.name + " has not been found, " +
                                        "please validate proxy configuration.");
                                    $callback(null, tls.createSecureContext({}));
                                }
                            });
                        } else if (!ObjectValidator.IsEmptyOrNull(config.ssl.cert) && !ObjectValidator.IsEmptyOrNull(config.ssl.key)) {
                            $callback(null, tls.createSecureContext(config.ssl));
                        } else {
                            $callback(null, tls.createSecureContext({}));
                        }
                    }
                },
                cert       : this.ssl.cert,
                key        : this.ssl.key
            }), this.properties.domain.ports.https);
        }
    }

    protected isRequestAllowed($request : any, $hostName : string, $protocol : string, $port : number, $ip? : string) : boolean {
        const origins : string[] = [
            "file://",
            $protocol + "127.0.0.1",
            this.properties.domain.location,
            $protocol + this.properties.domain.location,
            $protocol + this.properties.domain.location + ":" + $port,
            $protocol + $hostName,
            $protocol + $hostName + ":" + $port
        ];
        if (!ObjectValidator.IsEmptyOrNull($ip)) {
            if (origins.indexOf($ip) === -1) {
                origins.push($ip);
                origins.push($protocol + $ip);
                origins.push($protocol + $ip + ":" + $port);
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(this.properties.domain.origins)) {
            this.properties.domain.origins.forEach(($origin : string) : void => {
                if (origins.indexOf($origin) === -1) {
                    origins.push($origin);
                    origins.push($protocol + $origin);
                    origins.push($protocol + $origin + ":" + $port);
                }
            });
        }
        const origin : string = $request.origin;
        return (ObjectValidator.IsEmptyOrNull(origin) || origin === "null" ||
                origins.indexOf(origin) !== -1 || origins.indexOf($request.host) !== -1 ||
                StringUtils.StartsWith(origin, $protocol + "localhost")) &&
            !StringUtils.EndsWith($request.url, ".php");
    }

    protected resolveRequest($url : string, $data : any, $connector : IRequestConnector, $request : any, $keepAlive : boolean) : void {
        try {
            if (StringUtils.StartsWith($url, "/xorigin") &&
                ObjectValidator.IsString($data) && !ObjectValidator.IsEmptyOrNull($data)) {
                let requestProtocol : IWebServiceProtocol = JSON.parse($data);
                /// TODO: keepAlive should be also part of LCP
                requestProtocol.language = this.getRequestLanguage($request.httpRequest);
                let response : IResponse = new WebResponse($connector, requestProtocol);
                let responseId : string = "res_" + response.getId();
                const responseExists : boolean = this.responseBuffer.hasOwnProperty(responseId);
                const responseReinvoke : boolean = responseExists &&
                    this.responseBuffer[responseId].data.status === HttpStatusType.NOT_AUTHORIZED;

                if (!ObjectValidator.IsEmptyOrNull(requestProtocol.type) && (!responseExists || responseReinvoke)) {
                    if ($keepAlive && !responseReinvoke) {
                        this.responseBuffer[responseId] = response;
                    }
                    let args : AsyncRequestEventArgs = new AsyncRequestEventArgs("/liveContent/");
                    args.POST().Add(requestProtocol, "LiveProtocol");
                    args.POST().Add(response, "LiveResponse");
                    args.POST().Add($data, "Protocol");
                    args.POST().Add($connector, "Connector");
                    Loader.getInstance().getHttpResolver().ResolveRequest(args);
                    args = null;
                } else if (responseExists) {
                    this.responseBuffer[responseId].OnMessage(requestProtocol);
                } else {
                    LogIt.Info("Received message with incorrect protocol: " + $data);
                    response.OnError("Incorrect request data.");
                }
                response = null;
                responseId = "";
                requestProtocol = null;
            } else {
                let resolveEnabled : boolean = true;
                if ($url === "/") {
                    const filePath : string = this.fileSystem.NormalizePath(
                        Loader.getInstance().getProgramArgs().TargetBase() + "/index.html");
                    if (this.fileSystem.Exists(filePath)) {
                        LogIt.Debug("Fetch index page from: {0}", filePath);
                        resolveEnabled = false;
                        $connector.Send(require("fs").createReadStream(filePath));
                    }
                }
                if (resolveEnabled) {
                    const args : AsyncRequestEventArgs = new AsyncRequestEventArgs($url);
                    if (!ObjectValidator.IsString($data)) {
                        $data.nonce = this.getUID();
                    }
                    args.POST().Add($data, "Protocol");
                    args.POST().Add($connector, "Connector");
                    args.Owner(new HttpRequestParser("", $request));
                    HttpServer.httpArgs.Add(args, "arg_" + $connector.getOwnerId());
                    Loader.getInstance().getHttpResolver().ResolveRequest(args);
                }
            }
        } catch (ex) {
            LogIt.Info("Received message in incorrect format: \"" + $data + "\"");
            LogIt.Info(ex.stack);
            $connector.Send(this.getErrorProtocol("Unable to parse request data."));
        }
    }

    protected getAppConfiguration() : IProject {
        return this.properties;
    }

    private getDefaultHeaders() : any {
        const os : any = require("os");
        const defaultHeaders : any = {
            "date"        : Convert.TimeToGMTformat(new Date()),
            "server"      :
                this.environment.getAppName() + "/" + this.environment.getProjectVersion() + " (" + os.platform() + ")",
            "vary"        : "Accept-Encoding, Origin",
            "keep-alive"  : "timeout=2, max=100",
            "connection"  : "Keep-Alive",
            "content-type": "text/html"
        };
        this.getDefaultHeaders = () : any => {
            return defaultHeaders;
        };
        return defaultHeaders;
    }

    private getCSPPatterns() : any {
        const config : ICSPConfiguration = this.getAppConfiguration().domain.csp;
        const output : any = {};
        let patternsCount : number = 0;
        for (const policyGroup in config.policies) {
            if (config.policies.hasOwnProperty(policyGroup)) {
                const groups : any = config.policies[policyGroup];
                for (const groupName in groups) {
                    if (groups.hasOwnProperty(groupName)) {
                        const group : IContentSecurityPolicy = groups[groupName];
                        if (!ObjectValidator.IsSet(group.enabled) || group.enabled) {
                            if (group.isDefault) {
                                group.pattern = "__DEFAULT__";
                                if (output.hasOwnProperty(group.pattern)) {
                                    LogIt.Warning("Found more than one default CSP group. Overriding by: " + policyGroup + "." + groupName);
                                }
                            }
                            if (!output.hasOwnProperty(group.pattern)) {
                                patternsCount++;
                            }
                            output[group.pattern] = JsonUtils.Clone(group.policy);
                        }
                    }
                }
            }
        }
        if (patternsCount === 0) {
            LogIt.Warning("Missing at least one enabled CSP group. CSP headers will be not applied even that CSP is enabled.");
        } else if (ObjectValidator.IsEmptyOrNull(output.__DEFAULT__)) {
            LogIt.Warning("Missing default CSP group. URLs not matching group patterns will be without CSP headers.");
        }
        this.getCSPPatterns = () : any => {
            return output;
        };
        return output;
    }

    private getCSPHeaders($clientId : any) : any {
        const args : AsyncRequestEventArgs = HttpServer.httpArgs.getItem("arg_" + $clientId);
        if (ObjectValidator.IsEmptyOrNull(args)) {
            return {};
        }
        const url : string = args.Url();
        const config : ICSPConfiguration = this.getAppConfiguration().domain.csp;
        if (StringUtils.ContainsIgnoreCase(url, config.reportUri)) {
            return {};
        }
        const request = args.Owner();
        const protocol : any = args.POST().getItem("Protocol");
        if (ObjectValidator.IsEmptyOrNull(request) || ObjectValidator.IsEmptyOrNull(protocol) || ObjectValidator.IsString(protocol)) {
            return {};
        }
        const nonce : string = protocol.nonce;
        if (ObjectValidator.IsEmptyOrNull(nonce)) {
            return {};
        }
        const clientAddress : string = request.owner.socket.remoteAddress;
        const patterns : any = this.getCSPPatterns();
        let csp : any = {};
        let patternFound : boolean = false;
        for (const pattern in patterns) {
            if (patterns.hasOwnProperty(pattern) && (
                StringUtils.PatternMatched(pattern, url) || pattern === clientAddress)) {
                JsonUtils.Extend(csp, patterns[pattern]);
                patternFound = true;
            }
        }
        if (!patternFound) {
            csp = patterns.__DEFAULT__;
        }

        let reportEndpoint : string = config.reportUri;
        if (!StringUtils.ContainsIgnoreCase(reportEndpoint, "http://", "https://")) {
            if (!StringUtils.StartsWith(reportEndpoint, "/")) {
                reportEndpoint = "/" + reportEndpoint;
            }
            let origin : string = this.getAppConfiguration().domain.location;
            if (!ObjectValidator.IsEmptyOrNull(request.owner.headers.host)) {
                if (!ObjectValidator.IsEmptyOrNull(request.owner.headers["x-forwarded-host"])) {
                    origin = request.owner.headers["x-forwarded-host"];
                } else {
                    origin = request.owner.headers.host;
                }
            }
            let schema : string = "http://";
            if (!ObjectValidator.IsEmptyOrNull(request.owner.protocol)) {
                schema = request.owner.protocol;
            }
            if (!ObjectValidator.IsEmptyOrNull(request.owner.headers.referer) &&
                StringUtils.StartsWith(request.owner.headers.referer, "https://")) {
                schema = "https://";
            }
            reportEndpoint = schema + origin + reportEndpoint;
        }

        csp = JsonUtils.Extend({
            "report-to" : "csp-endpoint",
            "report-uri": reportEndpoint
        }, csp);

        const sourceStandardValues : string[] = [
            "none", "self", "unsafe-eval", "wasm-unsafe-eval", "unsafe-hashes", "unsafe-inline", "strict-dynamic", "report-sample",
            "inline-speculation-rules"
        ];
        let cspHeader : string = "";
        for (const key in csp) {
            if (csp.hasOwnProperty(key)) {
                if (ObjectValidator.IsBoolean(csp[key])) {
                    if (csp[key]) {
                        cspHeader += key + ";";
                    }
                } else {
                    if (ObjectValidator.IsString(csp[key])) {
                        csp[key] = [csp[key]];
                    }
                    cspHeader += key + " " + csp[key].map(($value : string) : string => {
                        $value = StringUtils.Replace($value, "<? @var response.nonce ?>", nonce);
                        if ($value[0] !== "'") {
                            if (sourceStandardValues.includes($value) ||
                                StringUtils.StartsWith($value, "nonce-") ||
                                StringUtils.StartsWith($value, "sha256-") ||
                                StringUtils.StartsWith($value, "sha384-") ||
                                StringUtils.StartsWith($value, "sha512-")) {
                                return "'" + $value + "'";
                            }
                        }
                        return $value;
                    }).join(" ") + ";";
                }
            }
        }

        return {
            "content-security-policy": cspHeader,
            "report-to"              : JSON.stringify({
                endpoints: [{url: reportEndpoint}],
                group    : "csp-endpoint",
                max_age  : 10886400
            })
        };
    }

    private serializeHeaders($line : string, $headers : any) : string {
        return Object.keys($headers)
            .reduce(($head : string[], $key : string) : string[] => {
                const values : string[] = $headers[$key];
                if (!ObjectValidator.IsArray(values)) {
                    $head.push($key + ": " + values);
                    return $head;
                }
                for (const value of values) {
                    $head.push($key + ": " + value);
                }
                return $head;
            }, [$line])
            .join("\r\n") + "\r\n\r\n";
    }

    private getTunnel($nameOrRequest : any) : any {
        let name : string;
        if (!ObjectValidator.IsString($nameOrRequest)) {
            if (!ObjectValidator.IsEmptyOrNull($nameOrRequest.headers.host)) {
                name = $nameOrRequest.headers.host;
            } else if (!ObjectValidator.IsEmptyOrNull($nameOrRequest.headers.origin)) {
                name = $nameOrRequest.headers.origin;
            }
        } else {
            name = $nameOrRequest;
        }
        name = StringUtils.Remove(name, "https://", "http://");
        if (!ObjectValidator.IsEmptyOrNull(name) && this.tunnels.hasOwnProperty(name)) {
            const output : any = this.tunnels[name];
            output.name = name;
            if (ObjectValidator.IsEmptyOrNull(output.location)) {
                output.location = "http://localhost";
            }
            if (!ObjectValidator.IsSet(output.ssl)) {
                output.ssl = {
                    cert: "",
                    key : ""
                };
            }
            if (ObjectValidator.IsEmptyOrNull(output.port)) {
                output.port = 80;
            }
            if (!ObjectValidator.IsSet(output.requested)) {
                output.requested = false;
            }
            return output;
        } else {
            return null;
        }
    }

    private sendHttpResponse($clientId : number, $response : any, $status : HttpStatusType, $headers : any, $body : any, $encoding : string,
                             $normalizeHeaders : boolean = false) : void {
        const Readable : any = require("stream").Readable;
        const zlib : any = require("zlib");
        if (ObjectValidator.IsEmptyOrNull($status)) {
            $status = HttpStatusType.SUCCESS;
        }
        if (ObjectValidator.IsEmptyOrNull($headers)) {
            $headers = {};
        }
        let stream : Readable;
        if (ObjectValidator.IsString($body)) {
            stream = new Readable();
            stream.push($body);
            stream.push(null);
        } else if (Buffer.isBuffer($body)) {
            stream = new Readable();
            stream.push($body, "binary");
            stream.push(null);
        } else {
            stream = $body;
        }

        const defaultHeaders : any = JsonUtils.Clone(this.getDefaultHeaders());
        if (this.getAppConfiguration().domain.csp.enabled) {
            try {
                const cspHeaders : any = JsonUtils.Clone(this.getCSPHeaders($clientId));
                JsonUtils.Extend(defaultHeaders, cspHeaders);
            } catch (ex) {
                LogIt.Error("Failed to extend headers by CSP.", ex);
            }
        }
        let key : string;
        for (key in defaultHeaders) {
            if (!$headers.hasOwnProperty(key) && !$headers.hasOwnProperty(StringUtils.ToLowerCase(key)) &&
                defaultHeaders.hasOwnProperty(key)) {
                $headers[key] = defaultHeaders[key];
            }
        }
        if ($normalizeHeaders) {
            const normalized : any = {};
            for (key in $headers) {
                if ($headers.hasOwnProperty(key)) {
                    normalized[StringUtils.ToLowerCase(key)] = $headers[key];
                }
            }
            $headers = normalized;
        }

        let encodingKey : string = "content-encoding";
        if (ObjectValidator.IsEmptyOrNull($headers[encodingKey])) {
            encodingKey = "Content-Encoding";
        }
        if (ObjectValidator.IsEmptyOrNull($headers[encodingKey])) {
            if (StringUtils.Contains($encoding, "gzip")) {
                $headers[encodingKey] = "gzip";
                $response.writeHead($status, $headers);
                stream.pipe(zlib.createGzip()).pipe($response);
            } else if (StringUtils.Contains($encoding, "deflate")) {
                $headers[encodingKey] = "deflate";
                $response.writeHead($status, $headers);
                stream.pipe(zlib.createDeflate()).pipe($response);
            } else {
                $response.writeHead($status, $headers);
                stream.on("end", () : void => {
                    $response.end();
                });
                stream.pipe($response);
            }
        } else {
            $response.writeHead($status, $headers);
            stream.pipe($response);
        }
        HttpServer.httpArgs.RemoveAt(HttpServer.httpArgs.getKeys().indexOf("arg_" + $clientId));
    }

    private resolveHttpRequest($url : string, $data : any, $clientId : number, $request : any, $response : any, $origin : string,
                               $token : string) : void {
        try {
            if (ObjectValidator.IsString($data) && StringUtils.StartsWith($data, "jsonData={")) {
                $data = StringUtils.Substring($data, 9);
            }
            let responseSend : boolean = false;
            let connector : any = {
                Send            : ($data : IResponseProtocol | IWebServiceProtocol) : void => {
                    const liveProtocol : IWebServiceProtocol = <IWebServiceProtocol>$data;
                    if (responseSend) {
                        LogIt.Debug("Data has been already send.");
                    } else if (ObjectValidator.IsSet(liveProtocol.id)) {
                        liveProtocol.origin = $origin;
                        liveProtocol.token = $token;
                        liveProtocol.language = this.getRequestLanguage($request);
                        const data : string = JSON.stringify(liveProtocol);
                        if (!ObjectValidator.IsEmptyOrNull(liveProtocol.clientId)) {
                            this.responsePersistence.Variable(liveProtocol.clientId.toString(), data);
                        }
                        this.sendHttpResponse(
                            $clientId,
                            $response,
                            HttpStatusType.SUCCESS,
                            {"content-type": "application/javascript"},
                            data,
                            $request.headers["accept-encoding"]);
                        responseSend = true;
                    } else {
                        let staticProtocol : IResponseProtocol = <IResponseProtocol>$data;
                        if (!ObjectValidator.IsSet(staticProtocol.body)) {
                            staticProtocol = <IResponseProtocol>{
                                body   : $data,
                                headers: {},
                                status : HttpStatusType.SUCCESS
                            };
                        }
                        if (!ObjectValidator.IsSet(staticProtocol.status)) {
                            staticProtocol.status = HttpStatusType.SUCCESS;
                        }
                        if (!ObjectValidator.IsSet(staticProtocol.headers)) {
                            staticProtocol.headers = {};
                        }
                        if (!ObjectValidator.IsSet(staticProtocol.normalize)) {
                            staticProtocol.normalize = false;
                        }
                        if ($request.method === HttpMethodType.HEAD) {
                            staticProtocol.body = "";
                        }
                        this.sendHttpResponse(
                            $clientId,
                            $response,
                            staticProtocol.status,
                            staticProtocol.headers,
                            staticProtocol.body,
                            $request.headers["accept-encoding"],
                            staticProtocol.normalize);
                        responseSend = true;
                    }
                },
                getOwnerId      : () : string => {
                    return $clientId + "";
                },
                getParentProcess: () : HttpServer => {
                    return this;
                }
            };
            this.resolveRequest($url, $data, connector, $request, false);
            connector = null;
        } catch (ex) {
            if (StringUtils.StartsWith($url, "/xorigin")) {
                this.sendHttpResponse(
                    $clientId,
                    $response,
                    HttpStatusType.SUCCESS,
                    {"content-type": "application/javascript"},
                    JSON.stringify(this.getErrorProtocol(ex, $origin, $token)),
                    $request.headers["accept-encoding"]);
            } else {
                this.sendHttpResponse(
                    $clientId,
                    $response,
                    HttpStatusType.ERROR,
                    {},
                    "Internal server error: " + ex.stack,
                    $request.headers["accept-encoding"]);
            }
        }
    }

    private setupWsResponse($response : any, $head : any) : void {
        $response.setTimeout(0);
        $response.setNoDelay(true);
        $response.setKeepAlive(true, 0);
        if ($head?.length) {
            $response.unshift($head);
        }
    }

    private getRequestOptions($request : any, $config : any, $headers? : any) : any {
        const headers : any = JSON.parse(JSON.stringify($request.headers));
        headers["x-forwarded-host"] = headers.host;
        headers.origin = headers.host;
        headers.referer = ($request.connection.encrypted ? "https://" : "http://") +
            headers.host + $request.url;
        headers.host = StringUtils.Remove($config.location, "https://", "http://") + ":" + $config.port;
        if (!ObjectValidator.IsEmptyOrNull($headers)) {
            Resources.Extend(headers, $headers);
        }

        const options : any = require("url").parse($config.location + ":" + $config.port + $request.url);
        options.method = $request.method;
        options.headers = headers;
        options.rejectUnauthorized = false;
        return options;
    }

    private getRequestLanguage($request : any) : string {
        let language : string = this.properties.domain.localization.default;
        if (this.properties.domain.localization.requestDriven) {
            const languages : string[] = StringUtils.Split($request.headers["accept-language"], ",");
            let currentWeight : number = 0;
            languages.forEach(($definition : string) : void => {
                $definition = $definition.trim();
                if ($definition !== "*") {
                    const values : string[] = StringUtils.Split($definition, ";q=");
                    const lang : string = StringUtils.Split(values[0], "-")[0];
                    let weight : number = StringUtils.ToDouble(values[1]) * 100;
                    if (!ObjectValidator.IsDigit(weight)) {
                        weight = 100;
                    }
                    if (weight >= currentWeight) {
                        currentWeight = weight;
                        language = lang;
                    }
                }
            });
        }
        return language;
    }

    private createWsProxyClient($serverRequest : any, $serverResponse : any, $serverHead : any, $options : any) : void {
        if (ObjectValidator.IsEmptyOrNull($options.headers)) {
            $options.headers = {};
        }
        $options.headers.upgrade = "websocket";
        const onErrorHandler : any = ($error : Error) : void => {
            LogIt.Warning("Proxy " + $error.stack);
        };
        this.setupWsResponse($serverResponse, $serverHead);
        const client : ClientRequest = ($options.protocol === "https:" ? require("https") : require("http"))
            .request($options, ($targetResponse : any) : void => {
                if (!$targetResponse.upgrade) {
                    $serverResponse.write(this.serializeHeaders("HTTP/" + $targetResponse.httpVersion + " " +
                        $targetResponse.statusCode + " " + $targetResponse.statusMessage, $targetResponse.headers));
                    $targetResponse.pipe($serverResponse);
                }
            });
        $serverRequest.on("aborted", () : void => {
            client.abort();
        });
        client.on("error", ($error : Error) : void => {
            onErrorHandler($error);
            $serverResponse.end();
        });
        client.on("upgrade", ($targetRequest : any, $targetResponse : any, $targetHead : any) : void => {
            $targetResponse.on("error", ($error : Error) : void => {
                onErrorHandler($error);
                $serverResponse.end();
            });
            $serverResponse.on("error", ($error : Error) : void => {
                onErrorHandler($error);
                $targetResponse.end();
            });
            this.setupWsResponse($targetResponse, $targetHead);
            $serverResponse.write(this.serializeHeaders("HTTP/1.1 101 Switching Protocols", $targetRequest.headers));
            $targetResponse.pipe($serverResponse).pipe($targetResponse);
        });
        client.end();
    }

    private getErrorProtocol($message : string | Error, $origin? : string, $token? : string) : any {
        let data : any = {};
        if (ObjectValidator.IsString($message)) {
            data = {
                code   : 0,
                file   : ObjectEncoder.Base64(__filename),
                line   : 0,
                message: ObjectEncoder.Base64(<string>$message),
                stack  : ""
            };
        } else {
            const ex : any = <Error>$message;
            data = {
                code   : ex.code,
                file   : ObjectEncoder.Base64(ex.message),
                line   : ex.line,
                message: ObjectEncoder.Base64(ex.message),
                stack  : ObjectEncoder.Base64(ex.stack)
            };
        }
        if (ObjectValidator.IsEmptyOrNull($origin)) {
            $origin = "http://localhost";
        }
        if (ObjectValidator.IsEmptyOrNull($token)) {
            $token = "";
        }
        return {
            data,
            origin: $origin,
            status: HttpStatusType.ERROR,
            token : $token,
            type  : "Server.Exception"
        };
    }

    private isRequestWithBody($request : any) : boolean {
        if ($request.method === HttpMethodType.GET) {
            return false;
        }
        if ($request.method === HttpMethodType.POST ||
            $request.method === HttpMethodType.PUT ||
            $request.method === HttpMethodType.PATCH ||
            $request.method === HttpMethodType.DELETE) {
            return true;
        }
        const knownTypes : string[] = ["Transfer-Encoding", "Content-Length"];
        for (const $type of knownTypes) {
            let value : string = $request.headers[$type];
            if (ObjectValidator.IsEmptyOrNull(value)) {
                value = $request.headers[StringUtils.ToLowerCase($type)];
            }
            if (!ObjectValidator.IsEmptyOrNull(value)) {
                return !($type === knownTypes[1] && value === "0");
            }
        }
        return false;
    }

    private getHttpGetFilterPatterns() : string[] {
        const output : string[] = [];
        for (const scope in this.properties.logger.httpGetFilterPatterns) {
            if (this.properties.logger.httpGetFilterPatterns.hasOwnProperty(scope)) {
                const patterns : string[] = <any>this.properties.logger.httpGetFilterPatterns[scope];
                patterns.forEach(($pattern : string) : void => {
                    if (!output.includes($pattern)) {
                        output.push($pattern);
                    }
                });
            }
        }
        this.getHttpGetFilterPatterns = () : string[] => {
            return output;
        };
        return output;
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { ResponseFactory } from "../HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "../Interfaces/IExternOptions.js";
import { IMessage, IResponse } from "../Interfaces/IResponse.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { Extern, Route } from "../Primitives/Decorators.js";

export class ExternAndRouteTestHandler extends BaseConnector {

    @Extern()
    public getVersion() : string {
        return "v" + this.getProtocolVersion();
    }

    @Extern()
    @Route("GET /test/syncNoArgs")
    @Route("GET /test/altEndpointForSyncNoArgs")
    public SyncValueNoArgs() : string {
        return "testSyncValueNoArgs";
    }

    @Extern()
    @Route("GET /test/syncValueWithArgs?param")
    @Route("DELETE /test/syncValueWithArgs?param")
    @Route("GET /test/syncValueWithUrlArgs/{param}")
    @Route("GET /test/syncValueWithHashArgs#{param}")
    public SyncValueWithArgs($param : string) : string {
        return "testSyncValue:" + $param;
    }

    @Extern()
    @Route("POST /test/SyncValueWithJsonArgs")
    @Route("PUT /test/SyncValueWithJsonArgs")
    public SyncValueWithJsonArgs($param : any) : string {
        return "testSyncValue:" + JSON.stringify($param);
    }

    @Extern()
    public SyncJsonWithJsonArgs($param : any) : any {
        return $param;
    }

    @Extern()
    public SyncBufferWithArgs($param : string) : Buffer {
        return Buffer.from($param);
    }

    @Extern()
    public SyncVoidWithIncorrectIResponse($param : string, $callback : IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        response.OnStart("testSyncIResponse:Start");
        response.OnChange("testSyncIResponse:Message");
        // response.OnComplete("testSyncIResponse:" + $param); // requires assert to object {type, data}
        response.Send("testSyncIResponse:" + $param);
    }

    @Extern(ExternResponseType.WITH_PROGRESS)
    public SyncWithProgress($param : string, $callback : IMessage) : string {
        $callback.OnChange("testSyncIResponse:Message");
        return "testSyncIResponse:" + $param;
    }

    @Extern(ExternResponseType.WITH_PROGRESS)
    public SyncWithIncorrectProgress($param : string, $callback : IResponse) : string {
        // simulate usage of IResponse instead of IMessage
        $callback.OnStart("testSyncIResponse:Start");
        $callback.OnChange("testSyncIResponse:Message");
        $callback.Send("testSyncIResponse:" + $param);
        return "testSyncAutoResponse:" + $param; // should never reach due to previous send
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public SyncWithStartAfterChange($callback : IResponse) : void {
        $callback.OnChange("testSyncIResponse:Message1");
        $callback.OnStart("testSyncIResponse:Start");
        $callback.OnChange("testSyncIResponse:Message2");
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public SyncWithStartAfterComplete($callback : IResponse) : void {
        $callback.OnChange("testSyncIResponse:Message1");
        $callback.OnComplete("testSyncIResponse:End1");
        $callback.OnStart("testSyncIResponse:Start");
        $callback.OnChange("testSyncIResponse:Message2");
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public SyncWithChangeAfterComplete($callback : IResponse) : void {
        $callback.OnStart("testSyncIResponse:Start");
        $callback.OnChange("testSyncIResponse:Message1");
        $callback.OnComplete("testSyncIResponse:End1");
        $callback.OnChange("testSyncIResponse:Message2");
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public SyncWithMultipleStarts($callback : IResponse) : void {
        $callback.OnStart("testSyncIResponse:Start");
        $callback.OnStart("testSyncIResponse:Start2");
        $callback.OnChange("testSyncIResponse:Message1");
        $callback.OnChange("testSyncIResponse:Message2");
        $callback.OnComplete("testSyncIResponse:End1");
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public SyncWithMultipleCompletes($callback : IResponse) : void {
        $callback.OnChange("testSyncIResponse:Message1");
        $callback.OnChange("testSyncIResponse:Message2");
        $callback.OnComplete("testSyncIResponse:End1");
        $callback.OnComplete("testSyncIResponse:End2");
    }

    @Extern()
    public SyncValueWithIncorrectIResponseNoVoid($param : string, $callback : IResponse) : string {
        const response : IResponse = ResponseFactory.getResponse($callback);
        // response.OnStart("testSyncIResponse:Start"); // may cause errors if FE not implemented correctly
        // response.OnChange("testSyncIResponse:Message"); // may cause errors if FE not implemented correctly
        response.Send("testSyncIResponse:" + $param);
        return "testSyncAutoResponse:" + $param; // should never reach due to $callback detection
    }

    @Extern(false)
    public SyncValueWithIResponseNoVoid($param : string, $callback : IResponse) : string {
        const response : IResponse = ResponseFactory.getResponse($callback);
        // response.OnStart("testSyncIResponse:Start"); // may cause errors if FE not implemented correctly
        response.Send("testSyncIResponse:" + $param);
        return "testSyncAutoResponse:" + $param; // should never reach as callback has higher priority
    }

    @Extern(false)
    public SyncValueWithIResponse($param : string, $callback : IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        // response.OnStart("testSyncIResponse:Start"); // may cause errors if FE not implemented correctly
        response.Send("testSyncIResponse:" + $param);
    }

    @Extern(false)
    public SyncValueWithoutIResponse($param : string) : string {
        return "testSyncIResponse:" + $param;
    }

    @Extern(false)
    public SyncValueWithFullIResponseNoVoid($param : string, $callback : IResponse) : string {
        const response : IResponse = ResponseFactory.getResponse($callback);
        response.OnStart("testSyncIResponse:Start"); // may cause errors if FE not implemented correctly
        response.OnChange("testSyncIResponse:Message");
        response.OnComplete("testSyncIResponse:" + $param);
        return "testSyncAutoResponse:" + $param; // should never reach as callback has higher priority
    }

    @Extern(false)
    public SyncValueWithFullIResponse($param : string, $callback : IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        response.OnStart("testSyncIResponse:Start"); // may cause errors if FE not implemented correctly
        response.OnChange("testSyncIResponse:Message");
        response.OnComplete("testSyncIResponse:" + $param);
    }

    @Extern(ExternResponseType.IMPLICIT_RETURN)
    @Route("GET /test/syncValueWithOpenArgs?param1&param2")
    public SyncValueWithOpenArgs(...$params : string[]) : string {
        return "testSyncValues:" + $params.join(",");
    }

    @Extern(ExternResponseType.DEFAULT)
    @Route("GET /test/syncValueWithOptionArgs?param&optional")
    public SyncValueWithOptionArgs($param : string, $optional? : string) : string {
        return "testSyncValueOptional:" + $param + ", optional: " + $optional;
    }

    @Extern(ExternResponseType.IMPLICIT_RETURN)
    public AsyncValueNoArgs() : Promise<string> {
        return new Promise<string>(($resolve) => {
            $resolve("testAsyncValueNoArgs");
        });
    }

    @Extern(ExternResponseType.DEFAULT)
    @Route("GET /test/asyncValueWithArgs?param")
    public AsyncValueWithArgs($param : string) : Promise<string> {
        return new Promise<string>(($resolve) => {
            $resolve("testAsyncValue:" + $param);
        });
    }

    @Extern(false)
    @Route("GET /test/asyncValueWithIResponse?param", {autoResponse: false})
    public AsyncValueWithIResponse($param : string, $callback : IResponse) : Promise<void> {
        return new Promise<void>(($resolve) => {
            const response : IResponse = ResponseFactory.getResponse($callback);
            // response.OnStart("testAsyncIResponse:Start"); // may cause errors if FE not implemented correctly
            response.Send("testAsyncIResponse:" + $param);
            $resolve(); // should never reach as callback has higher priority - just ends await
        });
    }

    @Extern(false)
    public AsyncValueWithIResponseNoVoid($param : string, $callback : IResponse) : Promise<string> {
        return new Promise<string>(($resolve) => {
            const response : IResponse = ResponseFactory.getResponse($callback);
            // response.OnStart("testAsyncIResponse:Start"); // may cause errors if FE not implemented correctly
            response.Send("testAsyncIResponse:" + $param);
            $resolve("testAsyncAutoResolve:" + $param); // should never reach as callback has higher priority
        });
    }

    @Extern()
    public AsyncValueWithIncorrectIResponse($param : string, $callback : IResponse) : Promise<void> {
        return new Promise<void>(($resolve) => {
            const response : IResponse = ResponseFactory.getResponse($callback);
            // response.OnStart("testAsyncAutoResponse:Start"); // may cause errors if FE not implemented correctly
            response.Send("testAsyncAutoResponse:" + $param);
            $resolve(); // should never reach as callback has higher priority - just ends await
        });
    }

    @Extern()
    public AsyncValueWithIncorrectIResponseNoVoid($param : string, $callback : IResponse) : Promise<string> {
        return new Promise<string>(($resolve) => {
            const response : IResponse = ResponseFactory.getResponse($callback);
            // response.OnStart("testAsyncAutoResponse:Start"); // may cause errors if FE not implemented correctly
            response.Send("testAsyncAutoResponse:" + $param);
            $resolve("testAsyncAutoResolve:" + $param); // should never reach due to $callback detection
        });
    }

    @Extern(false)
    public AsyncValueWithoutIResponse($param : string) : Promise<string> {
        return new Promise<string>(($resolve) => {
            $resolve("testAsyncAutoResolve:" + $param); // should never reach as callback has not been defined
        });
    }

    @Extern()
    public AsyncValueWithOpenArgs(...$params : string[]) : Promise<string> {
        return new Promise<string>(($resolve) => {
            $resolve("testAsyncValues:" + $params.join(","));
        });
    }

    @Extern()
    public AsyncValueWithOptionArgs($param : string, $optional? : string) : Promise<string> {
        return new Promise<string>(($resolve) => {
            $resolve("testAsyncValueOptional:" + $param + ", optional: " + $optional);
        });
    }

    @Extern(ExternResponseType.WITH_PROGRESS)
    // route with progress does not make a sense so expected output is from OnChange
    @Route("GET /test/asyncValueWithProgress?param", {responseType: ExternResponseType.WITH_PROGRESS})
    public AsyncValueWithProgress($param : string, $onChange : IMessage) : Promise<string> {
        $onChange.OnChange("testAsyncAutoResponse:Message");
        return new Promise<string>(($resolve) => {
            $resolve("testAsyncAutoResolve:" + $param);
        });
    }

    @Route("GET /test/routeValueWithIntParam?param=int()")
    @Route("GET /test/routeValueWithDefaultIntDefaultParam?param=integer(5)")
    @Route("GET /test/routeValueWithDefaultNumberDefaultParam?param=number(8)")
    @Route("GET /test/routeValueWithDefaultFloatDefaultParam?param=float(1.1)")
    @Route("GET /test/routeValueWithDefaultDoubleDefaultParam?param=double(2.2)")
    @Route("GET /test/routeValueWithBoolParam?param=bool()")
    @Route("GET /test/routeValueWithDefaultBoolDefaultParam?param=boolean(false)")
    @Route("GET /test/routeValueWithKeywordParam?param=int")
    @Route("GET /test/routeValueWithStringParam?param=string(test)")
    public RouteValueWithTypedArgs($param : any) : string {
        return "testRouteValue:" + Convert.ObjectToString($param, "", false);
    }

    @Route("GET /test/routeValueWithMultipleArgs?param1=boolean(false)&param2")
    public RouteValueWithMultipleArgs($param : boolean, $valueAssignedByOrder : string) : string {
        return "testRouteValues:" + $param + ", " + $valueAssignedByOrder;
    }
}

/* dev:end */

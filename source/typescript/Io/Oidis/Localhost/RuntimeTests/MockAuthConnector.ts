/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { FileSystemHandler } from "../Connectors/FileSystemHandler.js";
import { ResponseFactory } from "../HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "../Interfaces/IExternOptions.js";
import { IResponse } from "../Interfaces/IResponse.js";
import { Loader } from "../Loader.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { Extern } from "../Primitives/Decorators.js";

export class MockAuthConnector extends BaseConnector {

    @Extern(ExternResponseType.FULL_CONTROL)
    public static AuthMethodStaticWithCallback($callback : IResponse) : void {
        LogIt.Debug("AuthMethodStaticWithCallback called");
        ResponseFactory.getResponse($callback).Send(true);
    }

    @Extern()
    public static AuthMethodStaticWithReturn() : boolean {
        LogIt.Debug("AuthMethodStaticWithReturn called");
        return true;
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public AuthMethodWithCallback($callback : IResponse) : void {
        LogIt.Debug("AuthMethodWithCallback called, q: {0}", this.AuthMethodWithReturn());
        ResponseFactory.getResponse($callback).Send(true);
    }

    @Extern()
    public AuthMethodWithReturn() : boolean {
        LogIt.Debug("AuthMethodWithReturn called");
        return true;
    }

    @Extern()
    public AuthMethodVoid() : void {
        LogIt.Debug("AuthMethodVoid called");
    }

    @Extern()
    public AuthMethodNull() : any {
        LogIt.Debug("AuthMethodNull called");
        return null;
    }

    @Extern()
    public AuthMethodEmpty() : any {
        LogIt.Debug("AuthMethodEmpty called");
        return {};
    }

    @Extern()
    public AuthMethodBuffer() : string | Buffer {
        LogIt.Debug("AuthMethodBuffer called");
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const path : string = fileSystem.getTempPath() + "/" + Loader.getInstance().getAppConfiguration().name + "/test.txt";
        fileSystem.Write(path, "hello world");
        return fileSystem.Read(path);
    }
}

/* dev:end */

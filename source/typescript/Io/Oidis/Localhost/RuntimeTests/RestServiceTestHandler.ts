/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ResponseFactory } from "../HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "../Interfaces/IExternOptions.js";
import { IResponse } from "../Interfaces/IResponse.js";
import { Loader } from "../Loader.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { Extern } from "../Primitives/Decorators.js";

export class RestServiceTestHandler extends BaseConnector {

    @Extern()
    public SelfCallMethod($message : string) : boolean {
        Echo.Printf("SelfCallMethod message: {0}", $message);
        return true;
    }

    @Extern()
    public ExceptionTest($message : string) : void {
        throw new Error($message);
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public ErrorTest($message : string, $callback? : IResponse) : void {
        ResponseFactory.getResponse($callback).OnError($message, {responseType: 404}, "additional message");
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public UnpackTest($path : string, $callback? : IResponse) : void {
        Loader.getInstance().getFileSystemHandler().Unpack($path, null, $callback);
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public DownloadTest($callback? : IResponse) : void {
        Loader.getInstance().getFileSystemHandler().Download(
            "https://github.com/git-for-windows/git/releases/download/v2.21.0.windows.1/Git-2.21.0-64-bit.exe",
            $callback);
    }

    @Extern()
    public DataTransferTest($data : string) : number {
        return $data.length;
    }
}
/* dev:end */

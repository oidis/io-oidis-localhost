/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { WebSocketsClient } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/Clients/WebSocketsClient.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { ILiveContentErrorPromise } from "@io-oidis-services/Io/Oidis/Services/Interfaces/ILiveContentPromise.js";
import { IWebServiceClient } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceClient.js";
import { BaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { LiveContentWrapper } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/LiveContentWrapper.js";
import { WebServiceClientFactory } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/WebServiceClientFactory.js";

export class RestServiceTestConnector extends BaseConnector {

    public SelfCallMethod($message : string) : ILiveContentErrorPromise {
        return this.invoke("SelfCallMethod", $message);
    }

    public ExceptionTest($message : string) : ILiveContentErrorPromise {
        return this.invoke("ExceptionTest", $message);
    }

    public ErrorTest($message : string) : ILiveContentErrorPromise {
        return this.invoke("ErrorTest", $message);
    }

    public Unpack($path : string) : ILiveContentErrorPromise {
        return this.invoke("UnpackTest", $path);
    }

    public Download() : ILiveContentErrorPromise {
        return this.invoke("DownloadTest");
    }

    public DataTransfer($data : string) : ILiveContentErrorPromise {
        return this.invoke("DataTransferTest", $data);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.WEB_SOCKETS;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.WEB_SOCKETS] = "Io.Oidis.Localhost.RuntimeTests.RestServiceTestHandler";
        return namespaces;
    }
}

export class RestServiceTest extends RuntimeTestRunner {
    private handlerClassName : string = "Io.Oidis.Localhost.RuntimeTests.RestServiceTestHandler";

    public testWebsocketsConnection() : void {
        const client : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
            this.getRequest().getHostUrl() + "connector.config.jsonp");
        client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf("General error handler: {0}", $eventArgs.Exception().ToString());
        });

        const invoke : any = ($method : string, $message : string) : void => {
            LiveContentWrapper.InvokeMethod(client, this.handlerClassName, $method, $message)
                .Then(() : void => {
                    Echo.Printf("Function successfully invoked.");
                });
        };

        this.addButton("Self-Invoke function", () : void => {
            invoke("SelfCallMethod", "test message");
        });

        /// TODO: enable test after auth method will be possible
        // this.addButton("Secure Self-Invoke function", () : void => {
        //     invoke("SelfSecureCallMethod", "secure test message");
        // });
    }

    public testWebsocketsErrors() : void {
        const client : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
            this.getRequest().getHostUrl() + "connector.config.jsonp");
        (<WebSocketsClient>client).MaxReconnectsCount(10);
        // (<WebSocketsClient>client).MaxReconnectsCount(0);
        client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf("General error handler: {0}", $eventArgs.Exception().ToString());
        });

        this.addButton("Missing API invoke", () : void => {
            LiveContentWrapper.InvokeMethod(client, this.handlerClassName, "SomethingMissing", "test message")
                .Then(() : void => {
                    Echo.Printf("Function successfully invoked?");
                });
        });

        this.addButton("Exception invoke", () : void => {
            LiveContentWrapper.InvokeMethod(client, this.handlerClassName, "ExceptionTest", "test message")
                .OnError(($eventArgs : ErrorEventArgs) : void => {
                    Echo.Printf("Exception successfully handled: {0}", $eventArgs.Exception().ToString());
                })
                .Then(() : void => {
                    Echo.Printf("Function successfully invoked?");
                });
        });

        this.addButton("Error invoke", () : void => {
            LiveContentWrapper.InvokeMethod(client, this.handlerClassName, "ErrorTest", "test message")
                .OnError(($eventArgs : ErrorEventArgs, $result : any, $nextMessage : string) : void => {
                    Echo.Printf("Error successfully handled: {0}", $eventArgs.Exception().ToString());
                    Echo.Printf("{0}{1}", $result, $nextMessage);
                })
                .Then(() : void => {
                    Echo.Printf("Function successfully invoked?");
                });
        });

        this.addButton("Global exception invoke", () : void => {
            LiveContentWrapper.InvokeMethod(client, this.handlerClassName, "ExceptionTest", "test message")
                .Then(() : void => {
                    Echo.Printf("Function successfully invoked?");
                });
        });

        this.addButton("Global error invoke", () : void => {
            LiveContentWrapper.InvokeMethod(client, this.handlerClassName, "ErrorTest", "test message")
                .Then(() : void => {
                    Echo.Printf("Function successfully invoked?");
                });
        });
    }

    public testHttpConnection() : void {
        const client : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.REST_CONNECTOR,
            this.getRequest().getHostUrl() + "connector.config.jsonp");
        client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });

        this.addButton("Self-Invoke function", () : void => {
            LiveContentWrapper.InvokeMethod(client, this.handlerClassName, "SelfCallMethod", "test message")
                .Then(() : void => {
                    Echo.Printf("Function successfully invoked.");
                });
        });

        /// TODO: enable test after auth method will be possible
        // this.addButton("Secure Self-Invoke function", () : void => {
        //    LiveContentWrapper.InvokeMethod(client, this.handlerClassName, "SelfSecureCallMethod", "secure test message")
        //         .Then(() : void => {
        //             Echo.Printf("Function successfully invoked.");
        //         });
        // });
    }

    public testConnector() : void {
        const connector : RestServiceTestConnector = new RestServiceTestConnector(10,
            this.getRequest().getHostUrl() + "/connector.config.jsonp");
        this.addButton("Unpack invoke", () : void => {
            const basePath : string = this.getAbsoluteRoot() + "/../../test/resource/data/Io/Oidis/Localhost/Connectors";
            connector
                .Unpack(basePath + "/archTest1.zip")
                .Then(($path : string) : void => {
                    Echo.Printf("Unpacked to: {0}", $path);
                    connector
                        .Unpack(basePath + "/archTest1.tar.xz")
                        .Then(($path : string) : void => {
                            Echo.Printf("Unpacked to: {0}", $path);
                        });
                });
        });

        this.addButton("Download invoke", () : void => {
            Echo.Printf("<span id=\"status\"></span>");
            connector
                .Download()
                .Then(($data : any) : void => {
                    if ($data.type === "oncomplete") {
                        Echo.Printf("downloaded");
                    } else if ($data.type === "onstart") {
                        Echo.Printf("download stared");
                    } else if ($data.type === "onchange") {
                        document.getElementById("status").innerText = $data.data.currentValue;
                    }
                });
        });

        this.addButton("Large data transfer", () : void => {
            let data : string = "";
            while (data.length < 70 * 1024) {
                data += "Some test string...Some test string...Some test string...Some test string...";
            }
            Echo.Printf("Send message with length: {0}", data.length);
            connector
                .DataTransfer(data)
                .Then(($length : number) : void => {
                    Echo.Printf("Received message with length: {0}", $length);
                    this.assertEquals(data.length, $length);
                });
        });
    }
}

/* dev:end */

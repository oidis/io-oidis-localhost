/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { ILiveContentPromise } from "@io-oidis-services/Io/Oidis/Services/Interfaces/ILiveContentPromise.js";
import { BaseConnector, IFetchProgress } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";

export class ExternTestConnector extends BaseConnector {
    public SyncValueNoArgsIPromise() : ILiveContentPromise {
        return this.invoke("SyncValueNoArgs");
    }

    public SyncValueWithArgsIPromise($param : string) : ILiveContentPromise {
        return this.invoke("SyncValueWithArgs", $param);
    }

    public SyncValueWithIResponseIPromise($param : string) : ILiveContentPromise {
        return this.invoke("SyncValueWithIResponse", $param);
    }

    public SyncValueWithoutIResponseIPromise($param : string) : ILiveContentPromise {
        return this.invoke("SyncValueWithoutIResponse", $param);
    }

    public SyncValueWithFullIResponseIPromiseNoVoid($param : string) : IProgressPromise {
        return this.invokeFullIResponseIPromise("SyncValueWithFullIResponseNoVoid", $param);
    }

    public SyncValueWithFullIResponseIPromise($param : string) : IProgressPromise {
        return this.invokeFullIResponseIPromise("SyncValueWithFullIResponse", $param);
    }

    public SyncValueWithIncorrectIResponseNoVoidIPromise($param : string) : ILiveContentPromise {
        return this.invoke("SyncValueWithIncorrectIResponseNoVoid", $param);
    }

    public SyncValueWithOpenArgsIPromise(...$params : string[]) : ILiveContentPromise {
        return this.invoke("SyncValueWithOpenArgs", ...$params);
    }

    public SyncValueWithOptionArgsIPromise($param : string, $optional? : string) : ILiveContentPromise {
        return this.invoke("SyncValueWithOptionArgs", $param, $optional);
    }

    public SyncValueNoArgs() : Promise<string> {
        return this.asyncInvoke("SyncValueNoArgs");
    }

    public SyncValueWithArgs($param : string) : Promise<string> {
        return this.asyncInvoke("SyncValueWithArgs", $param);
    }

    public SyncValueWithJsonArgs($param : any) : Promise<string> {
        return this.asyncInvoke("SyncValueWithJsonArgs", $param);
    }

    public SyncJsonWithJsonArgs($param : any) : Promise<any> {
        return this.asyncInvoke("SyncJsonWithJsonArgs", $param);
    }

    public SyncBufferWithArgs($param : string) : Promise<any> {
        return this.asyncInvoke("SyncBufferWithArgs", $param);
    }

    public SyncValueWithIResponseNoVoid($param : string) : Promise<string> {
        return this.asyncInvoke("SyncValueWithIResponseNoVoid", $param);
    }

    public SyncValueWithIResponse($param : string) : Promise<string> {
        return this.asyncInvoke("SyncValueWithIResponse", $param);
    }

    public SyncValueWithoutIResponse($param : string) : Promise<string> {
        return Promise.race([
            new Promise(($resolve, $reject) : void => {
                setTimeout(() : void => {
                    $reject(new Error("SyncValueWithoutIResponse timeout"));
                }, 500);
            }),
            this.asyncInvoke("SyncValueWithoutIResponse", $param)
        ]);
    }

    public SyncValueWithIncorrectIResponseNoVoid($param : string) : Promise<string> {
        return this.asyncInvoke("SyncValueWithIncorrectIResponseNoVoid", $param);
    }

    public SyncValueWithOpenArgs(...$params : string[]) : Promise<string> {
        return this.asyncInvoke("SyncValueWithOpenArgs", ...$params);
    }

    public SyncValueWithOptionArgs($param : string, $optional? : string) : Promise<string> {
        return this.asyncInvoke("SyncValueWithOptionArgs", $param, $optional);
    }

    public SyncWithProgress($param : string, $onChange : ($data : IFetchProgress) => void) : Promise<string> {
        return this.progressWrapper("SyncWithProgress", [$param], $onChange);
    }

    public SyncWithIncorrectProgress($param : string, $onChange : ($data : IFetchProgress) => void) : Promise<string> {
        return this.progressWrapper("SyncWithIncorrectProgress", [$param], $onChange);
    }

    public SyncWithStartAfterChange($onChange : ($data : IFetchProgress) => void) : Promise<string> {
        return this.progressWrapper("SyncWithStartAfterChange", [], $onChange);
    }

    public SyncWithStartAfterComplete($onChange : ($data : IFetchProgress) => void) : Promise<string> {
        return this.progressWrapper("SyncWithStartAfterComplete", [], $onChange);
    }

    public SyncWithChangeAfterComplete($onChange : ($data : IFetchProgress) => void) : Promise<string> {
        return this.progressWrapper("SyncWithChangeAfterComplete", [], $onChange);
    }

    public SyncWithMultipleStarts($onChange : ($data : IFetchProgress) => void) : Promise<string> {
        return this.progressWrapper("SyncWithMultipleStarts", [], $onChange);
    }

    public SyncWithMultipleCompletes($onChange : ($data : IFetchProgress) => void) : Promise<string> {
        return this.progressWrapper("SyncWithMultipleCompletes", [], $onChange);
    }

    public AsyncValueNoArgs() : Promise<string> {
        return this.asyncInvoke("AsyncValueNoArgs");
    }

    public AsyncValueWithArgs($param : string) : Promise<string> {
        return this.asyncInvoke("AsyncValueWithArgs", $param);
    }

    public AsyncValueWithIResponse($param : string) : Promise<string> {
        return this.asyncInvoke("AsyncValueWithIResponse", $param);
    }

    public AsyncValueWithIResponseNoVoid($param : string) : Promise<string> {
        return this.asyncInvoke("AsyncValueWithIResponseNoVoid", $param);
    }

    public AsyncValueWithIncorrectIResponse($param : string) : Promise<string> {
        return this.asyncInvoke("AsyncValueWithIncorrectIResponse", $param);
    }

    public AsyncValueWithIncorrectIResponseNoVoid($param : string) : Promise<string> {
        return this.asyncInvoke("AsyncValueWithIncorrectIResponseNoVoid", $param);
    }

    public AsyncValueWithoutIResponse($param : string) : Promise<string> {
        return Promise.race([
            new Promise(($resolve, $reject) : void => {
                setTimeout(() : void => {
                    $reject(new Error("AsyncValueWithoutIResponse timeout"));
                }, 500);
            }),
            this.asyncInvoke("AsyncValueWithoutIResponse", $param)
        ]);
    }

    public AsyncValueWithOpenArgs(...$params : string[]) : Promise<string> {
        return this.asyncInvoke("AsyncValueWithOpenArgs", ...$params);
    }

    public AsyncValueWithOptionArgs($param : string, $optional? : string) : Promise<string> {
        return this.asyncInvoke("AsyncValueWithOptionArgs", $param, $optional);
    }

    public AsyncProgress($param : string, $onChange : ($data : IFetchProgress) => void) : Promise<string> {
        return this.progressWrapper("SyncVoidWithIncorrectIResponse", [$param], $onChange);
    }

    public AsyncValueWithProgress($param : string, $onChange : ($data : IFetchProgress) => void) : Promise<string> {
        return this.progressWrapper("AsyncValueWithProgress", [$param], $onChange);
    }

    public getVersion() : Promise<string> {
        return this.asyncInvoke("getVersion");
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.WEB_SOCKETS;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.WEB_SOCKETS] = "Io.Oidis.Localhost.RuntimeTests.ExternAndRouteTestHandler";
        return namespaces;
    }

    private invokeFullIResponseIPromise($method : string, $param : string) : IProgressPromise {
        const callbacks : any = {
            onComplete($result : string) : any {
                // declare default callback
            },
            onMessage($result : string) : any {
                // declare default callback
            },
            onStart($result : string) : any {
                // declare default callback
            }
        };
        this.invoke($method, $param)
            .Then(($result : any) : void => {
                if (ObjectValidator.IsSet($result.type)) {
                    if ($result.type === EventType.ON_START) {
                        callbacks.onStart($result.data);
                    } else if ($result.type === EventType.ON_CHANGE) {
                        callbacks.onMessage($result.data);
                    } else if ($result.type === EventType.ON_COMPLETE) {
                        callbacks.onComplete($result.data);
                    }
                }
            });
        const promise : IProgressPromise = <IProgressPromise>{
            OnMessage: ($callback : ($message : string) => void) : IProgressPromise => {
                callbacks.onMessage = $callback;
                return promise;
            },
            OnStart  : ($callback : ($message : string) => void) : IProgressPromise => {
                callbacks.onStart = $callback;
                return promise;
            },
            Then     : ($callback : ($result : string) => void) : void => {
                callbacks.onComplete = $callback;
            }
        };

        return promise;
    }
}

export class ExternAndRouteTest extends RuntimeTestRunner {
    private client : ExternTestConnector;

    constructor() {
        super();
        this.client = new ExternTestConnector(true, this.getRequest().getHostUrl() + "/connector.config.jsonp");
        this.setMethodFilter(
            // "testExternLCPversion",
            // "testExternSyncValueIPromise",
            // "testExternSyncValue",
            // "testExternSyncIResponse",
            // "testExternSyncProgress",
            // "testExternAsyncValue",
            // "testExternAsyncIResponse",
            // "testExternAsyncProgress",
            // "testIncorrectResponsesOrder",
            // "testRouteSyncValue",
            // "testRouteASyncValue",
            // "testRouteParamTypes",
            // "testRouteIncorrectInvoke",
            // "testRouteWithCallback"
        );
    }

    public async testExternLCPversion() : Promise<void> {
        this.assertEquals(await this.client.getVersion(), "v1.0");
    }

    public async testExternSyncValueIPromise() : Promise<void> {
        const useCases : any[] = [
            ($done : any) : void => {
                this.client.SyncValueNoArgsIPromise().Then(($value : string) : void => {
                    this.assertEquals($value, "testSyncValueNoArgs");
                    $done();
                });
            },
            ($done : any) : void => {
                this.client.SyncValueWithArgsIPromise("test").Then(($value : string) : void => {
                    this.assertEquals($value, "testSyncValue:test");
                    $done();
                });
            },
            ($done : any) : void => {
                this.client.SyncValueWithIResponseIPromise("test")
                    .Then(($value : any) : void => {
                        this.assertEquals($value, "testSyncIResponse:test");
                        $done();
                    });
            },
            ($done : any) : void => {
                setTimeout(() : void => {
                    $done();
                }, 500);
                this.client.SyncValueWithoutIResponseIPromise("test")
                    .Then(($value : any) : void => {
                        this.assertOk(false);
                        $done();
                    });
            },
            ($done : any) : void => {
                this.client.SyncValueWithFullIResponseIPromise("test")
                    .OnStart(($value : any) : void => {
                        this.assertEquals($value, "testSyncIResponse:Start");
                    })
                    .OnMessage(($value : any) : void => {
                        this.assertEquals($value, "testSyncIResponse:Message");
                    })
                    .Then(($value : any) : void => {
                        this.assertEquals($value, "testSyncIResponse:test");
                        $done();
                    });
            },
            ($done : any) : void => {
                this.client.SyncValueWithFullIResponseIPromiseNoVoid("test")
                    .OnStart(($value : any) : void => {
                        this.assertEquals($value, "testSyncIResponse:Start");
                    })
                    .OnMessage(($value : any) : void => {
                        this.assertEquals($value, "testSyncIResponse:Message");
                    })
                    .Then(($value : any) : void => {
                        this.assertEquals($value, "testSyncIResponse:test");
                        $done();
                    });
            },
            ($done : any) : void => {
                this.client.SyncValueWithIncorrectIResponseNoVoidIPromise("test").Then(($value : string) : void => {
                    this.assertEquals($value, "testSyncIResponse:test");
                    $done();
                });
            },
            ($done : any) : void => {
                this.client.SyncValueWithOpenArgsIPromise("test1", "test2").Then(($value : string) : void => {
                    this.assertEquals($value, "testSyncValues:test1,test2");
                    $done();
                });
            },
            ($done : any) : void => {
                this.client.SyncValueWithOptionArgsIPromise("test1").Then(($value : string) : void => {
                    this.assertEquals($value, "testSyncValueOptional:test1, optional: undefined");
                    $done();
                });
            },
            ($done : any) : void => {
                this.client.SyncValueWithOptionArgsIPromise("test1", "optional").Then(($value : string) : void => {
                    this.assertEquals($value, "testSyncValueOptional:test1, optional: optional");
                    $done();
                });
            }
        ];
        return new Promise<void>(($resolve : any) : void => {
            const runUseCase : any = ($index : number = 0) : void => {
                if ($index < useCases.length) {
                    useCases[$index](() : void => {
                        runUseCase($index + 1);
                    });
                } else {
                    $resolve();
                }
            };
            runUseCase();
        });
    }

    public async testExternSyncValue() : Promise<void> {
        this.assertEquals(await this.client.SyncValueNoArgs(), "testSyncValueNoArgs");
        this.assertEquals(await this.client.SyncValueWithArgs("test"), "testSyncValue:test");
        this.assertEquals(await this.client.SyncValueWithJsonArgs({test: true}), "testSyncValue:{\"test\":true}");
        this.assertDeepEqual(await this.client.SyncJsonWithJsonArgs({test: true}), {test: true});
        this.assertEquals(await this.client.SyncBufferWithArgs("test"), "test");
        this.assertEquals(await this.client.SyncValueWithOpenArgs("test1", "test2"), "testSyncValues:test1,test2");
        this.assertEquals(await this.client.SyncValueWithOptionArgs("test1"),
            "testSyncValueOptional:test1, optional: undefined");
        this.assertEquals(await this.client.SyncValueWithOptionArgs("test1", "optional"),
            "testSyncValueOptional:test1, optional: optional");
    }

    public async testExternSyncIResponse() : Promise<void> {
        this.assertEquals(await this.client.SyncValueWithIResponse("test"), "testSyncIResponse:test");
        try {
            await this.client.SyncValueWithoutIResponse("test");
        } catch (ex) {
            this.assertEquals(ex.message, "SyncValueWithoutIResponse timeout");
        }
        this.assertEquals(await this.client.SyncValueWithIResponseNoVoid("test"), "testSyncIResponse:test");
        this.assertEquals(await this.client.SyncValueWithIncorrectIResponseNoVoid("test"), "testSyncIResponse:test");
    }

    public async testExternSyncProgress() : Promise<void> {
        this.assertEquals(await this.client.SyncWithProgress("test", ($data : any) : void => {
            this.assertEquals($data, "testSyncIResponse:Message");
        }), "testSyncIResponse:test");
        this.assertEquals(await this.client.SyncWithIncorrectProgress("test", ($data : any) : void => {
            this.assertOk($data === "testSyncIResponse:Start" || $data === "testSyncIResponse:Message");
        }), "testSyncIResponse:test");
    }

    public async testIncorrectResponsesOrder() : Promise<void> {
        try {
            await this.client.SyncWithStartAfterChange(($data : any) : void => {
                this.assertEquals($data, "testSyncIResponse:Message1");
            });
        } catch (ex) {
            this.assertEquals(ex.message, "LCP Error: cannot notify onstart after onchange");
        }
        try {
            this.assertDeepEqual(await this.client.SyncWithStartAfterComplete(($data : any) : void => {
                this.assertEquals($data, "testSyncIResponse:Message1");
            }), {
                data: "testSyncIResponse:End1",
                type: "oncomplete"
            });
        } catch (ex) {
            this.assertEquals(ex.message, "LCP Error: cannot notify onstart after oncomplete");
        }
        this.assertDeepEqual(await this.client.SyncWithChangeAfterComplete(($data : any) : void => {
            this.assertOk($data === "testSyncIResponse:Start" ||
                $data === "testSyncIResponse:Message1");
        }), {
            data: "testSyncIResponse:End1",
            type: "oncomplete"
        });
        try {
            await this.client.SyncWithMultipleStarts(($data : any) : void => {
                this.assertEquals($data, "testSyncIResponse:Start");
            });
        } catch (ex) {
            this.assertEquals(ex.message, "LCP Error: onstart notify can be send only once");
        }
        this.assertDeepEqual(await this.client.SyncWithMultipleCompletes(($data : any) : void => {
            this.assertOk($data === "testSyncIResponse:Message1" ||
                $data === "testSyncIResponse:Message2");
        }), {
            data: "testSyncIResponse:End1",
            type: "oncomplete"
        });
    }

    public async testExternAsyncValue() : Promise<void> {
        this.assertEquals(await this.client.AsyncValueNoArgs(), "testAsyncValueNoArgs");
        this.assertEquals(await this.client.AsyncValueWithArgs("test"), "testAsyncValue:test");
        this.assertEquals(await this.client.AsyncValueWithOpenArgs("test1", "test2"), "testAsyncValues:test1,test2");
        this.assertEquals(await this.client.AsyncValueWithOptionArgs("test1"),
            "testAsyncValueOptional:test1, optional: undefined");
        this.assertEquals(await this.client.AsyncValueWithOptionArgs("test1", "optional"),
            "testAsyncValueOptional:test1, optional: optional");
    }

    public async testExternAsyncIResponse() : Promise<void> {
        this.assertEquals(await this.client.AsyncValueWithIResponse("test"), "testAsyncIResponse:test");
        this.assertEquals(await this.client.AsyncValueWithIResponseNoVoid("test"), "testAsyncIResponse:test");
        this.assertEquals(await this.client.AsyncValueWithIncorrectIResponse("test"), "testAsyncAutoResponse:test");
        this.assertEquals(await this.client.AsyncValueWithIncorrectIResponseNoVoid("test"), "testAsyncAutoResponse:test");
        try {
            await this.client.AsyncValueWithoutIResponse("test");
        } catch (ex) {
            this.assertEquals(ex.message, "AsyncValueWithoutIResponse timeout");
        }
    }

    public async testExternAsyncProgress() : Promise<void> {
        this.assertEquals(await this.client.AsyncProgress("test", ($data : any) : void => {
            this.assertOk($data === "testSyncIResponse:Start" || $data === "testSyncIResponse:Message");
        }), "testSyncIResponse:test");
        this.assertEquals(await this.client.AsyncValueWithProgress("test", ($data : any) : void => {
            this.assertEquals($data, "testAsyncAutoResponse:Message");
        }), "testAsyncAutoResolve:test");
    }

    public async testRouteSyncValue() : Promise<void> {
        this.assertEquals(await this.fetchResponse("/api/test/syncNoArgs"), "testSyncValueNoArgs");
        this.assertEquals(await this.fetchResponse("/api/test/altEndpointForSyncNoArgs"), "testSyncValueNoArgs");
        this.assertEquals(await this.fetchResponse("/api/test/syncValueWithArgs?param=test"), "testSyncValue:test");
        this.assertEquals(await this.fetchResponse("/api/test/syncValueWithOptionArgs?param=test"),
            "testSyncValueOptional:test, optional: undefined");
        this.assertEquals(await this.fetchResponse("/api/test/syncValueWithOptionArgs?param=test&optional=1"),
            "testSyncValueOptional:test, optional: 1");

        this.assertEquals(await this.fetchResponse("/api/test/SyncValueWithJsonArgs", {
            body   : JSON.stringify({test: "test"}),
            headers: {"Content-Type": "application/json"},
            method : HttpMethodType.POST
        }), "testSyncValue:{\\\"test\\\":\\\"test\\\"}");
        this.assertEquals(await this.fetchResponse("/api/test/SyncValueWithJsonArgs", {
            body   : JSON.stringify({test: "testPut"}),
            headers: {"Content-Type": "application/json"},
            method : HttpMethodType.PUT
        }), "testSyncValue:{\\\"test\\\":\\\"testPut\\\"}");
        this.assertEquals(await this.fetchResponse("/api/test/syncValueWithArgs?param=test", {
            method: HttpMethodType.DELETE
        }), "testSyncValue:test");
    }

    public async testRouteASyncValue() : Promise<void> {
        this.assertEquals(await this.fetchResponse("/api/test/asyncValueWithArgs?param=test"), "testAsyncValue:test");
    }

    public async testRouteParamTypes() : Promise<void> {
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithIntParam?param=1"),
            "testRouteValue:Object type: number. Return value: 1");
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithDefaultIntDefaultParam"),
            "testRouteValue:Object type: number. Return value: 5");
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithDefaultIntDefaultParam?param=10"),
            "testRouteValue:Object type: number. Return value: 10");
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithDefaultNumberDefaultParam"),
            "testRouteValue:Object type: number. Return value: 8");
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithDefaultFloatDefaultParam"),
            "testRouteValue:Object type: number. Return value: 1.1");
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithDefaultFloatDefaultParam?param=3.005"),
            "testRouteValue:Object type: number. Return value: 3.005");
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithDefaultDoubleDefaultParam"),
            "testRouteValue:Object type: number. Return value: 2.2");
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithDefaultDoubleDefaultParam?param=7.005"),
            "testRouteValue:Object type: number. Return value: 7.005");
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithBoolParam?param=true"),
            "testRouteValue:Object type: boolean. Return value: true");
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithDefaultBoolDefaultParam"),
            "testRouteValue:Object type: boolean. Return value: false");
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithDefaultBoolDefaultParam?param=true"),
            "testRouteValue:Object type: boolean. Return value: true");
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithStringParam"),
            "testRouteValue:test");

        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithIntParam?param=test"),
            "testRouteValue:Object type: number. Return value: NaN");

        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithMultipleArgs?param2=test"),
            "testRouteValues:false, test");
    }

    public async testRouteIncorrectInvoke() : Promise<void> {
        this.assertEquals(await this.fetchResponse("/api/test/routeValueWithKeywordParam"), "testRouteValue:int");
        try {
            await this.fetchResponse("/api/test/syncValueWithOpenArgs?param1=test&param2=test2");
        } catch (ex) {
            // open args are not supported for REST
            this.assertEquals(ex.message, "Passed more arguments then declared in route schema: " +
                "GET /test/syncValueWithOpenArgs?param1&param2");
        }
        try {
            await this.fetchResponse("/api/test/notExist");
        } catch (ex) {
            this.assertEquals(ex.message, "Unsupported method GET for section /test/notExist");
        }
        try {
            await this.fetchResponse("/api/test/syncNoArgs?missingParam=true");
        } catch (ex) {
            this.assertEquals(ex.message, "Passed parameters [missingParam] not declared in route schema: " +
                "GET /test/syncNoArgs");
        }
        try {
            await this.fetchResponse("/api/test/syncValueWithArgs?param=test&unexpectedParam=true");
        } catch (ex) {
            this.assertEquals(ex.message, "Passed parameters [unexpectedParam] not declared in route schema: " +
                "GET /test/syncValueWithArgs?param");
        }
        // progress for REST does not make a sense, so just validate that output will be first progress message
        this.assertEquals(await this.fetchResponse("/api/test/asyncValueWithProgress?param=true"),
            "\"data\":\"testAsyncAutoResponse:Message\",\"type\":\"onchange\"");

        try {
            /// TODO: this is currently not supported mainly because RESTResolver is registered only for
            //        variable section and subsection after /api/.
            //        Also Route decorator is not able to parse variables from url like /{param}.
            //        Anyway this is commonly used in REST and should be also generally supported somehow
            //        otherwise it must be implemented by custom resolver (older way).
            await Promise.race([
                new Promise(($resolve, $reject) : void => {
                    setTimeout(() : void => {
                        $reject(new Error("syncValueWithUrlArgs timeout"));
                    }, 500);
                }),
                this.fetchResponse("/api/test/syncValueWithUrlArgs/test")
            ]);
        } catch (ex) {
            this.assertEquals(ex.message, "Unsupported method GET for section /test/syncValueWithUrlArgs/test");
        }

        try {
            // HTTP request with #hash is possible only in browser, so it will never respond
            await Promise.race([
                new Promise(($resolve, $reject) : void => {
                    setTimeout(() : void => {
                        $reject(new Error("syncValueWithHashArgs timeout"));
                    }, 500);
                }),
                this.fetchResponse("/api/test/syncValueWithHashArgs#test")
            ]);
        } catch (ex) {
            this.assertEquals(ex.message, "syncValueWithHashArgs timeout");
        }
    }

    public async testRouteWithCallback() : Promise<void> {
        this.assertEquals(await this.fetchResponse("/api/test/asyncValueWithIResponse?param=test"),
            "testAsyncIResponse:test");
    }

    private async fetchResponse($url : string, $options? : any) : Promise<string> {
        const response = await fetch($url, $options);
        const output : string = await response.text();
        if (response.ok) {
            return output.substring(1, output.length - 1);
        } else {
            let error : any = output;
            try {
                error = JSON.parse(error);
            } catch (ex) {
                // just fallback to string output
            }
            if (ObjectValidator.IsString(error)) {
                throw new Error(error);
            } else if (!ObjectValidator.IsEmptyOrNull(error) && !ObjectValidator.IsEmptyOrNull(error.message)) {
                throw new Error(error.message);
            } else {
                throw new Error(response.statusText);
            }
        }
    }
}

interface IProgressPromise {
    OnStart($callback : ($message : string) => void) : IProgressPromise;

    OnMessage($callback : ($message : string) => void) : IProgressPromise;

    Then($callback : ($result : string) => void) : void;
}

/* dev:end */

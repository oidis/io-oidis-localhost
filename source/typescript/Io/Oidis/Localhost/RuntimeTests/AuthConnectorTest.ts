/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { ILiveContentErrorPromise } from "@io-oidis-services/Io/Oidis/Services/Interfaces/ILiveContentPromise.js";
import { BaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";

export class AuthTestConnector extends BaseConnector {

    public AuthMethodStaticWithCallback() : ILiveContentErrorPromise {
        return this.invoke("AuthMethodStaticWithCallback");
    }

    public AuthMethodStaticWithReturn() : ILiveContentErrorPromise {
        return this.invoke("AuthMethodStaticWithReturn");
    }

    public AuthMethodWithCallback() : ILiveContentErrorPromise {
        return this.invoke("AuthMethodWithCallback");
    }

    public AuthMethodWithReturn() : ILiveContentErrorPromise {
        return this.invoke("AuthMethodWithReturn");
    }

    public AuthMethodVoid() : ILiveContentErrorPromise {
        return this.invoke("AuthMethodVoid");
    }

    public AuthMethodNull() : ILiveContentErrorPromise {
        return this.invoke("AuthMethodNull");
    }

    public AuthMethodEmpty() : ILiveContentErrorPromise {
        return this.invoke("AuthMethodEmpty");
    }

    public AuthMethodBuffer() : ILiveContentErrorPromise {
        return this.invoke("AuthMethodBuffer");
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.WEB_SOCKETS;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.WEB_SOCKETS] = "Io.Oidis.Localhost.RuntimeTests.MockAuthConnector";
        return namespaces;
    }
}

export class AuthConnectorTest extends RuntimeTestRunner {
    public testConnector() : void {
        const connector : AuthTestConnector = new AuthTestConnector(10,
            this.getRequest().getHostUrl() + "/connector.config.jsonp");
        this.addButton("AuthMethodStaticWithCallback", () : void => {
            connector
                .AuthMethodStaticWithCallback()
                .Then(($status : boolean) : void => {
                    Echo.Printf("AuthMethodStaticWithCallback status: {0}", $status);
                });
        });
        this.addButton("AuthMethodStaticWithReturn", () : void => {
            connector
                .AuthMethodStaticWithReturn()
                .Then(($status : boolean) : void => {
                    Echo.Printf("AuthMethodStaticWithReturn status: {0}", $status);
                });
        });
        this.addButton("AuthMethodWithCallback", () : void => {
            connector
                .AuthMethodWithCallback()
                .Then(($status : boolean) : void => {
                    Echo.Printf("AuthMethodWithCallback status: {0}", $status);
                });
        });
        this.addButton("AuthMethodWithReturn", () : void => {
            connector
                .AuthMethodWithReturn()
                .Then(($status : boolean) : void => {
                    Echo.Printf("AuthMethodWithReturn status: {0}", $status);
                });
        });
        this.addButton("AuthMethodVoid", () : void => {
            connector
                .AuthMethodVoid()
                .Then(($retVal : any) : void => {
                    Echo.Printf("AuthMethodVoid status: {0}", $retVal);
                });
        });
        this.addButton("AuthMethodNull", () : void => {
            connector
                .AuthMethodNull()
                .Then(($retVal : any) : void => {
                    Echo.Printf("AuthMethodNull status: {0}", $retVal);
                });
        });
        this.addButton("AuthMethodEmpty", () : void => {
            connector
                .AuthMethodEmpty()
                .Then(($retVal : any) : void => {
                    Echo.Printf("AuthMethodEmpty status: {0}", $retVal);
                });
        });
        this.addButton("AuthMethodBuffer", () : void => {
            connector
                .AuthMethodBuffer()
                .Then(($retVal : any) : void => {
                    Echo.Printf("AuthMethodBuffer status: {0}", $retVal);
                });
        });
    }
}

/* dev:end */

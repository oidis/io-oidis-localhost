/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { ILocalization } from "@io-oidis-services/Io/Oidis/Services/Interfaces/DAO/IBaseConfiguration.js";

export class ErrorLocalizable extends Error {
    public readonly localizations : ILocalization;

    constructor($message : string, $localizations? : ILocalization) {
        super($message);
        this.localizations = $localizations;
        if (ObjectValidator.IsEmptyOrNull(this.localizations)) {
            this.localizations = {};
        }
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";

export class MainPage extends BasePanel {
    public readonly footerInfo : GuiCommons;

    constructor() {
        super();
        this.footerInfo = new GuiCommons();
    }

    protected innerCode() : string {
        this.footerInfo.Content("" +
            "Localhost version: " + this.getEnvironmentArgs().getProjectVersion() +
            ", build: " + this.getEnvironmentArgs().getBuildTime() +
            ", Copyright " + StringUtils.YearFrom(2019) + " Oidis");

        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<section style="background: var(--bs-dark);">
    <div class="container-fluid">
        <div class="row"></div>
        <div class="row" style="text-align: center;margin: 10%;">
            <div class="col"><img class="img-fluid d-xl-flex" src="resource/graphics/Io/Oidis/Localhost/Gui/Pages/MainPage/Logo.png" style="margin: auto;" /></div>
        </div>
        <div class="row">
            <div class="col">
                <p style="text-align: center;margin-top: 40px;color: var(--bs-light);" data-oidis-bind="${this.footerInfo}">Copyright ${StringUtils.YearFrom(2019)} Oidis</p>
            </div>
        </div>
    </div>
</section>`;
    }
}

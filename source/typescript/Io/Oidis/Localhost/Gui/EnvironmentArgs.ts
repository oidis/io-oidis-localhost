/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EnvironmentArgs as Parent } from "@io-oidis-commons/Io/Oidis/Commons/EnvironmentArgs.js";

/**
 * EnvironmentArgs class provides environment args mainly used by hosted content.
 */
export class EnvironmentArgs extends Parent {
    protected getConfigPaths() : string[] {
        return [];
    }
}

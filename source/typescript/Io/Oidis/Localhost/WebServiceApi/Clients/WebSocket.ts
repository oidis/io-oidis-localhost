/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { IFetchResolver } from "../../HttpProcessor/HttpResolver.js";
import { Loader } from "../../Loader.js";

export class WebSocket extends BaseObject {
    public static CONNECTING : number = 0;
    public static OPEN : number = 1;
    public static CLOSING : number = 2;
    public static CLOSED : number = 3;
    public onopen : any;
    public onclose : any;
    public onmessage : any;
    public onerror : any;
    public readyState : any;
    private client : any;

    public static setLogItClass() : void {
        // fallback for older builder
    }

    constructor($url : string, $protocols? : string, $requestOptions? : any) {
        super();
        const options : any = require("url").parse($url);
        if (process.env.hasOwnProperty("DISABLE_STRICT_SSL") || Loader.getInstance().getAppConfiguration().disableStrictSSL) {
            options.strictSSL = false;
            options.rejectUnauthorized = false;
        }
        $requestOptions = JsonUtils.Extend($requestOptions, options);

        this.onopen = () : void => {
            // default handler
        };
        this.onclose = () : void => {
            // default handler
        };
        this.onmessage = () : void => {
            // default handler
        };
        this.onerror = () : void => {
            // default handler
        };

        this.initCommunication({
            protocols: $protocols,
            url      : $requestOptions
        });
    }

    public close() : void {
        try {
            this.client.close();
        } catch (ex) {
            this.readyState = this.client.readyState;
            throw ex;
        }
    }

    public send($data : any) : void {
        try {
            this.client.send($data);
        } catch (ex) {
            this.readyState = this.client.readyState;
            throw ex;
        }
    }

    private initCommunication($options : any) : void {
        Loader.getInstance().getHttpResolver().FetchHelper($options)
            .then(($environment : IFetchResolver) : void => {
                const options : any = $environment.options;
                this.client = new ($environment.client)(options.url.href, options.protocols, false, false, {
                    agent             : options.url.agent,
                    rejectUnauthorized: options.url.rejectUnauthorized,
                    strictSSL         : options.url.strictSSL
                });
                this.client.onopen = () : void => {
                    this.readyState = this.client.readyState;
                    this.onopen();
                };
                this.client.onclose = () : void => {
                    this.readyState = this.client.readyState;
                    this.onclose();
                };
                this.client.onmessage = ($event : MessageEvent) : void => {
                    this.readyState = this.client.readyState;
                    this.onmessage($event);
                };
                this.client.onerror = () : void => {
                    this.readyState = this.client.readyState;
                    this.onerror();
                };
            })
            .catch(($error : Error) : void => {
                this.readyState = WebSocket.CLOSED;
                this.onerror($error);
            });
    }
}

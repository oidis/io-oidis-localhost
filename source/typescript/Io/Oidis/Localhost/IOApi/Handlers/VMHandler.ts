/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IOHandlerType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/IOHandlerType.js";
import { IOHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IOHandler.js";
import { IOHandlerFactory } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/IOHandlerFactory.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Resources } from "@io-oidis-services/Io/Oidis/Services/DAO/Resources.js";
import { FileSystemHandler } from "../../Connectors/FileSystemHandler.js";
import { Loader } from "../../Loader.js";

/**
 * VMHandler class provides mechanism for creation of node VM and its sandbox.
 */
export class VMHandler extends BaseObject {
    private path : string;
    private data : any;
    private sandbox : any;
    private successHandler : any;
    private errorHandler : any;
    private timeout : number;
    private timeoutId : number;

    constructor() {
        super();
        this.path = null;
        this.timeout = -1;
        this.sandbox = {};
        this.successHandler = () : void => {
            // default success handler
        };
        this.errorHandler = ($error : Error) : void => {
            try {
                const console : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.CONSOLE);
                if (!ObjectValidator.IsEmptyOrNull($error) && !ObjectValidator.IsEmptyOrNull($error.message)) {
                    console.Print($error.message);
                } else if (!ObjectValidator.IsEmptyOrNull(this.path)) {
                    console.Print("Failed to load resource at: " + this.path);
                } else {
                    console.Print("Failed to load resource");
                }
            } catch (ex) {
                console.log(ex.stack); // eslint-disable-line no-console
            }
        };
    }

    /**
     * @param {string} [$value] Specify path to external data, which should be loaded.
     * @returns {string} Returns path to external data if it has been specified otherwise null.
     */
    public Path($value? : string) : string {
        return this.path = Property.String(this.path, $value);
    }

    /**
     * @param {Buffer|string} [$value] Specify data which should be executed inside VM.
     * @returns {Buffer|string} Returns data suitable for execution inside VM.
     */
    public Data($value? : any) : any {
        if (ObjectValidator.IsSet($value)) {
            this.data = $value;
        }
        return this.data;
    }

    /**
     * @param {number} [$value] Specify maximal time in milliseconds for script load.
     * @returns {number} Returns maximal time allowed for script load.
     */
    public Timeout($value? : number) : number {
        return this.timeout = Property.PositiveInteger(this.timeout, $value);
    }

    /**
     * @param {object} [$value] Specify environment which should be merged with sandbox created inside VM.
     * @returns {object} Returns sandbox used by VM if it was already loaded otherwise environment which should be used.
     */
    public Sandbox($value? : any) : any {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            Resources.Extend(this.sandbox, $value);
        }
        return this.sandbox;
    }

    /**
     * @param {Function} $handler Specify handler which should be invoked on successful VM load.
     * @returns {void}
     */
    public SuccessHandler($handler : () => void) : void {
        if (!ObjectValidator.IsEmptyOrNull($handler)) {
            this.successHandler = $handler;
        }
    }

    /**
     * @param {Function} $handler Specify method which should be invoked in case of load or execution errors.
     * @returns {void}
     */
    public ErrorHandler($handler : ($error : Error | ErrorEvent) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull($handler)) {
            this.errorHandler = $handler;
        }
    }

    /**
     * Init creation of VM and execution of the configured script.
     * @returns {void}
     */
    public Load() : void {
        if (!ObjectValidator.IsEmptyOrNull(this.path) && ObjectValidator.IsEmptyOrNull(this.data)) {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            if (fileSystem.Exists(this.path)) {
                this.data = <string>fileSystem.Read(this.path);
            } else {
                this.errorHandler(new Error("Unable to find resource at: " + this.path));
            }
        }

        if (!ObjectValidator.IsEmptyOrNull(this.data)) {
            const vm : any = require("vm");
            const sandbox : any = {};
            vm.createContext(sandbox);
            Resources.Extend(sandbox, {
                Buffer,
                process,
                require,
                console,
                module    : {
                    exports: null
                },
                global    : {},
                window    : {},
                JsonpData : globalThis.JsonpData,
                setTimeout,
                setInterval,
                clearTimeout,
                clearInterval,
                Error,
                ErrorEvent: null,
                __filename,
                __dirname,
                document,
                localStorage,
                WebSocket
            });
            Resources.Extend(sandbox, this.sandbox);
            Loader.getInstance().getAppConfiguration().namespaces.forEach(($item : string) : void => {
                const rootPart = $item.split(".")[0];
                if (!sandbox.hasOwnProperty(rootPart) && globalThis.hasOwnProperty(rootPart)) {
                    sandbox[rootPart] = globalThis[rootPart];
                    sandbox.global[rootPart] = sandbox[rootPart];
                    sandbox.window[rootPart] = sandbox[rootPart];
                }
            });
            this.sandbox = sandbox;

            if (!ObjectValidator.IsEmptyOrNull(this.timeoutId)) {
                clearTimeout(this.timeoutId);
            }
            if (this.timeout > 0) {
                this.timeoutId = <any>setTimeout(($error : Error) : void => {
                    this.errorHandler($error);
                }, this.timeout, new Error("Timeout reached"));
            }
            vm.runInContext(this.data, sandbox, ObjectValidator.IsEmptyOrNull(this.path) ? "In memory" : this.path);
            this.successHandler();
            if (!ObjectValidator.IsEmptyOrNull(this.timeoutId)) {
                clearTimeout(this.timeoutId);
            }
        } else {
            this.errorHandler(new Error("Path or body for resource must be defined before load."));
        }
    }
}

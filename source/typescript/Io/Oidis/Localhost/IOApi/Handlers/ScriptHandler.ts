/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonpFileReader } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/Handlers/JsonpFileReader.js";
import { ScriptHandler as Parent } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/Handlers/ScriptHandler.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ProtectionType } from "../../Enums/ProtectionType.js";
import { EnvironmentArgs } from "../../EnvironmentArgs.js";
import { ResourceVerifier } from "../../Utils/ResourceVerifier.js";
import { VMHandler } from "./VMHandler.js";

let strip : any = {};
if (!globalThis.isBrowser) {
     strip = (await import("@nodejs/strip-json-comments/index.js")).default;
}

export class ScriptHandler extends Parent {
    private protectionMap : any = null;
    private protectionType : ProtectionType = ProtectionType.NONE;
    private publicKey : string;

    public Data($value? : any) : any {
        if (this.protectionMap === null || this.protectionType === ProtectionType.NONE) {
            return super.Data($value);
        }

        if (!ObjectValidator.IsEmptyOrNull($value)) {
            return super.Data($value);
        }

        let data : Buffer = null;
        if (this.protectionType === ProtectionType.ENCRYPTION) {
            data = Buffer.from(super.Data($value), "hex");
        } else if (this.protectionType === ProtectionType.SIGNATURE || this.protectionType === ProtectionType.HASH) {
            data = Buffer.from(super.Data($value), "utf8");
        } else {
            throw new Error("Unsupported protection type: " + this.protectionType);
        }

        if (data === null) {
            throw new Error("Data invalid.");
        }

        if (this.protectionType === ProtectionType.ENCRYPTION) {
            if ((<string[]>this.protectionMap).indexOf(this.Path()) > -1) {
                data = ResourceVerifier.DecryptResource(data, Buffer.alloc(32));
            }
        }

        if (this.protectionType === ProtectionType.SIGNATURE) {
            if (!ResourceVerifier.VerifyByName(data, this.publicKey,
                this.protectionMap, this.Path())) {
                throw new Error("Verification failed.");
            }
        }

        if (this.protectionType === ProtectionType.HASH) {
            if (!ResourceVerifier.VerifyHash(data, this.protectionMap, this.Path())) {
                throw new Error("Verification failed.");
            }
        }

        return data;
    }

    public setPublicKey($publicKey : string) : void {
        this.publicKey = $publicKey;
    }

    public setMap($map : any, protectionType : ProtectionType) : void {
        if (ObjectValidator.IsEmptyOrNull($map)) {
            return;
        }
        this.protectionType = protectionType;
        this.protectionMap = $map;
    }

    public Load() : void {
        const fs : any = require("fs");
        const path : any = require("path");

        if (ObjectValidator.IsEmptyOrNull(this.Path()) && ObjectValidator.IsEmptyOrNull(this.Data())) {
            this.errorHandler(new Error("Path or body for resource must be defined before load."));
        } else {
            const executeScript : boolean = ObjectValidator.IsEmptyOrNull(this.Path());
            let filepath : string = "";
            if (!executeScript) {
                filepath = path.normalize(this.Path());
            }

            const execute : any = () : void => {
                try {
                    const vm : VMHandler = new VMHandler();
                    const reader : any = ($data : any) : void => {
                        this.Data($data);
                    };
                    if (ObjectValidator.IsEmptyOrNull(JsonpFileReader.Data)) {
                        JsonpFileReader.Data = reader;
                    }
                    EnvironmentArgs.AppConfigData = reader;
                    vm.Path(filepath);
                    vm.Data(this.Data());
                    vm.Sandbox({
                        Io: globalThis.Io
                    });
                    vm.ErrorHandler(this.errorHandler);
                    vm.SuccessHandler(() : void => {
                        this.Data();
                        this.successHandler();
                    });
                    vm.Load();
                } catch (ex) {
                    this.errorHandler(ex);
                }
            };

            if (executeScript) {
                execute();
            } else if (fs.existsSync(filepath)) {
                this.Data(fs.readFileSync(filepath).toString());
                if (StringUtils.EndsWith(filepath, ".json")) {
                    try {
                        this.Data(JSON.parse(strip(this.Data())));
                        this.successHandler();
                    } catch (ex) {
                        this.errorHandler(ex);
                    }
                } else if (StringUtils.EndsWith(filepath, ".jsonp") || StringUtils.EndsWith(filepath, ".js")) {
                    execute();
                } else {
                    this.errorHandler(new Error("Unsupported script extension."));
                }
            } else {
                this.errorHandler(new Error("Unable to find script."));
            }
        }
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EnvironmentArgs as Parent, IEnvironmentConfig } from "@io-oidis-commons/Io/Oidis/Commons/EnvironmentArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IProject } from "./Interfaces/IProject.js";
import { ScriptHandler } from "./IOApi/Handlers/ScriptHandler.js";
import { Loader } from "./Loader.js";
import { EnvironmentHelper } from "./Utils/EnvironmentHelper.js";
import { ISchemaError, SchemaValidator } from "./Utils/SchemaValidator.js";

/**
 * EnvironmentArgs class provides environment args mainly set at build time.
 */
export class EnvironmentArgs extends Parent {
    private readonly isDebug : boolean;

    constructor() {
        super();
        this.isDebug = process.argv.indexOf("--debug") !== -1;
    }

    public HtmlOutputAllowed() : boolean {
        return false;
    }

    public getProcessID() : number {
        return process.pid;
    }

    public getProjectConfig() : IProject {
        return <IProject>super.getProjectConfig();
    }

    protected getConfigPaths() : string[] {
        let paths : string[] = super.getConfigPaths();
        if (!this.HtmlOutputAllowed()) {
            paths = [];
            let cwd : string = EnvironmentHelper.getNodejsRoot();
            if (StringUtils.Contains(cwd, "/dependencies/nodejs")) {
                cwd = StringUtils.Replace(cwd, "/dependencies/nodejs", "/build/target");
            }

            if (StringUtils.Contains(cwd, "/build/target")) {
                const privateConfPath : string = StringUtils.Remove(cwd + "/private.conf.json", "/build/target");
                paths.push(privateConfPath + "p");
                paths.push(privateConfPath);
            } else {
                const appName : string = this.getAppName();
                const getAppConfigs : any = ($cwd : string) : void => {
                    paths.push($cwd + "/" + appName + ".conf.json");
                    paths.push($cwd + "/" + appName + ".config.jsonp");
                };
                paths.push(cwd + "/" + appName + ".config.jsonp");
                paths.push(cwd + "/private.conf.json");
                getAppConfigs(cwd + "/..");
                let appDataPath : string = this.getProjectConfig().target.appDataPath;
                if (!ObjectValidator.IsEmptyOrNull(appDataPath)) {
                    appDataPath = StringUtils.Replace(appDataPath, "\\", "/");
                    this.getProjectConfig().target.appDataPath = appDataPath;
                    getAppConfigs(appDataPath);
                }
            }
        }
        return paths;
    }

    protected fileLoaderClass() : any {
        return ScriptHandler;
    }

    protected validate($config : IEnvironmentConfig) : boolean {
        let passed : boolean = false;
        const validator : SchemaValidator = this.getValidator();
        if (!ObjectValidator.IsEmptyOrNull(validator)) {
            try {
                const errors : ISchemaError[] = validator.Validate($config.data);
                if (!ObjectValidator.IsEmptyOrNull(errors)) {
                    this.printHandler("ERROR: invalid configuration detected at: " + $config.path + "\n" +
                        validator.ErrorsToString(errors), true);
                } else {
                    passed = true;
                }
            } catch (ex) {
                this.printHandler("> failed validation of " + $config.path + " " + ex.stack);
            }
        } else {
            this.printHandler("> validator is empty");
        }

        if (!passed || !super.validate($config)) {
            Loader.getInstance().Exit(-1);
            return false;
        }
        return true;
    }

    protected printHandler($message : string, $force : boolean = false) : void {
        if (this.isDebug || $force) {
            console.log($message); // eslint-disable-line no-console
        }
    }

    protected errorHandler($error : Error | ErrorEvent, $path? : string) : boolean {
        if (StringUtils.ContainsIgnoreCase($error.message, "Unable to find script")) {
            return super.errorHandler($error, $path);
        }
        this.printHandler("ERROR: " + (ObjectValidator.IsSet((<Error>$error).stack) ? (<Error>$error).stack : $error.message), true);
        Loader.getInstance().Exit(-1);
    }

    private getValidator() : SchemaValidator {
        let validator : SchemaValidator = null;
        try {
            validator = new SchemaValidator();
            validator.Schema(__dirname + "/../../resource/configs/package.conf.schema.json");
            validator.ErrorHandler(($error : Error) : void => {
                this.printHandler($error.message);
            });
        } catch (ex) {
            this.printHandler("> failed initialization of config validator\n" + ex.stack);
        }
        this.getValidator = () : any => {
            return validator;
        };
        return validator;
    }
}

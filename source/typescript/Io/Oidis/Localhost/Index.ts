/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IndexPage } from "@io-oidis-gui/Io/Oidis/Gui/Pages/IndexPage.js";
import { AsyncBasePageController } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/Resolvers/AsyncBasePageController.js";
import { AuthConnectorTest } from "./RuntimeTests/AuthConnectorTest.js";
import { ExternAndRouteTest } from "./RuntimeTests/ExternAndRouteTest.js";
import { RestServiceTest } from "./RuntimeTests/RestServiceTest.js";

export class Index extends AsyncBasePageController {

    constructor() {
        super();
        this.setInstanceOwner(new IndexPage());
    }

    protected async onSuccess($instance : IndexPage) : Promise<void> {
        $instance.headerTitle.Content("Oidis Localhost");
        $instance.headerInfo.Content("Oidis Framework code base for Node.js-based localhost services");

        let content : string = "";
        /* dev:start */
        content += `
            <h3>Runtime tests</h3>
            <a href="${RestServiceTest.CallbackLink()}">Back-end connection test</a>
            <a href="${AuthConnectorTest.CallbackLink()}">Auth connector access test</a>
            <a href="${ExternAndRouteTest.CallbackLink()}">Extern/Route test</a>
            `;
        /* dev:end */

        $instance.pageIndex.Content(content);
        $instance.footerInfo.Content("" +
            "version: " + this.getEnvironmentArgs().getProjectVersion() +
            ", build: " + this.getEnvironmentArgs().getBuildTime() +
            ", " + this.getEnvironmentArgs().getProjectConfig().target.copyright);
    }
}

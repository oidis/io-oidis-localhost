/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IOHandlerType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/IOHandlerType.js";
import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { ILoggerConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IProject.js";
import { IOHandlerFactory } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/IOHandlerFactory.js";
import { Loader as CommonsLoader } from "@io-oidis-commons/Io/Oidis/Commons/Loader.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Resources } from "@io-oidis-services/Io/Oidis/Services/DAO/Resources.js";
import { Loader as Parent } from "@io-oidis-services/Io/Oidis/Services/Loader.js";
import { ConnectorsRegister } from "./Connectors/ConnectorsRegister.js";
import { FileSystemHandler } from "./Connectors/FileSystemHandler.js";
import { Terminal } from "./Connectors/Terminal.js";
import { LogAdapterType } from "./Enums/LogAdapterType.js";
import { EnvironmentArgs } from "./EnvironmentArgs.js";
import "./ForceCompile.js";
import { HttpManager } from "./HttpProcessor/HttpManager.js";
import { HttpResolver } from "./HttpProcessor/HttpResolver.js";
import { HttpServer } from "./HttpProcessor/HttpServer.js";
import { RESTResolver } from "./HttpProcessor/Resolvers/RESTResolver.js";
import { CallbackResponse } from "./HttpProcessor/ResponseApi/Handlers/CallbackResponse.js";
import { ResponseFactory } from "./HttpProcessor/ResponseApi/ResponseFactory.js";
import { IProject, IProjectTarget } from "./Interfaces/IProject.js";
import { IResponse } from "./Interfaces/IResponse.js";
import { IRoute } from "./Interfaces/IRoute.js";
import { VMHandler } from "./IOApi/Handlers/VMHandler.js";
import { FileAdapter } from "./LogProcessor/Adapters/FileAdapter.js";
import { JsonAdapter } from "./LogProcessor/Adapters/JsonAdapter.js";
import { StdAdapter } from "./LogProcessor/Adapters/StdAdapter.js";
import { Logger } from "./LogProcessor/Logger.js";
import { IntegrityCheck } from "./Primitives/Decorators.js";
import { ProgramArgs } from "./Structures/ProgramArgs.js";
import { EnvironmentHelper } from "./Utils/EnvironmentHelper.js";
import { FirewallManager } from "./Utils/FirewallManager.js";
import { Metrics } from "./Utils/Metrics.js";
import { WebSocket } from "./WebServiceApi/Clients/WebSocket.js";

export class Loader extends Parent {
    protected logger : Logger;
    private fileSystem : FileSystemHandler;
    private terminal : Terminal;
    private server : HttpServer;
    private appArgs : ProgramArgs;
    private isClosing : boolean;
    private readonly startTime : Date;
    private connectorsRegister : ConnectorsRegister;

    public static Load($appConfig : string | IProject) : void {
        try {
            const instance : Loader = new (<any>this)();
            (<any>CommonsLoader).singleton = instance;
            instance.isClosing = false;

            if (process.argv.indexOf("--debug") !== -1) {
                console.log("Loading application config..."); // eslint-disable-line no-console
            }
            if (!ObjectValidator.IsSet((<any>globalThis).Io.Oidis.Builder)) {
                (<any>globalThis).Io.Oidis.Builder = {
                    DAO: {
                        Resources
                    }
                };
            }
            instance.logger = new Logger();
            instance.Process($appConfig);
            EnvironmentHelper.NormalizeProxySetting(instance.getAppConfiguration());
            instance.initDOM();
            instance.initLogIt();

            const exitHandler : any = () : void => {
                instance.Exit();
            };
            process.on("exit", exitHandler);
            process.on("SIGINT", exitHandler);
            process.on("SIGTERM", exitHandler);
            process.on("uncaughtException", ($ex) : void => {
                Metrics.Error($ex);
                ExceptionsManager.HandleException($ex);
            });
            process.on("warning", ($data : any) : void => {
                if ($data.name !== "ExperimentalWarning") {
                    LogIt.Warning($data.toString());
                }
            });
            (<any>global).window.close = exitHandler;

            if (!EnvironmentHelper.IsDetached()) {
                try {
                    if (EnvironmentHelper.IsTTY()) {
                        require("readline").emitKeypressEvents(process.stdin);
                        process.stdin.setRawMode(true);
                        process.stdin.on("keypress", ($character : string, $key : any) : void => {
                            if ($key.ctrl && ($key.name === "c" || $key.name === "q" || $key.name === "x")) {
                                Loader.getInstance().Exit();
                            }
                        });
                    } else {
                        process.stdin.on("data", ($value : string) : void => {
                            $value = (<any>$value.toString()).trim();
                            if ($value === "exit" || $value === "quit") {
                                Loader.getInstance().Exit();
                            }
                        });
                    }
                } catch (ex) {
                    // unable to hook on stdin
                }
            }

            instance.appArgs = instance.initProgramArgs();
            instance.appArgs.Parse([].concat(process.argv));

            instance.prepareLoader();

            if (instance.appArgs.IsIntegrityCheck()) {
                setTimeout(() : void => {
                    LogIt.Info("Starting integrity check...");
                    // TODO(mkelnar) check if <binpath?>/resource/metadata.json exists
                    (<any>instance).processIntegrityCheck()
                        .then(() : void => {
                            LogIt.Info("Integrity check succeed.");
                            instance.Exit(0);
                        })
                        .catch(($error : Error) : void => {
                            LogIt.Error($error.message);
                            instance.Exit(-1);
                        });
                }, 500);
            } else {
                instance.processProgramArgs();
            }
        } catch (ex) {
            process.stderr.write("Application start error. " + ex.stack);
            process.exit(1);
        }
    }

    public static getInstance() : Loader {
        return <Loader>super.getInstance();
    }

    private static getIntegrityMetadata() : any {
        const retVal : any = JSON.parse(Loader.getInstance().getFileSystemHandler()
            .Read(Loader.getInstance().getProgramArgs().BinBase() + "/resource/metadata.json").toString());
        Loader.getIntegrityMetadata = () : any => {
            return retVal;
        };
        return retVal;
    }

    @IntegrityCheck()
    private static async npmModules() : Promise<IIntegrityReport> {
        const report : IIntegrityReport = {name: "npmModules", cases: []};
        const modules : string[] = Loader.getIntegrityMetadata().modules;
        LogIt.Info("validate modules: {0}", Object.keys(modules).length);
        let issues : number = 0;
        for (const module of Object.keys(modules)) {
            if (Loader.getInstance().getProgramArgs().getIgnoredChecks().includes(module)) {
                report.cases.push({name: module, skipped: true});
                LogIt.Warning("Check for '" + module + "' module ignored by CLI option.");
                continue;
            }
            if (!StringUtils.Contains(module, "@types", "source-map-support")) {
                LogIt.Info("validating module: {0}", module);
                try {
                    await import(module);
                    report.cases.push({name: module});
                } catch ($e) {
                    report.cases.push({name: module, error: $e});
                    issues++; // eslint-disable-line @typescript-eslint/no-unused-vars
                    LogIt.Warning(module + ": " + $e.message);
                }
            }
        }
        return report;
    }

    @IntegrityCheck()
    private static async npmNativeModules() : Promise<IIntegrityReport> {
        const report : IIntegrityReport = {name: "npmNativeModules", cases: []};
        const modules : string[] = Loader.getIntegrityMetadata().native;
        let issues : number = 0;
        let osKey : string = "/linux";
        if (EnvironmentHelper.IsWindows()) {
            osKey = "/win";
        } else if (EnvironmentHelper.IsMac()) {
            osKey = "/darwin";
        }
        for (const module of modules) {
            if (Loader.getInstance().getProgramArgs().getIgnoredChecks().includes(module)) {
                report.cases.push({name: module, skipped: true});
                LogIt.Warning("Check for '" + module + "' module ignored by CLI option.");
                continue;
            }
            if (!StringUtils.ContainsIgnoreCase(module, osKey)) {
                report.cases.push({name: module, skipped: true});
                LogIt.Warning("Check for '" + module + "' module skipped, different platform.");
                continue;
            }
            LogIt.Info("validating native module: {0}", module);
            try {
                require(module);
                report.cases.push({name: module});
            } catch ($e) {
                report.cases.push({name: module, error: $e});
                issues++; // eslint-disable-line @typescript-eslint/no-unused-vars
                LogIt.Warning(module + ": " + $e.message);
            }
        }
        return report;
    }

    constructor() {
        super();

        this.startTime = new Date();
    }

    public getHttpResolver() : HttpResolver {
        return <HttpResolver>super.getHttpResolver();
    }

    public getHttpManager() : HttpManager {
        return <HttpManager>super.getHttpManager();
    }

    public getEnvironmentArgs() : EnvironmentArgs {
        return <EnvironmentArgs>super.getEnvironmentArgs();
    }

    public getAppConfiguration() : IProject {
        return this.getEnvironmentArgs().getProjectConfig();
    }

    public getProgramArgs() : ProgramArgs {
        return this.appArgs;
    }

    public getFileSystemHandler() : FileSystemHandler {
        if (!ObjectValidator.IsSet(this.fileSystem)) {
            this.fileSystem = this.initFileSystem();
            this.getFileSystemHandler = () : FileSystemHandler => {
                return this.fileSystem;
            };
        }
        return this.fileSystem;
    }

    public getTerminal() : Terminal {
        if (!ObjectValidator.IsSet(this.terminal)) {
            this.terminal = this.initTerminal();
            this.getTerminal = () : Terminal => {
                return this.terminal;
            };
        }
        return this.terminal;
    }

    public setAnonymizer($patterns? : any) : void {
        this.logger.setAnonymizer($patterns);
    }

    public Exit($exitCode? : number) : void {
        if (!this.isClosing) {
            this.isClosing = true;
            this.onCloseHandler(($handlerExitCode? : number) : void => {
                if (ObjectValidator.IsSet($handlerExitCode)) {
                    $exitCode = $handlerExitCode;
                }
                process.exit($exitCode);
            });
        }
    }

    public getUptime() : number {
        return new Date().getTime() - this.startTime.getTime();
    }

    public getConnectorsRegister() : ConnectorsRegister {
        if (ObjectValidator.IsEmptyOrNull(this.connectorsRegister)) {
            this.connectorsRegister = this.initConnectorsRegister();
            this.getConnectorsRegister = () : ConnectorsRegister => {
                return this.connectorsRegister;
            };
        }
        return this.connectorsRegister;
    }

    protected selfCheck() : void {
        try {
            const response : IResponse = ResponseFactory.getResponse(null);
            response.OnError = () : void => {
                this.Exit(1);
            };
            (<any>response).OnComplete = () : void => {
                this.Exit(0);
            };

            this.getFileSystemHandler().Download({
                streamOutput: true,
                url         : "http://localhost:" + this.getAppConfiguration().domain.ports.http
            }, response, () : boolean => {
                this.Exit(1);
                return false;
            });
        } catch (ex) {
            this.Exit(1);
        }
    }

    protected initProgramArgs() : ProgramArgs {
        return new ProgramArgs();
    }

    protected initEnvironment() : EnvironmentArgs {
        return new EnvironmentArgs();
    }

    protected initHttpServer() : HttpServer {
        return new HttpServer();
    }

    protected initResolver() : HttpResolver {
        return new HttpResolver("/");
    }

    protected initFileSystem() : FileSystemHandler {
        return new FileSystemHandler();
    }

    protected initTerminal() : Terminal {
        return new Terminal();
    }

    protected initConnectorsRegister() : ConnectorsRegister {
        return new ConnectorsRegister();
    }

    protected prepareLoader() : void {
        if (this.appArgs.IsVerbose()) {
            LogIt.setLevel(LogLevel.ALL);
        } else if (this.appArgs.IsDebug()) {
            LogIt.setLevel(LogLevel.DEBUG);
        }
        const config : ILoggerConfiguration = JsonUtils.Extend({}, this.getAppConfiguration().logger);
        if (this.appArgs.LogLevel() !== 0) {
            config.level = this.appArgs.LogLevel();
        }
        if (ObjectValidator.IsEmptyOrNull(config.debug) || this.appArgs.IsDebug()) {
            config.debug = this.appArgs.IsDebug();
        }
        if (ObjectValidator.IsEmptyOrNull(config.verbose) || this.appArgs.IsVerbose()) {
            config.verbose = this.appArgs.IsVerbose();
        }
        if (this.appArgs.IsDebug()) {
            this.getAppConfiguration().sentry.debug = true;
        }
        this.logger.setConfig(config);

        const target : IProjectTarget = this.getAppConfiguration().target;
        this.appArgs.BinBase(this.getFileSystemHandler().NormalizePath(__dirname + "/../.."));
        this.appArgs.ProjectBase(this.getCwd());
        this.appArgs.AppDataPath(target.appDataPath);

        if (!StringUtils.Contains(this.appArgs.ProjectBase(), "/build/target")) {
            if (ObjectValidator.IsEmptyOrNull(this.appArgs.AppDataPath())) {
                this.appArgs.AppDataPath(this.getFileSystemHandler().getTempPath() + "/" + this.getEnvironmentArgs().getProjectName());
            }
        } else {
            this.appArgs.AppDataPath(this.appArgs.ProjectBase());
        }

        if (ObjectValidator.IsEmptyOrNull(this.appArgs.TargetBase())) {
            if (ObjectValidator.IsEmptyOrNull(target.projectBase)) {
                target.projectBase = this.appArgs.BinBase();
            }
            const fs : any = require("fs");
            const targetPath : string = target.projectBase;
            if (!ObjectValidator.IsEmptyOrNull(targetPath) && fs.existsSync(targetPath)) {
                this.appArgs.TargetBase(this.getFileSystemHandler().NormalizePath(StringUtils.Remove(targetPath, "/index.html")));
            } else if (fs.existsSync(__dirname + "/../../../index.html")) {
                this.appArgs.TargetBase(this.getFileSystemHandler().NormalizePath(__dirname + "/../../.."));
            } else {
                this.appArgs.TargetBase(this.appArgs.BinBase());
            }
        } else if (StringUtils.EndsWith(this.appArgs.TargetBase(), "index.html")) {
            this.appArgs.TargetBase(StringUtils.Remove(this.getFileSystemHandler().NormalizePath(this.appArgs.TargetBase()),
                "/index.html"));
        }
        target.projectBase = this.appArgs.TargetBase();

        this.setAnonymizer({
            appData    : this.getProgramArgs().AppDataPath(),
            binBase    : this.getProgramArgs().BinBase(),
            projectBase: this.getProgramArgs().ProjectBase(),
            targetBase : this.getProgramArgs().TargetBase()
        });
        process.env.PATH = this.getTerminal().NormalizeEnvironmentPath(this.getAppEnvironmentPath());
    }

    protected processProgramArgs() : boolean {
        let processed : boolean = false;
        if (this.appArgs.IsTestRun()) {
            processed = true;
            if (this.appArgs.IsDebug()) {
                Echo.Print("Detected --test-run argument: processing of other args will be skipped");
            }
        } else if (this.appArgs.IsHelp()) {
            this.appArgs.IsHelp(false);
            processed = true;
            this.appArgs.setStdinHandler(($data : string) : void => {
                if (ObjectValidator.IsEmptyOrNull($data)) {
                    this.Exit();
                } else {
                    Echo.Println("");
                    this.appArgs.Parse(StringUtils.Split($data, " "));
                    this.processProgramArgs();
                }
            });
            this.appArgs.PrintHelp();
        } else if (this.appArgs.IsVersion()) {
            this.appArgs.PrintVersion();
            this.Exit();
        } else if (this.appArgs.IsPath()) {
            this.appArgs.PrintPath();
            this.Exit();
        } else if (this.appArgs.IsSelfCheck()) {
            processed = true;
            this.selfCheck();
        } else if (this.appArgs.ListRoutes()) {
            Echo.Printf("REST API routes: ");
            RESTResolver.getRoutes().forEach(($value : IRoute) : void => {
                Echo.Printf("    " + $value.options.httpMethod + " " + $value.route + " " + $value.methodName());
            });
            this.Exit(0);
            return;
        } else if (this.appArgs.StartServer() || this.appArgs.RestartServer()) {
            processed = true;
            if (!ObjectValidator.IsEmptyOrNull(this.server)) {
                this.server.Stop();
            }
            Echo.Print(this.printLogo());
            FirewallManager.AddServiceRule(
                "Oidis",
                "Oidis Framework services",
                this.getEnvironmentArgs().getAppName(),
                "Allow incoming network traffic to Oidis Framework",
                this.appArgs.ProjectBase() + "/" + this.getEnvironmentArgs().getAppName(), () : void => {
                    this.server = this.initHttpServer();
                    this.server.setOnCloseHandler(() : void => {
                        LogIt.Info("Server thread has been terminated.");
                        this.Exit();
                    });
                    this.server.Start();
                });
        } else if (this.appArgs.StopServer()) {
            processed = true;
            if (!ObjectValidator.IsEmptyOrNull(this.server)) {
                this.server.Stop();
            }
        }

        return processed;
    }

    protected onCloseHandler($done : ($exitCode? : number) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.server)) {
            this.server.Stop();
        }
        $done();
    }

    protected getHttpServer() : HttpServer {
        return this.server;
    }

    protected getAppEnvironmentPath() : string {
        return this.appArgs.ProjectBase();
    }

    protected getRequestPatterns() : string[] {
        let patterns : string[] = [
            process.cwd() + "/*",
            this.getCwd() + "/*",
            this.getProgramArgs().AppDataPath() + "/*",
            this.getProgramArgs().TargetBase() + "/*",
            this.getProgramArgs().BinBase() + "/*"
        ];
        const targetRequestPatterns : string[] = this.getAppConfiguration().target.requestPatterns;
        if (!ObjectValidator.IsEmptyOrNull(targetRequestPatterns)) {
            patterns = patterns.concat(targetRequestPatterns);
        }
        if (EnvironmentHelper.IsWindows()) {
            const output : string[] = [];
            patterns.forEach(($pattern : string) : void => {
                if (StringUtils.Contains($pattern, ":/")) {
                    $pattern = StringUtils.ToUpperCase($pattern[0]) + StringUtils.Substring($pattern, 1);
                }
                output.push($pattern);
            });
            patterns = output;
        }
        this.getRequestPatterns = () : string[] => {
            return patterns;
        };
        return patterns;
    }

    protected printLogo() : string {
        // https://patorjk.com/software/taag/#p=display&f=Doh&t=Oidis
        return "" +
            "                                                                             \n" +
            "                                          dddddddd                           \n" +
            "        OOOOOOOOO       iiii              d::::::d  iiii                     \n" +
            "      OO:::::::::OO    i::::i             d::::::d i::::i                    \n" +
            "    OO:::::::::::::OO   iiii              d::::::d  iiii                     \n" +
            "   O:::::::OOO:::::::O                    d:::::d                            \n" +
            "   O::::::O   O::::::Oiiiiiii     ddddddddd:::::d iiiiiii     ssssssssss     \n" +
            "   O:::::O     O:::::Oi:::::i   dd::::::::::::::d i:::::i   ss::::::::::s    \n" +
            "   O:::::O     O:::::O i::::i  d::::::::::::::::d  i::::i ss:::::::::::::s   \n" +
            "   O:::::O     O:::::O i::::i d:::::::ddddd:::::d  i::::i s::::::ssss:::::s  \n" +
            "   O:::::O     O:::::O i::::i d::::::d    d:::::d  i::::i  s:::::s  ssssss   \n" +
            "   O:::::O     O:::::O i::::i d:::::d     d:::::d  i::::i    s::::::s        \n" +
            "   O:::::O     O:::::O i::::i d:::::d     d:::::d  i::::i       s::::::s     \n" +
            "   O::::::O   O::::::O i::::i d:::::d     d:::::d  i::::i ssssss   s:::::s   \n" +
            "   O:::::::OOO:::::::Oi::::::id::::::ddddd::::::ddi::::::is:::::ssss::::::s  \n" +
            "    OO:::::::::::::OO i::::::i d:::::::::::::::::di::::::is::::::::::::::s   \n" +
            "      OO:::::::::OO   i::::::i  d:::::::::ddd::::di::::::i s:::::::::::ss    \n" +
            "        OOOOOOOOO     iiiiiiii   ddddddddd   dddddiiiiiiii  sssssssssss      \n" +
            "                                                                             \n" +
            `${this.getEnvironmentArgs().getAppName()} version: ${this.getEnvironmentArgs().getProjectVersion()}, build: ${this.getEnvironmentArgs().getBuildTime()}\n` +
            "                                                                             \n";
    }

    protected getCwd() : string {
        let cwd : string = EnvironmentHelper.getNodejsRoot();
        if (StringUtils.Contains(cwd, "/dependencies/nodejs")) {
            cwd = StringUtils.Replace(cwd, "/dependencies/nodejs", "/build/target");
        }
        this.getCwd = () : string => {
            return cwd;
        };
        return cwd;
    }

    protected initDOM() : any {
        const os : any = require("os");
        const fs : any = require("fs");
        const url : any = require("url");
        const LocalStorage : any = require("node-localstorage").LocalStorage;
        globalThis.WebSocket = <any>WebSocket;
        const {JSDOM, ResourceLoader, VirtualConsole} = require("jsdom");

        let localStoragePath : string;
        const cwd : string = this.getCwd();
        if (StringUtils.Contains(cwd, "/build/target")) {
            localStoragePath = cwd;
        } else {
            localStoragePath = this.getFileSystemHandler().getTempPath() + "/" +
                this.getEnvironmentArgs().getProjectName();
        }
        localStoragePath += "/localStorage";
        if (!fs.existsSync(localStoragePath)) {
            fs.mkdirSync(localStoragePath, {recursive: true});
        }
        const localStorageInstance : typeof LocalStorage = new LocalStorage(localStoragePath);

        this.patchResourceLoader(ResourceLoader);

        const virtualConsole : typeof VirtualConsole = new VirtualConsole();
        virtualConsole.on("jsdomError", ($error : Error) : void => {
            if (!StringUtils.ContainsIgnoreCase($error.message, "Could not load")) {
                LogIt.Warning($error.stack);
            }
        });
        let strictSSL = true;
        if (process.env.hasOwnProperty("DISABLE_STRICT_SSL") || Loader.getInstance().getAppConfiguration().disableStrictSSL) {
            strictSSL = false;
        }
        const dom : typeof JSDOM = new JSDOM("", {
            url                 : "http://" + this.getHttpManager().getRequest().getServerIP() + "/?" +
                "AppName=" + ObjectEncoder.Url(this.getEnvironmentArgs().getAppName()) + "&" +
                "AppPid=" + process.pid + "&" +
                "Platform=" + this.getEnvironmentArgs().getPlatform() + "&" +
                "ReleaseName=" + this.getEnvironmentArgs().getReleaseName(),
            contentType         : "text/html",
            userAgent           : "Node.js/" + process.version + " (" + os.platform() + "; rv: " + process.version + ") " +
                "Chrome/52.0.2743.116",
            includeNodeLocations: true,
            resources           : new ResourceLoader({
                strictSSL
            }),
            runScripts          : "dangerously",
            virtualConsole
        });
        this.getEnvironmentArgs().getNamespaces().forEach(($namespace : string) : void => {
            const parts : string[] = StringUtils.Split($namespace, ".");
            if (parts.length >= 1 && global.hasOwnProperty(parts[0])) {
                dom.window[parts[0]] = global[parts[0]];
            }
        });
        dom.window.scrollTo = () : void => {
            // default handler
        };
        (<any>dom.window).localStorage = localStorageInstance;
        (<any>dom.window).JsonpData = globalThis.JsonpData;
        (<any>dom.window).WebSocket = WebSocket;
        (<any>window).location = {
            __href: ""
        };
        Object.defineProperty(window.location, "href", {
            get() : string {
                return this.__href;
            },
            set($value : string) : void {
                this.__href = $value;
                const parsedUrl : any = url.parse($value);
                let property : string;
                for (property in parsedUrl) {
                    if (parsedUrl.hasOwnProperty(property) && property !== "href") {
                        this[property] = parsedUrl[property];
                    }
                }
            }
        });
        let domProperty : string;
        for (domProperty in dom.window) {
            if (domProperty !== "location") {
                window[domProperty] = dom.window[domProperty];
            }
        }
        for (domProperty in dom.window.location) {
            if (dom.window.location.hasOwnProperty(domProperty) && domProperty !== "href") {
                window.location[domProperty] = dom.window.location[domProperty];
            }
        }
        globalThis.localStorage = localStorageInstance;
        globalThis.NodeList = dom.window.NodeList;

        document = window.document;
        (<any>global).window = window;
        (<any>global).document = document;
        Object.keys((<any>global).window).forEach(($property : string) : void => {
            if (typeof global[$property] === "undefined") {
                global[$property] = (<any>global).window[$property];
                globalThis[$property] = global[$property];
            }
        });

        const element : HTMLElement = document.createElement("span");
        element.id = "Content";
        document.body.appendChild(element);
        (<any>dom).reconfigure({url: "file://" + (EnvironmentHelper.IsWindows() ? "/" : "") + this.getCwd() + "/"});
        return dom;
    }

    protected async processIntegrityCheck() : Promise<void> {
        let junitContent : string = "";
        const escapeXmlData : any = ($value : string) : string => {
            $value = StringUtils.Replace($value, this.getProgramArgs().AppDataPath(), "");
            $value = StringUtils.Replace($value, "&", "&amp;");
            $value = StringUtils.Replace($value, "'", "&apos;");
            $value = StringUtils.Replace($value, "<", "&lt;");
            $value = StringUtils.Replace($value, ">", "&gt;");
            $value = StringUtils.Replace($value, "\"", "&quot;");
            return $value;
        };
        const casePass : any = ($suite : string, $name : string) => {
            junitContent += `
    <testcase classname="${$suite}" name="${$name}"/>`;
        };
        const caseSkip : any = ($suite : string, $name : string) => {
            junitContent += `
    <testcase classname="${$suite}" name="${$name}">
        <skipped />
    </testcase>`;
        };
        const caseFail : any = ($suite : string, $name : string, $error : Error) => {
            junitContent += `
    <testcase classname="${$suite}" name="${$name}">
        <failure message="${escapeXmlData($error.message)}" type="failure">
            ${escapeXmlData($error.stack)}
        </failure>
    </testcase>`;
        };

        if (!Loader.getInstance().getFileSystemHandler()
            .Exists(Loader.getInstance().getProgramArgs().BinBase() + "/resource/metadata.json")) {
            throw new Error("Metadata file not found.");
        } else {
            LogIt.Debug("integrity metadata: " + Loader.getInstance().getFileSystemHandler()
                .Read(Loader.getInstance().getProgramArgs().BinBase() + "/resource/metadata.json").toString());
        }
        if (!ObjectValidator.IsEmptyOrNull((<any>window).oidisIntegrityCheckSteps)) {
            let issueCounter : number = 0;
            let casesTotal : number = 0;
            let timeoutId : number = null;
            const steps : any[] = [];
            // expect that last method in chain will be lastly generated by decorator, this is not bullet-proof but should be OK
            (<any>window).oidisIntegrityCheckSteps.map(($function : () => any) => $function()).forEach(($item : any) => {
                const index : number = steps.findIndex(($element) => $element.name === $item.name);
                if (index === -1) {
                    steps.push($item);
                } else {
                    steps[index].method = $item.method;
                }
            });
            const processCheck : any = async () : Promise<void> => {
                for await(const step of steps) {
                    try {
                        LogIt.Info(step.name + "...");
                        const report : IIntegrityReport = await Promise.race([
                            step.method(),
                            new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) : void => {
                                timeoutId = <any>setTimeout(() : void => {
                                    $reject(new Error("Integrity check timeout limit expired."));
                                }, 60000);  // TODO(mkelnar) apply configurable timeout per step here
                            })
                        ]);

                        if (!ObjectValidator.IsEmptyOrNull(timeoutId)) {
                            clearTimeout(timeoutId);
                        }
                        LogIt.Info(step.name + " succeed\n");
                        if (!ObjectValidator.IsEmptyOrNull(report)) {
                            report.cases.forEach(($item : IIntegrityCase) => {
                                casesTotal++;
                                if (!ObjectValidator.IsEmptyOrNull($item.error)) {
                                    issueCounter++;
                                    caseFail(report.name, $item.name, $item.error);
                                } else if (!ObjectValidator.IsEmptyOrNull($item.skipped) && $item.skipped) {
                                    caseSkip(report.name, $item.name);
                                } else {
                                    casePass(report.name, $item.name);
                                }
                            });
                        } else {
                            casesTotal++;
                            casePass(step.name, step.name);
                        }
                    } catch ($e) {
                        casesTotal++;
                        issueCounter++;
                        casePass(step.name, step.name, $e);
                        LogIt.Warning(step.name + " failed: " + $e.message);
                    }
                }
            };
            await Promise.race([
                processCheck(),
                new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) : void => {
                    timeoutId = <any>setTimeout(() : void => {
                        $reject(new Error("Integrity checks timeout limit expired. At least one check step takes too long."));
                    }, 65000);  // TODO(mkelnar) apply configurable timeout for whole check here
                })
            ]);
            if (!ObjectValidator.IsEmptyOrNull(timeoutId)) {
                clearTimeout(timeoutId);
            }

            const junitData : string = `<?xml version="1.0" encoding="UTF-8"?>
<testsuites name="Integrity tests" tests="${casesTotal}" failures="${issueCounter}" errors="${issueCounter}">${junitContent}
</testsuites>`;
            this.fileSystem.Write("integrityCheck.xml", junitData);

            if (issueCounter > 0 && !!process.env.OIDIS_CI_RUN) {
                throw Error("Integrity checks (" + issueCounter + ") failed.");
            }
        }
    }

    private patchResourceLoader($original : any) : void {
        const fs : any = require("fs");
        const path : any = require("path");
        const os : any = require("os");
        const origResourceFetch : any = $original.prototype.fetch;
        const instance : Loader = this; // eslint-disable-line @typescript-eslint/no-this-alias
        let patterns : string[] = null;
        let lastUrl : string = "";

        $original.prototype._fetch = origResourceFetch;
        $original.prototype.fetch = function ($url : string, $options : any) : any {
            try {
                let isRemote : boolean = false;
                if (!StringUtils.Contains($url, "http://", "https://")) {
                    if (EnvironmentHelper.IsWindows()) {
                        $url = StringUtils.Remove($url, "file:///");
                        if (StringUtils.Contains($url, ":/")) {
                            $url = StringUtils.ToUpperCase($url[0]) + StringUtils.Substring($url, 1);
                        }
                    }
                    $url = StringUtils.Remove($url, "file://");
                    if (StringUtils.Contains($url, "?")) {
                        $url = StringUtils.Substring($url, 0, StringUtils.IndexOf($url, "?"));
                    }
                    $url = path.resolve($url);
                    $url = StringUtils.Replace($url, "\\", "/");
                    const shapshotKey : string = "/snapshot/";
                    if (StringUtils.Contains($url, shapshotKey) && !StringUtils.StartsWith($url, shapshotKey)) {
                        $url = StringUtils.Substring($url, StringUtils.IndexOf($url, shapshotKey));
                    }
                    let error : Error;
                    if (!fs.existsSync($url)) {
                        if (lastUrl !== $url) {
                            LogIt.Debug("Resource has not been found at: {0} with cwd: {1}", $url, process.cwd());
                            lastUrl = $url;
                        }
                        error = new Error("Resource has not been found at: " + $url);
                    } else {
                        let accessAllowed : boolean = false;
                        if (patterns === null) {
                            patterns = instance.getRequestPatterns();
                        }
                        patterns.forEach(($pattern : string) : void => {
                            if (StringUtils.PatternMatched($pattern, $url)) {
                                accessAllowed = true;
                            }
                        });
                        if (!accessAllowed) {
                            if (lastUrl !== $url) {
                                LogIt.Debug("Access denied for resource at: {0} not in match with: {1}",
                                    $url, JSON.stringify(patterns));
                                lastUrl = $url;
                            }
                            error = new Error("Access denied for resource at: " + $url);
                        }
                    }
                    if (!ObjectValidator.IsEmptyOrNull(error)) {
                        return new Promise(($resolve : ($data : any) => void, $reject : ($ex : Error) => void) : void => {
                            $reject(error);
                        });
                    }
                    if (!StringUtils.StartsWith($url, "file://")) {
                        $url = "file:///" + $url;
                    }
                } else {
                    isRemote = true;
                }

                if (StringUtils.Contains($url, ".js")) {
                    return new Promise(($resolve : ($data : any) => void, $reject : ($ex : Error) => void) : void => {
                        const process : any = ($data : string) : void => {
                            try {
                                const vm : VMHandler = new VMHandler();
                                vm.Path($url);
                                vm.Data($data);
                                vm.Load();
                                vm.ErrorHandler(($ex : Error) : void => {
                                    LogIt.Debug("Failed to execute resource from: {0}",
                                        $url + os.EOL + $ex.stack);
                                    $reject($ex);
                                });
                                $resolve(Buffer.from(""));
                            } catch (ex) {
                                LogIt.Debug("Failed to process VM for resource from: {0}",
                                    $url + os.EOL + ex.stack);
                                $reject(ex);
                            }
                        };
                        const headers : any = {};
                        if (isRemote &&
                            StringUtils.StartsWith($url, "https://") &&
                            !StringUtils.StartsWith($options.referrer, "https://")) {
                            JsonUtils.Extend(headers, {
                                referrer: "https://localhost.oidis.io"
                            });
                        }
                        if (isRemote) {
                            const responseHandler : IResponse = new CallbackResponse(($headers : any, $body : string) : void => {
                                process($body);
                            });
                            responseHandler.OnError = () : void => {
                                LogIt.Debug("Failed to load resource from: {0}", $url);
                                $reject(new Error("Failed to load resource from: " + $url));
                            };
                            Loader.getInstance().getFileSystemHandler().Download({
                                headers,
                                streamOutput       : true,
                                streamReturnsBuffer: true,
                                url                : $url,
                                verbose            : false
                            }, responseHandler);
                        } else {
                            JsonUtils.Extend($options, headers);
                            origResourceFetch
                                .apply(this, [$url, $options])
                                .then(($data : string) : void => {
                                    process($data);
                                })
                                .catch(() : void => {
                                    LogIt.Debug("Failed to load resource from: {0}", $url);
                                    $reject(new Error("Failed to load resource from: " + $url));
                                });
                        }
                    });
                } else if (isRemote &&
                    StringUtils.ToLowerCase($options.element.nodeName) === "script" &&
                    $options.element.scriptType === "ScriptHandler") {
                    return origResourceFetch.apply(this, [$url, $options]);
                } else {
                    return new Promise(($resolve : ($data : any) => void, $reject : ($ex : Error) => void) : void => {
                        $reject(null);
                    });
                }
            } catch (ex) {
                LogIt.Debug("Failed request for resource from: {0}", $url + os.EOL + ex.message);
                return new Promise(($resolve : ($data : any) => void, $reject : ($ex : Error) => void) : void => {
                    $reject(ex);
                });
            }
        };
    }

    private initLogIt() : void {
        this.logger.setProjectBase(this.getCwd());
        this.logger.setAppDataPath(this.getAppConfiguration().target.appDataPath);
        (<FileAdapter>this.logger.getAdapter(LogAdapterType.FILE)).setProjectBase(this.getCwd());
        (<JsonAdapter>this.logger.getAdapter(LogAdapterType.JSON)).setProjectBase(this.getCwd());
        this.logger.setConfig(this.getAppConfiguration().logger);

        LogIt.Init(this.logger);

        const echoLogger : StdAdapter = new StdAdapter(false);
        echoLogger.Enabled(this.logger.getAdapter(LogAdapterType.STD).Enabled());
        Echo.Init(IOHandlerFactory.getHandler(IOHandlerType.CONSOLE), true);
        (<any>Echo).output.Print = ($message : string) : void => {
            echoLogger.Process({
                entryPoint: null,
                level     : LogLevel.INFO,
                message   : $message,
                severity  : LogSeverity.HIGH,
                time      : new Date().getTime(),
                trace     : null
            });
        };
        (<any>Echo).output.save = ($message : string) : void => {
            Echo.Init();
            (<any>Echo).output.Print($message);
        };
        Echo.Println = ($message : string) : void => {
            Echo.Print($message + echoLogger.NewLineType());
        };
    }
}

export interface IIntegrityReport {
    name : string;
    cases : IIntegrityCase[];
}

export interface IIntegrityCase {
    name : string;
    error? : Error;
    skipped? : boolean;
}

// generated-code-start
export const IIntegrityReport = globalThis.RegisterInterface(["name", "cases"]);
export const IIntegrityCase = globalThis.RegisterInterface(["name", "error", "skipped"]);
// generated-code-end

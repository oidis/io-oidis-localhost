/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";

export class HttpServerEventArgs extends EventArgs {
    private port : number;
    private clientId : number;

    constructor($port : number, $clientId? : number) {
        super();
        this.port = $port;
        this.clientId = $clientId;
    }

    public Port($value? : number) : number {
        return this.port = Property.Integer(this.port, $value);
    }

    public ClientId($value? : number) : number {
        return this.clientId = Property.Integer(this.clientId, $value);
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { HttpRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/GeneralEventOwner.js";
import { EventsManager as Parent } from "@io-oidis-gui/Io/Oidis/Gui/Events/EventsManager.js";
import { IEventsHandler } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/IEventsHandler.js";
import { Loader } from "../Loader.js";

export class EventsManager extends Parent {

    public Subscribe() : void {
        if (!Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed()) {
            const onResolveRequestHandler : IEventsHandler = ($eventArgs? : HttpRequestEventArgs | AsyncRequestEventArgs) : void => {
                LogIt.Debug("Back-end internal request detected for: {0}", $eventArgs.Url());
                Loader.getInstance().getHttpResolver().ResolveRequest($eventArgs);
            };
            this.setEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST, onResolveRequestHandler);
            this.setEvent(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST, onResolveRequestHandler);
        } else {
            super.Subscribe();
        }
    }
}

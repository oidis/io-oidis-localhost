/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/BaseAdapter.js";
import { SentryAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/SentryAdapter.js";
import { Logger as Parent } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { LogAdapterType } from "../Enums/LogAdapterType.js";
import { EnvironmentHelper } from "../Utils/EnvironmentHelper.js";
import { FileAdapter } from "./Adapters/FileAdapter.js";
import { JsonAdapter } from "./Adapters/JsonAdapter.js";
import { StdAdapter } from "./Adapters/StdAdapter.js";

export class Logger extends Parent {
    private projectBase : string;
    private appDataPath : string;
    private anonymizerSymbols : any;

    public setProjectBase($value : string) : void {
        this.projectBase = $value;
    }

    public setAppDataPath($value : string) : void {
        this.appDataPath = $value;
    }

    public getAnonymizerSymbols($force : boolean = false) : any {
        if (ObjectValidator.IsEmptyOrNull(this.anonymizerSymbols) || $force) {
            let anonymizeMap : string[];
            let keyFormat : string;
            if (EnvironmentHelper.IsWindows()) {
                keyFormat = "%{0}%";
                anonymizeMap = [
                    "TMP", "USERPROFILE", "HOMEPATH", "USERDOMAIN", "COMPUTERNAME", "USERNAME"
                ];
            } else {
                keyFormat = "${0}";
                anonymizeMap = [
                    "HOME", "HOSTNAME", "LOGNAME", "USER"
                ];
            }
            const binBase : string = StringUtils.Replace(require("path").normalize(__dirname + "/../.."), "\\", "/");
            const winProjectBase : string = StringUtils.Replace(this.projectBase, "/", "\\");
            const anonymizeValues : any = {
                BINBASE    : StringUtils.Replace(binBase, "/", "\\"),
                PROJECTBASE: winProjectBase,
                SOURCEBASE : StringUtils.Remove(winProjectBase, "\\build\\target"),
                appData    : this.appDataPath,
                binBase,
                projectBase: this.projectBase,
                sourceBase : StringUtils.Remove(this.projectBase, "/build/target")
            };
            anonymizeMap.forEach(($key : string) : void => {
                if (process.env.hasOwnProperty($key)) {
                    anonymizeValues[$key] = process.env[$key];
                    anonymizeMap.forEach(($key : string) : void => {
                        if (process.env.hasOwnProperty($key)) {
                            anonymizeValues[$key] = process.env[$key];
                            if (StringUtils.Contains(anonymizeValues[$key], "\\")) {
                                anonymizeValues[StringUtils.ToLowerCase($key)] =
                                    StringUtils.Replace(anonymizeValues[$key], "\\", "/");
                            }
                        }
                    });
                }
            });
            this.anonymizerSymbols = {
                format: keyFormat,
                values: anonymizeValues
            };
        }
        return this.anonymizerSymbols;
    }

    public getAdapter($type : LogAdapterType) : BaseAdapter {
        const name : string = "adapter" + $type;
        switch ($type) {
        case LogAdapterType.STD:
            if (ObjectValidator.IsEmptyOrNull(this[name])) {
                this[name] = new StdAdapter();
            }
            return this[name];
        case LogAdapterType.FILE:
            if (ObjectValidator.IsEmptyOrNull(this[name])) {
                this[name] = new FileAdapter();
            }
            return this[name];
        case LogAdapterType.JSON:
            if (ObjectValidator.IsEmptyOrNull(this[name])) {
                this[name] = new JsonAdapter();
            }
            return this[name];
        case LogAdapterType.SENTRY:
            if (ObjectValidator.IsEmptyOrNull(this[name])) {
                this[name] = new SentryAdapter();
            }
            return this[name];
        default:
            return null;
        }
    }

    protected buildInAdapters() : LogAdapterType[] {
        return [LogAdapterType.STD, LogAdapterType.FILE, LogAdapterType.JSON, LogAdapterType.SENTRY];
    }

    protected defaultAdapters() : LogAdapterType[] {
        return [LogAdapterType.STD, LogAdapterType.FILE, LogAdapterType.SENTRY];
    }
}

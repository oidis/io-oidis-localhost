/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { NewLineType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/NewLineType.js";
import { BaseAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/BaseAdapter.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ITraceBackEntry } from "@io-oidis-commons/Io/Oidis/Commons/Utils/TraceBack.js";
import { isMainThread } from "../../Utils/EnvironmentHelper.js";

export class FileAdapter extends BaseAdapter {
    private readonly os : any;
    private readonly fs : any;
    private readonly colors : any;
    private logsBackup : string;
    private logPath : string;
    private logFile : string;
    private projectBase : string;
    private autoPath : boolean;

    constructor() {
        super();
        this.os = require("os");
        this.fs = require("fs");
        this.colors = isMainThread ? require("colors") : {
            stripColors: ($value : string) : string => {
                return $value;
            }
        };
        this.logsBackup = "";
        this.autoPath = true;
    }

    public setProjectBase($value : string) : void {
        this.projectBase = Property.String(this.projectBase, $value);
    }

    public LogPath($value? : string) : string {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.autoPath = false;
            this.logPath = Property.String(this.logPath, $value);
            this.logPath = StringUtils.Replace(this.logPath, "\\", "/");
            this.logFile = this.logPath;
            this.logPath = StringUtils.Substring(this.logPath, 0, StringUtils.IndexOf(this.logPath, "/", false) + 1);
        } else if (this.autoPath) {
            const date : Date = new Date();
            const year : string = date.getUTCFullYear() + "";
            const day : string = date.getUTCDate() < 10 ? "0" + date.getUTCDate() : date.getUTCDate() + "";
            const monthNumber : number = date.getUTCMonth() + 1;
            const month : string = monthNumber < 10 ? "0" + monthNumber : monthNumber + "";
            this.logPath = this.projectBase + "/log/" + year + "/" + month;
            this.logPath = StringUtils.Replace(this.logPath, "\\", "/");
            this.logFile = this.logPath + "/" + day + "_" + month + "_" + year + ".txt";
        }
        return this.logFile;
    }

    public NewLineType() : NewLineType | string {
        return this.os.EOL;
    }

    public Process($trace : ILoggerTrace) : void {
        if (!this.fs.existsSync(this.LogPath())) {
            this.fs.mkdirSync(this.logPath, {recursive: true});
        }
        super.Process($trace);
    }

    protected timeToString($value : number) : string {
        return new Date($value).toUTCString();
    }

    protected traceBackToString($traceback : ITraceBackEntry) : string {
        return $traceback.at + " from " + $traceback.from;
    }

    protected getHandler() : any {
        return this.fs;
    }

    protected formatter($trace : ILoggerTrace) : ILoggerTrace {
        $trace.message = StringUtils.Format(this.verbose ? "[{0}][{1}] {3} -- {2}" : "[{0}][{1}] {3}",
            $trace.time,
            LogLevel[$trace.level],
            $trace.entryPoint,
            this.colors.stripColors($trace.message));
        return $trace;
    }

    protected print($trace : ILoggerTrace, $handler : any) : void {
        const data : string = this.logsBackup + $trace.message + <string>this.NewLineType();
        try {
            $handler.appendFileSync(this.LogPath(), data);
            this.logsBackup = "";
        } catch (ex) {
            console.error(ex); // eslint-disable-line no-console
            this.logsBackup += data;
        }
    }
}

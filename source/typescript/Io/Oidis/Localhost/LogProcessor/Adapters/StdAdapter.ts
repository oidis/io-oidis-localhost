/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { NewLineType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/NewLineType.js";
import { BaseAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/BaseAdapter.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "../../Enums/ColorType.js";
import { EnvironmentHelper } from "../../Utils/EnvironmentHelper.js";

export class StdAdapter extends BaseAdapter {
    private readonly os : any;
    private readonly printNewLine : boolean;
    private readonly symbols : any;

    constructor($withNewLine : boolean = true) {
        super();
        this.os = require("os");
        this.printNewLine = $withNewLine;

        if (!EnvironmentHelper.IsTTY()) {
            this.symbols = {
                ARROW: ">",
                BAR  : "=",
                FAIL : "x",
                OK   : "+"
            };
        } else {
            this.symbols = {
                ARROW: "→",
                BAR  : EnvironmentHelper.IsWindows() ? "█" : "▇",
                FAIL : "×",
                OK   : "✓"
            };
        }
    }

    public NewLineType() : NewLineType | string {
        return this.os.EOL;
    }

    protected formatter($trace : ILoggerTrace) : ILoggerTrace {
        $trace.message = StringUtils.Replace($trace.message, "[__BAR__]", this.symbols.BAR);
        $trace.message = StringUtils.Replace($trace.message, "[__FAIL__]", this.symbols.FAIL);
        $trace.message = StringUtils.Replace($trace.message, "[__OK__]", this.symbols.OK);
        $trace.message = StringUtils.Replace($trace.message, "[__ARROW__]", this.symbols.ARROW);

        if (this.printNewLine) {
            $trace.message += this.NewLineType();
        }

        if (EnvironmentHelper.IsTTY()) {
            if ($trace.level === LogLevel.ERROR) {
                $trace.message = $trace.message[ColorType.RED];
            } else if ($trace.level === LogLevel.WARNING) {
                $trace.message = $trace.message[ColorType.YELLOW];
            }
        }
        return super.formatter($trace);
    }

    protected getHandler() : any {
        const handler : any = EnvironmentHelper.IsDetached() ? console : process.stdout.write.bind(process.stdout);
        this.getHandler = () : any => {
            return handler;
        };
        return handler;
    }

    protected print($trace : ILoggerTrace) : void {
        try {
            ($trace.level === LogLevel.ERROR ? this.stderr() : this.stdout())($trace.message);
        } catch (ex) {
            if ($trace.level === LogLevel.ERROR) {
                console.error($trace.message); // eslint-disable-line no-console
            } else {
                console.log($trace.message); // eslint-disable-line no-console
            }
        }
    }

    private stdout() : any {
        const handler : any = EnvironmentHelper.IsDetached() ? this.getHandler().log.bind(console) : this.getHandler();
        this.stdout = () : any => {
            return handler;
        };
        return handler;
    }

    private stderr() : any {
        const handler : any = EnvironmentHelper.IsDetached() ? this.getHandler().error.bind(console) : this.getHandler();
        this.stderr = () : any => {
            return handler;
        };
        return handler;
    }
}

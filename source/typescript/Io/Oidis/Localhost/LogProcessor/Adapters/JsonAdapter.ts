/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ITraceBackEntry, TraceBack } from "@io-oidis-commons/Io/Oidis/Commons/Utils/TraceBack.js";
import { Logger } from "../Logger.js";
import { FileAdapter } from "./FileAdapter.js";

export class JsonAdapter extends FileAdapter {
    private readonly traces : ILoggerTrace[];
    private withMeta : boolean;
    private metaPrinted : boolean;

    constructor() {
        super();
        this.traces = [];
        this.withMeta = false;
        this.metaPrinted = false;
    }

    public LogPath($value? : string) : string {
        if (ObjectValidator.IsEmptyOrNull($value)) {
            return StringUtils.Replace(super.LogPath(), ".txt", ".json");
        } else {
            this.metaPrinted = false;
        }
        return super.LogPath($value);
    }

    public IncludeMetadata($value? : boolean) : boolean {
        return this.withMeta = Property.Boolean(this.withMeta, $value);
    }

    protected traceBackToString($traceback : ITraceBackEntry) : any {
        return $traceback;
    }

    protected formatter($trace : ILoggerTrace) : ILoggerTrace {
        if ($trace.level === LogLevel.ERROR) {
            $trace.trace = TraceBack.Parse(<any>{stack: $trace.message});
            $trace.message = $trace.message.split("\n")[0];
        } else if (!this.verbose) {
            delete $trace.trace;
        }
        if (!this.verbose) {
            delete $trace.entryPoint;
        }
        return $trace;
    }

    protected getOwner() : Logger {
        return super.getOwner();
    }

    protected print($trace : ILoggerTrace, $handler : any) : void {
        this.traces.push($trace);
        $handler.writeFileSync(this.LogPath(), JSON.stringify(this.traces));
        if ((this.withMeta || this.debug) && !this.metaPrinted) {
            $handler.writeFileSync(this.LogPath() + ".dat", JSON.stringify(this.getOwner().getAnonymizerSymbols()));
            this.metaPrinted = true;
        }
    }
}

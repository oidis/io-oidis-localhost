/* ********************************************************************************************************* *
 *
 * Copyright 2020-2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Io.Oidis.Builder.Loader;
const LogIt = Io.Oidis.Commons.Utils.LogIt;
const ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
const EnvironmentHelper = Io.Oidis.Localhost.Utils.EnvironmentHelper;
const StringUtils = Io.Oidis.Commons.Utils.StringUtils;
const DependenciesInstall = Io.Oidis.Builder.Tasks.Composition.DependenciesInstall;
const OSType = Io.Oidis.Builder.Enums.OSType;
const Patcher = Io.Oidis.Builder.Utils.Patcher;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();

let cwd;

function finalize($done) {
    Patcher.getInstance().Rollback($done);
}

function fail($info) {
    finalize(() => {
        LogIt.Error($info);
    });
}

function validate($done) {
    let moduleNodePath = cwd + "/build/Release/bufferutil.node";
    if (filesystem.Exists(moduleNodePath)) {
        CheckModule(moduleNodePath, ($status) => {
            $done($status);
        });
    } else {
        $done(false);
    }
}

function build($done) {
    filesystem.Delete(cwd + "/prebuilds", () => {
        if (EnvironmentHelper.IsWindows() || (DependenciesInstall.getCurrentInstallOsType() === OSType.LINUX)) {
            terminal.Spawn((EnvironmentHelper.IsMac() ? "CXX=clang++ " : "") + "npm", ["rebuild", "bufferutil", "--build-from-source=bufferutil"],
                {cwd: cwd + "/../../"}, ($exitCode) => {
                    if ($exitCode === 0) {
                        $done();
                    } else {
                        LogIt.Error("Bufferutil build failed");
                    }
                });
        } else if (DependenciesInstall.getCurrentInstallOsType() === OSType.IMX) {
            const list = [cwd + "/Release"];

            let index = 0;
            const removeFolders = function ($handler) {
                if (index < list.length) {
                    filesystem.Delete(list[index++], () => {
                        removeFolders($handler);
                    });
                } else {
                    $handler();
                }
            };

            removeFolders(() => {
                const env = process.env;
                env.CC = "aarch64-linux-gnu-gcc";
                env.CXX = "aarch64-linux-gnu-g++";
                env.LINK = "aarch64-linux-gnu-g++";
                env.RANLIB = "aarch64-linux-gnu-ranlib";
                env.AR = "aarch64-linux-gnu-ar";

                env.CC_host = "gcc";
                env.CXX_host = "g++";
                env.LINK_host = "g++";
                env.RANLIB_host = "ranlib";
                env.AR_host = "ar";

                env.module_path = cwd;

                terminal.Spawn(EnvironmentHelper.getNodejsRoot() + "/node_modules/npm/bin/node-gyp-bin/node-gyp",
                    ["rebuild", "--dest-cpu=arm64", "--dest-os=linux", "--cross-compiling", "--target_arch=arm64"],
                    {cwd: cwd, env: env},
                    ($exitCode) => {
                        if ($exitCode === 0) {
                            finalize($done);
                        } else {
                            LogIt.Error("Can not cross-compile bufferutil");
                        }
                    });
            })
        } else if (EnvironmentHelper.IsMac()) {
            LogIt.Warning("Bufferutil build script disabled on MAC");
            $done();
        } else {
            fail("Build is not implemented for demand platform");
        }
    });
}

Process = function ($cwd, $args, $done) {
    cwd = $cwd;
    validate(($status) => {
        if ($status !== true) {
            LogIt.Info("Starting bufferutil build...");
            build($done);
        } else {
            LogIt.Info("Bufferutil is ready.");
            finalize($done);
        }
    })
};

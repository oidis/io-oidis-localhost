/* ********************************************************************************************************* *
 *
 * Copyright 2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Io.Oidis.Builder.Loader;
const LogIt = Io.Oidis.Commons.Utils.LogIt;
const ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
const EnvironmentHelper = Io.Oidis.Localhost.Utils.EnvironmentHelper;
const StringUtils = Io.Oidis.Commons.Utils.StringUtils;
const Patcher = Io.Oidis.Builder.Utils.Patcher;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();

let cwd;

function finalize($done) {
    Patcher.getInstance().Rollback($done);
}

function fail($info) {
    finalize(() => {
        LogIt.Error($info);
    });
}

function validate($done) {
    let moduleNodePath = cwd + "/build/Release/VersionInfo.node";
    if (filesystem.Exists(moduleNodePath)) {
        CheckModule(moduleNodePath, ($status) => {
            $done($status);
        });
    } else {
        $done(false);
    }
}

function build($done) {
    filesystem.Delete(cwd + "/build", () => {
        if (EnvironmentHelper.IsWindows()) {
            terminal.Spawn("npm", ["run", "rebuild"],
                {cwd}, ($exitCode) => {
                    if ($exitCode === 0) {
                        $done();
                    } else {
                        LogIt.Error("win-version-info build failed");
                    }
                });
        } else {
            fail("Build is not implemented for demand platform");
        }
    });
}

Process = function ($cwd, $args, $done) {
    cwd = $cwd;
    if (EnvironmentHelper.IsWindows()) {
        validate(($status) => {
            if ($status !== true) {
                LogIt.Info("Starting win-version-info build...");
                build($done);
            } else {
                LogIt.Info("win-version-info is ready.");
                finalize($done);
            }
        })
    } else {
        LogIt.Debug("win-version-info customized build skipped for non-Win platforms.");
        $done();
    }
};

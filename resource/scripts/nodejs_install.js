/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Io.Oidis.Builder.Loader;
const LogIt = Io.Oidis.Commons.Utils.LogIt;
const ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
const StringUtils = Io.Oidis.Commons.Utils.StringUtils;
const EnvironmentHelper = Io.Oidis.Localhost.Utils.EnvironmentHelper;
const StringReplaceType = Io.Oidis.Builder.Enums.StringReplaceType;
const StringReplace = Io.Oidis.Builder.Tasks.Utils.StringReplace;
const DependenciesInstall = Io.Oidis.Builder.Tasks.Composition.DependenciesInstall;
const OSType = Io.Oidis.Builder.Enums.OSType;
const Resources = Io.Oidis.Services.DAO.Resources;
const ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();
const projectBase = Loader.getInstance().getAppProperties().projectBase;
const appConfig = Loader.getInstance().getProjectConfig();

const http = require("http");
const nodejsVersion = "18.12.1";
// look at https://nodejs.org/dist/v<nodejs-version>/SHASUMS256.txt
// and find win-x86/node.lib file
const nodejsx86Sha = "b1c6dc670911d85ef1704fa56f4cc4c7e1071f4869778398e6d88b3b0b565978";
let server;

let cwd;
let installPath;
let args;
let configPaths;
let legacyInstall = false;

function execute($args, $callback) {
    const env = process.env;

    if (DependenciesInstall.getCurrentInstallOsType() === OSType.IMX) {
        env.NODEJSRE_SKIP_LOADER = 1;
        env.NODEJSRE_NODE_PATH = installPath;

        if (EnvironmentHelper.IsEmbedded()) {
            LogIt.Error("\nEmbedded builder can not be used to build arm build of NodeJsRE based project because NPM is not " +
                "able to run from embedded builder at this time. Please use shared/static build of io-oidis-builder project instead.");
        } else {
            const oidisExecute = function () {
                terminal.Spawn("oidis", [EnvironmentHelper.getNodejsRoot() + "/node_modules/npm/bin/npm-cli.js"].concat($args), {
                    cwd: installPath,
                    env: env
                }, $callback);
            };

            if (!filesystem.Exists(cwd + "/builder_patch/node") && EnvironmentHelper.IsEmbedded()) {
                env.PATH = cwd + "/builder_patch:" + env.PATH;
                filesystem.CreateDirectory(cwd + "/builder_patch");
                terminal.Spawn("ln", ["-s", "/usr/bin/oidis", cwd + "/builder_patch" + "/node"], cwd, () => {
                    oidisExecute();
                });
            } else {
                env.PATH = EnvironmentHelper.getNodejsRoot() + ":" + env.PATH;
                oidisExecute();
            }
        }
    } else {
        if (EnvironmentHelper.IsMac()) {
            env.CC = "clang";
            env.CXX = "clang++";
        }
        env.PATH = terminal.NormalizeEnvironmentPath(cwd);
        terminal.Spawn("npm", $args, {
            cwd: installPath,
            env: env
        }, $callback);
    }
}

function checkBinary($path, $callback) {
    if (!legacyInstall) {
        $callback();
        return;
    }
    LogIt.Info("Checking module binary compatibility at: " + $path);
    if (EnvironmentHelper.IsWindows()) {
        $callback(StringUtils.Contains(filesystem.Read($path).toString(), "node.dll"));
    } else if (EnvironmentHelper.IsMac()) {
        terminal.Execute("lipo", [$path, "-verify_arch", "x86_64"], {
            verbose: false,
            advanced: {noTerminalLog: true, noExitOnStderr: true}
        }, ($exitCode, $std) => {
            if ($exitCode !== 0) {
                LogIt.Warning("Can not validate " + $path + " binary format.\n" + [].concat($std));
                $callback(false);
            } else {
                $callback(true);
            }
        });
    } else {
        const transTable = {
            "linux": "X86-64",
            "imx": "AArch64"
        };
        terminal.Execute("readelf", ["-h", $path], {
            verbose: false,
            advanced: {noTerminalLog: true, noExitOnStderr: true}
        }, ($exitCode, $std) => {
            if ($exitCode !== 0) {
                LogIt.Warning("Can not validate " + $path + " binary format.\n" + [].concat($std));
                $callback(false);
            } else {
                const lines = $std[0].split("\n");
                const line = lines[lines.findIndex((item) => {
                    return item.trim().startsWith("Machine");
                })];
                if (!StringUtils.ContainsIgnoreCase(line, transTable[DependenciesInstall.getCurrentInstallOsType()])) {
                    LogIt.Warning("Library format not matching expected " +
                        transTable[DependenciesInstall.getCurrentInstallOsType()] + "\n" + $path + " >> \n" + [].concat($std));
                    $callback(false)
                } else {
                    $callback(true);
                }
            }
        });
    }
}

function checkModule($modulePath, $callback) {
    let modules = $modulePath;
    if (ObjectValidator.IsString($modulePath)) {
        modules = [$modulePath];
    }

    let index = 0;
    let status = true, required = true;
    const processNext = () => {
        if (index < modules.length) {
            const module = modules[index++];
            checkBinary(module, ($status) => {
                LogIt.Info("Checking module by require...");
                if ((DependenciesInstall.getCurrentInstallOsType() === OSType.LINUX) || EnvironmentHelper.IsWindows() || EnvironmentHelper.IsMac()) {
                    let script = "\"" +
                        "try{" +
                        "   require('" + filesystem.NormalizePath(module) + "');" +
                        "   console.log('>VALIDATION SUCCEED<');" +
                        "} catch(e) {" +
                        "   console.log('>VALIDATION FAILED<: '+e.stack);" +
                        "}" +
                        "\"";

                    const env = JSON.parse(JSON.stringify(process.env));
                    if (Loader.getInstance().getProgramArgs().IsDebug()) {
                        env.NODE_DEBUG = "module,resource_manager";
                    }
                    terminal.Execute(
                        "node",
                        ["-e", script],
                        {
                            cwd,
                            verbose: Loader.getInstance().getProgramArgs().IsDebug(),
                            advanced: {noTerminalLog: !Loader.getInstance().getProgramArgs().IsDebug(), noExitOnStderr: true},
                            env
                        },
                        ($exitCode, $std) => {
                            status &= $status;
                            required &= $exitCode === 0 && !StringUtils.Contains($std[1], ">VALIDATION FAILED<:");
                            processNext();
                        });
                } else {
                    LogIt.Warning("Native module require is supported only for WIN, LINUX, and MAC host os. Skipping this check...");
                    status &= $status;
                    processNext();
                }
            });
        } else {
            $callback(status, required);
        }
    };

    processNext();
}

function validateNative($callback) {
    const list = filesystem.Expand(cwd + "/build/node_modules/**/*.node");
    let log = "\nValidating native modules\n[binary-format, require(\"<module-path>\")]   <module-path>\n";
    let isCorrupted = false;

    let index = 0;
    const processNext = function () {
        if (index < list.length) {
            const item = list[index++];
            if (!StringUtils.Contains(item, "fswin")) {
                checkModule(item, ($binaryFormat, $required) => {
                    log += "\n" + StringUtils.Format("[{0} {1}]   {2}", $binaryFormat ? "OK" : "NOK", $required ? "OK" : "NOK", item);
                    isCorrupted |= !$binaryFormat || !$required;
                    processNext();
                });
            } else {
                processNext();
            }
        } else {
            LogIt.Info(StringUtils.Format("{0}\n", log));
            $callback(!isCorrupted);
        }
    };

    processNext();
}

function validate($callback) {
    const packageConf = JSON.parse(filesystem.Read(installPath + "/package.json").toString());
    const required = [];
    const optional = [];
    let name;
    if (packageConf.hasOwnProperty("devDependencies")) {
        for (name in packageConf.devDependencies) {
            if (packageConf.devDependencies.hasOwnProperty(name)) {
                required.push(name);
            }
        }
    }
    if (packageConf.hasOwnProperty("optionalDependencies")) {
        for (name in packageConf.optionalDependencies) {
            if (packageConf.optionalDependencies.hasOwnProperty(name)) {
                optional.push(name);
            }
        }
    }
    if (ObjectValidator.IsEmptyOrNull(filesystem.Expand(installPath + "/node_modules/*"))) {
        LogIt.Info("> All of the NodeJS modules are missing\n");
        $callback(false);
    } else {
        execute(["list", "--depth=0"], ($exitCode, $std) => {
            let missing = [];
            required.forEach(($name) => {
                if (!ObjectValidator.IsEmptyOrNull($std[0].match(new RegExp("--\\sUNMET.*\\s" + $name + "@", "gm")))) {
                    missing.push($name);
                }
            });
            if (missing.length === 0) {
                let missingOptional = [];
                optional.forEach(($name) => {
                    if (!ObjectValidator.IsEmptyOrNull($std[0].match(new RegExp("--\\sUNMET.*\\s" + $name + "@", "gm")))) {
                        missingOptional.push($name);
                    }
                });
                if (missingOptional.length !== 0) {
                    LogIt.Info("> NodeJS instance is missing some of optional dependencies: " +
                        JSON.stringify(missingOptional));
                }
                validateNative($callback);
            } else {
                LogIt.Info("> NodeJS instance is missing some of dependencies: " + JSON.stringify(missing));
                $callback(false);
            }
        });
    }
}

function configureNpm($callback, $includeMSVS) {
    const configs = [
        ["prefer-offline", "true"],
        ["maxsockets", "100"],
        ["strict-ssl", "false"],
        ["fetch-retries", "25"], ["fetch-retry-mintimeout", "500"], ["fetch-retry-maxtimeout", "1000"]
    ];
    if (EnvironmentHelper.IsWindows() && $includeMSVS) {
        LogIt.Warning("\nPython and Microsoft Build Tools for C++ (or Visual Studio) has to be available and due to " +
            "possible problems with admin rights an internal installation is not part of this install script.\n" +
            "Check for proper environment settings is not explicitly defined while it is too complex, so if build result" +
            "is \"strange\" please check full npm log in dependencies/nodejs/build/patched-appdata/npm-cache/_logs.\n" +
            "For more info please check official documentation for node-gyp at https://www.npmjs.com/package/node-gyp\n");
        configs.push(["python", "python"]);
        configs.push(["msvs_version", "2017"]);
        configs.push(["cmake_js_G", "Visual Studio 15 2017 Win64"]);
    }
    const setNextConfig = ($index) => {
        if ($index < configs.length) {
            const config = configs[$index];
            execute(["config", "set", config[0], config[1]], () => {
                setNextConfig($index + 1);
            });
        } else {
            $callback();
        }
    };
    setNextConfig(0);
}

function invokeModuleBuild($scriptObj, $callback) {
    if ($scriptObj.hasOwnProperty("name") && $scriptObj.hasOwnProperty("path")) {
        const vm = require("vm");
        const envBackup = JSON.stringify(process.env);
        const sandbox = {
            process,
            require,
            console,
            module: {
                exports: null
            },
            global: {
                legacyInstall
            },
            window: {},
            CheckModule($path, $done) {
                checkModule($path, $done)
            },
            Process($cwd, $args, $done) {
                LogIt.Warning("Custom build skipped: script did not specify module.exports and " +
                    "also did not override global Process method");
                $done();
            }
        };
        Loader.getInstance().getProjectConfig().namespaces.forEach(($item) => {
            const rootPart = $item.split(".")[0];
            if (!sandbox.hasOwnProperty(rootPart) && global.hasOwnProperty(rootPart)) {
                sandbox[rootPart] = global[rootPart];
                sandbox.global[rootPart] = sandbox[rootPart];
                sandbox.window[rootPart] = sandbox[rootPart];
            }
        });
        let script = projectBase + "/resource/scripts/" + $scriptObj.name;
        if (!filesystem.Exists(script)) {
            script = filesystem.Expand(Loader.getInstance().getAppProperties().dependenciesResources + "/scripts/" + $scriptObj.name);
            if (!ObjectValidator.IsEmptyOrNull(script)) {
                script = script[0];
            }
        }
        const modulePath = installPath + "/node_modules/" + $scriptObj.path;
        let attributes = [];
        if ($scriptObj.hasOwnProperty("attributes")) {
            attributes = $scriptObj.attributes;
        }
        if (filesystem.Exists(modulePath)) {
            LogIt.Info(">> Running build-script " + script + " in " + modulePath);
            vm.createContext(sandbox);
            vm.runInContext(filesystem.Read(script), sandbox, script);
            sandbox.Process(modulePath, attributes, () => {
                process.env = JSON.parse(envBackup);
                $callback();
            });
        } else {
            LogIt.Warning("Skipping build-script for \"" + modulePath + "\". Module has not been installed.");
            $callback();
        }
    } else {
        LogIt.Error("Wants to run build-script without name or path option. Both has to be specified. " +
            "Please check your nodejs dependency configuration in \"install-script\":{\"attributes\":[...]}");
    }
}

function updatePath($path, $cwd) {
    if (StringUtils.StartsWith($path, "resource/") &&
        !filesystem.Exists(projectBase + "/" + $path) &&
        filesystem.Exists($cwd + "/" + $path)) {
        $path = $cwd + "/" + $path;
    }
    return $path;
}

function createPackageConf($pathOrConfig, $parent) {
    const preparePkg = ($config) => {
        let pkg = {};
        try {
            if (ObjectValidator.IsEmptyOrNull($config.pkg)) {
                $config.pkg = {};
            } else {
                pkg = JSON.parse(JSON.stringify($config.pkg));
            }
            if (ObjectValidator.IsEmptyOrNull(pkg)) {
                pkg = {assets: [], patches: []};
            } else {
                if (ObjectValidator.IsEmptyOrNull(pkg.assets)) {
                    pkg.assets = [];
                }
                if (ObjectValidator.IsEmptyOrNull(pkg.patches)) {
                    pkg.patches = [];
                }
            }
        } catch (ex) {
            LogIt.Debug($config.pkg);
            LogIt.Error(ex);
        }
        return pkg;
    };

    const configs = [$parent];
    const traverse = ($path) => {
        if (!filesystem.Exists($path)) {
            LogIt.Warning("Config not found at: " + $path);
        } else {
            let config = filesystem.Read($path).toString();
            if (ObjectValidator.IsEmptyOrNull(config)) {
                config = {};
                LogIt.Warning("Found empty config at: " + $path);
            } else {
                config = JSON.parse(config);
            }
            configs.push(config);
            if (config.hasOwnProperty("extends")) {
                traverse(config.extends);
            }
        }
    };
    if (ObjectValidator.IsString($pathOrConfig)) {
        traverse($pathOrConfig);
    } else {
        configs.push($pathOrConfig);
    }
    configs.reverse();
    if (ObjectValidator.IsEmptyOrNull(configs[0])) {
        configs[0] = {};
    }
    const output = JSON.parse(JSON.stringify(configs[0]));
    const assets = [];
    const patches = [];
    const registered = new ArrayList();
    const mergeAssets = ($config) => {
        $config.assets.forEach(($asset) => {
            if (assets.indexOf($asset) === -1) {
                assets.push($asset);
            }
        });
    };
    const mergePatches = ($config) => {
        $config.patches.forEach(($patch) => {
            registered.Add({patch: $patch, cwd: $config.cwd}, StringUtils.getCrc(JSON.stringify($patch)));
        });
    };
    if (configs.length === 1) {
        const parentPkg = preparePkg(configs[0]);
        mergeAssets(parentPkg);
        mergePatches(parentPkg);
    } else if (configs.length > 1) {
        for (let $index = 1; $index < configs.length; $index++) {
            const child = configs[$index];
            const parent = configs[$index - 1];
            const childPkg = preparePkg(child);
            const parentPkg = preparePkg(parent);

            const depKey = "dependencies/";
            let configPath = child.extends;
            if (ObjectValidator.IsEmptyOrNull(configPath)) {
                configPath = child.configCwd;
            }
            const depIndex = StringUtils.IndexOf(configPath, depKey) + depKey.length;
            const cwd = StringUtils.Substring(configPath, 0,
                StringUtils.IndexOf(configPath, "/", true, depIndex));
            childPkg.cwd = cwd;
            parentPkg.cwd = cwd;

            Resources.Extend(output, child);
            mergeAssets(parentPkg);
            mergeAssets(childPkg);
            mergePatches(parentPkg);
            mergePatches(childPkg);
        }
    }
    registered.foreach(($patch) => {
        if (!ObjectValidator.IsEmptyOrNull($patch.patch.src)) {
            $patch.patch.src = updatePath($patch.patch.src, $patch.cwd);
        }
        if (!ObjectValidator.IsEmptyOrNull($patch.patch.script)) {
            $patch.patch.script = updatePath($patch.patch.script, $patch.cwd);
        }
        patches.push($patch.patch);
    });
    output.pkg = {assets, patches};

    return output;
}

function install($callback) {
    let packageConf = {};
    let files = [];
    configPaths.forEach(($value) => {
        try {
            const objectType = JSON.parse($value);
            let name;
            for (name in objectType) {
                if (objectType.hasOwnProperty(name)) {
                    files.push(objectType[name]);
                }
            }
        } catch (ex) {
            files.push($value);
        }
    });
    files.forEach(($path) => {
        packageConf = createPackageConf($path, packageConf);
    });
    if (!ObjectValidator.IsEmptyOrNull(appConfig.nodejsConfig)) {
        if (!ObjectValidator.IsEmptyOrNull(appConfig.nodejsConfig.pkg)) {
            const assets = [];
            const patches = [];

            let pkgName;
            for (pkgName in appConfig.nodejsConfig.pkg) {
                if (appConfig.nodejsConfig.pkg.hasOwnProperty(pkgName)) {
                    const pkg = appConfig.nodejsConfig.pkg[pkgName];
                    const depKey = "dependencies/";
                    const depIndex = StringUtils.IndexOf(pkg.configCwd, depKey) + depKey.length;
                    const cwd = StringUtils.Substring(pkg.configCwd, 0,
                        StringUtils.IndexOf(pkg.configCwd, "/", true, depIndex));
                    if (!ObjectValidator.IsEmptyOrNull(pkg.assets)) {
                        pkg.assets.forEach(($item) => {
                            if (assets.indexOf($item) === -1) {
                                assets.push($item);
                            }
                        });
                    }
                    if (!ObjectValidator.IsEmptyOrNull(pkg.patches)) {
                        let patchName;
                        for (patchName in pkg.patches) {
                            if (pkg.patches.hasOwnProperty(patchName)) {
                                let patch = pkg.patches[patchName];
                                if (ObjectValidator.IsEmptyOrNull(patch.enabled)) {
                                    patch.enabled = true;
                                }
                                if (patch.enabled) {
                                    delete patch.enabled;
                                    if (!ObjectValidator.IsEmptyOrNull(patch.src)) {
                                        patch.src = updatePath(patch.src, cwd);
                                    }
                                    if (!ObjectValidator.IsEmptyOrNull(patch.script)) {
                                        patch.script = updatePath(patch.script, cwd);
                                    }
                                    patches.push(patch);
                                }
                            }
                        }
                    }
                }
            }
            appConfig.nodejsConfig.pkg = {assets, patches};
        }
        packageConf = createPackageConf(appConfig.nodejsConfig, packageConf);
    }

    let script;
    for (script in packageConf.buildScripts) {
        if (packageConf.buildScripts.hasOwnProperty(script)) {
            let item;
            if (ObjectValidator.IsString(packageConf.buildScripts[script])) {
                item = {name: packageConf.buildScripts[script], path: script};
            } else {
                item = packageConf.buildScripts[script];
            }
            if (args.findIndex(($value) => {
                return JSON.stringify($value) === JSON.stringify(item);
            }) === -1) {
                args.push(item);
            }
        }
    }
    delete packageConf.configCwd;
    delete packageConf.buildScripts;
    filesystem.Write(installPath + "/package.json",
        StringReplace.Content(JSON.stringify(packageConf, null, 2), StringReplaceType.VARIABLES));
    filesystem.Rename(installPath + "/package-lock.json", installPath + "/package-lock.json.backup");

    validate(($status) => {
        filesystem.Rename(installPath + "/package-lock.json.backup", installPath + "/package-lock.json");
        if ($status) {
            LogIt.Info("Install skipped: NodeJS is up-to-date");
            validateNative($callback);
        } else {
            filesystem.Delete(installPath + "/package-lock.json");
            // delete npm cache before install to prevent fs.stat issue on <cache>/ea/.. sub-folder, not sure why it's happening
            // NOTE: process.env.HOME is overwritten by this script, but for sure there is string check too
            if (legacyInstall && ObjectValidator.IsEmptyOrNull(filesystem.Expand(installPath + "/node_modules/*")) &&
                !EnvironmentHelper.IsWindows() && StringUtils.Contains(process.env.HOME, "/build/patched-") &&
                filesystem.Exists(process.env.HOME)) {
                filesystem.Delete(process.env.HOME);
            }
            const installArgs = ["install", "--legacy-peer-deps"];
            if (Loader.getInstance().getProgramArgs().IsVerbose()) {
                installArgs.push("--verbose");
            }
            LogIt.Info("Install starting...\n");
            const postInstall = () => {
                if (!legacyInstall) {
                    LogIt.Info("Customized module build is disabled for regular install (use legacyInstall if you need it)");
                    $callback();
                    return;
                }
                if (args.length > 1) {
                    LogIt.Info("Customized module build preparation...");
                    let index = 1;

                    if (EnvironmentHelper.IsWindows() && !legacyInstall) {
                        // patch win_delay_load_hook.cc to not override parent module by GetModuleHandle(null) which
                        //   will find node.exe (or however it is named) anyway (it loads main .exe which creates process chain).
                        //   Default behaviour <win-sdk>\VC\Tools\MSVC\14.16.27023\include\delayhlp.cpp uses current module which is dll
                        //   if this is not overridden by delay load routine (like inside win_delay_load_hook.cc)
                        let hooksToPatch = [];
                        let configsToPatch = [];
                        for (const item of args.concat([
                            {name: "node-gyp", path: cwd + "/node_modules"},
                            {name: "node-gyp", path: cwd + "/build/node_modules/node-gyp"}
                        ])) {
                            hooksToPatch = hooksToPatch.concat(filesystem.Expand(item.path + "/**/win_delay_load_hook.cc"));
                            configsToPatch = configsToPatch.concat(filesystem.Expand(item.path + "/**/addon.gypi"));
                        }
                        LogIt.Debug("Found delay loads to patch: {0}", hooksToPatch);

                        for (const item of hooksToPatch) {
                            LogIt.Info("Patching module delay load handler: " + item);
                            filesystem.Write(item, filesystem.Read(item).toString().replace("GetModuleHandle(NULL)", "NULL"));
                        }
                        for (const item of configsToPatch) {
                            LogIt.Info("Patching module config: " + item);
                            filesystem.Write(item, filesystem.Read(item).toString().replace(/'DelayLoadDLLs':\s*\[.*]/g, "'DelayLoadDLLs': [ 'node.dll' ]"));
                        }
                    }

                    const buildModule = function () {
                        if (index < args.length) {
                            invokeModuleBuild(args[index++], buildModule);
                        } else {
                            validateNative($callback);
                        }
                    };

                    buildModule();
                } else {
                    LogIt.Info("No script for customized module build is defined.");
                    validateNative($callback);
                }
            };
            execute(installArgs, ($exitCode, $std) => {
                if ($exitCode === 0) {
                    postInstall();
                } else {
                    if (StringUtils.Contains($std[0] + $std[1], "ECONNRESET")) {
                        LogIt.Info("Network issue detected trying once more time...\n");
                        execute(installArgs, ($exitCode) => {
                            if ($exitCode === 0) {
                                postInstall();
                            } else {
                                LogIt.Error("NodeJS install has failed");
                            }
                        });
                    } else {
                        LogIt.Error("NodeJS install has failed");
                    }
                }
            });
        }
    });
}

function getSha($path) {
    const sha = require("crypto").createHash("sha256");
    sha.update(filesystem.Read($path));
    return sha.digest("hex");
}

function startMockServer($callback) {
    let tarPath = "";
    let shasumList = "";
    let shasumPath = "";
    if (!legacyInstall) {
        $callback();
        return;
    }

    server = http.createServer();
    server.on("request", ($request, $response) => {
        LogIt.Info("request.url: " + $request.url);

        if ($request.url === "/node.lib") {
            $response.writeHead(200);
            const readStream = require("fs").createReadStream(cwd + "/node.lib");
            readStream.pipe($response);
        } else if ($request.url === "/node-v" + nodejsVersion + "-headers.tar.gz") {
            $response.writeHead(200);
            const readStream = require("fs").createReadStream(tarPath);
            readStream.pipe($response);
        } else if ($request.url === "/SHASUMS256.txt") {
            $response.writeHead(200);
            $response.end(shasumList);
        } else if ($request.url === "/cmake-js.json") {
            const readStream = require("fs").createReadStream(cwd + "/build/patched-appdata/cmake-js.json");
            readStream.pipe($response);
        } else {
            $response.writeHead(404);
            $response.end();
        }
    });
    server.listen(0, () => {
        LogIt.Info("Starting nodejs-install mock server");
        // initialize mock server
        const mockData = {};
        mockData["https://nodejs.org/download/release/v" + nodejsVersion + "/win-x64/node.lib"] = "http://127.0.0.1:" +
            server.address().port + "/node.lib";
        mockData["https://nodejs.org/dist/v" + nodejsVersion + "/win-x64/node.lib"] = "http://127.0.0.1:" +
            server.address().port + "/node.lib";

        mockData["https://nodejs.org/download/release/v" + nodejsVersion + "/node-v" + nodejsVersion + "-headers.tar.gz"] = "http://127.0.0.1:" +
            server.address().port + "/node-v" + nodejsVersion + "-headers.tar.gz";
        mockData["https://nodejs.org/dist/v" + nodejsVersion + "/node-v" + nodejsVersion + "-headers.tar.gz"] = "http://127.0.0.1:" +
            server.address().port + "/node-v" + nodejsVersion + "-headers.tar.gz";

        mockData["https://nodejs.org/download/release/v" + nodejsVersion + "/SHASUMS256.txt"] = "http://127.0.0.1:" +
            server.address().port + "/SHASUMS256.txt";
        mockData["https://nodejs.org/dist/v" + nodejsVersion + "/SHASUMS256.txt"] = "http://127.0.0.1:" +
            server.address().port + "/SHASUMS256.txt";

        process.env.NODEJSRE_MOCK_SERVER_MAP = JSON.stringify(mockData);

        const dirPath = filesystem.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() + "/downloads";
        filesystem.Write(cwd + "/include/OIDIS_PATCH.h", "// this is patched package");
        filesystem.Copy(cwd + "/include", dirPath + "/node-v" + nodejsVersion + "/include/node", ($status) => {
            if ($status) {
                filesystem.Pack(dirPath + "/node-v" + nodejsVersion, {type: "tar.gz"}, ($outputPath) => {
                    filesystem.Delete(dirPath, () => {
                        tarPath = $outputPath;
                        shasumList = StringUtils.Format("" +
                            nodejsx86Sha + " win-x86/node.lib\n" +  // official distr.
                            "{0} win-x64/node.lib\n" +
                            "{1} node-v" + nodejsVersion + "-headers.tar.gz\n", getSha(cwd + "/node.lib"), getSha(tarPath));
                        shasumPath = require("path").dirname(tarPath) + "/shasum.txt";
                        filesystem.Write(shasumPath, shasumList);

                        const mockData2 = {};
                        mockData2["nodelib"] = cwd + "/node.lib";
                        mockData2["headers"] = tarPath;
                        mockData2["shasum"] = shasumPath;
                        process.env.NODEJSRE_MOCK_NPM_FILES = JSON.stringify(mockData2);
                        $callback();
                    });
                });
            } else {
                LogIt.Error("Can not prepare data for tarball");
            }
        });
    });
}

function stopMockServer($callback) {
    LogIt.Info("Stop nodejs-install mock server");
    if (!legacyInstall) {
        $callback();
        return;
    }
    server.close($callback);
}

Process = function ($cwd, $args, $done) {
    cwd = $cwd;
    installPath = $cwd + "/build";
    args = [];
    configPaths = ["bin/project/configs/nodejs.config.json"];
    const envBackup = JSON.parse(JSON.stringify(process.env));
    legacyInstall = filesystem.Exists($cwd + "/libnode.dll") || filesystem.Exists($cwd + "/node.dll") ||
        filesystem.Exists($cwd + "/libnode.so") || filesystem.Exists($cwd + "/libnode.dylib");

    if (process.env.hasOwnProperty("OIDIS_LEGACY_INSTALL") && process.env.OIDIS_LEGACY_INSTALL === "1") {
        legacyInstall = true;
    }
    process.env.PATH = terminal.NormalizeEnvironmentPath(cwd);
    process.env.NODE_PATH = cwd + "/build/node_modules";
    if (legacyInstall) {
        LogIt.Info(">> using legacy installation procedure");
        process.env.URSA_VERBOSE = true;
        process.env.SKIP_SASS_BINARY_DOWNLOAD_FOR_CI = true;
        process.env.BUILD_ONLY = true;
    }
    if (Loader.getInstance().getAppConfiguration().disableStrictSSL) {
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    }
    const patchedHome = filesystem.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getTargetConfig().name +
        "/npm-patched-home";
    if (EnvironmentHelper.IsWindows()) {
        process.env["APPDATA"] = cwd + "/build/patched-appdata";
        process.env["LOCALAPPDATA"] = cwd + "/build/patched-appdata";
        process.env["OIDIS_CMAKE_GENERATOR"] = "Visual Studio 15 2017 Win64";

        // use temp folder for patched npm userprofile because npm/win/antivir/whatever creates INet* folders inside this patched folder
        //   which are locked or something and thus could not be deleted from nodejs process (or even from IDEA), thus needs manual
        //   delete operation at this time. So replaced by temp folder with weak delete (could delete data but ignore failures)
        process.env["USERPROFILE"] = patchedHome
    } else {
        process.env["HOME"] = patchedHome
    }

    $args.forEach(($item) => {
        if (ObjectValidator.IsString($item)) {
            if (!configPaths.Contains($item)) {
                configPaths.push($item);
            }
        } else {
            if (args.findIndex(($value) => {
                return JSON.stringify($value) === JSON.stringify($item);
            }) === -1) {
                args.push($item);
            }
        }
    });

    const postInstall = function ($callback) {
        if (EnvironmentHelper.IsLinux() || EnvironmentHelper.IsMac()) {
            terminal.Spawn("find", [".", "-name", "\".git\"", "|", "xargs", "rm", "-rf"], cwd, () => {
                filesystem.Delete(projectBase + "/node_modules");
                terminal.Spawn("ln", ["-sf", cwd + "/build/node_modules", projectBase + "/node_modules"], projectBase, () => {
                    terminal.Spawn("ln", ["-sf", cwd + "/build/package.json", projectBase + "/package.json"], projectBase, () => {
                        $callback();
                    });
                });
            });
        } else if (EnvironmentHelper.IsWindows()) {
            filesystem.Write(projectBase + "/package.json", JSON.stringify({
                dependencies: {
                    "@types/node": "latest"
                }
            }, null, 2));
            filesystem.Copy(cwd + "/build/node_modules/@types", projectBase + "/node_modules/@types", () => {
                $callback();
            });
        } else {
            $callback();
        }
    };

    const finalize = function () {
        postInstall(() => {
            stopMockServer(() => {
                process.env = envBackup;
                $done();
            });
        });
    };

    const prepare = function ($callback) {
        if (!filesystem.Exists(installPath)) {
            filesystem.CreateDirectory(installPath);
        }

        if (EnvironmentHelper.IsWindows()) {
            filesystem.Delete(process.env.USERPROFILE, ($status) => {
                if (!$status) {
                    LogIt.Warning("Patched USERPROFILE folder for npm install could not be removed. This could happen and should not " +
                        "break anything. But it could be deleted manually. [" + process.env.USERPROFILE + "]");
                }
                $callback();
            });
        } else {
            // reconstruct nodejs package to Win form
            // include/node/* -> nodejs/include/*
            // bin/node -> nodejs/node
            // lib/node_modules -> nodejs/node_modules
            // symlink relative from nodejs/.: node_modules/npm/bin/npm-cli.js -> nodejs/npm
            // LICENSE.txt -> nodejs/LICENSE.txt
            const clean = ($cleanCb) => {
                filesystem.Delete($cwd + "/bin", ($status) => {
                    if (!$status) {
                        LogIt.Error("Unable to clean /bin");
                    }
                    filesystem.Delete($cwd + "/lib", ($status) => {
                        if (!$status) {
                            LogIt.Error("Unable to clean /lib");
                        }
                        filesystem.Delete($cwd + "/share", ($status) => {
                            if (!$status) {
                                LogIt.Error("Unable to clean /share");
                            }
                            filesystem.Delete($cwd + "/CHANGELOG.md", ($status) => {
                                if (!$status) {
                                    LogIt.Error("Unable to clean /CHANGELOG.md");
                                }
                                filesystem.Delete($cwd + "/README.md", ($status) => {
                                    if (!$status) {
                                        LogIt.Error("Unable to clean /README.md");
                                    }
                                    $cleanCb();
                                })
                            })
                        })
                    })
                })
            }

            if (filesystem.Exists($cwd + "/node_modules")) {
                $callback();
                return;
            }
            filesystem.Copy($cwd + "/bin/node", $cwd + "/node", ($status) => {
                if (!$status) {
                    LogIt.Error("Unable to copy /bin/node");
                }
                filesystem.Copy($cwd + "/lib/node_modules", $cwd + "/node_modules", ($status) => {
                    if (!$status) {
                        LogIt.Error("Unable to copy npm sources");
                    }
                    terminal.Spawn("ln", ["-s", "node_modules/npm/bin/npm-cli.js", "npm"], cwd, ($exitCode) => {
                        if ($exitCode !== 0) {
                            LogIt.Error("Unable to create npm symlink");
                        }
                        clean($callback);
                    });
                });
            });
        }
    };

    prepare(() => {
        configureNpm(() => {
            startMockServer(() => {
                if (!filesystem.Exists(installPath)) {
                    prepare(() => {
                        install(finalize);
                    });
                } else {
                    install(finalize);
                }
            });
        }, false);
    });
};

#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2014-2016 Freescale Semiconductor, Inc.
# * Copyright 2017-2018 NXP
# * Copyright 2019-<? @var build.year ?> Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

sourceFile="${BASH_SOURCE[0]}"
while [ -h "$sourceFile" ]; do
  sourceLink="$(readlink "$sourceFile")"
  if [[ $sourceLink == /* ]]; then
    sourceFile="$sourceLink"
  else
    sourceDir="$(dirname "$sourceFile")"
    sourceFile="$sourceDir/$sourceLink"
  fi
done
binBase="$(cd -P "$(dirname "$sourceFile")" && pwd)"

oidisExecutablePath=$binBase/../"<? @var project.target.name ?>"
oidisSharedPath=$binBase/../resource/nodejs

nodeBaseBinaryPath=$binBase/../../../dependencies/nodejs/node
loaderJsFilePath=""
nodeInspectMode=""

if [[ $1 == '--inspect-brk='* ]]; then
  nodeInspectMode="$1"
  shift
fi

if [ -x "$oidisExecutablePath" ]; then
  # embedded application here
  nodeBaseBinaryPath=$oidisExecutablePath
else
  if [ -x "$oidisSharedPath" ]; then
    # shared build
    export NODE_PATH="${NODEJSRE_NODE_PATH:-${binBase}/../resource/nodejs/node_modules}"
    nodeBaseBinaryPath=$binBase/../node
  else
    # static build
    export NODE_PATH="${NODEJSRE_NODE_PATH:-${binBase}/../../../dependencies/nodejs/build/node_modules}"
  fi

  if [[ -z $NODEJSRE_SKIP_LOADER ]]; then
    loaderJsFilePath=$binBase/../resource/javascript/loader.min.js
  fi
fi

# shellcheck disable=SC2068
"${nodeBaseBinaryPath}"${nodeInspectMode:+ $nodeInspectMode} --experimental-vm-modules${loaderJsFilePath:+ $loaderJsFilePath} ${@}
#echo "${nodeBaseBinaryPath}${nodeInspectMode:+ $nodeInspectMode}${loaderJsFilePath:+ $loaderJsFilePath} ${@}"

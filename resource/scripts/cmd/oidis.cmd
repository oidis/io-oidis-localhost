@setlocal enableextensions enabledelayedexpansion
@echo off
REM * ********************************************************************************************************* *
REM *
REM * Copyright 2014-2016 Freescale Semiconductor, Inc.
REM * Copyright 2017-2018 NXP
REM * Copyright 2019-<? @var build.year ?> Oidis
REM *
REM * SPDX-License-Identifier: BSD-3-Clause
REM * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
REM * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
REM *
REM * ********************************************************************************************************* *

set "oidisExecutablePath=%~dp0..\<? @var project.target.name ?>.exe"
set "oidisSharedPath=%~dp0..\resource\nodejs"
set "nodeBaseBinaryPath=%~dp0..\..\..\dependencies\nodejs\node.exe"
set "loaderJsFilePath="
set "nodeInspectMode="

set "args=%*"
IF "%1" == "--inspect-brk" (
    set "nodeInspectMode=%1=%2 "
    set "args=%3 %4 %5 %6 %7 %8 %9"
)

IF EXIST !oidisExecutablePath! (
    set "nodeBaseBinaryPath=%oidisExecutablePath%"
) ELSE (
    IF EXIST !oidisSharedPath! (
        set "NODE_PATH=%~dp0..\resource\nodejs\node_modules"
        set "nodeBaseBinaryPath=%~dp0..\node.exe"
    ) ELSE (
        set "NODE_PATH=%~dp0..\..\..\dependencies\nodejs\build\node_modules"
    )
    IF DEFINED NODEJSRE_NODE_PATH (
        set "NODE_PATH=%NODEJSRE_NODE_PATH%"
    )
    IF NOT DEFINED NODEJSRE_SKIP_LOADER (
        set "loaderJsFilePath=%~dp0\..\resource\javascript\loader.min.js "
    )
)

!nodeBaseBinaryPath! !nodeInspectMode!--experimental-vm-modules !loaderJsFilePath!!args!

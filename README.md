# io-oidis-localhost

> Oidis Framework code base for localhost based on Node.js

## Requirements

This library does not have any special requirements but it depends on the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder). See the Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`oidis docs` command from the {projectRoot} folder.

## History

### v2022.2.0
Fixed extern method invocation context (token delegation), minor cleanup.
### v2022.1.1
Enabled override of page loader definition.
### v2022.1.0
Added support for integrity checks. Several minor bugfixes.
### v2022.0.0
Integrated self-check feature. Enhanced configuration of nodejs config and optional NPM packages. Various minor fixes.
### v2021.2.0
Integrated novel logger. Updated nodejs dependency. Several minor bugfixes and enhancements.
### v2021.0.0
NPM's modules updated to the newest compatible version with Node 14.15 together with corresponding changes in install script. Added support
for builtin async support (async/await) in @Extern methods. Reduced memory leak in response management.
### v2020.3.0
Various bugfixes and enhancements. Added getters for project data paths into filesystem handler. Added Elastic search connector. Http server 
is now able to start on the local machine from symbolic localhost domain name instead local IP address.
### v2020.1.0
Added ability to suppress strictSSL option for HTTP clients. General bugfixes. Added proper handling for GET, DELETE and CONTINUE HTTP 
methods.
### v2020.0.1
Identity update. Change of configuration files format. Added full support for configs in jsonp format. 
### v2020.0.0
Use relative path for ARM compiler in build scripts for NPM native modules.
### v2019.3.0
Refactoring of namespaces. Migration to gitlab. Initial update of identity for project fork. 
Added ability to override DOM resource loader. Bug fixes for node install script. File system handler enhancements and bug fixes.
Migration to solutions instead of releases. Added full support for OSX and Linux. Fixed store of log messages into file.
Added support for encrypted resources and hash protection. Bug fixes for memory leaks. Unification of VM and Script handlers. 
### v2019.1.0
Updates for node install script mainly focused on OSX. Fixed issues with elastic agent and greenlock initiation. 
Added support for external typedefs. Usage of decorators for security mechanisms and simplified external API declaration. 
Enable to use alternative origins. Fixed resolve of schema and origins over proxy.
### v2019.0.2
Bug fixes for singleton of elastic search agent. Enable to use WUI Localhost in role of runtime environment. 
Added full support for OSX. Usage of new app loader.
### v2019.0.1
Added robust support for cross compile of native libraries. Enable to filter file system logs. Added docker image configuration. 
Fixed creation of cerst under reverse proxy. Usage of hub location property. Enable to extend default.config. 
Enable to extend localhost to different namespace then Com. Added support for communication with native websocket chunks. 
Enable to send Errors with additional arguments.
### v2019.0.0
Fixed unpack in embedded mode on all platforms. Usage of websocket client from node module. Added CLI args normalization for easy parsing. 
Added support for letsencrypt certificates. Added ability to run localhost in role of reverse proxy. 
Added ability to send monitoring data to elastic APM. Optimization of RAM usage and stronger restrictions for request acceptance.
Fixed slash sensitive anonymization.  
### v2018.3.0
Added implementation for code base of WUI Connector, WUI Hub and WUI Builder.
### v2018.1.0
Refactoring to general code base focused on localhost functionality.
### v2.1.0
Conversion into the TypeScript and Node.js service.
### v2.0.2
Added fixed version for dependencies.
### v2.0.1
Added support for stand-alone toolchain. Bug fixes for user agent validation, reflection and cookies listing. Clean up of unit tests.
### v2.0.0
Namespaces refactoring.
### v1.0.1
Added PHP 7 syntax update.
### v1.0.0
Initial release.

## License

This software is owned or controlled by Oidis.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
 
See the `LICENSE.txt` file for more details.

---

Copyright 2010-2013 Jakub Cieslar,
Copyright 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright 2017-2019 [NXP](http://nxp.com/),
Copyright 2019-2025 [Oidis](https://www.oidis.io/)

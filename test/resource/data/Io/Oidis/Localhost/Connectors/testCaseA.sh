#!/bin/sh
# * ********************************************************************************************************* *
# *
# * Copyright 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

for i in {1..5}
do
  echo "\"testCaseA $i\""
  ping -c 1 127.0.0.1 >> /dev/null
done
echo "\"testCaseA exit\""
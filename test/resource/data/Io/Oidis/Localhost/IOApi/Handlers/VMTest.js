/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const LogIt = Io.Oidis.Commons.Utils.LogIt;

Process = function ($cwd, $args, $done) {
    LogIt.Info(">" + $cwd + ": " + $args.join(","));
    $done(true);
};

/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { VMHandler } from "../../../../../../../../source/typescript/Io/Oidis/Localhost/IOApi/Handlers/VMHandler.js";

export class VMHandlerTest extends UnitTestRunner {

    public testLoad() : void {
        const vm : VMHandler = new VMHandler();
        vm.Path("test/resource/data/Io/Oidis/Localhost/IOApi/Handlers/VMTest.js");
        vm.Sandbox({
            Process($cwd : string, $args : string[], $done : any) {
                assert.ok(false, "Default Process should be overridden by external script.");
            }
        });
        vm.SuccessHandler(() : void => {
            vm.Sandbox().Process("test cwd", ["arg1", "arg2"], ($status : boolean) : void => {
                assert.ok($status);
            });
        });
        vm.ErrorHandler(($error : Error) : void => {
            Echo.Printf($error.stack);
            assert.ok(false, "VM Handler should not reach this point.");
        });
        vm.Load();
    }
}

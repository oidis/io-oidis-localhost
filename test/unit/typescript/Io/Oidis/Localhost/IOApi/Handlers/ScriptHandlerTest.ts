/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ProtectionType } from "../../../../../../../../source/typescript/Io/Oidis/Localhost/Enums/ProtectionType.js";
import { ScriptHandler } from "../../../../../../../../source/typescript/Io/Oidis/Localhost/IOApi/Handlers/ScriptHandler.js";

export class ScriptHandlerTest extends UnitTestRunner {

    public testDecrypt() : void {
        assert.doesNotThrow(() => {
            const sh : ScriptHandler = new ScriptHandler();
            sh.Path("test/resource/data/Io/Oidis/Localhost/Utils/encryptedResource.enc");
            sh.setMap(
                ["test/resource/data/Io/Oidis/Localhost/Utils/encryptedResource.enc"],
                ProtectionType.ENCRYPTION);
            sh.Load();
        });
        assert.doesNotThrow(() => {
            const sh : ScriptHandler = new ScriptHandler();
            sh.Path("test/resource/data/Io/Oidis/Localhost/Utils/encryptedResource.enc");
            sh.setMap(
                ["test/resource/data/Io/Oidis/Localhost/Utils/encryptedResourceX.enc"],
                ProtectionType.ENCRYPTION);
            sh.Load();
        });
    }

    public testDataSet() : void {
        assert.doesNotThrow(() => {
            const sh : ScriptHandler = new ScriptHandler();
            sh.Data("foobar");
            assert.equal(sh.Data(), "foobar");
        });
    }

    public testSignatureVerification() : void {
        assert.doesNotThrow(() => {
            const sh : ScriptHandler = new ScriptHandler();
            sh.Path("test/resource/data/Io/Oidis/Localhost/Utils/SelfExtractor.jsonp");
            sh.setMap({
                "test/resource/data/Io/Oidis/Localhost/Utils/SelfExtractor.jsonp":
                    "3040021e0275913fd1a45104dc10175de28ec7080d839cf2f5a4c54a020dd31485" +
                    "92021e020c6fd11e195512e06d417499822a3f12628b3cc4eee3ddc8c7b23f88d4"
            }, ProtectionType.SIGNATURE);
            sh.setPublicKey("-----BEGIN PUBLIC KEY-----\n" +
                "MFIwEAYHKoZIzj0CAQYFK4EEAAMDPgAECAWc30DP/wsROheNjXke3zWgpr9BVO5f\n" +
                "ZN5ibHwZFRLnJ72wMrEZre7s+d3WLU0+v6sWc/7GPcrSEc8Q\n" +
                "-----END PUBLIC KEY-----\n");
            sh.Load();
        });
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { HttpResolver } from "../../../../../../../source/typescript/Io/Oidis/Localhost/HttpProcessor/HttpResolver.js";

class MockHttpResolver extends HttpResolver {
    public FetchOptionsResolver($options : any) : any {
        return super.fetchOptionsResolver($options);
    }

    public DetectProxy($url : string, $config? : any) : any {
        return super.detectProxy($url, $config);
    }
}

export class HttpResolverTest extends UnitTestRunner {

    constructor() {
        super();
        this.setMethodFilter(
            // "testProxyIdentification_Default",
            // "testFetchOptionsResolver_HTTP_HTTPS",
            // "testFetchOptionsResolver_HTTP_HTTPS_Proxy",
            // "testFetchOptionsResolver_WS_WSS",
            // "testFetchOptionsResolver_WS_WSS_Proxy"
        );
    }

    public testDetectProxy_Default() : void {
        const config : any = {
            httpProxy : "",
            httpsProxy: "",
            noProxy   : ""
        };
        assert.deepEqual(this.getHttpResolver().DetectProxy("http://hub.dev.oidis.io:80/some#hox", config), {
            excluded: false,
            agent   : "",
            client  : "http",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("ws://hub.dev.oidis.io", config), {
            excluded: false,
            agent   : "",
            client  : "ws",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("https://hub.dev.oidis.io:80/some#hox", config), {
            excluded: false,
            agent   : "",
            client  : "https",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("wss://hub.dev.oidis.io", config), {
            excluded: false,
            agent   : "",
            client  : "wss",
            noProxy : true,
            proxyUrl: ""
        });
    }

    public testDetectProxy_HTTP_WS() : void {
        const config : any = {
            httpProxy : "http://proxy.com:8080",
            httpsProxy: "",
            noProxy   : ""
        };
        assert.deepEqual(this.getHttpResolver().DetectProxy("http://hub.dev.oidis.io:80/some#hox", config), {
            excluded: false,
            agent   : "",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("ws://hub.dev.oidis.io", config), {
            excluded: false,
            agent   : "ws",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        config.noProxy = "*";
        assert.deepEqual(this.getHttpResolver().DetectProxy("http://hub.dev.oidis.io:80/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "http",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("ws://hub.dev.oidis.io", config), {
            excluded: true,
            agent   : "",
            client  : "ws",
            noProxy : true,
            proxyUrl: ""
        });
    }

    public testDetectProxy_HTTP_WS_filter() : void {
        const config : any = {
            httpProxy : "http://proxy.com:8080",
            httpsProxy: "",
            noProxy   : "*dev.oidis.io,*.dev.nxp.com,.sw.some.org,.dome.io,less.io"
        };
        assert.deepEqual(this.getHttpResolver().DetectProxy("http://hub.dev.oidis.io:80/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "http",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("ws://hub.dev.oidis.io", config), {
            excluded: true,
            agent   : "",
            client  : "ws",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("http://vit.dev.nxp.com/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "http",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("ws://vit.dev.nxp.com", config), {
            excluded: true,
            agent   : "",
            client  : "ws",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("http://hub.eap.oidis.io:80/some#hox", config), {
            excluded: false,
            agent   : "",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("ws://hub.eap.oidis.io", config), {
            excluded: false,
            agent   : "ws",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("http://vit.eap.nxp.com/some#hox", config), {
            excluded: false,
            agent   : "",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("ws://vit.eap.nxp.com", config), {
            excluded: false,
            agent   : "ws",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("http://vit.eap.sw.some.org/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "http",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("ws://vit.eap.sw.some.org", config), {
            excluded: true,
            agent   : "",
            client  : "ws",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("http://vit.eap.sw.dome.io/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "http",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("ws://vit.eap.sw.dome.io", config), {
            excluded: true,
            agent   : "",
            client  : "ws",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("http://dome.io/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "http",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("ws://dome.io", config), {
            excluded: true,
            agent   : "",
            client  : "ws",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("http://less.io/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "http",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("ws://less.io", config), {
            excluded: true,
            agent   : "",
            client  : "ws",
            noProxy : true,
            proxyUrl: ""
        });
    }

    public testDetectProxy_HTTPS_WSS() : void {
        const config : any = {
            httpProxy : "",
            httpsProxy: "http://proxy.com:8080",
            noProxy   : ""
        };
        assert.deepEqual(this.getHttpResolver().DetectProxy("https://hub.dev.oidis.io:80/some#hox", config), {
            excluded: false,
            agent   : "https",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("wss://hub.dev.oidis.io", config), {
            excluded: false,
            agent   : "wss",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        config.noProxy = "*";
        assert.deepEqual(this.getHttpResolver().DetectProxy("https://hub.dev.oidis.io:80/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "https",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("wss://hub.dev.oidis.io", config), {
            excluded: true,
            agent   : "",
            client  : "wss",
            noProxy : true,
            proxyUrl: ""
        });
    }

    public testDetectProxy_HTTPS_WSS_filter() : void {
        const config : any = {
            httpProxy : "",
            httpsProxy: "http://proxy.com:8080",
            noProxy   : "*dev.oidis.io,*.dev.nxp.com,.sw.some.org,.dome.io,less.io"
        };
        assert.deepEqual(this.getHttpResolver().DetectProxy("https://hub.dev.oidis.io:80/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "https",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("wss://hub.dev.oidis.io", config), {
            excluded: true,
            agent   : "",
            client  : "wss",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("https://vit.dev.nxp.com/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "https",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("wss://vit.dev.nxp.com", config), {
            excluded: true,
            agent   : "",
            client  : "wss",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("https://hub.eap.oidis.io:80/some#hox", config), {
            excluded: false,
            agent   : "https",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("wss://hub.eap.oidis.io", config), {
            excluded: false,
            agent   : "wss",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("https://vit.eap.nxp.com/some#hox", config), {
            excluded: false,
            agent   : "https",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("wss://vit.eap.nxp.com", config), {
            excluded: false,
            agent   : "wss",
            client  : "http",
            noProxy : false,
            proxyUrl: "http://proxy.com:8080"
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("https://vit.eap.sw.some.org/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "https",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("wss://vit.eap.sw.some.org", config), {
            excluded: true,
            agent   : "",
            client  : "wss",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("https://vit.eap.sw.dome.io/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "https",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("wss://vit.eap.sw.dome.io", config), {
            excluded: true,
            agent   : "",
            client  : "wss",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("https://dome.io/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "https",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("wss://dome.io", config), {
            excluded: true,
            agent   : "",
            client  : "wss",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("https://less.io/some#hox", config), {
            excluded: true,
            agent   : "",
            client  : "https",
            noProxy : true,
            proxyUrl: ""
        });
        assert.deepEqual(this.getHttpResolver().DetectProxy("wss://less.io", config), {
            excluded: true,
            agent   : "",
            client  : "wss",
            noProxy : true,
            proxyUrl: ""
        });
    }

    public testFetchOptionsResolver_HTTP_HTTPS() : void {
        let config : any = {
            url        : "http://hub.dev.oidis.io:80/some#hox",
            proxyConfig: {
                httpProxy : "",
                httpsProxy: "",
                noProxy   : ""
            }
        };
        let request : any = this.getHttpResolver().FetchOptionsResolver(config);
        assert.equal(request.client, require("http"));
        assert.deepEqual(request.options, JsonUtils.Extend({method: HttpMethodType.GET, headers: {}, body: ""}, config));
        assert.ok(ObjectValidator.IsEmptyOrNull(request.proxy));
        assert.ok(ObjectValidator.IsEmptyOrNull(request.agent));

        config = {
            url        : "https://hub.dev.oidis.io:80/some#hox",
            proxyConfig: {
                httpProxy : "",
                httpsProxy: "",
                noProxy   : ""
            }
        };
        request = this.getHttpResolver().FetchOptionsResolver(config);
        assert.equal(request.client, require("https"));
        assert.deepEqual(request.options, JsonUtils.Extend({method: HttpMethodType.GET, headers: {}, body: ""}, config));
        assert.ok(ObjectValidator.IsEmptyOrNull(request.proxy));
        assert.ok(ObjectValidator.IsEmptyOrNull(request.agent));
    }

    public testFetchOptionsResolver_HTTP_HTTPS_Proxy() : void {
        let config : any = {
            url        : "http://hub.dev.oidis.io:80/some#hox",
            proxyConfig: {
                httpProxy : "http://proxy.com:8080",
                httpsProxy: "",
                noProxy   : ""
            }
        };
        let request : any = this.getHttpResolver().FetchOptionsResolver(config);
        assert.equal(request.client, require("http"));
        assert.equal(JSON.stringify(request.options), JSON.stringify({
            body       : "",
            headers    : {Host: "hub.dev.oidis.io"},
            method     : "GET",
            url        : {
                protocol: "http:",
                slashes : true,
                auth    : null,
                host    : "proxy.com:8080",
                port    : "8080",
                hostname: "proxy.com",
                hash    : null,
                search  : null,
                query   : null,
                pathname: "/",
                path    : "http://hub.dev.oidis.io:80/some#hox",
                href    : "http://proxy.com:8080/",
                method  : "GET",
                headers : {Host: "hub.dev.oidis.io"}
            },
            proxyConfig: {httpProxy: "http://proxy.com:8080", httpsProxy: "", noProxy: ""}
        }));
        assert.equal(JSON.stringify(request.proxy), JSON.stringify({
            protocol: "http:",
            slashes : true,
            auth    : null,
            host    : "proxy.com:8080",
            port    : "8080",
            hostname: "proxy.com",
            hash    : null,
            search  : null,
            query   : null,
            pathname: "/",
            path    : "http://hub.dev.oidis.io:80/some#hox",
            href    : "http://proxy.com:8080/",
            method  : "GET",
            headers : {Host: "hub.dev.oidis.io"}
        }));
        assert.ok(ObjectValidator.IsEmptyOrNull(request.agent));

        config = {
            url        : "https://hub.dev.oidis.io:80/some#hox",
            proxyConfig: {
                httpProxy : "",
                httpsProxy: "http://proxy.com:8080",
                noProxy   : ""
            }
        };
        request = this.getHttpResolver().FetchOptionsResolver(config);
        assert.equal(request.client, require("https"));
        assert.deepEqual(request.options, JsonUtils.Extend({method: HttpMethodType.GET, headers: {}, body: ""}, config));
        assert.equal(JSON.stringify(request.proxy), JSON.stringify({
            protocol: "http:",
            slashes : true,
            auth    : null,
            host    : "proxy.com:8080",
            port    : "8080",
            hostname: "proxy.com",
            hash    : null,
            search  : null,
            query   : null,
            pathname: "/",
            path    : "hub.dev.oidis.io:80",
            href    : "http://proxy.com:8080/",
            method  : "CONNECT"
        }));
        assert.equal(request.agent, require("https").Agent);
    }

    public testFetchOptionsResolver_WS_WSS() : void {
        let config : any = {
            url        : "ws://hub.dev.oidis.io",
            proxyConfig: {
                httpProxy : "",
                httpsProxy: "",
                noProxy   : ""
            }
        };
        let request : any = this.getHttpResolver().FetchOptionsResolver(config);
        assert.equal(request.client, require("websocket").w3cwebsocket);
        assert.deepEqual(request.options, JsonUtils.Extend({method: HttpMethodType.GET, headers: {}, body: ""}, config));
        assert.ok(ObjectValidator.IsEmptyOrNull(request.proxy));
        assert.ok(ObjectValidator.IsEmptyOrNull(request.agent));

        config = {
            url        : "wss://hub.dev.oidis.io",
            proxyConfig: {
                httpProxy : "",
                httpsProxy: "",
                noProxy   : ""
            }
        };
        request = this.getHttpResolver().FetchOptionsResolver(config);
        assert.equal(request.client, require("websocket").w3cwebsocket);
        assert.deepEqual(request.options, JsonUtils.Extend({method: HttpMethodType.GET, headers: {}, body: ""}, config));
        assert.ok(ObjectValidator.IsEmptyOrNull(request.proxy));
        assert.ok(ObjectValidator.IsEmptyOrNull(request.agent));
    }

    public testFetchOptionsResolver_WS_WSS_Proxy() : void {
        let config : any = {
            url        : "ws://hub.dev.oidis.io",
            proxyConfig: {
                httpProxy : "http://proxy.com:8080",
                httpsProxy: "",
                noProxy   : ""
            }
        };
        let request : any = this.getHttpResolver().FetchOptionsResolver(config);
        assert.equal(request.client, require("websocket").w3cwebsocket);
        assert.deepEqual(request.options, JsonUtils.Extend({method: HttpMethodType.GET, headers: {}, body: ""}, config));
        assert.deepEqual(JSON.stringify(request.proxy), JSON.stringify({
            protocol: "http:",
            slashes : true,
            auth    : null,
            host    : "proxy.com:8080",
            port    : "8080",
            hostname: "proxy.com",
            hash    : null,
            search  : null,
            query   : null,
            pathname: "/",
            path    : "hub.dev.oidis.io:80",
            href    : "http://proxy.com:8080/",
            method  : "CONNECT"
        }));
        assert.equal(request.agent, require("http").Agent);

        config = {
            url        : "wss://hub.dev.oidis.io",
            proxyConfig: {
                httpProxy : "",
                httpsProxy: "http://proxy.com:8080",
                noProxy   : ""
            }
        };
        request = this.getHttpResolver().FetchOptionsResolver(config);
        assert.equal(request.client, require("websocket").w3cwebsocket);
        assert.deepEqual(request.options, JsonUtils.Extend({method: HttpMethodType.GET, headers: {}, body: ""}, config));
        assert.equal(JSON.stringify(request.proxy), JSON.stringify({
            protocol: "http:",
            slashes : true,
            auth    : null,
            host    : "proxy.com:8080",
            port    : "8080",
            hostname: "proxy.com",
            hash    : null,
            search  : null,
            query   : null,
            pathname: "/",
            path    : "hub.dev.oidis.io:443",
            href    : "http://proxy.com:8080/",
            method  : "CONNECT"
        }));
        assert.equal(request.agent, require("https").Agent);
    }

    protected getHttpResolver() : MockHttpResolver {
        return new MockHttpResolver();
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { ILiveContentFormatterPromise } from "@io-oidis-services/Io/Oidis/Services/Interfaces/ILiveContentFormatterPromise.js";
import { BaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { Loader } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Loader.js";

class MockConnector extends BaseConnector {
    public UndefinedMethod() : ILiveContentFormatterPromise {
        return this.invoke("UNDEFINED_METHOD");
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.WEB_SOCKETS;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.WEB_SOCKETS] = "Io.Oidis.Hub.UNKNOWN_CLASS";
        return namespaces;
    }
}

export class WSProxyTest extends UnitTestRunner {

    constructor() {
        super();
        this.setMethodFilter(
            // "testWsExtern",
            // "testWssExtern",
            // "testProxyWsExtern",
            // "testProxyWssExtern",
            // "testWsIntern",
            // "testWssIntern",
            // "testProxyWsIntern",
            // "testProxyWssIntern"
        );
    }

    public testWsExtern() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.assertCommunication("http://hub.dev.oidis.io", $done);
        };
    }

    public testWssExtern() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.assertCommunication("https://hub.dev.oidis.io", $done);
        };
    }

    public testProxyWsExtern() : IUnitTestRunnerPromise {
        // WS over proxy from SwIS to external server could is failing because on most proxies it is simply blocked
        // on the opposite WSS is encrypted and proxy know nothing about data and thus WSS is "more stable" in such communication
        return ($done : () => void) : void => {
            Loader.getInstance().getAppConfiguration().httpProxy = "http://emea.nics.nxp.com:8080";
            this.assertCommunication("http://hub.dev.oidis.io", $done);
        };
    }

    public testProxyWssExtern() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            Loader.getInstance().getAppConfiguration().httpsProxy = "http://emea.nics.nxp.com:8080";
            this.assertCommunication("https://hub.dev.oidis.io", $done);
        };
    }

    public testWsIntern() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.assertCommunication("http://vit.dev.nxp.com", $done);
        };
    }

    public testWssIntern() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            Loader.getInstance().getAppConfiguration().disableStrictSSL = true;
            this.assertCommunication("https://vit.dev.nxp.com", $done);
        };
    }

    public testProxyWsIntern() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            Loader.getInstance().getAppConfiguration().httpProxy = "http://emea.nics.nxp.com:8080";
            this.assertCommunication("http://vit.dev.nxp.com", $done);
        };
    }

    public testProxyWssIntern() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            Loader.getInstance().getAppConfiguration().httpsProxy = "http://emea.nics.nxp.com:8080";
            Loader.getInstance().getAppConfiguration().disableStrictSSL = true;
            this.assertCommunication("https://vit.dev.nxp.com", $done);
        };
    }

    protected setUp() : void {
        delete process.env.http_proxy;
        delete process.env.HTTP_PROXY;
        delete process.env.https_proxy;
        delete process.env.HTTPS_PROXY;
        Loader.getInstance().getAppConfiguration().httpProxy = "";
        Loader.getInstance().getAppConfiguration().httpsProxy = "";
        Loader.getInstance().getAppConfiguration().noProxy = "";
        Loader.getInstance().getAppConfiguration().disableStrictSSL = false;
    }

    private assertCommunication($url : string, $done : any) : void {
        const connector : MockConnector = new MockConnector(1, $url + "/connector.config.jsonp");
        connector.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            assert.equal($eventArgs.Message(), "Required class \"Io.Oidis.Hub.UNKNOWN_CLASS\" does not exist.");
            this.initSendBox();
            $done();
        });
        connector.UndefinedMethod();
    }
}

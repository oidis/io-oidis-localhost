/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { FileSystemHandler } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { Loader } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Loader.js";
import { EnvironmentHelper } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";

export class HttpProxyTest extends UnitTestRunner {
    private filesystem : FileSystemHandler;

    constructor() {
        super();
        this.setMethodFilter(
            // "testExternHttps",
            // "testExternHttp",
            // "testProxyExternHttps",
            // "testProxyExternHttp",
            // "testInternHttps",
            // "testInternHttp"
        );
    }

    public testExternHttps() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.filesystem.Download({
                method      : "GET",
                url         : "https://hub.dev.oidis.io",
                streamOutput: true
            }, ($headers : any, $body : string) : void => {
                assert.ok(StringUtils.ContainsIgnoreCase($body, "<title>io-oidis-hub</title>"));
                $done();
            }, () : boolean => {
                assert.ok(false, "Failed to make extern https");
                $done();
                return true;
            });
        };
    }

    public testExternHttp() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.filesystem.Download({
                method      : "GET",
                url         : "http://hub.dev.oidis.io",
                streamOutput: true
            }, ($headers : any, $body : string) : void => {
                assert.ok(StringUtils.ContainsIgnoreCase($body, "<title>io-oidis-hub</title>"));
                $done();
            }, () : boolean => {
                assert.ok(false, "Failed to make extern http");
                $done();
                return true;
            });
        };
    }

    public testProxyExternHttps() : IUnitTestRunnerPromise {
        // if proxy is in form http://<user>:<pass>@<proxy-url><port>, then <user>:<pass> needs to be encoded like
        // "Basic BASE64(<user>:<pass>)" and passed to proxy connection header Proxy-Authorization
        // there is no other standardized way to pass creds to proxy, so support only this basic authorization
        // (just as preparation for future) ... same behaviour as curl
        return ($done : () => void) : void => {
            // this client type http/https depends on proxy url protocol which is commonly HTTP so HTTPS protocol in proxy url should
            // be notified as error (https proxy doesn't make sense in most cases because it is normal that proxy is INSIDE internal
            // system and nobody takes care about self-signed certs then because of strictSSL CA check disablement is not easy
            // in some clients)
            EnvironmentHelper.NormalizeProxySetting({
                httpsProxy: "http://emea.nics.nxp.com:8080"
            });
            this.filesystem.Download({
                method      : "GET",
                url         : "https://hub.oidis.io",
                streamOutput: true,
                strictSSL   : false
            }, ($headers : any, $body : string) : void => {
                assert.ok(StringUtils.ContainsIgnoreCase($body, "<title>io-oidis-hub</title>"));
                $done();
            }, () : boolean => {
                assert.ok(false, "Failed to make intern https");
                $done();
                return true;
            });
        };
    }

    public testProxyExternHttp() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            EnvironmentHelper.NormalizeProxySetting({
                httpProxy: "http://emea.nics.nxp.com:8080"
            });
            this.filesystem.Download({
                method      : "GET",
                url         : "http://hub.oidis.io",
                streamOutput: true,
                strictSSL   : false
            }, ($headers : any, $body : string) : void => {
                assert.ok(StringUtils.ContainsIgnoreCase($body, "<title>io-oidis-hub</title>"));
                $done();
            }, () : boolean => {
                assert.ok(false, "Failed to make intern http");
                $done();
                return true;
            });
        };
    }

    public testInternHttps() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.filesystem.Download({
                method      : "GET",
                url         : "https://vit.dev.nxp.com",
                streamOutput: true,
                strictSSL   : false
            }, ($headers : any, $body : string) : void => {
                assert.ok(StringUtils.ContainsIgnoreCase($body, "<title>io-vit-application</title>"));
                $done();
            }, () : boolean => {
                assert.ok(false, "Failed to make intern https");
                $done();
                return true;
            });
        };
    }

    public testInternHttp() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.filesystem.Download({
                method      : "GET",
                url         : "http://vit.dev.nxp.com",
                streamOutput: true,
                strictSSL   : false
            }, ($headers : any, $body : string) : void => {
                assert.ok(StringUtils.ContainsIgnoreCase($body, "<title>io-vit-application</title>"));
                $done();
            }, () : boolean => {
                assert.ok(false, "Failed to make intern http");
                $done();
                return true;
            });
        };
    }

    public testProxyInternHttps() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            // this client type http/https depends on proxy url protocol which is commonly HTTP so HTTPS protocol in proxy url should
            // be notified as error (https proxy doesn't make sense in most cases because it is normal that proxy is INSIDE internal
            // system and nobody takes care about self-signed certs then because of strictSSL CA check disablement is not easy
            // in some clients)
            EnvironmentHelper.NormalizeProxySetting({
                httpsProxy: "http://emea.nics.nxp.com:8080"
            });
            this.filesystem.Download({
                method      : "GET",
                url         : "https://vit.dev.nxp.com",
                streamOutput: true,
                strictSSL   : false
            }, ($headers : any, $body : string) : void => {
                assert.ok(StringUtils.ContainsIgnoreCase($body, "<title>io-vit-application</title>"));
                $done();
            }, () : boolean => {
                assert.ok(false, "Failed to make intern https");
                $done();
                return true;
            });
        };
    }

    public testProxyInternHttp() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            EnvironmentHelper.NormalizeProxySetting({
                httpProxy: "http://emea.nics.nxp.com:8080"
            });
            this.filesystem.Download({
                method      : "GET",
                url         : "http://vit.dev.nxp.com",
                streamOutput: true,
                strictSSL   : false
            }, ($headers : any, $body : string) : void => {
                assert.ok(StringUtils.ContainsIgnoreCase($body, "<title>io-vit-application</title>"));
                $done();
            }, () : boolean => {
                assert.ok(false, "Failed to make intern http");
                $done();
                return true;
            });
        };
    }

    protected setUp() : void {
        delete process.env.http_proxy;
        delete process.env.HTTP_PROXY;
        delete process.env.https_proxy;
        delete process.env.HTTPS_PROXY;
        delete Loader.getInstance().getAppConfiguration().httpProxy;
        delete Loader.getInstance().getAppConfiguration().httpsProxy;
        delete Loader.getInstance().getAppConfiguration().noProxy;
    }

    protected before() : void {
        this.filesystem = Loader.getInstance().getFileSystemHandler();
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Loader } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Loader.js";
import { ResourceVerifier } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Utils/ResourceVerifier.js";

export class ResourceVerifierTest extends UnitTestRunner {
    private publicKeyData : string = "";
    private resourceSignature : string = "";

    constructor() {
        super();
        // this.setMethodFilter("testVerifyMap");
    }

    public __IgnoretestDecryptData() : void {
        assert.doesNotThrow(() => {
            const encResourceData : string = Loader.getInstance().getFileSystemHandler().Read(
                "test/resource/data/Io/Oidis/Localhost/Utils/encryptedResource.enc").toString("utf8");
            const res : Buffer = ResourceVerifier.DecryptResource(Buffer.from(encResourceData, "hex"),
                Buffer.alloc(32));
            assert.notEqual(res, null);
            assert.equal(StringUtils.Contains(res.toString(), "Copyright (c) 2019 Oidis"), true);
        });
    }

    public testVerifyData() : void {
        let res : boolean = ResourceVerifier.VerifySignature(Buffer.from("foobar\n"),
            this.publicKeyData,
            this.resourceSignature);
        assert.equal(res, true);

        res = ResourceVerifier.VerifySignature(Buffer.from("foobar"),
            this.publicKeyData,
            this.resourceSignature);
        assert.equal(res, false);
    }

    public testVerifyByName() : void {
        const map : any = {
            fooResource: this.resourceSignature
        };

        let res : boolean = ResourceVerifier.VerifyByName(Buffer.from("foobar\n"),
            this.publicKeyData,
            map,
            "fooResource");
        assert.equal(res, true);

        res = ResourceVerifier.VerifyByName(Buffer.from("foobar"),
            this.publicKeyData,
            map,
            "fooResource");
        assert.equal(res, false);

        res = ResourceVerifier.VerifyByName(Buffer.from("foobar\n"),
            this.publicKeyData,
            null,
            "fooResource");
        assert.equal(res, false);

        res = ResourceVerifier.VerifyByName(Buffer.from("foobar\n"),
            this.publicKeyData,
            {},
            "fooResource");
        assert.equal(res, false);

        res = ResourceVerifier.VerifyByName(Buffer.from("foobar\n"),
            this.publicKeyData,
            map,
            "_fooResource");
        assert.equal(res, false);
    }

    public __IgnoretestVerifyMap() : void {
        const testResourcesPath : string = this.getAbsoluteRoot() + "/../../test/resource/data/Io/Oidis/Localhost/Utils";
        const map : any = {};
        map[testResourcesPath + "/SelfExtractor.jsonp"] =
            "3040021e0275913fd1a45104dc10175de28ec7080d839cf2f5a4c54a020dd3148592021e020c6fd11" +
            "e195512e06d417499822a3f12628b3cc4eee3ddc8c7b23f88d4";
        map[testResourcesPath + "/wuirunner.config.jsonp"] =
            "3040021e0088b6e90d975880399e2e15aefb0096352f7f65df5866c4c1c6cd1d18f0021e1355e43d8" +
            "73965440ff66c3b8d0e87a1ab3c88c58a7022a98b1f222e4368";

        let res : boolean = ResourceVerifier.VerifyMap(this.publicKeyData, map);
        assert.equal(res, true);

        res = ResourceVerifier.VerifyMap(this.publicKeyData, {});
        assert.equal(res, true);

        res = ResourceVerifier.VerifyMap(this.publicKeyData, null);
        assert.equal(res, true);

        const mapFail : any = {};
        mapFail[testResourcesPath + "/SelfExtractor.jsonp"] =
            "3040021e0088b6e90d975880399e2e15aefb0096352f7f65df5866c4c1c6cd1d18f0021e1355e" +
            "43d873965440ff66c3b8d0e87a1ab3c88c58a7022a98b1f222e4368";
        mapFail[testResourcesPath + "/wuirunner.config.jsonp"] =
            "3040021e0275913fd1a45104dc10175de28ec7080d839cf2f5a4c54a020dd3148592021e020c6" +
            "fd11e195512e06d417499822a3f12628b3cc4eee3ddc8c7b23f88d4";
        res = ResourceVerifier.VerifyMap(this.publicKeyData, mapFail);
        assert.equal(res, false);

        res = ResourceVerifier.VerifyMap(this.publicKeyData, {foo: "bar"});
        assert.equal(res, false);

        res = ResourceVerifier.VerifyMap(this.publicKeyData, {
            "test/resource/data/Io/Oidis/Localhost/Utils/SelfExtractor.jsonp": "bar"
        });
        assert.equal(res, false);
    }

    protected setUp() : void {
        this.publicKeyData = "-----BEGIN PUBLIC KEY-----\n" +
            "MFIwEAYHKoZIzj0CAQYFK4EEAAMDPgAECAWc30DP/wsROheNjXke3zWgpr9BVO5f\n" +
            "ZN5ibHwZFRLnJ72wMrEZre7s+d3WLU0+v6sWc/7GPcrSEc8Q\n" +
            "-----END PUBLIC KEY-----\n";
        this.resourceSignature = "3040021e1e3581c82eaf4d3840d4df146719d12b720b6bdf95793762" +
            "15e8fa703dec021e159bb35d163df1a4a7bc8b3ec9b57ffaa82617fb1d6d2acf99a8ff1888f4";
    }
}

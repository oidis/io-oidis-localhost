/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Loader } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Loader.js";
import { EnvironmentHelper } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";

export class EnvironmentHelperTest extends UnitTestRunner {
    protected envBackup : any;

    constructor() {
        super();
        // this.setMethodFilter(
        //     "testProxyCfgToEnv",
        //     "testProxyEnvToCfg",
        //     "testProxyHttpEnvCorrupted",
        //     "testProxyHttpsEnvCorrupted",
        //     "testProxyNoEnvCorrupted",
        //     "testProxyWrongSetup",
        // );
    }

    public testProxyCfgToEnv() : void {
        // config to env
        Loader.getInstance().getAppConfiguration().httpProxy = "http://proxy.io:8080";
        Loader.getInstance().getAppConfiguration().httpsProxy = "http://Sproxy.io:8080";
        Loader.getInstance().getAppConfiguration().noProxy = "*.anything";  // noProxy=*, disables proxy completely

        this.proxySetupSync();

        assert.equal(process.env.http_proxy, "http://proxy.io:8080");
        assert.equal(process.env.HTTP_PROXY, "http://proxy.io:8080");
        assert.equal(process.env.https_proxy, "http://Sproxy.io:8080");
        assert.equal(process.env.HTTPS_PROXY, "http://Sproxy.io:8080");
        assert.equal(process.env.no_proxy, "*.anything");
        assert.equal(process.env.NO_PROXY, "*.anything");
        assert.equal(Loader.getInstance().getAppConfiguration().httpProxy, "http://proxy.io:8080");
        assert.equal(Loader.getInstance().getAppConfiguration().httpsProxy, "http://Sproxy.io:8080");
        assert.equal(Loader.getInstance().getAppConfiguration().noProxy, "*.anything");

        // rewrite
        Loader.getInstance().getAppConfiguration().httpProxy = "http://proxy.io:9999";
        Loader.getInstance().getAppConfiguration().httpsProxy = "http://Sproxy.io:9999";
        Loader.getInstance().getAppConfiguration().noProxy = "*.everything";

        this.proxySetupSync();

        assert.equal(process.env.http_proxy, "http://proxy.io:9999");
        assert.equal(process.env.HTTP_PROXY, "http://proxy.io:9999");
        assert.equal(process.env.https_proxy, "http://Sproxy.io:9999");
        assert.equal(process.env.HTTPS_PROXY, "http://Sproxy.io:9999");
        assert.equal(process.env.no_proxy, "*.everything");
        assert.equal(process.env.NO_PROXY, "*.everything");
        assert.equal(Loader.getInstance().getAppConfiguration().httpProxy, "http://proxy.io:9999");
        assert.equal(Loader.getInstance().getAppConfiguration().httpsProxy, "http://Sproxy.io:9999");
        assert.equal(Loader.getInstance().getAppConfiguration().noProxy, "*.everything");
    }

    public testProxyEnvToCfg() : void {
        process.env.http_proxy = "http://proxy.io:8080";
        process.env.HTTPS_PROXY = "http://Sproxy.io:8080";
        process.env.no_proxy = "*.anything";

        this.proxySetupSync();

        assert.equal(process.env.http_proxy, "http://proxy.io:8080");
        assert.equal(process.env.HTTP_PROXY, "http://proxy.io:8080");
        assert.equal(Loader.getInstance().getAppConfiguration().httpProxy, "http://proxy.io:8080");
        assert.equal(process.env.https_proxy, "http://Sproxy.io:8080");
        assert.equal(process.env.HTTPS_PROXY, "http://Sproxy.io:8080");
        assert.equal(Loader.getInstance().getAppConfiguration().httpsProxy, "http://Sproxy.io:8080");
        assert.equal(process.env.no_proxy, "*.anything");
        assert.equal(process.env.NO_PROXY, "*.anything");
        assert.equal(Loader.getInstance().getAppConfiguration().noProxy, "*.anything");

        // rewrite
        process.env.http_proxy = "";  // empty should be handled as undefined
        process.env.HTTPS_PROXY = "";
        process.env.no_proxy = "";
        Loader.getInstance().getAppConfiguration().httpProxy = "";
        Loader.getInstance().getAppConfiguration().httpsProxy = "";

        process.env.HTTP_PROXY = "http://proxy.io:9999";
        process.env.https_proxy = "http://Sproxy.io:9999";
        process.env.NO_PROXY = "*.everything";

        this.proxySetupSync();

        assert.equal(process.env.http_proxy, "http://proxy.io:9999");
        assert.equal(process.env.HTTP_PROXY, "http://proxy.io:9999");
        assert.equal(Loader.getInstance().getAppConfiguration().httpProxy, "http://proxy.io:9999");
        assert.equal(process.env.https_proxy, "http://Sproxy.io:9999");
        assert.equal(process.env.HTTPS_PROXY, "http://Sproxy.io:9999");
        assert.equal(Loader.getInstance().getAppConfiguration().httpsProxy, "http://Sproxy.io:9999");

        assert.equal(process.env.no_proxy, "*.anything");   // no override because it is in config now
        assert.equal(process.env.NO_PROXY, "*.anything");
        assert.equal(Loader.getInstance().getAppConfiguration().noProxy, "*.anything");
    }

    public testProxyHttpEnvCorrupted() : void {
        process.env.http_proxy = "http://proxy.io:8080";
        process.env.HTTP_PROXY = "http://proxy.io:9090";

        // assert.throws in UnitTestRunner not working properly with native errors properly
        assert.throws = ($block : any, $msg : string) : void => {
            try {
                $block();
            } catch ($e) {
                assert.equal($e.message, $msg);
            }
        };

        assert.throws(() => {
            this.proxySetupSync();
        }, "process.env contains both of http_proxy and HTTP_PROXY with different values");

    }

    public testProxyHttpsEnvCorrupted() : void {
        process.env.HTTPS_PROXY = "http://proxy.io:8080";
        process.env.https_proxy = "http://another.io:8080";

        // assert.throws in UnitTestRunner not working properly with native errors properly
        assert.throws = ($block : any, $msg : string) : void => {
            try {
                $block();
            } catch ($e) {
                assert.equal($e.message, $msg);
            }
        };

        assert.throws(() => {
            this.proxySetupSync();
        }, "process.env contains both of https_proxy and HTTPS_PROXY with different values");
    }

    public testProxyNoEnvCorrupted() : void {
        process.env.no_proxy = "*.any";
        process.env.NO_PROXY = "*.something";

        // assert.throws in UnitTestRunner not working properly with native errors properly
        assert.throws = ($block : any, $msg : string) : void => {
            try {
                $block();
            } catch ($e) {
                assert.equal($e.message, $msg);
            }
        };

        assert.throws(() => {
            this.proxySetupSync();
        }, "process.env contains both of no_proxy and NO_PROXY with different values");
    }

    public testProxyWrongSetup() : void {
        // assert.throws in UnitTestRunner not working properly with native errors properly
        assert.throws = ($block : any, $msg : string) : void => {
            try {
                $block();
            } catch ($e) {
                assert.equal($e.message, $msg);
            }
        };

        process.env.http_proxy = "proto://some:port";
        assert.throws(() => {
            this.proxySetupSync();
        }, "Invalid URL");

        process.env.http_proxy = "https://some:8080";
        assert.throws(() => {
            this.proxySetupSync();
        }, "Secured proxy is not supported, please use http://<proxy>:<port> for http_proxy instead.");

        process.env.http_proxy = "";
        process.env.https_proxy = "https://some:8080";
        assert.throws(() => {
            this.proxySetupSync();
        }, "Secured proxy is not supported, please use http://<proxy>:<port> for https_proxy instead.");

        process.env.https_proxy = "";
        process.env.http_proxy = "http://some:8080";
        assert.doesNotThrow(() => {
            this.proxySetupSync();
        });

        process.env.no_proxy = "some:.*dome,dome some,rome.come";  // space unsupported
        assert.throws(() => {
            this.proxySetupSync();
        }, "no_proxy option contains unsupported symbols: some:.*dome,dome some,rome.come");

        process.env.no_proxy = "some:UNKN,.*dome,rome.come";  // ! unsupported
        assert.throws(() => {
            this.proxySetupSync();
        }, "no_proxy option contains host with wrong port definition: some:UNKN");

        process.env.no_proxy = "some:189,.*dome,rome.come";  // ! unsupported
        assert.doesNotThrow(() => {
            this.proxySetupSync();
        });
    }

    protected setUp() : void {
        /// TODO: remove dependency on AppConfiguration -> this should be applied directly in Loader only
        Loader.getInstance().getAppConfiguration().httpProxy = "";
        Loader.getInstance().getAppConfiguration().httpsProxy = "";
        Loader.getInstance().getAppConfiguration().noProxy = "";
        this.envBackup = JSON.stringify(process.env);
        delete process.env.http_proxy;
        delete process.env.https_proxy;
        delete process.env.HTTP_PROXY;
        delete process.env.HTTPS_PROXY;
        delete process.env.no_proxy;
        delete process.env.NO_PROXY;
    }

    protected tearDown() : void {
        process.env = JSON.parse(this.envBackup);
    }

    private proxySetupSync() : void {
        EnvironmentHelper.NormalizeProxySetting(Loader.getInstance().getAppConfiguration());
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Elastic } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Connectors/Elastic.js";
import { IElasticConfiguration } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Interfaces/IProject.js";
import { Loader } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Loader.js";

export class MockElastic extends Elastic {

    public async init() : Promise<void> {
        return super.init();
    }

    public async createMapping() : Promise<void> {
        return super.createMapping();
    }

    public async deleteMapping() : Promise<void> {
        return super.deleteMapping();
    }

    public async mappingExists() : Promise<boolean> {
        return super.mappingExists();
    }

    public async process($data : any[]) : Promise<string[]> {
        return super.process($data);
    }

    public async pushData($data : any) : Promise<string> {
        return super.pushData($data);
    }

    public async deleteItem($id : string) : Promise<void> {
        return super.deleteItem($id);
    }

    public async findById($id : string) : Promise<any> {
        return super.findById($id);
    }

    public async find($dsl : any) : Promise<any[]> {
        return super.find($dsl);
    }

    public async deleteByQuery($query : any) : Promise<void> {
        return super.deleteByQuery($query);
    }

    public async getAll() : Promise<any[]> {
        return this.find({query: {match_all: {}}});
    }

    public async delay($ms? : number) : Promise<void> {
        return super.delay($ms);
    }
}

export class ElasticTest extends UnitTestRunner {
    private elastic : MockElastic;

    constructor() {
        super();

        /// TODO: requires automation for back-end instance at http://127.0.0.1:8085 or mock-server configuration
        this.setMethodFilter("ignoreAll");
    }

    public testCreateMapping() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.elastic.init()
                .then(() : Promise<void> => {
                    return this.elastic.deleteMapping();
                })
                .then(() : Promise<boolean> => {
                    return this.elastic.mappingExists();
                })
                .then(($exists : boolean) : Promise<void> => {
                    assert.equal($exists, false);
                    return this.elastic.createMapping();
                })
                .then(() : Promise<boolean> => {
                    return this.elastic.mappingExists();
                })
                .then(($exists : boolean) : Promise<void> => {
                    assert.equal($exists, true);
                    return this.elastic.deleteMapping();
                })
                .then(() : Promise<boolean> => {
                    return this.elastic.mappingExists();
                })
                .then(($exists : boolean) : void => {
                    assert.equal($exists, false);
                    $done();
                })
                .catch(($error : Error) : void => {
                    assert.equal($error.message, "");
                });
        };
    }

    public testOperations() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.elastic.process(
                [
                    {name: "test1", id: "1", timestamp: "2020-09-03T10:45:15.909Z"},
                    {name: "test2", id: "2", timestamp: "2020-09-03T10:45:16.909Z"},
                    {name: "test3", id: "3", timestamp: "2020-09-03T10:45:17.909Z"},
                    {name: "test4", id: "4", timestamp: "2020-09-03T10:45:18.909Z"},
                    {name: "test5", id: "5", timestamp: "2020-09-03T10:45:19.909Z"}
                ])
                .then(($ids : string[]) : Promise<any> => {
                    assert.deepEqual($ids, ["1", "2", "3", "4", "5"]);
                    return this.elastic.delay();  // elastic updates indexes once per second, so delay is needed after create
                })
                .then(() : Promise<any[]> => {
                    return this.elastic.getAll();
                })
                .then(($items : any[]) : Promise<void> => {
                    assert.deepEqual($items.length, 5);
                    return this.elastic.findById("2");
                })
                .then(($item : any) : Promise<any> => {
                    assert.deepEqual($item, {name: "test2", timestamp: "2020-09-03T10:45:16.909Z"});
                    return this.elastic.find({query: {match: {name: "test3"}}});
                })
                .then(($item : any) : void => {
                    assert.deepEqual($item, [{name: "test3", timestamp: "2020-09-03T10:45:17.909Z"}]);
                    $done();
                })
                .catch(($error : Error) : void => {
                    assert.deepEqual($error.message, "");
                });
        };
    }

    public testProcess() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.elastic.process([{name: "testA"}, {name: "testB"}])
                .then(() : void => {
                    $done();
                })
                .catch(($error : Error) : void => {
                    assert.equal($error, null);
                });
        };
    }

    protected before() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.elastic = new MockElastic();
            Loader.getInstance().getAppConfiguration().elastic = <IElasticConfiguration>{
                index   : "data",
                location: "http://127.0.0.1:8085",
                mapping : "test/resource/data/Io/Oidis/Localhost/Connectors/elastic.mapping.json",
                pass    : "",
                user    : ""
            };
            this.elastic.init()
                .then(() : void => {
                    $done();
                })
                .catch(($error : Error) : void => {
                    LogIt.Debug("CleanUp error: {0}", $error);
                    $done();
                });
        };
    }

    protected after() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.elastic.deleteMapping()
                .then(() : Promise<void> => {
                    return this.elastic.deleteByQuery({query: {match_all: {}}});
                })
                .then(() : void => {
                    $done();
                })
                .catch(($error : Error) : void => {
                    LogIt.Debug("CleanUp error: {0}", $error);
                    $done();
                });
        };
    }
}

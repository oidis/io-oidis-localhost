/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ITerminalOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/TerminalConnector.js";
import { IExecuteResult, Terminal } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Connectors/Terminal.js";
import { ResponseFactory } from "../../../../../../../source/typescript/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { Loader } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Loader.js";
import { EnvironmentHelper } from "../../../../../../../source/typescript/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";

export class TerminalTest extends UnitTestRunner {
    private terminal : Terminal;

    constructor() {
        super();
        // this.setMethodFilter("testExecuteEcho");
    }

    public testExecute() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.Execute("ping", [!EnvironmentHelper.IsWindows() ? "-c 2" : "", "127.0.0.1"], null,
                ($exitCode : number, $std : string[]) : void => {
                    assert.equal($exitCode, 0);
                    if (EnvironmentHelper.IsWindows()) {
                        assert.patternEqual($std.toString(),
                            "\r\n" +
                            "Pinging 127.0.0.1 with 32 bytes of data:\r\n" +
                            "Reply from 127.0.0.1: bytes=32 time* TTL=128\r\n*");
                    } else if (EnvironmentHelper.IsMac()) {
                        assert.patternEqual($std.toString(),
                            "PING 127.0.0.1 (127.0.0.1): 56 data bytes\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=* ms\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=* ms\n*,");
                    } else {
                        assert.patternEqual($std.toString(),
                            "PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=* ms\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=* ms\n*");
                    }
                    $done();
                });
        };
    }

    public testExecuteAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.ExecuteAsync("ping", [!EnvironmentHelper.IsWindows() ? "-c 2" : "", "127.0.0.1"], null)
                .then(($result : IExecuteResult) : void => {
                    assert.equal($result.exitCode, 0);
                    if (EnvironmentHelper.IsWindows()) {
                        assert.patternEqual($result.std.toString(),
                            "\r\n" +
                            "Pinging 127.0.0.1 with 32 bytes of data:\r\n" +
                            "Reply from 127.0.0.1: bytes=32 time* TTL=128\r\n*");
                    } else if (EnvironmentHelper.IsMac()) {
                        assert.patternEqual($result.std.toString(),
                            "PING 127.0.0.1 (127.0.0.1): 56 data bytes\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=* ms\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=* ms\n*,");
                    } else {
                        assert.patternEqual($result.std.toString(),
                            "PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=* ms\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=* ms\n*");
                    }
                    $done();
                })
                .catch(($error : Error) : void => {
                    assert.equal($error, null);
                });
        };
    }

    public testExecuteWithEnv() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const options : ITerminalOptions = {
                env: {TEST_VAR_PATH: "my path"}
            };

            this.terminal.Execute("echo", [EnvironmentHelper.IsWindows() ? "%TEST_VAR_PATH%" : "$TEST_VAR_PATH"], options,
                ($exitCode : number, $std : string[]) : void => {
                    assert.equal($exitCode, 0);
                    assert.deepEqual($std, ["my path" + require("os").EOL, ""]);
                    $done();
                });
        };
    }

    public testExecuteNonExistingCommand() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.Execute("bla", [], null,
                ($exitCode : number, $std : string[]) : void => {
                    if (EnvironmentHelper.IsWindows()) {
                        assert.equal($exitCode, 1);
                        assert.deepEqual($std, [
                            "",
                            "\'bla\' is not recognized as an internal or external command,\r\n" +
                            "operable program or batch file.\r\n"
                        ]);
                    } else {
                        assert.equal($exitCode, 127);
                        assert.deepEqual($std, [
                            "",
                            EnvironmentHelper.IsMac() ?
                                "/bin/sh: bla: command not found\n" :
                                "/bin/sh: 1: bla: not found\n"
                        ]);
                    }
                    $done();
                });
        };
    }

    public testExecuteTestCaseA() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.Execute(EnvironmentHelper.IsWindows() ? "testCaseA.cmd" : "./testCaseA.sh", [""],
                this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Localhost/Connectors/",
                ($exitCode : number, $std : string[]) : void => {
                    assert.equal($exitCode, 0);
                    if (EnvironmentHelper.IsWindows() || EnvironmentHelper.IsMac()) {
                        const EOL : string = require("os").EOL;
                        assert.deepEqual($std, [
                            "\"testCaseA 1\"" + EOL +
                            "\"testCaseA 2\"" + EOL +
                            "\"testCaseA 3\"" + EOL +
                            "\"testCaseA 4\"" + EOL +
                            "\"testCaseA 5\"" + EOL +
                            "\"testCaseA exit\"" + EOL,
                            ""
                        ]);
                    } else {
                        assert.deepEqual($std, [
                            "\"testCaseA {1..5}\"\n" +
                            "\"testCaseA exit\"\n",
                            ""
                        ]);
                    }
                    $done();
                });
        };
    }

    public testExecuteErrorCwd() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.Execute(EnvironmentHelper.IsWindows() ? "testCaseA.cmd" : "./testCaseA.sh", [""], null,
                ($exitCode : number, $std : string[]) : void => {
                    if (EnvironmentHelper.IsWindows()) {
                        assert.equal($exitCode, 1);
                        assert.deepEqual($std, [
                            "",
                            "\'testCaseA.cmd\' is not recognized as an internal or external command,\r\n" +
                            "operable program or batch file.\r\n"
                        ]);
                    } else {
                        assert.equal($exitCode, 127);
                        if (EnvironmentHelper.IsMac()) {
                            assert.deepEqual($std, [
                                "",
                                "/bin/sh: ./testCaseA.sh: No such file or directory\n"
                            ]);
                        } else {
                            assert.deepEqual($std, [
                                "",
                                "/bin/sh: 1: ./testCaseA.sh: not found\n"
                            ]);
                        }
                    }

                    $done();
                });
        };
    }

    public testExecuteEcho() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            if (EnvironmentHelper.IsWindows()) {
                this.terminal.Execute("echo", ["%PROCESSOR_ARCHITECTURE%"], null,
                    ($exitCode : number, $std : string[]) : void => {
                        assert.equal($exitCode, 0);
                        assert.deepEqual($std, ["AMD64\r\n", ""]);
                        $done();
                    });
            } else if (EnvironmentHelper.IsMac()) {
                this.terminal.Execute("echo", ["$BASH"], null,
                    ($exitCode : number, $std : string[]) : void => {
                        assert.equal($exitCode, 0);
                        assert.deepEqual($std, ["/bin/sh\n", ""]);
                        $done();
                    });
            } else {
                this.terminal.Execute("echo", ["$HOME"], null,
                    ($exitCode : number, $std : string[]) : void => {
                        assert.equal($exitCode, 0);
                        assert.deepEqual($std, [process.env.HOME + "\n", ""]);
                        $done();
                    });
            }
        };
    }

    public testLongStdData() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            if (EnvironmentHelper.IsWindows()) {
                const cmd : string = "\"" + this.getAbsoluteRoot() +
                    "/test/resource/data/Io/Oidis/Localhost/Connectors/stdio test.exe\"";
                const args : string[] = ["--stdout=9000", "--stdout-prefix=stdout_prefixed_", "--exit=111"];

                this.terminal.Execute(cmd, args, null, ($exitCode : number, $std : string[]) : void => {
                    assert.equal($exitCode, 3221225595);
                    assert.deepEqual($std, ["", ""]);
                    $done();
                });
            } else {
                // TODO(mkelnar) add stdio test resources and build them before, stdio test.exe is currently only for windows
                $done();
            }
        };
    }

    public testExecuteVerbose() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.Execute("echo", ["hello"], {
                verbose: true
            }, ($exitCode : number, $std : string[]) : void => {
                assert.equal($exitCode, 0);
                assert.deepEqual($std, ["hello" + require("os").EOL, ""]);
                $done();
            });
        };
    }

    public testWithoutShell() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.Execute("echo", ["test"], {
                shell: false
            }, ($exitCode : number, $std : string[]) : void => {
                assert.equal($exitCode, 0);
                assert.deepEqual($std, ["test" + require("os").EOL, ""]);
                $done();
            });
        };
    }

    public testNULL() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.Execute("", [], null, ($exitCode : number, $std : string[]) : void => {
                assert.equal($exitCode, 0);
                assert.deepEqual($std, ["", ""]);
                $done();
            });
        };
    }

    public __IgnoretestExecuteProxy() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.Execute("echo", [EnvironmentHelper.IsWindows() ? "%HTTP_PROXY%" : "$HTTP_PROXY"], {
                    env: {HTTP_PROXY: "http://user:pass@proxy.domain.org:3128/"}
                },
                ($exitCode : number, $std : string[]) : void => {
                    assert.equal($exitCode, 0);
                    assert.deepEqual($std, ["http://user:pass@proxy.domain.org:3128/" + require("os").EOL, ""]);
                    $done();
                });
        };
    }

    public testExecuteResponse() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.Execute("echo", ["test"], null,
                ResponseFactory.getResponse(($exitCode : number, $std : string[]) : void => {
                    assert.equal($exitCode, 0);
                    assert.deepEqual($std, ["test" + require("os").EOL, ""]);
                    $done();
                }));
        };
    }

    public testSpawn() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.Spawn("ping", [!EnvironmentHelper.IsWindows() ? "-c 2" : "", "127.0.0.1"], null,
                ($exitCode : number, $std : string[]) : void => {
                    assert.equal($exitCode, 0);
                    if (EnvironmentHelper.IsWindows()) {
                        assert.patternEqual($std.toString(),
                            "\r\n" +
                            "Pinging 127.0.0.1 with 32 bytes of data:\r\n" +
                            "Reply from 127.0.0.1: bytes=32 time* TTL=128\r\n*");
                    } else if (EnvironmentHelper.IsMac()) {
                        assert.patternEqual($std.toString(),
                            "PING 127.0.0.1 (127.0.0.1): 56 data bytes\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=* ms\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=* ms\n*,");
                    } else {
                        assert.patternEqual($std.toString(),
                            "PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=* ms\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=* ms\n*");
                    }
                    $done();
                });
        };
    }

    public testSpawnAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.SpawnAsync("ping", [!EnvironmentHelper.IsWindows() ? "-c 2" : "", "127.0.0.1"], null)
                .then(($result : IExecuteResult) : void => {
                    assert.equal($result.exitCode, 0);
                    if (EnvironmentHelper.IsWindows()) {
                        assert.patternEqual($result.std.toString(),
                            "\r\n" +
                            "Pinging 127.0.0.1 with 32 bytes of data:\r\n" +
                            "Reply from 127.0.0.1: bytes=32 time* TTL=128\r\n*");
                    } else if (EnvironmentHelper.IsMac()) {
                        assert.patternEqual($result.std.toString(),
                            "PING 127.0.0.1 (127.0.0.1): 56 data bytes\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=* ms\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=* ms\n*,");
                    } else {
                        assert.patternEqual($result.std.toString(),
                            "PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=* ms\n" +
                            "64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=* ms\n*");
                    }
                    $done();
                })
                .catch(($error : Error) : void => {
                    assert.equal($error, null);
                });
        };
    }

    public __IgnoretestElevate() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.terminal.Elevate("ping", ["localhost"], "", ($exitCode : number) : void => {
                assert.equal($exitCode, 0);
                $done();
            });
        };
    }

    // TODO(mkelnar) revalidate change in Terminal.Kill... the "<pipe> MORE +1" produces invalid output
    //  "Too many arguments in command line" so Kill() wasn't work properly
    public testKill() : IUnitTestRunnerPromise {
        (<any>process).traceProcessWarnings = true;
        return ($done : () => void) : void => {
            if (EnvironmentHelper.IsMac()) {
                // TODO(mkelnar) enable kill test after kill will be enabled in terminal
                $done();
                return;
            }
            let cmd : string = "C:/Windows/notepad.exe";
            const args : string[] = [];
            if (!EnvironmentHelper.IsWindows()) {
                cmd = "ping";
                args.push("127.0.0.1");
            }
            setTimeout(() : void => {
                this.terminal.Kill(cmd, ($status : boolean) : void => {
                    assert.ok($status);
                    $done();
                });
            }, 2000);
            // it takes more than 0.5s to start notepad process, still experimental bulgarian constant
            // native implementation uses more efficient way with "scanning" processes to check if it is already opened
            this.terminal.Spawn(cmd, args, {
                detached: true
            }, ($exitCode : number, $std : string[]) : void => {
                assert.equal($exitCode, 0);
            });
        };
    }

    public __IgnoretestOpen() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const cmd = this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Localhost/Connectors/testFile.txt";
            this.terminal.Open(cmd, ($exitCode : number) : void => {
                assert.equal($exitCode, 0);
                $done();
            });
        };
    }

    protected before() : void {
        this.terminal = Loader.getInstance().getTerminal();
    }
}
